﻿using LogManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace LogManagertest
{
    class Program
    {
        static void Main(string[] args)
        {
            Logger logger = new Logger();     

            Task<bool> _task = logger.Log("Test", Logger.LogTypeEnum.INFO, "1444", MethodBase.GetCurrentMethod().Name, "userTest", "Ceci est un test");
            _task.Wait();
            bool res = _task.Result;
        }
    }
}
