﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using log4net;
using System.IO;
using log4net.Repository;
using log4net.Config;
using System.Threading.Tasks;

namespace LogManager
{
    /// <summary>
    /// The logger class is used to log messages.
    /// </summary>
    public class Logger
    {
        #region Private member(s)

#if LOG4NET
       private static ILog logger = null;
       private static string configFile = string.Empty;
       private static bool configFileLoaded = false;
#endif

        #endregion

        #region Enum

        /// <summary>
        /// Log type enum.
        /// </summary>
        public enum LogTypeEnum
        {
            /// <summary>
            /// Information
            /// </summary>
            INFO,

            /// <summary>
            /// Fatal
            /// </summary>
            FATAL,

            /// <summary>
            /// Error
            /// </summary>
            ERROR,

            /// <summary>
            /// Warning
            /// </summary>
            WARN,

            /// <summary>
            /// Debug
            /// </summary>
            DEBUG
        }

        #endregion

        #region Method(s)

        #region InitialzeConfig

        /// <summary>
        /// 
        /// </summary>
        private static void InitializeConfig()
        {
            configFile = @".\config\LogManager.xml";

            if (File.Exists(configFile))
            {
                ILoggerRepository repository = log4net.LogManager.GetRepository();

                if (!repository.Configured)
                    XmlConfigurator.ConfigureAndWatch(repository, new FileInfo(configFile));

                configFileLoaded = true;
            }
        }

        #endregion

        #region Log method

        /// <summary>
        /// Logging function.
        /// </summary>
        /// <param name="loggerName">The name of the logger.</param>
        /// <param name="type">Log type - LogTypeEnum.</param>
        /// <param name="pid">Process id</param>
        /// <param name="methodName"></param>
        ///<param name="userName"></param>
        /// <param name="logMessage">Log message.</param>
        /// <returns></returns>
        public async Task<bool> Log(string loggerName, LogTypeEnum type, string pid, string methodName, string userName, string logMessage)
        {
            try
            {
                if (!configFileLoaded)
                    InitializeConfig();

                logger = log4net.LogManager.GetLogger(loggerName);

                log4net.GlobalContext.Properties["pid"] = pid;
                log4net.GlobalContext.Properties["methodName"] = methodName;
                log4net.GlobalContext.Properties["userName"] = userName;

                switch (type)
                {
                    case LogTypeEnum.INFO:
                        if (logger.IsInfoEnabled)
                            await Task.Run(() => logger.Info(logMessage));
                        break;
                    case LogTypeEnum.FATAL:
                        if (logger.IsFatalEnabled)
                            await Task.Run(() => logger.Fatal(logMessage));
                        break;
                    case LogTypeEnum.ERROR:
                        if (logger.IsErrorEnabled)
                            await Task.Run(() => logger.Error(logMessage));
                        break;
                    case LogTypeEnum.WARN:
                        if (logger.IsWarnEnabled)
                            await Task.Run(() => logger.Warn(logMessage));
                        break;
                    case LogTypeEnum.DEBUG:
                        if (logger.IsDebugEnabled)
                            await Task.Run(() => logger.Debug(logMessage));
                        break;
                }

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        #endregion
        
        #region InvokeLog method

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<object> InvokeLog(dynamic input)
        {
            return await Log((string)input.loggerName,
                            (LogTypeEnum)Enum.Parse(typeof(LogTypeEnum), (string)input.loggerType, true),
                            (string)input.pid,
                            (string)input.methodName,
                            (string)input.userName,
                            (string)input.message);
        }

        #endregion

        #endregion
    }

}
