﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMTP.Properties;
using System.Reflection;
using System.IO;
using System.Threading.Tasks;
using System.Net.Mail;
using System.Net;
using MimeKit;
using MailKit.Net.Smtp;
using MailKit.Net.Imap;

namespace SMTP
{
    /// <summary>
    /// 
    /// </summary>
    public class SMTPMail
    {
        #region Propertie(s)

        public string SMTPHost { get; set; }
        public int SMTPPort { get; set; }
        public string IMAPHost { get; set; }
        public int IMAPPort { get; set; }
        public string Address { get; set; }
        public string DisplayName { get; set; }
        public string Password { get; set; }
        public string Recipients { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public string SentFolder { get; set; }
        public bool HasAttachmentFiles { get; set; }
        
        #endregion

        #region Method(s)

        #region InvokeSendMail method

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public async Task<object> InvokeSendMail(dynamic input)
        {
            SMTPHost = (string)input.smtpHost;
            SMTPPort = (int)input.smtpPort;
            IMAPHost = (string)input.imapHost;
            IMAPPort = (int)input.imapPort;
            Address = (string)input.address;
            DisplayName = (string)input.displayName;
            Password = (string)input.password;
            Recipients = (string)input.recipients;
            Subject = (string)input.subject;
            Body = (string)input.body;
            SentFolder = (string)input.sentFolder;
            HasAttachmentFiles = (bool)input.hasAttachmentFiles;

            return await SendMailWithMailKit();
        }

        #endregion

        #region SendMail method

        /// <summary>
        /// Sends emails.
        /// </summary>
        public async Task<bool> SendMail()
        {
            try
            {
                using (MailMessage mail = new MailMessage())
                {
                    mail.From = new MailAddress(Address, DisplayName);
                    mail.To.Add(Recipients);
                    mail.Subject = Subject;
                    mail.Body = Body;
                    mail.IsBodyHtml = true;
                    mail.BodyEncoding = System.Text.Encoding.UTF8;
                    mail.SubjectEncoding = System.Text.Encoding.UTF8;

                    if (HasAttachmentFiles)
                        mail.Attachments.Add(new Attachment("C:\\SomeFile.txt"));

                    System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient(SMTPHost, SMTPPort);
                    smtp.Credentials = new NetworkCredential(Address, Password);
                    smtp.EnableSsl = true;
                    smtp.DeliveryMethod = SmtpDeliveryMethod.Network;

                    // Send email.
                    await Task.Run(() => smtp.Send(mail));
                    
                    return true;
                    
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        #endregion SendMail method

        #region SendMailWithMailKit method

        /// <summary>
        /// Sends emails with MailKit cross-platform.
        /// </summary>
        public async Task<bool> SendMailWithMailKit()
        {
            try
            {
                var message = new MimeMessage();
                message.From.Add(new MailboxAddress(DisplayName, Address));
                message.To.Add(new MailboxAddress(Recipients));
                message.Subject = Subject;

                message.Body = new TextPart("html")
                {
                    Text = Body
                };

                using (var client = new MailKit.Net.Smtp.SmtpClient())
                {
                    // For demo-purposes, accept all SSL certificates (in case the server supports STARTTLS)
                    client.ServerCertificateValidationCallback = (s, c, h, e) => true;

                    client.Connect(SMTPHost, SMTPPort);

                    // Note: since we don't have an OAuth2 token, disable
                    // the XOAUTH2 authentication mechanism.
                    client.AuthenticationMechanisms.Remove("XOAUTH2");

                    // Note: only needed if the SMTP server requires authentication
                    client.Authenticate(Address, Password);

                    await client.SendAsync(message);

                    client.Disconnect(true);
                }

                //
                // Save copy of message in "Sent" folder
                using (var client = new ImapClient())
                {
                    // For demo-purposes, accept all SSL certificates
                    client.ServerCertificateValidationCallback = (s, c, h, e) => true;

                    client.Connect(IMAPHost, IMAPPort, true);

                    // Note: since we don't have an OAuth2 token, disable
                    // the XOAUTH2 authentication mechanism.
                    client.AuthenticationMechanisms.Remove("XOAUTH2");

                    // Note: only needed if the SMTP server requires authentication
                    client.Authenticate(Address, Password);

                    // Save copy of message in Sent folder
                    var sent = client.GetFolder(SentFolder);
                    await sent.AppendAsync(message);

                    client.Disconnect(true);
                }

                return true;

            }
            catch (Exception)
            {
                return false;
            }
        }

        #endregion SendMailWithMailKit method

        #endregion
    }
}
