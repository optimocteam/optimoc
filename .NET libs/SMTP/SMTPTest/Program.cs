﻿using SMTP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMTPTest
{
    class Program
    {
        static void Main(string[] args)
        {
            //
            // Send Mail With System.Net.Mail
#if NETMAIL
            SMTPMail smtpMail = new SMTPMail();
            smtpMail.SMTPHost = "SSL0.OVH.NET";
            smtpMail.SMTPPort = 465;
            smtpMail.Address = "no-reply@optimoc.fr";
            smtpMail.DisplayName = "Optimoc";
            smtpMail.Password = "qwerty_123456789";
            smtpMail.Recipients = "georges.bahsa@optimoc.fr";
            smtpMail.Subject = "Ceci est un test";
            smtpMail.Body = "Ceci est un test";
            smtpMail.HasAttachmentFiles = false;

            Task<bool> _task = smtpMail.SendMail();
            _task.Wait();
            bool res = _task.Result;
#endif

#if MAILKIT
            //
            // Send Mail With MailKit cross-platform

            SMTPMail smtpMail = new SMTPMail();
            smtpMail.SMTPHost = "smtp.optimoc.fr";
            smtpMail.SMTPPort = 587;
            smtpMail.IMAPHost = "imap.optimoc.fr";
            smtpMail.IMAPPort = 993;
            smtpMail.Address = "no-reply@optimoc.fr";
            smtpMail.DisplayName = "Optimoc";
            smtpMail.Password = "qwerty_123456789";
            smtpMail.Recipients = "georges.bahsa@optimoc.fr";
            smtpMail.Subject = "Ceci est un test";
            smtpMail.Body = "Ceci est un test";
            smtpMail.SentFolder = "INBOX.INBOX.Sent";
            smtpMail.HasAttachmentFiles = false;

            Task<bool> _task = smtpMail.SendMailWithMailKit();
            _task.Wait();
            bool res = _task.Result;
#endif
        }
    }
}
