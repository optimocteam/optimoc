appServices.service('projectService',function(userService){
    this.projectData ={};
    this.getProjects = function(){
        return this.projectData;
    }
    this.set=function(projects,callback){
        if(callback){
            console.log("[projectService] with callback : SET projects");
            callback(projects)
        }
        else{
            console.log("[projectService] without callback : SET projects");
            this.projectData = projects;
            userService.set('projects',this.projectData);
        }
        return true;
    }
});
