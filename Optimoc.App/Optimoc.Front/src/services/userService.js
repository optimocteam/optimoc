appServices.service('userService',['$q',function($q){
    /* V2 */

    this.createUserRequest = function(){

    };
    /* V1 */

    this.userData = {};
    this.user = function(){
        return this.userData;
    }
    this.set = function(prop,value, callback){
        if(callback){
            console.log("[userService] with callback : SET "+prop+"");
            callback()
        }
        else{
            console.log("[userService] without callback : SET "+prop+"");
            this.userData[prop] = value;

        }
        return true;
    }

    this.get = function(prop, callback){
        var that = this;
        if(callback){
            console.log("[userService] with callback : GET "+prop+"");
            //console.log(callback);
            callback(that.userData[prop]);
        }else{
            console.log("[userService] without callback : GET "+prop+"");

            return this.userData[prop];
        }
    }
}]);
