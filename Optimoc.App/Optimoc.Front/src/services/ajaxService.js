appServices.service('ajaxService',['$http','$q','localStorageService','$location','userService',function($http,$q,localStorageService,$location,userService){

    //TODO : Move into a conf file
    this.ajaxBasePort = 42000;
    if($location.$$host=="www.optimoc.front"){
        this.ajaxBasePath ="http://www.optimoc.back";this.ajaxBaseURL = this.ajaxBasePath+":"+this.ajaxBasePort;
    }else{
        this.ajaxBasePath ="http://demo.optimoc.fr";this.ajaxBaseURL = this.ajaxBasePath+":"+this.ajaxBasePort;
    }
    var ajaxBaseURL = this.ajaxBasePath+":"+this.ajaxBasePort;

    this.ajaxRequest = function(config){
        var _ajaxBaseUrl = this.ajaxBasePath+":"+this.ajaxBasePort;
        var _method = "GET";
        var _apiModule = "/user";

        if(config.method!=null)
            _method=config.method;

        if(config.apiModule!=null)
            _apiModule = config.apiModule;

        var _url = _ajaxBaseUrl+_apiModule;
        if(config.url!=null)
            _url = config.url

        var _data = {};
        if(config.data!=null) {
            _data = config.data;
        }

        var ajxRq = {
            method:_method,
            url: _url,
            data:_data
        };
        return $http({method:ajxRq.method,url:ajxRq.url,data:ajxRq.data});


    };





    this.setUser = function(firstname,lastname,password,email){
        console.log("[ajaxService]: setUser");

        var method = "POST";
        var url = ajaxBaseURL+'/user';
        var httpData = {action:"userRegistration",data:{firstname:firstname,
            lastname:lastname,
            password:password,
            email:email}};

        var def = $q.defer();
        return $http({method:method,url:url,data:httpData})
            .success(function(data){
                console.log(data);
                if(data.data=="User already exist"){return def.reject(data);}
                else {
                    data.data.role = "MOE Concepteur";
                    userService.set('userID', data.data.userID);
                    userService.set('userAPIKey', data.data.userAPIKey);
                    userService.set('role', data.data.role);
                    localStorageService.set('userAcc', data.data, 14);
                    localStorageService.cookie.set('userAPIKey', data.data.userAPIKey, 14);
                    return def.resolve();
                }
            })
            .error(function(){
                return def.reject('500');
            });
    }
    this.getLogin=function(email,password){
        console.log("[ajaxService]: getLogin");

        var method = "POST";
        var url = ajaxBaseURL+'/user';
        var httpData = {action:"userLogin",data:{email:email,password:password}};

        var def = $q.defer();
        return $http({method:method,url:url,data:httpData})
            .success(function(data){
                console.log(data);
                if(data.data=="Erreur: Mauvais identifiants"){return def.reject(data);}
                else {
                    data.data.role = "MOE Concepteur";
                    userService.set('userID', data.data.userID);
                    userService.set('userAPIKey', data.data.userAPIKey);
                    userService.set('role', data.data.role);

                    localStorageService.set('userAcc', data.data, 14);
                    localStorageService.cookie.set('userAPIKey', data.data.userAPIKey, 14);
                    return def.resolve();
                }
            })
            .error(function(){
                return def.reject('500');
            });
    };
    //Given an $UserAPIKey retrieve default informations
    this.getUserInfo=function(){
        console.log("[ajaxService]: getUserInfo");
        var method = "POST";
        var url = ajaxBaseURL+'/user';
        var httpData = {action:"userInformation",data:{userAPIKey:localStorageService.cookie.get('userAPIKey')}};
        console.log(httpData);
        var def = $q.defer();
        return $http({method:method,url:url,data:httpData})
            .success(function(data){
                console.log(data);
                if(data.data=="Erreur: API Key incorrect"){return def.reject(data);}
                else{console.log('user');/*userService.set('user',data.data);*/return def.resolve(data);}
            })
            .error(function(){
                return def.reject('500');
            });
    };



    this.getProjects=function(){
        console.log("[ajaxService]: getProjects");
        var method = "POST";
        var url = ajaxBaseURL+'/project';
        var httpData = {action:"getProjects",data:{userAPIKey:localStorageService.cookie.get('userAPIKey')}};
        console.log(httpData);
        var def = $q.defer();
        return $http({method:method,url:url,data:httpData})
            .success(function(data){
                console.log(data);
                if(data.data=="Erreur: Mauvais identifiants"){return def.reject(data);}
                else{userService.set('projects',data.data);return def.resolve(data);}
            })
            .error(function(){
                return def.reject('500');
            });
    };

    this.createProject=function(data){


        console.log("[ajaxService]: createProject "+data.projectName);
        var method = "POST";
        var url = ajaxBaseURL+'/project';
        var httpData = {action:"createProject",data:{   projectName: data.projectName, projectDescription: data.projectDescription,userAPIKey:localStorageService.cookie.get('userAPIKey')}};
        console.log(httpData);
        var def = $q.defer();
        var that=this;
        return $http({method:method,url:url,data:httpData})
            .success(function(data){
                console.log(data);
                if(data.data=="Erreur: Mauvais identifiants"){return def.reject(data);}
                else{that.getProjects();return def.resolve(data);}
            })
            .error(function(){
                return def.reject('500');
            });

    };
    this.deleteProject=function(projectID,projectName){
        console.log("[ajaxService]: deleteProject "+projectName);
        var method = "POST";
        var url = ajaxBaseURL+'/project';
        var httpData = {action:"deleteProject",data:{   projectName: projectName, projectID:projectID,userAPIKey:localStorageService.cookie.get('userAPIKey')}};
        console.log(httpData);
        var def = $q.defer();
        var that=this;
        return $http({method:method,url:url,data:httpData})
            .success(function(data){
                console.log(data);
                if(data.data=="Erreur: Mauvais identifiants"){return def.reject(data);}
                else{that.getProjects();return def.resolve(data);}
            })
            .error(function(){
                return def.reject('500');
            });
    };


    /* Metrics */
    this.setMetricCaracteristics=function(projectID,metricsCaracteristics){

        console.log("[ajaxService]: setMetricCaracteristics "+projectID);
        var method = "POST";
        var url = ajaxBaseURL+'/metric';
        var httpData = {action:"setMetricCaracteristics",data:{   projectID:projectID,userAPIKey:localStorageService.cookie.get('userAPIKey'),metricsCaracteristics: metricsCaracteristics}};
        console.log(httpData);
        var def = $q.defer();
        var that=this;
        return $http({method:method,url:url,data:httpData})
            .success(function(data){
                console.log(data);
                if(data.data=="Erreur: Mauvais identifiants"){return def.reject(data);}
                else{return def.resolve(data);}
            })
            .error(function(){
                return def.reject('500');
            });
    };
    this.getMetrics = function(projectID){
        console.log("[ajaxService]: getMetrics "+projectID);
        var method = "POST";
        var url = ajaxBaseURL+'/metric';
        var httpData = {action:"getMetric",data:{   projectID:projectID,userAPIKey:localStorageService.cookie.get('userAPIKey')}};
        console.log(httpData);
        var def = $q.defer();
        var that=this;
        return $http({method:method,url:url,data:httpData})
            .success(function(data){
                console.log(data);
                if(data.data=="Erreur: Mauvais identifiants"){return def.reject(data);}
                else{return def.resolve(data);}
            })
            .error(function(){
                return def.reject('500');
            });
    };

    this.getAjaxURL = function(){
        return ajaxBaseURL;
    }
}]);