
appServices.service('metricService',['localStorageService','$http','$q','$location','ajaxService',function(localStorageService,$http,$q,$location,ajaxService){
    this.projectID = $location.path().substr(-8,1);
    var that = this;

    this.submitAction = function() {
        console.log('Submit Action');
        this.projectID=$('[name="projectID"]').val();
        var metricsCaracteristics = {
            constrLevel: $('[name="constr-level"]').val(),
            surfaceRDC: $('[name="surface-rdc"]').val(),
            surfaceRDplus: $('[name="surface-rdplus"]').val(),
            vault: $('[name="vault"]').val(),
            sanitWC: $('[name="sanit-wc"]').val(),
            sanitSDB: $('[name="sanit-SDB"]').val(),
            bedroom: $('[name="bedroom"]').val(),
            dressing: $('[name="dressing"]').val(),
            office: $('[name="office"]').val(),
            cellar: $('[name="cellar"]').val(),
            linearFundation: $('[name="linear-fundation"]').val(),
            dimensionFundationDepth: $('[name="dimension-fundation-depth"]').val(),
            dimensionFundationWidth: $('[name="dimension-fundation-width"]').val(),
            heigthCrawlSpace: $('[name="heigth-crawl-space"]').val(),
            shearWall: $('[name="shear-wall"]').val(),
            upstairsWall: $('[name="upstairs-wall"]').val(),
            stiffenersStakeCrawlSpace: $('[name="stiffeners-stake-crawl-space"]').val(),
            stiffenersStakeUpstairs: $('[name="stiffeners-stake-upstairs"]').val(),
            timbers: $('[name="timbers"]').val(),
            stake: $('[name="stake"]').val(),
            surfaceShedSlab: $('[name="surface-shed-slab"]').val()
        };
        for (var property in metricsCaracteristics) {
            if(metricsCaracteristics[property]==''){
                metricsCaracteristics[property]=0;
            };
        }
        metricsCaracteristics.surfaceUnder = parseInt(metricsCaracteristics.surfaceRDC - metricsCaracteristics.surfaceRDplus);

        $q.all([ajaxService.setMetricCaracteristics(this.projectID,metricsCaracteristics)]).then(function(value) {
            window.location.reload();
        }, function(reason) {
            // errorCallback
            console.log('*******FALSE***********');
        });

    };

    this.getMetricsData=function(){
        var metricsData = {"data":[
            {
            name:"Prestations de services pour les missions d'assistance à maitrise d'ouvrage, d'economiste de la construction et d'ODPC",
            qte:0,
            unit:"F",
            pricePerUnit:0,
            totalHT:0}
        ]};
        var metricsData =  ajaxService.getMetricsData(this.projectID);

        return metricsData;
    };
    /*this.drawTable=function(metricsData){
        $('#collapse0').collapse('show');

    };*/
    //this.loadDataTable=function(){
        /*var metricsData = ajaxService.getMetricsData(this.projectID);
        console.log(metricsData);
        $('#dataTableRow').removeClass('hidden');
        $('#stepFormRow').addClass('hidden');
        $('#collapse0').collapse('show');*/

        //var that = this;

       /* var tableMetrics = $('#metres').DataTable({
            serverSide: true,
            processing: true,
            ajax:{
                url:ajaxService.ajaxBaseURL+"/metric",
                type:"POST",
                "data": {
                    "action": "getMetric",
                    "projectID": this.projectID,
                    "userAPIKey": localStorageService.cookie.get('userAPIKey')
                },
                "dataSrc":function(json){
                    console.log(json.data);
                    return json.data;
                }
            },
            "columns":[
                {title:"ID",visible:false},
                {title:"Designation",render:function( data, type, full) {
                        return "<a metricid='"+full[0]+"' metrictype='id' href='javascript:void(0);'>"+data+"</a>";
                    }
                },
                {title:"Quantité",render:function( data, type, full) {
                    return "<a metricid='"+full[0]+"' metrictype='qte' href='javascript:void(0);'>"+data+"</a>";
                }},
                {title:"Lots",visible:false},
                {title:"Unité"},
                {title:"P.U HT",render:function( data, type, full) {
                    return "<a metricid='"+full[0]+"' metrictype='pu' href='javascript:void(0);'>"+data+"</a>";
                }},
                {title:"Total H.T"}
            ]*/
                /*"url":ajaxService.ajaxBaseURL+"/metric",
                "type":"POST",
                "data": {
                    "action": "getMetric",
                    "projectID": this.projectID,
                    "userAPIKey": localStorageService.cookie.get('userAPIKey')
                },
                "columns": [
                    { "data": "name" },
                    { "data": "position" },
                    { "data": "office" },
                    { "data": "extn" },
                    { "data": "start_date" },
                    { "data": "salary" }
                ]*/
        //});

        /*'ajax':{
            "type":"POST",*/
                /*"data": {
                    "action": "getMetric",
                    "projectID": this.projectID,
                    "userAPIKey": localStorageService.cookie.get('userAPIKey')
                },*/
            //},
            //"processing": true,
            //"serverSide": true,
            /*"columns": [
                { "data": "name" },
                { "data": "position" },
                { "data": "office" },
                { "data": "extn" },
                { "data": "start_date" },
                { "data": "salary" }
            ]*/

        /*var windotable = $('#metres').DataTable({
            "language":{
                "sProcessing":     "Traitement en cours...",
                "sSearch":         "Rechercher&nbsp;:",
                "sLengthMenu":     "Afficher _MENU_ &eacute;l&eacute;ments",
                "sInfo":           "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                "sInfoEmpty":      "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
                "sInfoFiltered":   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                "sInfoPostFix":    "",
                "sLoadingRecords": "Chargement en cours...",
                "sZeroRecords":    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                "sEmptyTable":     "Aucune donn&eacute;e disponible dans le tableau",
                "oPaginate": {
                    "sFirst":      "Premier",
                    "sPrevious":   "Pr&eacute;c&eacute;dent",
                    "sNext":       "Suivant",
                    "sLast":       "Dernier"
                },
                "oAria": {
                    "sSortAscending":  ": activer pour trier la colonne par ordre croissant",
                    "sSortDescending": ": activer pour trier la colonne par ordre d&eacute;croissant"
                }
            },
            "processing":true,
            "serverSide":true,
            "ajax":that.getMetricsData(),
            "columns":[
                {
                    title:"ID",visible:false
                },
                {
                    title:"Designation",
                        render:function( data, type, full) {
                    return "<a metricid='"+full[0]+"' metrictype='id' href='javascript:void(0);'>"+data+"</a>";
                    }
                },
                {title:"Quantité",render:function( data, type, full) {
                    return "<a metricid='"+full[0]+"' metrictype='qte' href='javascript:void(0);'>"+data+"</a>";
                }},
                {title:"Lots",visible:false},
                {title:"Unité"},
                {title:"P.U HT",render:function( data, type, full) {
                    return "<a metricid='"+full[0]+"' metrictype='pu' href='javascript:void(0);'>"+data+"</a>";
                }},
                {title:"Total H.T"}
            ],*/
        /*
                "url":that.getMetricsData(),
                "data":function(d){
                    console.log(d);
                }
            },*/
            /*{
                "url":ajaxService.ajaxBaseURL+"/metric",
                "type":"POST",
                "data": {
                    "action": "getMetric",
                    "projectID": this.projectID,
                    "userAPIKey": localStorageService.cookie.get('userAPIKey')
                },
                "dataSrc":function(json){
                    console.log("@@@@@@@@");
                    var data = json.data;

                    for(var index=0;index<=data.length-1;index++){
                        console.log(data[index]);
                    }
                    return json;
                }
            },*/
            /*"columns":[
                {
                    title:"ID",visible:false
                },
                {
                    title:"Designation",
                    render:function( data, type, full) {
                        return "<a metricid='"+full[0]+"' metrictype='id' href='javascript:void(0);'>"+data+"</a>";
                    }
                },
                {title:"Quantité",render:function( data, type, full) {
                    return "<a metricid='"+full[0]+"' metrictype='qte' href='javascript:void(0);'>"+data+"</a>";
                }},
                {title:"Lots",visible:false},
                {title:"Unité"},
                {title:"P.U HT",render:function( data, type, full) {
                    return "<a metricid='"+full[0]+"' metrictype='pu' href='javascript:void(0);'>"+data+"</a>";
                }},
                {title:"Total H.T"}
            ],*/
            //"columnDefs":[{"visible":false,"targets":2}],//Cache la 3eme colomne "lots"
            /*"order":[[3,'asc']],//Organise par lots ascendent
            "displayLength":420,//Because this is the answer
            "bLengthChange": false,
*/

            /*"data":function(json){
             console.log("DATA CALLED");
             return json;
             },*/

           /* "createdRow":function(row,data,index){
                console.log("CreateRowCalled");
            },*/
           /* "drawCallback": function ( obj ) {
                console.log("Draw Callback Called");*/
                //console.log('¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤');
                /*$q.all([obj.json.data]).then(function(value) {
                    // successCallback
                    //console.log(obj.json.data)
                }, function(reason) {
                    // errorCallback
                    console.log('*******FALSE***********');
                });*/
                //console.log(obj.json);
                //console.log('¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤');
                //console.log(obj);
                /*if(obj!==undefined && obj.json!==undefined){
                    if(obj.json.error==false){
                        if(obj.json.data!==undefined){
                            //console.log(obj.json.data);
                            var lots = obj.json.data.lots;
                        }
                    }else{

                    }
                }
                if(lots!==undefined){
                    //console.log(lots);
                    var go = lots.grosOeuvre;
                    var implantation = go.implantation;
                    var grosOeuvre = go.grosOeuvre;
                    var tableauEmbrasure = go.tableauEmbrasuresAppuis;
                    var divers = go.divers;
                }
                var api = this.api();
                var t = api.$('td');
                var rows = api.rows( {page:'current'} ).nodes();

                /** Parti organisant les lots en groupement de catégories **/
                /*var last=null;

                api.column(3, {page:'current'} ).data().each( function ( group, i ) {
                    if ( last !== group ) {
                        $(rows).eq( i ).before(
                            '<tr class="group"><td colspan="5">'+group+'</td></tr>'
                        );

                        last = group;
                    }
                } );*/

            /*},"initComplete": function () {
                console.log("Init Complete");*/
                /*$('#metres td a').editable({
                    "url": "http://demo.optimoc.fr:42000/metric",
                    ajaxOptions: {type: 'post'},
                    type: 'text',
                    pk: 1,
                    params: function (params) {
                        //originally params contain pk, name and value
                        params.action = "setMetric";
                        params.userid = "0001";
                        params.userapikey = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
                        params.projectid = "0001";
                        console.log($(this).attr('id'));
                        params.requestdata = JSON.stringify({
                            id: $(this).attr('metricid'),
                            col: $(this).attr('metrictype'),
                            value: params.value
                        });
                        console.log(params);
                        return params;
                    },
                    success:function(){
                        setTimeout( function () {
                            windotable.ajax.reload();
                        }, 100 );
                    }
                });*/
                /*$('#metres tbody').on( 'click', 'tr.group', function () {
                    var currentOrder = windotable.order()[0];
                    if ( currentOrder[0] === 2 && currentOrder[1] === 'asc' ) {
                        windotable.order( [ 2, 'desc' ] ).draw();
                    }
                    else {
                        windotable.order( [ 2, 'asc' ] ).draw();
                    }
                } );*/
            /*},
        });*/
    //}
}]);