﻿"use strict";

(function () {
    appControllers.controller("projectMetricFormCtrl", ["$scope", "$location", "projectsData", "metricService", "ajaxService",  "Math",
        function ($scope, $location, projectsData, metricService, ajaxService, Math) {

        //
        // Walls
        var nWalls = 12;
        $scope.walls = [];

        for (var i = 1; i <= nWalls; i++)
            $scope.walls.push(i);
        
        //
        // Function calcVolume
        $scope.calcVolume = function (length, height, depth) {

            if (length != undefined && height != undefined && depth != undefined)
                return Math.round(length * height * depth, 2);
            else
                return 0;
        };

        //
        // Function calcSurface
        $scope.calcSurface = function (length, height, depth) {

            if (length != undefined && height != undefined)
                return Math.round(length * height, 2);
            else
                return 0;
        };

        //
        //
        $scope.calcGableParams = function () {
            $scope.metricsCaracteristics.gable.lengthHalf = $scope.metricsCaracteristics.gable.lengthTotal / 2;
            $scope.metricsCaracteristics.gable.surface = parseFloat((($scope.metricsCaracteristics.gable.lengthTotal / 2) * 0.33 * ($scope.metricsCaracteristics.gable.lengthTotal / 2)).toFixed(2));
        }

        //
        // Function getProjectIdFromUrl
        $scope.getProjectIdFromUrl = function () {

            var path = $location.path().replace("/metric-form", "");
            return path.substr(path.lastIndexOf("/") + 1, path.length);
        };
        
        //
        // Function setMetricCaracteristicsDefaultValues
        $scope.setMetricCaracteristicsDefaultValues = function () {

            //
            // Default values for metricsCaracteristics
            $scope.metricsCaracteristics = {};

            //
            // Caracteristics
            $scope.metricsCaracteristics.caracteristics = {};
            $scope.metricsCaracteristics.caracteristics.coating = 0;
            $scope.metricsCaracteristics.caracteristics.waterproofing = 0;

            //
            // Fundation
            $scope.metricsCaracteristics.fundation = {};
            $scope.metricsCaracteristics.fundation.heightFundation = {};
            $scope.metricsCaracteristics.fundation.depthFundation = {};
            $scope.metricsCaracteristics.fundation.volumeFundation = {};

            for (var i = 1; i <= nWalls; i++) {

                $scope.metricsCaracteristics.fundation["heightFundation"][i] = 0.5;
                $scope.metricsCaracteristics.fundation["depthFundation"][i] = 0.5;
            }

            //
            // CrawlSpace
            $scope.metricsCaracteristics.crawlSpace = {};
            $scope.metricsCaracteristics.crawlSpace.heightCrawlSpace = {};
            $scope.metricsCaracteristics.crawlSpace.surfaceCrawlSpace = {};

            for (var i = 1; i <= nWalls; i++)
                $scope.metricsCaracteristics.crawlSpace["heightCrawlSpace"][i] = 0.6;

            //
            // GroundElevation
            $scope.metricsCaracteristics.groundElevation = {};
            $scope.metricsCaracteristics.groundElevation.heightGroundElevation = {};
            $scope.metricsCaracteristics.groundElevation.surfaceGroundElevation = {};

            for (var i = 1; i <= nWalls; i++)
                $scope.metricsCaracteristics.groundElevation["heightGroundElevation"][i] = 2.8;

            //
            // FirstElevation
            $scope.metricsCaracteristics.firstElevation = {};
            $scope.metricsCaracteristics.firstElevation.heightFirstElevation = {};
            $scope.metricsCaracteristics.firstElevation.surfaceFirstElevation = {};

            for (var i = 1; i <= nWalls; i++)
                $scope.metricsCaracteristics.firstElevation["heightFirstElevation"][i] = 2.8;
            //
            // SecondElevation
            $scope.metricsCaracteristics.secondElevation = {};
            $scope.metricsCaracteristics.secondElevation.surfaceSecondElevation = {};
        }
        
        //if (/^\d+$/.test($scope.getIdFromUrl())) {
        $scope.projectID = $scope.getProjectIdFromUrl();
        //}

        let project = projectsData.projectsData.find(function (p) { return p.projectID == $scope.projectID; });

        if (project) {

            ajaxService.getMetricsCaracteristics($scope.projectID)
                .then(function (metricsCaracteristicsData) {

                    if (metricsCaracteristicsData != null) {
                        if (Object.keys(metricsCaracteristicsData).length !== 0)
                            $scope.metricsCaracteristics = metricsCaracteristicsData;
                        else
                            $scope.setMetricCaracteristicsDefaultValues();
                    }
                    else
                        $scope.setMetricCaracteristicsDefaultValues();
                });

            $scope.projectName = project.projectName;
            $scope.projectType = project.projectType;
            $scope.projectDescription = project.projectDescription;
        }
        
        //
        //
        $scope.save = function () {

            $scope.disableAll = true;

            var quantitative = $scope.metricsCaracteristics.quantitative;

            var caracteristics = $scope.metricsCaracteristics.caracteristics;
                               
            var fundation = $scope.metricsCaracteristics.fundation;
            for (var i = 1; i <= nWalls; i++)
                fundation.volumeFundation[i] = parseFloat($("#volumeFundation" + i).val());

            var crawlSpace = $scope.metricsCaracteristics.crawlSpace;
            for (var i = 1; i <= nWalls; i++)
                crawlSpace.surfaceCrawlSpace[i] = parseFloat($("#surfaceCrawlSpace" + i).val());

            var groundElevation = $scope.metricsCaracteristics.groundElevation;
            for (var i = 1; i <= nWalls; i++)
                groundElevation.surfaceGroundElevation[i] = parseFloat($("#surfaceGroundElevation" + i).val());

            var firstElevation = $scope.metricsCaracteristics.firstElevation;
            for (var i = 1; i <= nWalls; i++)
                firstElevation.surfaceFirstElevation[i] = parseFloat($("#surfaceFirstElevation" + i).val());

            var secondElevation = $scope.metricsCaracteristics.secondElevation;
            for (var i = 1; i <= nWalls; i++)
                secondElevation.surfaceSecondElevation[i] = parseFloat($("#surfaceSecondElevation" + i).val());

            var gable = $scope.metricsCaracteristics.gable;

            metricService.saveMetricsCaracteristics($scope.projectID, "NEUF", quantitative, caracteristics, fundation, crawlSpace, groundElevation, firstElevation, secondElevation, gable);
        };

        //
        //
        $scope.copyLengthFundationDatas = function (elevation)
        {
            var elevationObj;

            if (elevation == "crawlSpace")
                elevationObj = $scope.metricsCaracteristics.crawlSpace.lengthCrawlSpace;

            if (elevation == "groundElevation")
                elevationObj = $scope.metricsCaracteristics.groundElevation.lengthGroundElevation;

            for (var item in $scope.metricsCaracteristics.fundation.lengthFundation) {

                for (var i = 1; i <= nWalls; i++) {
                    elevationObj[i] = $scope.metricsCaracteristics.fundation.lengthFundation[i];

                }
            }
        }

        //
        //
        $scope.setHeightFundationDefaultValues = function () {

            for (var i = 1; i <= nWalls; i++)
                $scope.metricsCaracteristics.fundation["heightFundation"][i] = $scope.defaultHeightFundation;
        }

        //
        //
        $scope.setDepthFundationDefaultValues = function () {

            for (var i = 1; i <= nWalls; i++)
                $scope.metricsCaracteristics.fundation["depthFundation"][i] = $scope.defaultDepthFundation;
        }

        //
        //
        $scope.setHeightVSDefaultValues = function () {

            for (var i = 1; i <= nWalls; i++)
                $scope.metricsCaracteristics.crawlSpace["heightCrawlSpace"][i] = $scope.defaultHeightVS;
        }

        //
        //
        $scope.setHeightGroundElevationDefaultValues = function () {

            for (var i = 1; i <= nWalls; i++)
                $scope.metricsCaracteristics.groundElevation["heightGroundElevation"][i] = $scope.defaultHeightGroundElevation;
        }


        //
        //
        $scope.setHeightFirstElevationDefaultValues = function () {

            for (var i = 1; i <= nWalls; i++)
                $scope.metricsCaracteristics.firstElevation["heightFirstElevation"][i] = $scope.defaultHeightFirstElevation;
        }

        //
        //
        $scope.setHeightSecondElevationDefaultValues = function () {

            for (var i = 1; i <= nWalls; i++)
                $scope.metricsCaracteristics.secondElevation["heightSecondElevation"][i] = $scope.defaultHeightSecondElevation;
        }

    }]);

}());