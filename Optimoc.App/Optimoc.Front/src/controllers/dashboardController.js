"use strict";

(function () {
    appControllers.controller("dashboardCtrl", ["$scope", "ajaxService", "projectsData", "Array", function ($scope, ajaxService, projectsData, Array) {

        if (projectsData) {

            $scope.aggregateData = projectsData.aggregateData;
            $scope.projects = projectsData.projectsData;
            $scope.filterByProjectType = Array.projectType;
            $scope.filterByProjectStatus = Array.projectStatus;
        }
    }]);

}());