"use strict";

(function () {
    appControllers.controller("projectCtrl", ["$scope", "ajaxService", "projectsData", "$anchorScroll", "localStorageService", "$uibModal", "toastr", "userService",
        function ($scope, ajaxService, projectsData, $anchorScroll, localStorageService, $uibModal, toastr, userService) {

            $scope.aggregateData = projectsData.aggregateData;
            $scope.projects = projectsData.projectsData;

            $scope.showCreateForm = false;
            //$scope.showArchivedProjects = false;

            $scope.errors = {
                alreadyExist: false
            };

            $scope.createProjectFormData = {
                projectName: "",
                projectDescription: "",
                projectType: ""
            };

            //
            // Function editProject
            $scope.editProject = function (projectID, projectName, projectDescription, projectStatus) {

                let oldProjectStatus = projectStatus;

                let modalInstance = $uibModal.open({
                    animation: true,
                    templateUrl: "views/modal/project-edit-modal.html",
                    controller: "ProjectEditModalInstanceCtrl",
                    size: "md",
                    backdrop: "static",
                    resolve: {
                        projectID: function () {
                            return projectID;
                        },
                        projectName: function () {
                            return projectName;
                        },
                        projectDescription: function () {
                            return projectDescription;
                        },
                        projectStatus: function () {
                            return projectStatus;
                        }
                    }
                });

                modalInstance.result.then(function (newProjectStatus) {

                    $scope.aggregateData[newProjectStatus].count++;
                    $scope.aggregateData[oldProjectStatus].count--;

                    setTimeout(function () {
                        $scope.$apply(function () {

                            toastr.success("Projet modifié!");
                        });
                    }, 500);

                }, function () { });
            };

            //
            // Function createProject
            $scope.createProject = function () {
                var modalInstance = $uibModal.open({
                    animation: true,
                    templateUrl: "views/modal/project-create-modal.html",
                    controller: "ProjectCreateModalInstanceCtrl",
                    size: "md",
                    backdrop: "static"
                });

                modalInstance.result.then(function (project) {

                    setTimeout(function () {
                        $scope.$apply(function () {

                            $scope.projects.push(project);
                            $scope.aggregateData[project.projectStatus].count++
                            toastr.success("Projet créé!");
                            // $scope.errors.alreadyExist = false;
                            // $scope.createProjectFormData.projectType = "";
                            $anchorScroll();
                        });
                    }, 1000);

                }, function () { });
            }

            //
            // Function cloneProject
            $scope.cloneProject = function (projectID, projectName) {

                var modalInstance = $uibModal.open({
                    animation: true,
                    templateUrl: "views/modal/project-clone-modal.html",
                    controller: "ProjectCloneModalInstanceCtrl",
                    size: "md",
                    backdrop: "static",
                    resolve: {
                        projectID: function () {
                            return projectID;
                        },
                        projectName: function () {
                            return projectName;
                        }
                    }
                });

                modalInstance.result.then(function (project) {

                    setTimeout(function () {
                        $scope.$apply(function () {

                            $scope.projects.push(project);
                            $scope.aggregateData[project.projectStatus].count++
                            toastr.success("Projet dupliqué!");
                            $anchorScroll();
                        });
                    }, 500);

                }, function () { });
            };

            //
            // Function deleteProject
            $scope.deleteProject = (projectID, projectName) => {
                swal({
                    html: true,
                    title: "Êtes-vous sûr ?",
                    text: `Vous ne pourrez plus récupérer le projet "${projectName}" !`,
                    icon: "warning",
                    buttons:
                    {
                        confirm: {
                            text: "Oui, supprimer",
                            value: true,
                            visible: true,
                            closeModal: false
                        },
                        cancel: {
                            text: "Annuler",
                            value: false,
                            visible: true
                        }
                    },
                    dangerMode: true,
                    closeOnEsc: true,
                    closeOnClickOutside: false,
                    backdrop:true
                })
                .then((value) => {
                    if(value){
                        $(".swal-button__loader").remove();
                        $("button.swal-button--confirm").attr("disabled", "disabled");
                        $(".swal-button--cancel").attr("disabled", "disabled");
                        $(".swal-button--confirm").append("&nbsp;<i class='fa fa-refresh fa-spin'></i>");
                        return value;
                    }
                })
                .then((value) => {
                    if(value)
                        ajaxService.ajaxRequest({
                            method: "POST",
                            apiModule: "/project",
                            data: {
                                action: "deleteProject",
                                data: {
                                    userAPIKey: userService.getUserData()["userAPIKey"],
                                    projectID: projectID
                                }
                            }
                        }).then((res) => {
            
                            var error = res.data["error"];
            
                            if (!error) {
                                swal("Supprimé!", "Le projet a été supprimé", "success");

                                var index = $scope.projects.findIndex(p => p.projectID == projectID);
                                $scope.aggregateData[$scope.projects[index].projectStatus].count--;
                                $scope.projects.splice(index, 1);
                            }
                        });
                })
                .catch(err => {
                    if (err) {
                        swal("Oh noes!", "La suppression a échoué!", "error");
                    } else {
                        swal.stopLoading();
                        swal.close();
                    }
                });
            };

            //
            // Function archiveProject
            $scope.archiveProject = function (projectID, projectName) {

                let modalInstance = $uibModal.open({
                    animation: true,
                    templateUrl: "views/modal/project-archive-modal.html",
                    controller: "ProjectArchiveModalInstanceCtrl",
                    size: "md",
                    backdrop: "static",
                    resolve: {
                        projectID: function () {
                            return projectID;
                        },
                        projectName: function () {
                            return projectName;
                        },
                        action: function () {
                            return "ARCHIVE";
                        }
                    }
                });

                modalInstance.result.then(function () {

                    let index = $scope.projects.findIndex(p => p.projectID == projectID);
                    delete $scope.projects[index]["metrics"];
                    delete $scope.projects[index]["metricsCaracteristics"];
                    $scope.projects[index]["isArchived"] = true;
                    $scope.aggregateData[$scope.projects[index].projectStatus].count--;
                    $scope.aggregateData.ARCHIVE.count++;

                    setTimeout(function () {
                        toastr.success("Projet archivé!");
                        $anchorScroll();
                    }, 500);

                }, function () { });
                
            };

            //
            // Function unArchiveProject
            $scope.unArchiveProject = function (projectID, projectName) {

                let modalInstance = $uibModal.open({
                    animation: true,
                    templateUrl: "views/modal/project-archive-modal.html",
                    controller: "ProjectArchiveModalInstanceCtrl",
                    size: "md",
                    backdrop: "static",
                    resolve: {
                        projectID: function () {
                            return projectID;
                        },
                        projectName: function () {
                            return projectName;
                        },
                        action: function () {
                            return "UNARCHIVE";
                        }
                    }
                });

                modalInstance.result.then(function (res) {

                    let index = $scope.projects.findIndex(p => p.projectID == projectID);
                    $scope.projects[index]["metrics"] = res.metrics;
                    $scope.projects[index]["metricsCaracteristics"] = res.metricsCaracteristics;
                    $scope.projects[index]["isArchived"] = false;
                    $scope.aggregateData[$scope.projects[index].projectStatus].count++;
                    $scope.aggregateData.ARCHIVE.count--;

                    setTimeout(function () {
                        toastr.success("Projet désarchivé!");
                        $anchorScroll();
                    }, 500);

                }, function () { });

            };

        }]);
}());