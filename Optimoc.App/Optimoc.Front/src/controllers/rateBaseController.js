﻿"use strict";

(function () {

    appControllers.controller("rateBaseCtrl", ["$scope", "$location", "$locale", "$filter", "$uibModal", "userService", "ajaxService", "localStorageService", "dtTranslationService", "toastr", "Utils",
        function ($scope, $location, $locale, $filter, $uibModal, userService, ajaxService, localStorageService, dtTranslationService, toastr, Utils) {

            var langCode = $locale.id;
            var user = userService.getUserData();
            var tableRateBase;
            $scope.linkActive = "new";

            //
            // initEditable function
            $scope.initEditable = function () {

                // #region Editable

                $(".ng-dt table .designation a").editable({
                    name: "designation",
                    mode: "inline",
                    type: "textarea",
                    url: ajaxService.getAjaxURL() + "/rateBase",
                    ajaxOptions: {
                        dataType: "json",
                        type: "POST"
                    },
                    params: function (params) {

                        params.action = "updateRateBase";
                        params.userAPIKey = localStorageService.cookie.get("optimoc_user_api_key");
                        params.rateBaseName = $scope.getRateBaseName();
                        params.lotId = $(this).parents(".panel-collapse").attr("lot-id");
                        return params;
                    },
                    display: function (value, res) {

                        tableRateBase = $scope.getCurrentTableRateBase();

                        if (res != undefined) {
                            if (!res.error && res.newValue != null && res.lotIndex != null && res.designationIndex != null) {

                                value = res.newValue;
                                var index = tableRateBase.findIndex(elt => elt.lotId == res.lotIndex);
                                tableRateBase[index].data[res.designationIndex].name = value;
                            }
                        }

                        var match = value.match(/([^.*]*)\s:/);

                        if (match)
                            value = value.replace(match[1], "<strong>[" + match[1] + "]</strong>");

                        $(this).html(value + "&nbsp;&nbsp;<i class='glyphicon glyphicon-pencil'></i>");
                    }
                });

                $(".ng-dt table .pricePerUnit a").on('shown', function (e, editable) {

                    if (parseInt(editable.input.$input.val()) === 0)
                        editable.input.$input.val("");

                    editable.input.$input.keypress(function (e) {

                        var value = $(this).val();

                        if (value) {

                            if (/(.*)(\,|\.)[0-9][0-9]/.test(value) && e.charCode != 13)
                                e.preventDefault();
                        }
                    });
                });

                $(".ng-dt table .pricePerUnit a").editable({
                    name: "pu",
                    //value: "",
                    validate: function (value) {
                        //return Utils.validateNumber($locale.id, value);
                        return Utils.validateNumber(null, value);
                    },
                    url: ajaxService.getAjaxURL() + "/rateBase",
                    ajaxOptions: {
                        dataType: "json",
                        type: "POST"
                    },
                    params: function (params) {
                        params.action = "updateRateBase";
                        params.userAPIKey = localStorageService.cookie.get("optimoc_user_api_key");
                        params.rateBaseName = $scope.getRateBaseName();
                        params.lotId = $(this).parents(".panel-collapse").attr("lot-id");
                        return params;
                    },
                    title: "Entrer le nouveau P.U HT",
                    display: function (value, res) {

                        if (res != undefined) {
                            if (!res.error && res.newValue != null)
                                $(this).html($filter("number")(res.newValue, 2));
                        }
                        else
                            $filter("number")(value, 2);
                    }
                });

                $(".ng-dt .lot .custom").editable({
                    name: "lotName",
                    mode: "inline",
                    showbuttons: false,
                    url: ajaxService.getAjaxURL() + "/rateBase",
                    ajaxOptions: {
                        dataType: "json",
                        type: "POST"
                    },
                    params: function (params) {
                        params.action = "updateRateBase";
                        params.userAPIKey = localStorageService.cookie.get("optimoc_user_api_key");
                        params.rateBaseName = $scope.getRateBaseName();
                        return params;
                    },
                    tpl: "<input type='text' style='width: 250px;height: 27px;'>",
                    display: function (value, res) {

                        tableRateBase = $scope.getCurrentTableRateBase();

                        if (res != undefined) {
                            if (!res.error && res.newValue != null && res.lotIndex != null) {

                                value = res.newValue;
                                var index = tableRateBase.findIndex(elt => elt.lotId == res.lotIndex);
                                tableRateBase[index].name = value;
                            }
                        }

                        $(this).html($filter("uppercase")(value));
                    }
                });

                $(".ng-dt table .unit a").editable({
                    name: "unit",
                    //value: "",
                    url: ajaxService.getAjaxURL() + "/rateBase",
                    ajaxOptions: {
                        dataType: "json",
                        type: "POST"
                    },
                    params: function (params) {
                        params.action = "updateRateBase";
                        params.userAPIKey = localStorageService.cookie.get("optimoc_user_api_key");
                        params.rateBaseName = $scope.getRateBaseName();
                        params.lotId = $(this).parents(".panel-collapse").attr("lot-id");
                        return params;
                    },
                    title: "Entrer la nouvelle unité",
                    display: function (value) {
                        $(this).html($filter("uppercase")(value));
                    }
                });

                // #endregion

            }

            //
            // searchTable function
            $scope.searchTable = function () {

                var id = angular.element(".tab-pane.active").attr("id");
                var tableRateBaseClone = Utils.cloneArray($scope.getCurrentTableRateBase());
                var searchText = (id === "new") ? $scope.searchTextForNew : (id === "reno") ? $scope.searchTextForReno : undefined;

                if (searchText) {

                    var result = [];
                    var expected = ('' + searchText).toLowerCase();
                    var match, data;

                    for (var lotIndex in tableRateBaseClone) {

                        match = false;
                        data = {};
                        var lotData = tableRateBaseClone[lotIndex].data;

                        for (var designationIndex in lotData) {

                            var actual = ('' + lotData[designationIndex].name).toLowerCase();

                            if (actual.indexOf(expected) !== -1) {

                                match = true;
                                data[designationIndex] = lotData[designationIndex];
                            }
                        }

                        if (match) {

                            result.push({
                                lotId: tableRateBaseClone[lotIndex].lotId,
                                name: tableRateBaseClone[lotIndex].name,
                                isCustom: tableRateBaseClone[lotIndex].isCustom,
                                data: data
                            });

                            setTimeout(function () {
                                $scope.initEditable();
                                angular.element(".tab-pane.active .panel-collapse").collapse("show");
                            }, 500);
                        }
                    }

                    if (id === "new")
                        $scope.tableRateBaseForNewClone = result;
                    else if (id === "reno")
                        $scope.tableRateBaseForRenoClone = result;
                }
                else
                    setTimeout(function () {
                        $scope.initEditable();
                        angular.element(".tab-pane.active .panel-collapse").collapse("hide");
                    }, 1000);
            };

            //
            // createLot function
            $scope.createLot = function () {

                waitingDialog.show({ dialogSize: "sm" });
                tableRateBase = $scope.getCurrentTableRateBase();

                ajaxService.createLot("rateBase", { rateBaseName: $scope.getRateBaseName() })
                    .then(function (res) {

                        if (!res.error) {

                            setTimeout(function () {
                                $scope.initEditable();
                            }, 500);

                            waitingDialog.hide();

                            var newLot = new Object();
                            newLot.lotId = res.lotIndex;

                            Object.keys(res.newLot).map(prop => {
                                newLot[prop] = res.newLot[prop];
                            });

                            tableRateBase.push(newLot);
                            toastr.info("Nouveau lot créé !");
                        }
                    });
            }

            //
            // deleteLot function
            $scope.deleteLot = function (lotIndex, lotName) {

                tableRateBase = $scope.getCurrentTableRateBase();
                let rateBaseName = $scope.getRateBaseName();

                swal({
                    html: true,
                    title: "Êtes-vous sûr ?",
                    text: `Vous ne pourrez plus récupérer le lot "${lotName}" !`,
                    icon: "warning",
                    buttons:
                    {
                        confirm: {
                            text: "Oui, supprimer",
                            value: true,
                            visible: true,
                            closeModal: false
                        },
                        cancel: {
                            text: "Annuler",
                            value: false,
                            visible: true
                        }
                    },
                    dangerMode: true,
                    closeOnEsc: true,
                    closeOnClickOutside: false,
                    backdrop:true
                })
                .then((value) => {
                    if(value){
                        $(".swal-button__loader").remove();
                        $("button.swal-button--confirm").attr("disabled", "disabled");
                        $(".swal-button--cancel").attr("disabled", "disabled");
                        $(".swal-button--confirm").append("&nbsp;<i class='fa fa-refresh fa-spin'></i>");
                        return value;
                    }
                })
                .then((value) => {
                    if(value)
                        ajaxService.deleteLot("rateBase", { lotIndex: lotIndex, lotName: lotName, rateBaseName: rateBaseName })
                        .then(res => {
    
                            if (!res.error) {
                                swal("Supprimé!", "Le lot a été supprimé", "success");
                                
                                setTimeout(() => {
                                    $scope.initEditable();
                                }, 500);
        
                                var index = tableRateBase.findIndex(elt => elt.lotId == lotIndex);
                                tableRateBase.splice(index, 1);
                            }
                        });
                })
                .catch(err => {
                    if (err) {
                        swal("Oh noes!", "La suppression a échoué!", "error");
                    } else {
                        swal.stopLoading();
                        swal.close();
                    }
                });

            }

            //
            // createDesignation function
            $scope.createDesignation = function (lotIndex, lotName) {

                waitingDialog.show({ dialogSize: "sm" });
                tableRateBase = $scope.getCurrentTableRateBase();

                ajaxService.createDesignation("rateBase", { lotIndex: lotIndex, lotName: lotName, rateBaseName: $scope.getRateBaseName() })
                    .then(function (res) {

                        if (!res.error) {

                            setTimeout(function () {
                                $scope.initEditable();
                            }, 500);

                            waitingDialog.hide();
                            var index = tableRateBase.findIndex(elt => elt.lotId == lotIndex);
                            tableRateBase[index].data[res.designationIndex] = res.newDesignation;
                            toastr.info("Nouvelle désignation créée !");
                        }
                    });
            };

            //
            // deleteDesignation function
            $scope.deleteDesignation = function (lotIndex, lotName, designationName, designationIndex) {

                waitingDialog.show({ dialogSize: "sm" });
                tableRateBase = $scope.getCurrentTableRateBase();

                ajaxService.deleteDesignation("rateBase",
                    {
                        lotIndex: lotIndex,
                        lotName: lotName,
                        designationIndex: designationIndex,
                        designationName: designationName,
                        rateBaseName: $scope.getRateBaseName()
                    })
                    .then(function (res) {

                        if (!res.error) {

                            setTimeout(function () {
                                $scope.initEditable();
                            }, 500);

                            waitingDialog.hide();
                            var index = tableRateBase.findIndex(elt => elt.lotId == lotIndex);
                            delete tableRateBase[index].data[designationIndex];
                            toastr.success("Désignation supprimée !");
                        }
                    });
            }

            //
            // collapse function
            $scope.collapse = function () {

                var id = angular.element(".tab-pane.active").attr("id");
                var isCollapsed = (id === "new") ? $scope.isCollapsedForNew = !$scope.isCollapsedForNew : (id === "reno") ? $scope.isCollapsedForReno = !$scope.isCollapsedForReno : undefined;

                if (isCollapsed)
                    angular.element(".tab-pane.active .panel-collapse").collapse("show");
                else
                    angular.element(".tab-pane.active .panel-collapse").collapse("hide");
            }

            //
            // buildTableRateBase function
            $scope.buildTableRateBase = function (rateBase) {

                return Object.keys(rateBase).map(lotId => {

                    var obj = new Object();

                    for (var prop in rateBase[lotId]) {

                        obj.lotId = lotId;

                        if (prop === "isCustom")
                            obj.isCustom = (rateBase[lotId].isCustom === undefined) ? false : rateBase[lotId].isCustom;
                        else
                            obj[prop] = rateBase[lotId][prop];
                    }

                    return obj;

                });
            }

            //
            // getRateBaseInCSV function
            $scope.getRateBaseInCSV = function () {
                
                var rateBaseName = $scope.getRateBaseName();

                $scope.showSpinner = true;
                $scope.disableAll = true;

                ajaxService.getRateBaseInCSV(rateBaseName)
                    .then(function (csv) {

                        setTimeout(function () {

                            $scope.$apply(function () {
                                if (window.navigator.msSaveOrOpenBlob) {
                                    var blob = new Blob([decodeURIComponent(encodeURI("\ufeff" + csv))], {
                                        type: "text/csv;charset=utf-8;"
                                    });
                                    navigator.msSaveBlob(blob, "filename.csv");
                                } else {
                                    var a = document.createElement('a');
                                    a.href = "data:attachment/csv;charset=utf-8," + encodeURI("\ufeff" + csv);
                                    a.target = "_blank";
                                    a.download = "Base Tarifaire - " + user["firstName"] + " " + user["lastName"] + " - " + rateBaseName + ".csv";
                                    //document.body.appendChild(a);
                                    a.click();
                                }

                                $scope.showSpinner = false;
                                $scope.disableAll = false;
                            });
                        }, 1000);
                    });
            };

            //
            // getRateBaseName function
            $scope.getRateBaseName = function () {

                var id = angular.element(".tab-pane.active").attr("id");
                return (id === "new") ? "NEUF" : (id === "reno") ? "RENOVATION" : undefined;
            }

            //
            // getCurrentTableRateBase function
            $scope.getCurrentTableRateBase = function () {

                var id = angular.element(".tab-pane.active").attr("id");
                return (id === "new") ? $scope.tableRateBaseForNew : (id === "reno") ? $scope.tableRateBaseForReno : undefined;
            }

            //
            // Ajax
            $scope.showLoading = true;
                
            ajaxService.getRateBases()
                .then(function (rateBases) {

                    //
                    // Build tables
                    $scope.tableRateBaseForNew = $scope.buildTableRateBase(rateBases.find(rb => rb.name == "NEUF").rates);
                    $scope.tableRateBaseForReno = $scope.buildTableRateBase(rateBases.find(rb => rb.name == "RENOVATION").rates);

                    //
                    // Watch tableRateBaseForNew
                    $scope.$watch(
                        "tableRateBaseForNew",
                        function (newValue, oldValue) {
                            $scope.tableRateBaseForNewClone = Utils.cloneArray(newValue);
                        },
                        true);

                    //
                    // Watch tableRateBaseForReno
                    $scope.$watch(
                        "tableRateBaseForReno",
                        function (newValue, oldValue) {
                            $scope.tableRateBaseForRenoClone = Utils.cloneArray(newValue);
                        },
                        true);

                    //
                    // Call initEditable function
                    setTimeout(function () {
                        $scope.initEditable();
                    }, 1000);

                    $scope.showLoading = false;
                },
                function (error) {
                    console.error(error);
                });
        }]);
} ());