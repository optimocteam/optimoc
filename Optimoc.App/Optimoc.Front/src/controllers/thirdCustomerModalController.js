﻿"use strict";

(function () {

    appControllers.controller('thirdCustomerModalInstanceCtrl', ["$scope", "$http", "$uibModalInstance", "ajaxService",
        function ($scope, $http, $uibModalInstance, ajaxService) {

            let customerID = ($scope.$resolve.customerID) ? $scope.$resolve.customerID : null;
            let userAPIKey = ($scope.$resolve.userAPIKey) ? $scope.$resolve.userAPIKey : null;

            $scope.titles = ($scope.$resolve.titles) ? $scope.$resolve.titles : null;
            $scope.status = ($scope.$resolve.status) ? $scope.$resolve.status : null;
            $scope.customerName = ($scope.$resolve.customerName) ? $scope.$resolve.customerName : null;
            $scope.action = $scope.$resolve.action;

            if (customerID) {

                $scope.showLoading = true;

                ajaxService.getCustomer(customerID)
                    .then(function (result) {

                        if (!result.error) {

                            //setTimeout(function () {
                            //    $scope.$apply(function () {
                                $scope.customer = angular.copy(result.customerData);
                                $scope.showLoading = false;
                            //    });
                            //}, 5000);
                        }

                    }, function (error) {
                        console.error(error);
                    });

               
            }
            else
                $scope.customer = {};            

            //
            //
            $scope.getLocation = function (val) {

                return $http.get('//maps.googleapis.com/maps/api/geocode/json', {
                    params: {
                        address: val,
                        sensor: false,
                        componentRestrictions: { country: "FR" }
                    }
                }).then(function (response) {

                    return response.data.results.map(function (place) {
                        return place.formatted_address;
                    });
                });
            };

            //
            //
            $scope.onSelect = function (item, model, label) {
                
                var array = model.split(",");

                if (array.length === 4) {

                    $scope.customer.address.street = array[0];
                    $scope.customer.address.city = array[1].trim();
                    $scope.customer.address.state = array[2].trim().split(" ")[0];
                    $scope.customer.address.zip = array[2].trim().split(" ")[1];
                    $scope.customer.address.country = array[3].split(" ")[1].trim();
                }
                else if (array.length === 3) {

                    $scope.customer.address.street = array[0];
                    $scope.customer.address.city = array[1].trim().split(" ")[1];
                    $scope.customer.address.zip = array[1].trim().split(" ")[0];
                    $scope.customer.address.country = array[2].split(" ")[1].trim();
                }
            };

            //
            //
            $scope.ok = function () {

                switch ($scope.action) {

                    case "ADD":

                        $scope.customer.userAPIKey = userAPIKey;
                        $scope.showSpinner = true;
                        $scope.disableAll = true;

                        ajaxService.createCustomer($scope.customer)
                            .then(function (customerData) {

                                if (customerData) {

                                    setTimeout(function () {
                                        $scope.$apply(function () {
                                            $scope.showSpinner = false;
                                            $scope.disableAll = false;
                                            $uibModalInstance.close(customerData);
                                        });
                                    }, 300);
                                }
                            });

                        break;

                    case "UPD":

                        $scope.showSpinner = true;
                        $scope.disableAll = true;

                        ajaxService.updateCustomer($scope.customer)
                            .then(function (error) {

                                if (!error) {

                                    setTimeout(function () {
                                        $scope.$apply(function () {
                                            $scope.showSpinner = false;
                                            $scope.disableAll = false;
                                            $uibModalInstance.close($scope.customer);
                                        });
                                    }, 300);
                                }
                            });

                        break;
                }
            };

            //
            //
            $scope.cancel = function () {
                $uibModalInstance.dismiss("cancel");
            };
        }]);
}());