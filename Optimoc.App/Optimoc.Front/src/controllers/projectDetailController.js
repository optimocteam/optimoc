"use strict";

(function () {
    appControllers.controller("projectDetailCtrl", ["$scope", "$window", "$location", "$http", "$anchorScroll", "$uibModal", "ajaxService", "projectsData", "localStorageService", "toastr",
        function ($scope, $window, $location, $http, $anchorScroll, $uibModal, ajaxService, projectsData, locationStorageService, toastr) {

            $location.hash('top');
            $anchorScroll();

            $scope.errors = {
                alreadyExist: false
            };

            //
            //
            $scope.getProjectIdFromUrl = function () {

                var path = $location.path();
                var match = path.match(/.*\/(.*)\/edit$/);

                if (match)
                    return match[1];
                else
                    return path.substr(path.lastIndexOf("/") + 1, path.length);
            };

            $scope.projectID = $scope.getProjectIdFromUrl();

            //
            // Project
            let project = projectsData.projectsData.find(function (p) { return p.projectID == $scope.projectID; });

            if (project) {

                $scope.projectName = project.projectName;
                $scope.projectDescription = project.projectDescription;

                if (project.customer)
                    $scope.customer = project.customer ? project.customer : null;

            } else {
                $location.path("/app/project");
            }

            //
            // chooseCustomer function
            $scope.chooseCustomer = function () {

                var modalInstance = $uibModal.open({
                    animation: true,
                    templateUrl: "views/modal/project-detail-customer-modal.html",
                    controller: "projectDetailCustomerModalInstanceCtrl",
                    size: "md",
                    resolve: {

                        projectID: function () {
                            return $scope.projectID
                        }
                    }
                });

                modalInstance.result.then(function (customer) {

                    if (customer != null) {
                        project.customer = $scope.customer = customer;
                        toastr.success("Client affecté au projet!");
                    }
                    else
                        toastr.warning("Erreur dans le choix du client!");

                }, function () { });
            }

            //
            // path
            if (project.projectType == "RENOVATION" || project.projectType == "VIERGE" || (project.hasMetricsCaracteristics && project.projectType == "NEUF"))
                $scope.path = "/app/project/" + project.projectID + "/metric";
            else
                $scope.path = "/app/project/" + project.projectID + "/metric-form";
        }]);
}());