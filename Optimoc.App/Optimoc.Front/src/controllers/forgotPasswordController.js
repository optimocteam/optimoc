﻿'use strict';

(function () {
    appControllers.controller("forgotPasswordCtrl", ["$scope", "ajaxService", "$filter", "$location",
        function ($scope, ajaxService, $filter, $location) {

            //console.log("forgotPassword Ctrl");
            $scope.year = $filter("date")(new Date(), "yyyy");

            //
            //
            $scope.forgotPassword = function () {

                ajaxService
                    .forgotPassword($scope.email)
                    .then(function (result) {

                        if (!result.error) {
                            swal("Envoyé!", `L'email a été envoyé à ${$scope.email}`, "success");
                            setTimeout(function () {
                                $scope.$apply(function () {
                                    $location.path("/login");
                                });
                            }, 3000);
                        }
                        else if (result.error_code === "API_USER_NOT_EXIST_ERROR")
                            swal("Oh noes!", `L'email n'a pas été envoyé à ${$scope.email}. Cette adresse email n'existe pas.`, "error");

                    }, function (error) {
                    });
            };
        }]);
}());