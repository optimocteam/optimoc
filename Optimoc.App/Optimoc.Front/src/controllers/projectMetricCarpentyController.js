﻿"use strict";

(function () {
    appControllers.controller("projectMetricCarpentyCtrl", ["$scope", "$location", "$locale", "$filter", "$compile", "localStorageService", "ajaxService", "projectsData", "metricService", "toastr", "Utils", "dtTranslationService", "DTOptionsBuilder", "DTColumnBuilder",
        function ($scope, $location, $locale, $filter, $compile, localStorageService, ajaxService, projectsData, metricService, toastr, Utils, dtTranslationService, DTOptionsBuilder, DTColumnBuilder) {

            $scope.getProjectIdFromUrl = function () {

                var path = $location.path().replace("/metric-carpenty", "");
                return path.substr(path.lastIndexOf("/") + 1, path.length);
            };
            
            //if (/^\d+$/.test($scope.getProjectIdFromUrl())) {
            $scope.projectID = $scope.getProjectIdFromUrl();
            //}
            
            let project = projectsData.projectsData.find(function (p) { return p.projectID == $scope.projectID; });

            if (project)
                $scope.projectName = project.projectName;
            

            $scope.carpentyMetrics = metricService.getMetricsCarpentyData();
            $scope.tableHeaders = ["#", "designationName", "width", "height", "quantity", "unit", "pricePerUnit", "key", "isCustom", "trash"];
            $scope.tableCarpenty = $scope.carpentyMetrics.data;

            //
            //
            let langCode = $locale.id.split("-")[0];

            $scope.dtOptions = DTOptionsBuilder.newOptions()
                .withOption("lengthChange", false)
                .withOption("paging", false)
                .withOption("info", false)
                .withOption("createdRow", function (row, data, dataIndex) {
                    // Recompiling so we can bind Angular directive to the DT
                    if (data.isCustom) {
                        $("td.designation a", row).addClass("custom");
                        $("td.trash", row).html(
                            "<a href=\"javascript:void(0);\" ng-click=\"deleteDesignation(8,'" + data.key + "')\" style=\"color:inherit\">" +
                            "<i class=\"glyphicon glyphicon-trash\"></i>" +
                            "</a>");

                        $compile(angular.element(row).contents())($scope);
                    }
                })
                .withLanguage(dtTranslationService.translations[langCode]);

            $scope.dtColumns = [
                DTColumnBuilder.newColumn("#").notVisible(),
                DTColumnBuilder.newColumn("designationName").withTitle("Désignation").withOption("width", "45%").notSortable()
                /*.renderWith(function (data) {
                    return  "<a ng-hide=" + data.isCustom + " href=\"javascript:void(0);\" data-pk='" + data.key + "'>" + data.name + "</a>" +
                            "<a ng-show=" + data.isCustom + " class=\"custom\" href=\"javascript:void(0); \" data-pk='" + data.key + "'>" + data.name + "</a >";
                })*/,
                DTColumnBuilder.newColumn("width").withTitle("Largeur").withOption("width", "10%").notSortable(),
                DTColumnBuilder.newColumn("height").withTitle("Hauteur").withOption("width", "10%").notSortable(),
                DTColumnBuilder.newColumn("quantity").withTitle("Quantité").withOption("width", "10%").notSortable(),
                DTColumnBuilder.newColumn("unit").withTitle("Unité").withOption("width", "10%").notSortable(),
                DTColumnBuilder.newColumn("pricePerUnit").withTitle("P.U HT").withOption("width", "15%").notSortable(),
                DTColumnBuilder.newColumn("key").notVisible(),
                DTColumnBuilder.newColumn("isCustom").notVisible(),
                DTColumnBuilder.newColumn("trash").withTitle("").withOption("width", "10%").notSortable()
                /*.renderWith(function (data) {
                    return  "<a ng-hide=" + !data.isCustom + " href=\"javascript:void(0);\" ng-click=\"deleteDesignation(8,'" + data.key + "')\" style=\"color:inherit\">" +
                                "<i class=\"glyphicon glyphicon-trash\"></i>" +
                            "</a>";
                })*/
            ];

            setTimeout(function () {
                $scope.initEditable();
            }, 1000);

            //
            // initEditable function
            $scope.initEditable = function () {

                $(".carpenty-table table .designation a").editable({
                    name: "designation",
                    mode: "inline",
                    type: "textarea",
                    url: ajaxService.getAjaxURL() + "/metric",
                    ajaxOptions: {
                        dataType: "json",
                        type: "POST"
                    },
                    params: function (params) {
                        params.action = "updateMetric";
                        params.userAPIKey = localStorageService.cookie.get("optimoc_user_api_key");
                        params.projectID = $scope.projectID;
                        return params;
                    },
                    display: function (value) {

                        var match = value.match(/([^.*]*)\s:/);

                        if (match)
                            value = value.replace(match[1], "<strong>[" + match[1] + "]</strong>");

                        if (value)
                            $(this).html(value + "&nbsp;&nbsp;<i class='glyphicon glyphicon-pencil'></i>");
                    },
                    success: function (res) {
                        if (!res.error) {
                            $scope.$apply(function () {
                                $scope.carpentyMetrics = res.metrics[8];
                            });
                        }
                    }
                });

                $(".carpenty-table table .Q a, .carpenty-table table .pricePerUnit a, .carpenty-table table .width a, .carpenty-table table .height a").on('shown', function (e, editable) {

                    if (parseInt(editable.input.$input.val()) === 0)
                        editable.input.$input.val("");

                    editable.input.$input.keypress(function (e) {

                        var value = $(this).val();

                        if (value) {

                            if (/(.*)(\,|\.)[0-9][0-9]/.test(value) && e.charCode != 13)
                                e.preventDefault();
                        }
                    });
                });

                $(".carpenty-table table .Q a").editable({
                    //value: "",
                    validate: function (value) {
                        return Utils.validateNumber(value);
                    },
                    url: ajaxService.getAjaxURL() + "/metric",
                    ajaxOptions: {
                        dataType: "json",
                        type: "POST"
                    },
                    params: function (params) {
                        params.action = "updateMetric";
                        params.userAPIKey = localStorageService.cookie.get("optimoc_user_api_key");
                        params.projectID = $scope.projectID;
                        return params;
                    },
                    title: "Entrer la nouvelle quantité",
                    display: function (value, res) {

                        if (res != undefined) {

                            if (!res.error)
                                $(this).html($filter("number")(res.newValue, 2));
                        }
                        else
                            $filter("number")(value, 2);
                    },
                    success: function (res) {
                        if (!res.error) {
                            $scope.$apply(function () {
                                $scope.carpentyMetrics = res.metrics[8];
                            });
                        }
                    }
                });

                $(".carpenty-table table .pricePerUnit a").editable({
                    name: "pu",
                    //value: "",
                    validate: function (value) {
                        //return Utils.validateNumber($locale.id, value);
                        return Utils.validateNumber(null, value);
                    },
                    url: ajaxService.getAjaxURL() + "/metric",
                    ajaxOptions: {
                        dataType: "json",
                        type: "POST"
                    },
                    params: function (params) {
                        params.action = "updateMetric";
                        params.userAPIKey = localStorageService.cookie.get("optimoc_user_api_key");
                        params.projectID = $scope.projectID;
                        return params;
                    },
                    title: "Entrer le nouveau P.U HT",
                    display: function (value, res) {

                        if (res != undefined) {

                            if (!res.error)
                                $(this).html($filter("number")(res.newValue, 2));
                        }
                        $filter("number")(value, 2);
                    },
                    success: function (res) {
                        if (!res.error) {
                            $scope.$apply(function () {
                                $scope.carpentyMetrics = res.metrics[8];
                            });
                        }
                    }
                });

                $(".carpenty-table table .width a").editable({
                    name: "width",
                    validate: function (value) {
                        return Utils.validateNumber(null, value);
                    },
                    url: ajaxService.getAjaxURL() + "/metric",
                    ajaxOptions: {
                        dataType: "json",
                        type: "POST"
                    },
                    params: function (params) {
                        params.action = "updateMetric";
                        params.userAPIKey = localStorageService.cookie.get("optimoc_user_api_key");
                        params.projectID = $scope.projectID;
                        return params;
                    },
                    title: "Entrer la nouvelle largeur",
                    display: function (value, res) {

                        if (res != undefined) {

                            if (!res.error)
                                $(this).html($filter("number")(res.newValue, 2));
                        }
                        $filter("number")(value, 2);
                    },
                    success: function (res) {
                        if (!res.error) {
                            $scope.$apply(function () {
                                $scope.carpentyMetrics = res.metrics[8];
                            });
                        }
                    }
                });

                $(".carpenty-table table .height a").editable({
                    name: "height",
                    validate: function (value) {
                        return Utils.validateNumber(null, value);
                    },
                    url: ajaxService.getAjaxURL() + "/metric",
                    ajaxOptions: {
                        dataType: "json",
                        type: "POST"
                    },
                    params: function (params) {
                        params.action = "updateMetric";
                        params.userAPIKey = localStorageService.cookie.get("optimoc_user_api_key");
                        params.projectID = $scope.projectID;
                        return params;
                    },
                    title: "Entrer la nouvelle hauteur",
                    display: function (value, res) {

                        if (res != undefined) {

                            if (!res.error)
                                $(this).html($filter("number")(res.newValue, 2));
                        }
                        $filter("number")(value, 2);
                    },
                    success: function (res) {
                        if (!res.error) {
                            $scope.$apply(function () {
                                $scope.carpentyMetrics = res.metrics[8];
                            });
                        }
                    }
                });
            }

            //
            // createDesignation function
            $scope.createDesignation = function (lotIndex) {

                waitingDialog.show({ dialogSize: "sm" });

                ajaxService.createDesignation("metric", { projectID: $scope.projectID, lotIndex: lotIndex })
                    .then(function (res) {

                        if (!res.error) {
                            
                            waitingDialog.hide();
                            $scope.carpentyMetrics.data[res.designationIndex] = res.newDesignation;
                            toastr.info("Nouvelle désignation créée !");

                            setTimeout(function () {
                                $scope.initEditable();
                            }, 500);

                        }
                    });
            }

            //
            // deleteDesignation function
            $scope.deleteDesignation = (lotIndex, designationIndex) => {

                swal({
                    html: true,
                    title: "Êtes-vous sûr ?",
                    text: `Vous ne pourrez plus récupérer la désignation "${$scope.carpentyMetrics.data[designationIndex].name}" !`,
                    icon: "warning",
                    buttons:
                    {
                        confirm: {
                            text: "Oui, supprimer",
                            value: true,
                            visible: true,
                            closeModal: false
                        },
                        cancel: {
                            text: "Annuler",
                            value: false,
                            visible: true
                        }
                    },
                    dangerMode: true,
                    closeOnEsc: true,
                    closeOnClickOutside: false,
                    backdrop:true
                })
                .then((value) => {
                    if(value){
                        $(".swal-button__loader").remove();
                        $("button.swal-button--confirm").attr("disabled", "disabled");
                        $(".swal-button--cancel").attr("disabled", "disabled");
                        $(".swal-button--confirm").append("&nbsp;<i class='fa fa-refresh fa-spin'></i>");
                        return value;
                    }
                })
                .then((value) => {
                    if(value){                        
                        ajaxService.deleteDesignation("metric", { projectID: $scope.projectID, lotIndex: lotIndex, designationIndex: designationIndex })
                        .then(res => {
    
                            if (!res.error) {
                                swal("Supprimé!", "La désignation a été supprimée", "success");        
                                delete $scope.carpentyMetrics.data[designationIndex];
                                setTimeout(() => {
                                    $scope.initEditable();
                                }, 500);
                            }
                        });
                    }
                })
                .catch(err => {
                    if (err) {
                        swal("Oh noes!", "La suppression a échoué!", "error");
                    } else {
                        swal.stopLoading();
                        swal.close();
                    }
                });

                /*waitingDialog.show({ dialogSize: "sm" });

                ajaxService.deleteDesignation("metric", { projectID: $scope.projectID, lotIndex: lotIndex, designationIndex: designationIndex })
                    .then(function (res) {

                        if (!res.error) {
                            
                            waitingDialog.hide();
                            delete $scope.carpentyMetrics.data[designationIndex];
                            toastr.success("Désignation supprimée !");

                            setTimeout(function () {
                                $scope.initEditable();
                            }, 500);
                        }
                    });*/
            }
            
            //
            // Watch carpenty metrics
            $scope.$watch("carpentyMetrics", function (newValue, oldValue) {
                metricService.setMetricsCarpentyData(newValue);
            }, true);

        }]);

} ());