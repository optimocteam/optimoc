'use strict';

(function () {
    appControllers.controller("registerCtrl", ["$scope", "$location", "ajaxService", "$uibModal", "$filter",
        function ($scope, $location, ajaxService, $uibModal, $filter) {

            //console.log("Reg Ctrl loaded");
            $scope.className = "page-register pace-done";
            $scope.year = $filter("date")(new Date(), "yyyy");

            $scope.reg = {
                firstname: "",
                lastname: "",
                password: "",
                email: "",
                role: "",
                agreeTerms: false
            };

            $scope.reg.errors = {
                firstname: false,
                lastname: false,
                password: false,
                email: false,
                roleMissing: false,
                agreeTerms: false,
                alreadyExist: false
            };
            
            //
            // Get roles
            ajaxService.getRoles(null).then(function (rolesData) {
                $scope.reg.roles = rolesData;
            });
            
            $scope.onSelect = function (item, model) {
                if (model)
                    $scope.reg.errors.roleMissing = false;
            };

            $scope.register = function () {

                $scope.reg.errors.alreadyExist = false;
                $scope.reg.errors.passwordsNotMatch = false;
                $scope.reg.errors.createFailed = false;
                $scope.reg.errors.agreeTerms = false;

                if ($scope.reg.firstname &&
                    $scope.reg.lastname &&
                    $scope.reg.password &&
                    $scope.reg.confirmPassword &&
                    $scope.reg.email) {

                    if (!$scope.reg.role) {
                        $scope.reg.errors.roleMissing = true;
                        return;
                    }

                    if ($scope.reg.agreeTerms) {

                        /*var role = $scope.reg.roles.filter(function (obj) {
                            return obj.roleID == $scope.reg.roleID;
                        })[0];*/

                        waitingDialog.show({ dialogSize: "sm" });

                        var rq = ajaxService.ajaxRequest({
                            method: "POST",
                            apiModule: "/user",
                            data: {
                                action: "createUser",
                                data: {
                                    firstname: $scope.reg.firstname,
                                    lastname: $scope.reg.lastname,
                                    password: $scope.reg.password,
                                    confirmPassword: $scope.reg.confirmPassword,
                                    email: $scope.reg.email,
                                    role: $scope.reg.role
                                }
                            }
                        });
                        rq.then(function (res) {

                            var error = res.data["error"];
                            var error_code = res.data["error_code"];

                            waitingDialog.hide();

                            if (error) {

                                switch (error_code.toUpperCase()) {

                                    case "API_USER_EXIST_ERROR".toUpperCase():
                                        $scope.reg.errors.alreadyExist = true;
                                        break;

                                    case "API_USER_PASSWORDS_NOT_MATCH_ERROR".toUpperCase():
                                        $scope.reg.errors.passwordsNotMatch = true;
                                        break;

                                    case "TECH_USER_CREATION_FAILED_ERROR".toUpperCase():
                                        $scope.reg.errors.creationFailed = true;
                                        break;

                                    case "TECH_COMPANY_CREATION_FAILED_ERROR".toUpperCase():
                                        $scope.reg.errors.creationFailed = true;
                                        break;
                                }
                            }
                            else {
                                //$scope.success = true;

                                var modalInstance = $uibModal.open({
                                    animation: true,
                                    templateUrl: 'views/modal/welcome-modal.html',
                                    controller: 'welcomeModalInstanceCtrl',
                                    size: 'lg',
                                    resolve: {}
                                });

                                modalInstance.closed.then(function (selectedItem) {
                                    $location.path("/login");
                                }, function () {
                                    console.info("user registred : " + $scope.reg.firstname + " " + $scope.reg.firstname + "at" + new Date());
                                });
                            }
                        }, function (err) {
                            console.error(err);
                        });
                    } else
                        $scope.reg.errors.agreeTerms = true;
                }
            };
        }]);
} ());