﻿"use strict";

(function () {

    appControllers.controller("ProjectCreateModalInstanceCtrl", ["$scope", "$uibModalInstance", "ajaxService",
        function ($scope, $uibModalInstance, ajaxService) {

            $scope.createProjectFormData = {
                projectName: "",
                projectDescription: ""
            };

            $scope.errors = {
                alreadyExist: false
            };

            $scope.ok = function () {

                $scope.showSpinner = true;
                $scope.disableAll = true;

                if ($scope.createProjectFormData.projectName !== "") {

                    var promiseCreateProject = ajaxService.createProject({
                        projectName: $scope.createProjectFormData.projectName,
                        projectDescription: $scope.createProjectFormData.projectDescription == "" ? "N/A" : $scope.createProjectFormData.projectDescription,
                        projectType: $scope.createProjectFormData.projectType
                    });

                    promiseCreateProject.then(function (res) {
                        
                        var error = res.error;
                        $scope.showSpinner = false;
                        $scope.disableAll = false;

                        if (!error)
                            $uibModalInstance.close(res.project);
                        else {

                            if (res.reason) {
                                
                                if (res.reason.toUpperCase() == "projectName_exist".toUpperCase())
                                    $scope.errors.alreadyExist = true;
                            }
                        }
                    });
                }
            };

            $scope.cancel = function () {
                $uibModalInstance.dismiss("cancel");
            };
        }]);

} ());