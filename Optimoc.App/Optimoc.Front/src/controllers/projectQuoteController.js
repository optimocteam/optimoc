"use strict";

(function () {

    appControllers.controller("projectQuoteCtrl", ["$scope", "$location", "$anchorScroll", "$filter", "userService", "projectsData", "localStorageService", "ajaxService", "metricService", "companyService", "browserService", "Utils", "toastr", "$uibModal",
        function ($scope, $location, $anchorScroll, $filter, userService, projectsData, locationStorageService, ajaxService, metricService, companyService, browserService, Utils, toastr, $uibModal ) {

            $location.hash('top');
            $anchorScroll();

            $scope.showTotalHT = true;
            $scope.showVat = true;
            $scope.showTotalTTC = true;
            $scope.showQte = true;
            //$scope.tax = 0;
            $scope.totalTTC = 0;
            $scope.colspanLotName = 6;
            $scope.colspanLotTotal = 4;

            //
            // Project
            $scope.getProjectIdFromUrl = function () {

                var path = $location.path();
                var match = path.match(/.*\/(.*)\/quote/);

                if (match)
                    return match[1];
                else
                    return path.substr(path.lastIndexOf("/") + 1, path.length);
            };

            //if (/^\d+$/.test($scope.getIdFromUrl()))
            $scope.projectID = $scope.getProjectIdFromUrl();

            let project = projectsData.projectsData.find(function (p) { return p.projectID == $scope.projectID; });

            if (project) {

                $scope.projectName = project.projectName;
                $scope.projectDescription = project.projectDescription;
                //$scope.customer = project.customer ? project.customer : null;

                if (project.customerID) {

                    ajaxService.getCustomer(project.customerID)
                        .then(function (result) {

                            if (!result.error)
                                $scope.customer = project.customer = result.customerData;

                        }, function (error) {
                            console.error(error);
                        });
                }
                else
                    $scope.customer = null;

            } else {
                $location.path("/app/project");
            }

            //
            // Company
            $scope.company = companyService.getCompanyData();

            //
            //
            (function () {

                $scope.now = $filter('date')(new Date(), "dd MMMM yyyy");

                // #region Editable

                setTimeout(function () {
                    $(".quote-table .designation span").editable({
                        disabled: true,
                        display: function (value) {

                            var match = value.match(/([^.*]*)\s:/);

                            if (match)
                                value = value.replace(match[1], "<strong>[" + match[1] + "]</strong>");

                            $(this).html(value);
                        }
                    });
                }, 1000);

                // #endregion

                //
                //
                $scope.showLoading = true;

                //
                // Get metrics
                ajaxService.getMetrics($scope.projectID)
                    .then(function (metricsData) {

                        var res = metricService.buildTableMetrics(metricsData, true, true);

                        setTimeout(function () {
                            $scope.$apply(function () {
                                $scope.tableMetrics = res.tableMetrics;
                                $scope.totalHT = res.totalHT;
                                $scope.totalTTC = res.totalTTC;
                                //$scope.tax = project.projectTax;
                                // $scope.totalTTC = $scope.totalHT * (1 + $scope.tax / 100);
                                $scope.showLoading = false;
                            });
                        }, 500);
                    },
                        function (error) {
                            console.error(error);
                    });

                //
                // Get disclaimer text
                ajaxService.getDisclaimerText($scope.projectID)
                    .then(function (res) {

                        setTimeout(function () {
                            $scope.$apply(function () {
                                $scope.disclaimerText = res.disclaimerText;
                            });
                        }, 500);
                    },
                    function (error) {
                        console.error(error);
                    });

                //
                //
                $('#toggle-totalHT').change(function () {
                    $scope.$apply(function () {
                        $scope.showTotalHT = $('#toggle-totalHT').prop("checked");
                    });
                });

                $('#toggle-vat').change(function () {
                    $scope.$apply(function () {
                        $scope.showVat = $('#toggle-vat').prop("checked");

                        if($scope.showVat) {
                            $scope.colspanLotName++;
                            $scope.colspanLotTotal++;
                        }
                        else {
                            $scope.colspanLotName--;
                            $scope.colspanLotTotal--;
                        }
                    });
                });

                $('#toggle-totalTTC').change(function () {
                    $scope.$apply(function () {
                        $scope.showTotalTTC = $('#toggle-totalTTC').prop("checked");
                    });
                });

                $('#toggle-qte').change(function () {
                    $scope.$apply(function () {
                        $scope.showQte = $('#toggle-qte').prop("checked");
                        
                        if($scope.showQte) {
                            $scope.colspanLotName++;
                            $scope.colspanLotTotal++;
                        }
                        else {
                            $scope.colspanLotName--;
                            $scope.colspanLotTotal--;
                        }
                    });
                });

            }());

            //
            //
            $scope.downloadQuote = function () {

                if ($scope.tableMetrics !== null) {

                    var userLastName = userService.getPropValue("lastName");
                    var userID = userService.getPropValue("userID");
                    var companyID = userService.getPropValue("companyID");

                    var tableMetrics = JSON.parse(JSON.stringify($scope.tableMetrics));

                    for (var lotIndex in tableMetrics) {

                        var lotName = tableMetrics[lotIndex].lotName;
                        var lotData = tableMetrics[lotIndex].data;

                        for (var elemIndex in lotData) {

                            if (lotData[elemIndex] !== null && typeof lotData[elemIndex] === "object") {

                                if (lotName === "Electricite" || lotName == "Plomberie") {

                                    var match = lotData[elemIndex].name.match(/([^.*]*)\s:/);

                                    if (match) {
                                        lotData[elemIndex].room = match[1];
                                        lotData[elemIndex].name = lotData[elemIndex].name.replace(match[1], "");
                                    } else {
                                        lotData[elemIndex].room = "";
                                        lotData[elemIndex].name = lotData[elemIndex].name;
                                    }
                                }
                            }
                        }
                    }

                    waitingDialog.show({ dialogSize: "sm" });

                    console.log(`colspanLotName ${$scope.colspanLotName} - colspanLotTotal ${$scope.colspanLotTotal}`)

                    ajaxService.ajaxRequest({
                        method: "POST",
                        apiModule: "/report",
                        //responseType: "arraybuffer",
                        data: {
                            action: "getQuote",
                            userLastName: userLastName,
                            userID: userID,
                            reportData: JSON.stringify({
                                imgName: "u" + userID + "_" + companyID,
                                projectName: $scope.projectName,
                                totalHTVisible: $('#toggle-totalHT').prop("checked"),
                                vatVisible: $('#toggle-vat').prop("checked"),
                                totalTTCVisible: $('#toggle-totalTTC').prop("checked"),
                                qteVisible: $('#toggle-qte').prop("checked"),
                                now: $scope.now,
                                company: $scope.company,
                                customer: $scope.customer,
                                tableMetrics: tableMetrics,
                                totalHT: $scope.totalHT,
                                //tax: parseFloat($scope.tax),
                                totalTTC: $scope.totalTTC,
                                disclaimerText: $scope.disclaimerText,
                                colspanLotName: $scope.colspanLotName,
                                colspanLotTotal: $scope.colspanLotTotal
                            })
                        }
                    }).then(function (res) {

                        let error = res.data["error"];

                        if (!error) {

                            let newTab = browserService.openNewTab(res.data["urlFile"]);

                            if (!newTab || newTab.closed || newTab.closed === "undefined" || typeof newTab === "undefined")
                                toastr.warning("Veuillez autoriser les pop-up sur votre navigateur pour l'affichage du devis!", { timeOut: 10000 });
                            //else
                            //    browserService.updateTabLocation(res.data["urlFile"], newTab);

                            waitingDialog.hide();
                        }
                    });
                }
            };

            //
            //
            $scope.formatAddress = function (address) {
                Utils.formatAddress(address);
            };

            //
            // Function Calc TTC total
            // $scope.calc = function (keyEvent) {

            //     if (keyEvent == undefined || keyEvent.which === 13) {

            //         if (!isNaN($scope.tax) && $scope.tax != "") {

            //             $scope.totalTTC = $scope.totalHT + $scope.totalHT * $scope.tax / 100;

            //             $scope.showLoader = true;

            //             ajaxService.updateProject({
            //                 projectID: $scope.projectID,
            //                 projectTax: $scope.tax
            //             }).then(function (res) {

            //                 var error = res.data["error"];

            //                 if (!error) {

            //                     setTimeout(function () {
            //                         $scope.$apply(function () {
            //                             $scope.showLoader = false;
            //                         });
            //                     }, 100);
            //                 }
            //             });
            //         }
            //     }
            // };

            //
            // Function editDisclaimerText
            $scope.editDisclaimerText = function () {

                let modalInstance = $uibModal.open({
                    animation: true,
                    templateUrl: "views/modal/project-quote-edit-disclaimer.html",
                    controller: "ProjectQuoteEditDisclaimerModalInstanceCtrl",
                    size: "lg",
                    backdrop: "static",
                    resolve: {
                        projectID: function () {
                            return $scope.projectID;
                        },
                        disclaimerText: function () {
                            return $scope.disclaimerText;
                        }
                    }
                });

                modalInstance.result.then(function (disclaimerText) {


                    setTimeout(function () {
                        $scope.$apply(function () {
                            $scope.disclaimerText = disclaimerText;
                            toastr.success("Mentions l&eacute;gales mises &agrave; jour!");
                        });
                    }, 500);

                }, function () { });
            };
        }]);
}());