﻿"use strict";

(function () {

    appControllers.controller("ProjectArchiveModalInstanceCtrl", ["$scope", "$uibModalInstance", "ajaxService", "projectID", "projectName", "action",
        function ($scope, $uibModalInstance, ajaxService, projectID, projectName, action) {

            $scope.projectName = projectName;
            $scope.action = action;

            $scope.ok = function () {

                $scope.showSpinner = true;
                $scope.disableAll = true;

                let promiseArchiveProject = (action == "ARCHIVE") ? ajaxService.archiveProject(projectID) : ajaxService.unArchiveProject(projectID); 
                promiseArchiveProject.then(function (res) {

                    var error = res.error;

                    if (!error) {

                        $scope.showSpinner = false;
                        $scope.disableAll = false;

                        if (res.metrics && res.metricsCaracteristics)
                            $uibModalInstance.close(res);
                        else
                            $uibModalInstance.close();
                    }
                });
            }

            $scope.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            };
        }]);

} ());