'use strict';

(function () {
    appControllers.controller("applicationCtrl", ["$scope", "$location", "$filter", "userService", "localStorageService", function ($scope, $location, $filter, userService, localStorageService) {
        
        $scope.user = userService.getUserData();
        $scope.year = $filter("date")(new Date(), "yyyy");
    }]);
}());
