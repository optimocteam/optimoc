﻿"use strict";

(function () {

    appControllers.controller("projectDetailCustomerModalInstanceCtrl", ["$scope", "$uibModalInstance", "ajaxService", "Utils", "projectID",
        function ($scope, $uibModalInstance, ajaxService, Utils, projectID) {

            $scope.customer = "";

            //
            //
            $scope.searchBy = [
                {
                    searchID : "1",
                    searchName : "Raison sociale"
                },
                {
                    searchID: "2",
                    searchName : "Nom"
                }
            ];

            //
            // Get Customers infos
            
            ajaxService.getCustomers(["title", "socialReason", "lastName", "firstName", "email", "address", "phoneNumber"])
                .then(function (result) {
                    $scope.customers = Utils.objectToArray(result.customersData);

                }, function (error) {
                    console.error(error);
                });


            //
            // OK
            $scope.ok = function () {

                //var customerSelected = customers[$scope.customers.findIndex(c => c.customerID === $scope.customer.customerID)];
                $scope.showSpinner = true;
                $scope.disableAll = true;

                ajaxService.setCustomerToProject(projectID, $scope.customers.selected.customerID)
                    .then(function (error) {

                        if (!error) {

                            setTimeout(function () {
                                $scope.$apply(function () {
                                    $uibModalInstance.close($scope.customers.selected);
                                });
                            }, 300);
                        }
                        else
                            $uibModalInstance.close(null);

                        $scope.showSpinner = false;
                        $scope.disableAll = false;
                    });
            };

            //
            // CANCEL
            $scope.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            };

        }]);
}());