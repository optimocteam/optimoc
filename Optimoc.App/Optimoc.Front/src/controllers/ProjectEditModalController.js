﻿"use strict";

(function () {

    appControllers.controller('ProjectEditModalInstanceCtrl', ["$scope", "$uibModalInstance", "ajaxService", "projectID", "projectName", "projectDescription", "projectStatus", "Array",
        function ($scope, $uibModalInstance, ajaxService, projectID, projectName, projectDescription, projectStatus, Array) {

            $scope.projectName = projectName;
            $scope.projectDescription = projectDescription;
            $scope.projectStatus = Array.projectStatus;
            $scope.projectStatus.selected = projectStatus;

            $scope.errors = {
                alreadyExist: false
            };
            
            //
            //
            $scope.ok = function () {

                $scope.showSpinner = true;
                $scope.disableAll = true;

                ajaxService.updateProject({
                    projectID: projectID,
                    projectName: $scope.projectName,
                    projectDescription: $scope.projectDescription == "" ? "N/A" : $scope.projectDescription,
                    projectStatus: $scope.projectStatus.selected,
                })
                    .then(function (res) {

                        var error = res.data["error"];
                        $scope.showSpinner = false;
                        $scope.disableAll = false;

                        if (!error)
                            $uibModalInstance.close($scope.projectStatus.selected);
                        else {

                            if (res.data["reason"]) {
                                var reason = res.data["reason"];

                                if (reason.toUpperCase() == "projectName_exist".toUpperCase())
                                    $scope.errors.alreadyExist = true;
                            }
                        }
                    });
            };

            $scope.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            };
        }]);

}());