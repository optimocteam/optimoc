﻿"use strict";

(function () {
    appControllers.controller("thirdCtrl", ["$scope", "$locale", "tableTitles", "tableStatus", "$uibModal", "localStorageService", "ajaxService", "dtTranslationService", "DTOptionsBuilder", "DTColumnBuilder", "toastr", "Utils",
        function ($scope, $locale, tableTitles, tableStatus, $uibModal, localStorageService, ajaxService, dtTranslationService, DTOptionsBuilder, DTColumnBuilder, toastr, Utils) {

            let langCode = $locale.id.split("-")[0];
            let userAPIKey = localStorageService.cookie.get("optimoc_user_api_key");
            let tableCustomers = [];
            $scope.linkActive = "customer";

            //
            //
            (function () {

                $scope.showLoading = true;

                ajaxService.getCustomers(["socialReason", "lastName", "firstName", "email", "phoneNumber", "status"])
                    .then(function (result) {
                        $scope.tableCustomers = tableCustomers = Utils.objectToArray(result.customersData);
                        $scope.showLoading = false;
                            
                        //if (tableCustomers.length == 0)
                        //$scope.dtOptions.language.sEmptyTable = "Aucune donn&eacute;e disponible dans le tableau";

                    }, function (error) {
                        console.error(error);
                    });
            }());
            
            //
            //
            $scope.tableCustomersHeaders = ["fullName", "socialReason", "phoneNumber", "email", "status", "modify", "delete"];
            $scope.dtInstance = {};

            $scope.dtOptions = DTOptionsBuilder.newOptions()
                //.withOption("lengthChange", false)
                //.withOption("paging", false)
                .withOption("info", false)
                .withOption("oLanguage", { sEmptyTable: "Vous n'avez aucun client actuellement. Créez-en un." })
                .withDOM("lrtp")
                .withLanguage(dtTranslationService.translations[langCode]);

            $scope.dtColumns = [
                DTColumnBuilder.newColumn("fullName").withTitle("NOM"),
                DTColumnBuilder.newColumn("socialReason").withTitle("RAISON SOCIALE"),
                DTColumnBuilder.newColumn("phoneNumber").withTitle("TELEPHONE"),
                DTColumnBuilder.newColumn("email").withTitle("EMAIL"),
                DTColumnBuilder.newColumn("status").withTitle("STATUT"),
                DTColumnBuilder.newColumn("modify").withTitle("").notSortable(),
                DTColumnBuilder.newColumn("delete").withTitle("").notSortable()
            ]

            //
            //
            $scope.createCustomer = function () {

                var modalInstance = $uibModal.open({
                    animation: true,
                    templateUrl: "views/modal/third-customer-modal.html",
                    controller: "thirdCustomerModalInstanceCtrl",
                    size: "lg",
                    resolve: {

                        action: function () {
                            return "ADD";
                        },
                        userAPIKey: function () {
                            return userAPIKey;
                        },
                        titles: function () {
                            return tableTitles;
                        },
                        status: function () {
                            return tableStatus;
                        }
                    }
                });

                modalInstance.result.then(function (customer) {

                    $scope.tableCustomers.push(customer);
                    toastr.success("Fiche client créée!");
                    
                }, function () { });
            };

            //
            //
            $scope.updateCustomer = function (customerID) {
                
                var modalInstance = $uibModal.open({
                    animation: true,
                    templateUrl: "views/modal/third-customer-modal.html",
                    controller: "thirdCustomerModalInstanceCtrl",
                    size: "lg",
                    resolve: {

                        action: function () {
                            return "UPD";
                        },
                        customerID: function () {
                            return customerID;
                        },
                        titles: function () {
                            return tableTitles;
                        },
                        status: function () {
                            return tableStatus;
                        }
                    }
                });

                modalInstance.result.then(function (customer) {

                    var customerIndex = tableCustomers.findIndex(c => c.customerID === customerID);
                    $scope.tableCustomers[customerIndex] = customer;
                    toastr.success("Fiche client modifiée!");

                }, function () { });
            }

            //
            //
            $scope.deleteCustomer = function (customerID) {
                
                let customer = tableCustomers.find(function (c) { return c.customerID == customerID; });

                swal({
                    html: true,
                    title: "Êtes-vous sûr ?",
                    text: `Vous ne pourrez plus récupérer la fiche client de "${customer.firstName} ${customer.lastName}" !`,
                    icon: "warning",
                    buttons:
                    {
                        confirm: {
                            text: "Oui, supprimer",
                            value: true,
                            visible: true,
                            closeModal: false
                        },
                        cancel: {
                            text: "Annuler",
                            value: false,
                            visible: true
                        }
                    },
                    dangerMode: true,
                    closeOnEsc: true,
                    closeOnClickOutside: false,
                    backdrop:true
                })
                .then((value) => {
                    if(value){
                        $(".swal-button__loader").remove();
                        $("button.swal-button--confirm").attr("disabled", "disabled");
                        $(".swal-button--cancel").attr("disabled", "disabled");
                        $(".swal-button--confirm").append("&nbsp;<i class='fa fa-refresh fa-spin'></i>");
                        return value;
                    }
                })
                .then((value) => {
                    if(value)
                        ajaxService.deleteCustomer(customerID, userAPIKey)
                        .then(res => {
    
                            if (!res.error) {
                                swal("Supprimé!", "La fiche client a été supprimée", "success");
        
                                var customerIndex = tableCustomers.findIndex(c => c.customerID === customerID);
                                $scope.tableCustomers.splice(customerIndex, 1);
                            }
                        });
                })
                .catch(err => {
                    if (err) {
                        swal("Oh noes!", "La suppression a échoué!", "error");
                    } else {
                        swal.stopLoading();
                        swal.close();
                    }
                });
            }

            //
            //
            $scope.searchTable = function () {
                $scope.dtInstance.DataTable.search($scope.searchText).draw();
            };

            //
            //
            $scope.formatAddress = function (address) {
                return Utils.formatAddress(address);
            }

            //$scope.sendEmail = function (email) {
            //    console.log(email);
            //}
        }]);

} ());