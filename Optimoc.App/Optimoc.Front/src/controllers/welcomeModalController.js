﻿"use strict";

(function () {

    appControllers.controller('welcomeModalInstanceCtrl', ["$scope", "$uibModalInstance", function ($scope, $uibModalInstance) {

        $scope.ok = function () {
            $uibModalInstance.close("");
        };
    }]);

}());