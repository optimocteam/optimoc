﻿'use strict';

(function () {
    appControllers.controller("resetPasswordCtrl", ["$scope", "ajaxService", "$filter", "$location",
        function ($scope, ajaxService, $filter, $location) {

            //console.log("resetPassword Ctrl");
            $scope.year = $filter("date")(new Date(), "yyyy");
            $scope.errors = {
                passwordsNotMatch: false
            };
            const query = $location.search();

            //
            //
            $scope.resetPassword = function () {

                ajaxService
                    .resetPassword($scope.newPassword, $scope.confirmPassword, query.emailAddress, query.token)
                    .then(function (result) {

                        if (result.error) {

                            switch(result.error_code) {

                                case "API_USER_NOT_EXIST_ERROR" :
                                    //toastr.error("Compte optimoc introuvable", {timeOut: 6000});
                                    swal("Oh noes!", "Cette adresse email n'existe pas.", "error");
                                    break;
                                
                                case "API_USER_PASSWORDS_NOT_MATCH_ERROR" :
                                    $scope.errors.passwordsNotMatch = true;
                                    break;

                                case "API_USER_RESET_PASSWORD_TOKEN_EXPIRED_ERROR" :
                                    //toastr.error("Ce lien de réinitialisation a expiré", {timeOut: 6000});
                                    swal("Oh noes!", "Ce lien de réinitialisation a expiré.", "error");
                                    break;
                                    
                                case "API_USER_BAD_RESET_PASSWORD_TOKEN_ERROR" :
                                    //toastr.error("Ce lien de réinitialisation n'est plus valide", {timeOut: 6000});
                                    swal("Oh noes!", "Ce lien de réinitialisation n'est plus valide.", "error");
                                    break;
                            }
                        }
                        else
                            //toastr.success("Le mot de passe a bien été modifié.");
                            swal("Modifié!", "Le mot de passe a bien été modifié.", "success");

                        setTimeout(function () {
                            $scope.$apply(function () {
                                $location.search({});
                                $location.path("/login");
                            });
                        }, 3000);

                    }, function (error) {
                    });
            };
        }]);
}());