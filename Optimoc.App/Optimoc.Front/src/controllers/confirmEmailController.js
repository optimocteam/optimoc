﻿'use strict';

(function () {

    appControllers.controller("confirmEmailCtrl", ["$scope", "$location", "ajaxService", "$filter",
        function ($scope, $location, ajaxService, $filter) {

            var query = $location.search();
            $scope.year = $filter("date")(new Date(), "yyyy");
            $scope.hasError = false;

            waitingDialog.show({ dialogSize: "sm" });

            ajaxService.confirmEmail(query.emailAddress, query.key)
                .then(function (result) {

                    waitingDialog.hide();

                    if (result.error) {

                        $scope.hasError = true;

                        switch (result.error_code.toUpperCase()) {

                            case "API_USER_EMAIL_CONFIRMED_ERROR":
                                $scope.message = "Votre compte a déjà été activé, vous serez redirigé dans quelques instants vers la page d'acceuil...";
                                break;

                            case "API_USER_BAD_REGISTER_TOKEN_ERROR":
                                $scope.message = "Un problème est survenu lors de l'activation. Veuillez prendre contact avec l'équipe support...";
                                break;

                            case "API_USER_NOT_EXIST_ERROR":
                                $scope.message = "Ce compte n'existe pas, n'hésitez à en créer un...";
                                break;

                            case "API_USER_REGISTER_TOKEN_EXPIRED_ERROR":
                                $scope.message = "Ce lien d'activation a expiré, n'hésitez à recréer un compte...";
                                break;
                        }
                    }
                    else {
                        $scope.h2 = "Bienvenue sur la plateforme Optimoc";
                        $scope.message = "Vous venez d'activer votre compte, vous serez redirigé dans quelques instants vers la page d'acceuil...";
                        //$scope.message = "Vous venez d'activer votre compte. Vous allez recevoir un code d'activation par sms à utiliser lors de votre première connexion à Optimoc...";
                    }

                    setTimeout(function () {
                        $scope.$apply(function () {
                            $location.search({});
                            $location.path("/login");
                        });
                    }, 7000);
                    
                }, function (error) {
                });
        }]);

}());