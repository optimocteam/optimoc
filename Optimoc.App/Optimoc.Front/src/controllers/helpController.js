﻿"use strict";

(function () {
    appControllers.controller('helpCtrl', ["$sce", function ($sce) {
        console.log("Help Ctrl loaded");
        this.config = {
            video1: [
                { src: $sce.trustAsResourceUrl("https://app.optimoc.fr/videos/optimoc_video_1.mp4"), type: "video/mp4" }
            ],
            video2: [
                { src: $sce.trustAsResourceUrl("https://app.optimoc.fr/videos/optimoc_video_2.mp4"), type: "video/mp4" }
            ],
            video3: [
                { src: $sce.trustAsResourceUrl("https://app.optimoc.fr/videos/optimoc_video_3.mp4"), type: "video/mp4" }
            ],
            video4: [
                { src: $sce.trustAsResourceUrl("https://app.optimoc.fr/videos/optimoc_video_4.mp4"), type: "video/mp4" }
            ],
            video5: [
                { src: $sce.trustAsResourceUrl("https://app.optimoc.fr/videos/optimoc_video_5.mp4"), type: "video/mp4" }
            ],
            video6: [
                { src: $sce.trustAsResourceUrl("https://app.optimoc.fr/videos/optimoc_video_6.mp4"), type: "video/mp4" }
            ],
            tracks: [
                {
                    src: "http://www.videogular.com/assets/subs/pale-blue-dot.vtt",
                    kind: "subtitles",
                    srclang: "en",
                    label: "English",
                    default: ""
                }
            ],
            plugins: {
                poster: "https://app.optimoc.fr/videos/poster_video1.png"
            }
        };
    }]);

}());