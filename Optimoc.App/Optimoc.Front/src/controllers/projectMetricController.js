"use strict";

(function () {
    appControllers.controller("projectMetricCtrl",
        ["$scope", "$filter", "projectsData", "ajaxService", "metricService", "$location", "$http", "$uibModal", "localStorageService", "$anchorScroll", "toastr", "Utils",
            function ($scope, $filter, projectsData, ajaxService, metricService, $location, $http, $uibModal, localStorageService, $anchorScroll, toastr, Utils) {

                //$location.hash("top");
                //$anchorScroll();

                $scope.tableMetrics;
                $scope.headers = ["Désignation", "Quantité", "Unité", "P.U HT", "TVA", "Total HT", "Total TTC"];
                $scope.vat = [
                    { value : "0", text : "0%" },
                    { value: "5.5", text : "5,5%" },
                    { value: "10", text : "10%" },
                    { value: "20", text : "20%" }
                ];

                $scope.getProjectIdFromUrl = function () {
                    var path = $location.path().replace("/metric", "");
                    return path.substr(path.lastIndexOf("/") + 1, path.length);
                };

                $scope.projectID = $scope.getProjectIdFromUrl();

                //
                // Retrieve Project datas
                let project = projectsData.projectsData.find(function (p) { return p.projectID == $scope.projectID; });
                $scope.showButtonCaracteristics = project.hasMetricsCaracteristics && project.projectType == "NEUF";

                let metrics;

                if (project) {

                    $scope.showLoading = true;

                    ajaxService.getMetrics($scope.projectID)
                        .then(function (metricsData) {

                            //
                            // Default Caracteristics for RENOVATION
                            if (Object.keys(metricsData).length === 0 && project.projectType == "RENOVATION") {

                                metricService.saveMetricsCaracteristics($scope.projectID, project.projectType, null, null, null, null, null, null, null, null)
                                    .then(function (metricsData) {

                                        metricService.setMetricsCarpentyData(metricsData[8]);
                                        metrics = metricsData;
                                        $scope.loadTableMetrics(metrics);

                                        setTimeout(function () {
                                            $scope.initEditable();
                                        }, 500);
                                    });
                            } else {
                                metricService.setMetricsCarpentyData(metricsData[8]);
                                metrics = metricsData;
                                $scope.loadTableMetrics(metrics);

                                //
                                // Call initEditable function
                                setTimeout(function () {
                                    $scope.initEditable();
                                }, 500);
                            }
                        });

                    $scope.projectName = project.projectName;
                    $scope.projectType = project.projectType;

                } else {
                    $location.path("/app/project");
                }

                //
                // initEditable function
                $scope.initEditable = function () {

                    // #region Editable

                    $(".metrics-table table .designation a").editable({
                        name: "designation",
                        mode: "inline",
                        type: "textarea",
                        url: ajaxService.getAjaxURL() + "/metric",
                        ajaxOptions: {
                            dataType: "json",
                            type: "POST"
                        },
                        params: function (params) {
                            params.action = "updateMetric";
                            params.userAPIKey = localStorageService.cookie.get("optimoc_user_api_key");
                            params.projectID = $scope.projectID;
                            return params;
                        },
                        display: function (value) {

                            var match = value.match(/([^.*]*)\s:/);

                            if (match)
                                value = value.replace(match[1], "<strong>[" + match[1] + "]</strong>");

                            if (value)
                                $(this).html(value + "&nbsp;&nbsp;<i class='glyphicon glyphicon-pencil'></i>");
                        },
                        success: function (res) {
                            if (!res.error) {

                                $scope.$apply(function () {
                                    metrics = res.metrics;
                                });
                            }
                        }
                    });

                    $(".metrics-table table .qte a, .metrics-table table .pricePerUnit a").on('shown', function (e, editable) {

                        if (parseInt(editable.input.$input.val()) === 0)
                            editable.input.$input.val("");

                        editable.input.$input.submit(function (e) {
                            console.log(editable);

                        });

                        editable.input.$input.keypress(function (e) {
                            
                            var value = $(this).val();

                            if (value) {
                                
                                if (/(.*)(\,|\.)[0-9][0-9]/.test(value) && e.charCode != 13) {
                                    e.preventDefault();
                                }
                            }
                            //else if (e.charCode == 13)
                            //    $(this).val(0);
                        });
                    });

                    //$(document).on("click", ".btn.editable-submit", function () {
                    //    $("a[data-pk!='newsletter']").editable("toggleDisabled");

                    //});

                    $(".metrics-table table .qte a").editable({
                        name: "qte",
                        //value: "",
                        validate: function (value) {
                            //return Utils.validateNumber($locale.id, value);
                            return Utils.validateNumber(null, value);
                        },
                        url: ajaxService.getAjaxURL() + "/metric",
                        ajaxOptions: {
                            dataType: "json",
                            type: "POST"
                        },
                        params: function (params) {
                            params.action = "updateMetric";
                            params.userAPIKey = localStorageService.cookie.get("optimoc_user_api_key");
                            params.projectID = $scope.projectID;
                            return params;
                        },
                        title: "Entrer la nouvelle quantité",
                        display: function (value) {
                            $filter("number")(value, 2);
                        },
                        success: function (res) {
                            //$("a.editable").editable("toggleEnabled");
                            if (!res.error) {

                                metrics = res.metrics;
                                $scope.loadTableMetrics(metrics);
                            }
                        }
                        //savenochange: true
                    });

                    $(".metrics-table table .vat a").editable({
                        name: "vat",
                        type: "select",
                        source: $scope.vat,
                        showbuttons: false,
                        emptytext: "0%",
                        url: ajaxService.getAjaxURL() + "/metric",
                        ajaxOptions: {
                            dataType: "json",
                            type: "POST"
                        },
                        params: function (params) {
                            params.action = "updateMetric";
                            params.userAPIKey = localStorageService.cookie.get("optimoc_user_api_key");
                            params.projectID = $scope.projectID;
                            return params;
                        },
                        title: "Sélectionner une TVA",
                        success: function (res) {

                            if (!res.error) {

                                metrics = res.metrics;
                                $scope.loadTableMetrics(metrics);
                            }
                        }
                    });
                    
                    $(".metrics-table table .pricePerUnit a").editable({
                        name: "pu",
                        //value: "",
                        validate: function (value) {
                            //return Utils.validateNumber($locale.id, value);
                            return Utils.validateNumber(null, value);
                        },
                        url: ajaxService.getAjaxURL() + "/metric",
                        ajaxOptions: {
                            dataType: "json",
                            type: "POST"
                        },
                        params: function (params) {
                            params.action = "updateMetric";
                            params.userAPIKey = localStorageService.cookie.get("optimoc_user_api_key");
                            params.projectID = $scope.projectID;
                            return params;
                        },
                        title: "Entrer le nouveau P.U HT",
                        display: function (value) {
                            $filter("number")(value, 2);
                        },
                        success: function (res) {
                            if (!res.error) {

                                metrics = res.metrics;
                                $scope.loadTableMetrics(metrics);
                            }
                        }
                    });

                    $(".metrics-table .lot .custom").editable({
                        name: "lotName",
                        mode: "inline",
                        showbuttons: false,
                        url: ajaxService.getAjaxURL() + "/metric",
                        ajaxOptions: {
                            dataType: "json",
                            type: "POST"
                        },
                        params: function (params) {
                            params.action = "updateMetric";
                            params.userAPIKey = localStorageService.cookie.get("optimoc_user_api_key");
                            params.projectID = $scope.projectID;
                            return params;
                        },
                        tpl: "<input type='text' style='width: 250px;height: 27px;'>",
                        display: function (value, res) {

                            $(this).html($filter("uppercase")(value));
                        },
                        success: function (res) {
                            if (!res.error) {

                                $scope.$apply(function () {
                                    metrics = res.metrics;
                                });
                            }
                        }
                    });

                    $(".metrics-table table .unit a").editable({
                        name: "unit",
                        //value: "",
                        url: ajaxService.getAjaxURL() + "/metric",
                        ajaxOptions: {
                            dataType: "json",
                            type: "POST"
                        },
                        params: function (params) {
                            params.action = "updateMetric";
                            params.userAPIKey = localStorageService.cookie.get("optimoc_user_api_key");
                            params.projectID = $scope.projectID;
                            return params;
                        },
                        title: "Entrer la nouvelle unité",
                        display: function (value) {
                            $(this).html($filter("uppercase")(value));
                        },
                        success: function (res) {
                            if (!res.error) {

                            }
                        }
                    });

                    // #endregion

                };

                //
                // loadTableMetrics function
                $scope.loadTableMetrics = function (metricsData) {
                    
                    setTimeout(function () {
                        $scope.$apply(function () {

                            var res = metricService.buildTableMetrics(metricsData, false, false);
                            $scope.tableMetrics = res.tableMetrics;
                            $scope.totalHT = res.totalHT;
                            $scope.totalTTC = res.totalTTC;
                            $scope.showLoading = false;
                        });
                    }, 500);
                };

                //
                //
                $scope.change = function (lotId) {
                    //var selectedIndexesLot = [];
                    var isChecked;
                    //$('input[type="checkbox" ]:checked').each(function () {
                    //    selectedIndexesLot.push($(this).attr("id"));
                    //});
                    isChecked = $('input:checkbox[id="' + lotId + '"]').prop("checked");

                    var promiseUpdateMetric = ajaxService.ajaxRequest({
                        method: "POST",
                        apiModule: "/metric",
                        data: {
                            action: "updateMetric",
                            userAPIKey: localStorageService.cookie.get("optimoc_user_api_key"),
                            projectID: $scope.projectID,
                            lotIndex: lotId,
                            isChecked: isChecked
                        }
                    });

                    promiseUpdateMetric.then(function (res) {

                        if (!res.data.error) {

                            metrics[lotId].selected = isChecked;
                            $scope.loadTableMetrics(metrics);
                        }
                    });
                };

                //
                // createLot function
                $scope.createLot = function () {

                    waitingDialog.show({ dialogSize: "sm" });

                    ajaxService.createLot("metric", { projectID: $scope.projectID })
                        .then(function (res) {

                            if (!res.error) {

                                setTimeout(function () {
                                    $scope.initEditable()
                                }, 500);

                                waitingDialog.hide();
                                metrics[res.lotIndex] = res.newLot;
                                $scope.loadTableMetrics(metrics);
                                toastr.info("Nouveau lot créé !");
                                $anchorScroll();
                            }
                        });
                }

                //
                // deleteLot function
                $scope.deleteLot = function (lotIndex, lotName) {

                    swal({
                        html: true,
                        title: "Êtes-vous sûr ?",
                        text: `Vous ne pourrez plus récupérer le lot "${lotName}" !`,
                        icon: "warning",
                        buttons:
                        {
                            confirm: {
                                text: "Oui, supprimer",
                                value: true,
                                visible: true,
                                closeModal: false
                            },
                            cancel: {
                                text: "Annuler",
                                value: false,
                                visible: true
                            }
                        },
                        dangerMode: true,
                        closeOnEsc: true,
                        closeOnClickOutside: false,
                        backdrop:true
                    })
                    .then((value) => {
                        if(value){
                            $(".swal-button__loader").remove();
                            $("button.swal-button--confirm").attr("disabled", "disabled");
                            $(".swal-button--cancel").attr("disabled", "disabled");
                            $(".swal-button--confirm").append("&nbsp;<i class='fa fa-refresh fa-spin'></i>");
                            return value;
                        }
                    })
                    .then((value) => {
                        if(value)
                            ajaxService.deleteLot("metric", { lotIndex: lotIndex, projectID: $scope.projectID })
                            .then(res => {
        
                                if (!res.error) {
                                    swal("Supprimé!", "Le lot a été supprimé", "success");
                                    
                                    setTimeout(() => {
                                        $scope.initEditable();
                                    }, 500);
            
                                    delete metrics[lotIndex];
                                    $scope.loadTableMetrics(metrics);
                                }
                            });
                    })
                    .catch(err => {
                        if (err) {
                            swal("Oh noes!", "La suppression a échoué!", "error");
                        } else {
                            swal.stopLoading();
                            swal.close();
                        }
                    });
                }

                //
                // createDesignation function
                $scope.createDesignation = function (lotIndex) {
                    
                    waitingDialog.show({ dialogSize: "sm" });

                    ajaxService.createDesignation("metric", { projectID: $scope.projectID, lotIndex: lotIndex })
                        .then(function (res) {

                            if (!res.error) {

                                waitingDialog.hide();

                                if (res.diversSeparator && res.diversSeparatorIndex)
                                    metrics[lotIndex].data[res.diversSeparatorIndex] = res.diversSeparator;

                                metrics[lotIndex].data[res.designationIndex] = res.newDesignation;

                                $scope.loadTableMetrics(metrics);
                                toastr.info("Nouvelle désignation créée !");

                                setTimeout(function () {
                                    $scope.initEditable();
                                }, 500);
                            }
                        });
                }

                //
                // deleteDesignation function
                $scope.deleteDesignation = function (lotIndex, designationIndex) {

                    waitingDialog.show({ dialogSize: "sm" });

                    ajaxService.deleteDesignation("metric", { projectID: $scope.projectID, lotIndex: lotIndex, designationIndex: designationIndex })
                        .then(function (res) {

                            if (!res.error) {

                                setTimeout(function () {
                                    $scope.initEditable();
                                }, 500);

                                waitingDialog.hide();

                                if (res.diversSeparatorIndex)
                                    delete metrics[lotIndex].data[res.diversSeparatorIndex];

                                delete metrics[lotIndex].data[designationIndex];

                                $scope.loadTableMetrics(metrics);
                                toastr.success("Désignation supprimée !");
                            }
                        });
                }

                //
                // collapse function
                $scope.collapse = function () {

                    $scope.isCollapsed = !$scope.isCollapsed;

                    if ($scope.isCollapsed)
                        angular.element(".panel-collapse").collapse("show");
                    else
                        angular.element(".panel-collapse").collapse("hide");
                }
              
            }]);

} ());