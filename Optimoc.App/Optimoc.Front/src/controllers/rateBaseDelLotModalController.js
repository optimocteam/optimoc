﻿"use strict";

(function () {

    appControllers.controller("rateBaseDelLotModalInstanceCtrl", ["$scope", "$uibModalInstance", "ajaxService", "lotIndex", "lotName", "rateBaseName",
        function ($scope, $uibModalInstance, ajaxService, lotIndex, lotName, rateBaseName) {

            $scope.lotName = lotName;

            $scope.ok = function () {

                $scope.showSpinner = true;
                $scope.disableAll = true;

                ajaxService.deleteLot("rateBase", { lotIndex: lotIndex, lotName: lotName, rateBaseName: rateBaseName })
                    .then(function (res) {

                        if (!res.error) {

                            $scope.showSpinner = false;
                            $scope.disableAll = false;
                            $uibModalInstance.close();
                        }
                    });
            };

            $scope.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            };
        }]);

} ());