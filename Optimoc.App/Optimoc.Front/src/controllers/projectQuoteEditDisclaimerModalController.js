﻿"use strict";

(function () {

    appControllers.controller('ProjectQuoteEditDisclaimerModalInstanceCtrl', ["$scope", "$uibModalInstance", "ajaxService", "projectID", "disclaimerText", 
        function ($scope, $uibModalInstance, ajaxService, projectID, disclaimerText) {

            $scope.disclaimerText = disclaimerText;
            
            //
            //
            $scope.ok = function () {

                $scope.showSpinner = true;
                $scope.disableAll = true;

                let disclaimerText = $("#disclaimerText").html().replace(/<div>/gi, '<br>').replace(/<\/div>/gi, '');

                ajaxService.updateProject({
                    projectID: projectID,
                    disclaimerText: disclaimerText

                }).then(function (res) {

                    var error = res.data["error"];
                    $scope.showSpinner = false;
                    $scope.disableAll = false;

                    if (!error)
                        $uibModalInstance.close(disclaimerText);
                });
            };

            $scope.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            };
        }]);

}());