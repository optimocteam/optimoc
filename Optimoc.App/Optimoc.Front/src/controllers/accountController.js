﻿"use strict";

(function () {

    appControllers.controller("accountCtrl", ["$scope", "Upload", "companyService", "ajaxService", "userService", "localStorageService", "ImageLoader", "toastr",
        function ($scope, Upload, companyService, ajaxService, userService, localStorageService, ImageLoader, toastr) {

            $scope.linkActive = "personnal";

            //
            //
            $scope.company = companyService.getCompanyData();

            //
            //
            setTimeout(function () {
                (function () {

                    //
                    // Personnal informations
                    $(".lastName, .firstName, .phoneNumber, .email, .street, .additional, .zip, .city, .country").editable({
                        mode: "inline",
                        toggle: "manual",
                        emptytext: "",
                        validate: function (value) {

                            switch ($(this).attr("id")) {

                                case "phoneNumber":

                                    if (!/^0[1-678]([-. ]?[0-9]{2}){4}$/.test(value))
                                        return "Ce numéro de téléphone n'est pas valide.";

                                    break;

                                case "email":

                                    if (!/[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/.test(value))
                                        return "Cette adresse e-mail n'est pas valide.";

                                    break;

                                case "address.zip":

                                    if ($.isNumeric(value) == "" || value.length != 5)
                                        return "Ce code postal n'est pas valide.";

                                    break;
                            }
                        },
                        url: ajaxService.getAjaxURL() + "/user",
                        ajaxOptions: {
                            dataType: "json",
                            type: "POST"
                        },
                        params: function (params) {
                            params.action = "updateUser";
                            params.userAPIKey = localStorageService.cookie.get("optimoc_user_api_key");
                            params.originalValue = $(this).text();
                            return params;
                        },
                        display: function (value) {
                            
                            $(this).html(value);
                        },
                        success: function (res) {
                            if (!res.error) {

                                if (res.data.id == "lastname") {
                                    $scope.user.lastName = res.data.newValue;
                                    userService.setPropValue("lastName", res.data.newValue);
                                }
                                else if (res.data.id == "firstname") {
                                    $scope.user.firstName = res.data.newValue;
                                    userService.setPropValue("firstName", res.data.newValue);
                                }
                                else
                                    userService.setPropValue(res.data.id, res.data.newValue);

                                toastr.info("Mise à jour effectuée!");
                            }
                        }
                    });

                    $("#edit-lastName").click(function (e) {
                        e.stopPropagation();
                        $(".lastName").editable("toggle");
                    });

                    $("#edit-firstName").click(function (e) {
                        e.stopPropagation();
                        $(".firstName").editable("toggle");
                    });

                    $("#edit-phoneNumber").click(function (e) {
                        e.stopPropagation();
                        $(".phoneNumber").editable("toggle");
                    });

                    $("#edit-email").click(function (e) {
                        e.stopPropagation();
                        $(".email").editable("toggle");
                    });

                    $("#edit-street").click(function (e) {
                        e.stopPropagation();
                        $(".street").editable("toggle");
                    });

                    $("#edit-additional").click(function (e) {
                        e.stopPropagation();
                        $(".additional").editable("toggle");
                    });

                    $("#edit-city").click(function (e) {
                        e.stopPropagation();
                        $(".city").editable("toggle");
                    });

                    $("#edit-zip").click(function (e) {
                        e.stopPropagation();
                        $(".zip").editable("toggle");
                    });

                    $("#edit-country").click(function (e) {
                        e.stopPropagation();
                        $(".country").editable("toggle");
                    });

                    //
                    // Company informations
                    $(".name, .socialReason, .legalForm, .siret, .ape, .taxNumber, .insurance, .cPhoneNumber, .cStreet, .cAdditional, .cZip, .cCity, .cCountry").editable({
                        mode: "inline",
                        toggle: "manual",
                        emptytext: "",
                        validate: function (value) {

                            switch ($(this).attr("id")) {

                                case "phoneNumber":

                                    if (!/^0[1-678]([-. ]?[0-9]{2}){4}$/.test(value))
                                        return "Ce numéro de téléphone n'est pas valide.";

                                    break;

                                case "address.zip":

                                    if ($.isNumeric(value) == "" || value.length != 5)
                                        return "Ce code postal n'est pas valide.";

                                    break;
                            }
                        },
                        url: ajaxService.getAjaxURL() + "/company",
                        ajaxOptions: {
                            dataType: "json",
                            type: "POST"
                        },
                        params: function (params) {
                            params.action = "updateCompany";
                            params.companyID = $scope.company.companyID;
                            params.originalValue = $(this).text();
                            return params;
                        },
                        display: function (value) {
                            $(this).html(value);
                        },
                        success: function (res) {
                            if (!res.error) {

                                if (res.data.id == "name")
                                    $scope.company.name = res.data.newValue;

                                companyService.setPropValue(res.data.id, res.data.newValue);
                                toastr.info("Mise à jour effectuée!");
                            }
                        },
                        inputclass: "account-input-editable",
                    });

                    $("#edit-name").click(function (e) {
                        e.stopPropagation();
                        $(".name").editable("toggle");
                    });

                    $("#edit-socialReason").click(function (e) {
                        e.stopPropagation();
                        $(".socialReason").editable("toggle");
                    });

                    $("#edit-legalForm").click(function (e) {
                        e.stopPropagation();
                        $(".legalForm").editable("toggle");
                    });

                    $("#edit-siret").click(function (e) {
                        e.stopPropagation();
                        $(".siret").editable("toggle");
                    });

                    $("#edit-ape").click(function (e) {
                        e.stopPropagation();
                        $(".ape").editable("toggle");
                    });

                    $("#edit-taxNumber").click(function (e) {
                        e.stopPropagation();
                        $(".taxNumber").editable("toggle");
                    });
                    
                    $("#edit-insurance").click(function (e) {
                        e.stopPropagation();
                        $(".insurance").editable("toggle");
                    });
                    
                    $("#edit-cPhoneNumber").click(function (e) {
                        e.stopPropagation();
                        $(".cPhoneNumber").editable("toggle");
                    });

                    $("#edit-cStreet").click(function (e) {
                        e.stopPropagation();
                        $(".cStreet").editable("toggle");
                    });

                    $("#edit-cAdditional").click(function (e) {
                        e.stopPropagation();
                        $(".cAdditional").editable("toggle");
                    });

                    $("#edit-cZip").click(function (e) {
                        e.stopPropagation();
                        $(".cZip").editable("toggle");
                    });

                    $("#edit-cCity").click(function (e) {
                        e.stopPropagation();
                        $(".cCity").editable("toggle");
                    });

                    $("#edit-cCountry").click(function (e) {
                        e.stopPropagation();
                        $(".cCountry").editable("toggle");
                    });

                } ());
            }, 1000);

            //
            // uploadFiles function
            $scope.uploadFiles = function (fileName, folder, file, errFiles) {
                
                $scope.errFile = errFiles && errFiles[0];
                var url;

                if ($scope.errFile) {

                    if ($scope.errFile.$error === "maxSize") {
                        toastr.warning("Fichier volumineux", { timeOut: 4000 });
                    }
                }

                if (file) {

                    Upload.rename(file, fileName);

                    Upload.upload({
                        url: ajaxService.getAjaxURL() + "/upload",
                        data: {
                            file: file,
                            folder: folder
                        }
                    }).then(function (res) {

                        if (!res.data.error) {

                            var imgSrc = "/images/" + folder + "/" + res.config.data.file.ngfName;

                            ImageLoader.loadImage(imgSrc).then(function (loadedSrc) {

                                if (folder === "avatars") {
                                    $scope.user.avatar = loadedSrc + "?_ts=" + new Date().getTime();
                                }
                                else if (folder === "logos") {
                                    $scope.company.logo = loadedSrc + "?_ts=" + new Date().getTime();
                                    companyService.setPropValue("logo", $scope.company.logo);
                                }
                            });
                        }

                    }, function (res) {

                        if (res.status > 0) {
                            // $scope.errorMsg = res.status + ': ' + res.data;
                            console.log('Error status: ' + res.status + " - " + res.data);
                        }

                    }, function (evt) {
                        var progressPercentage = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
                        //console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
                        //$scope.progress = 'progress: ' + progressPercentage + '% ';
                        file.progress = progressPercentage;
                    });
                }
            }
        }]);
} ());