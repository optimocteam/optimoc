"use strict";

(function () {
    appControllers.controller("loginCtrl", ["$scope", "$location", "localStorageService", "ajaxService", "userService", "$filter", "$cookies",
        function ($scope, $location, localStorageService, ajaxService, userService, $filter, $cookies) {
            
            $scope.className = "page-login pace-done";
            $scope.year = $filter("date")(new Date(), "yyyy");
            
            $scope.log = {
                email: null,
                password: null
            };

            $scope.log.errors = {
                badLogin: false,
                badPassword: false,
                disconnectedServer: false
            };
            
            $scope.login = /*async*/ function () {

                $scope.log.errors.badLogin = false;
                $scope.log.errors.disconnectedServer = false;

                if ($scope.log.password != "" && $scope.log.password != null && $scope.log.email != "" && $scope.log.email != null) {

                    /*await*/ ajaxService.getUserInfo($scope.log.email, $scope.log.password)
                        .then(function (result) {

                            if (result.error) {
                                if(result.error_code === "API_USER_PASSWORDS_NOT_MATCH_ERROR")
                                    $scope.log.errors.badPassword = true;
                                else if(result.error_code === "API_USER_EMAIL_NOT_CONFIRMED_ERROR")
                                    swal("Oh noes!", "Ce compte n'est pas activé. Le lien d'activation se trouve dans l'email reçu lors de l'inscription.", "error");
                                else if(result.error_code === "API_USER_NOT_EXIST_ERROR")
                                    $scope.log.errors.badLogin = true;
                                else
                                    $scope.log.errors.disconnectedServer = true;

                            } else if (result.userData == null) {
                                $scope.log.errors.badLogin = true;
                            }
                            else
                                $location.path("/app/dashboard");

                        }, function (error) {
                            $scope.log.errors.disconnectedServer = true;
                        });
                }
            };
        }]);

} ());