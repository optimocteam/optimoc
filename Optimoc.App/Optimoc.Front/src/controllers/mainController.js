"use strict";

(function () {
    appControllers.controller("mainCtrl", ["$scope", "localStorageService", "$location", function ($scope, localStorageService, $location) {

        //console.log("Main Ctrl loaded");
        $scope.optimocLogoSrc = "images/logo-optimoc.png";

        if (localStorageService.cookie.get("optimoc_user_api_key") == undefined) {
            if ($location.path() != "/register" &&
                $location.path() != "/account/confirmEmail"&&
                $location.path() != "/forgotPassword" &&
                $location.path() != "/account/resetPassword")
                $location.path("/login");
        }
    }]);
}());