"use strict";

(function () {
    appControllers.controller("logoutCtrl", ["$scope", "localStorageService", "ajaxService", "$window",
        function ($scope, localStorageService, ajaxService, $window) {
           
            ajaxService.logoutUser()
            .then(function (res) {
                
                var isLoggedOut = res;

                if (isLoggedOut) {

                    //$scope.className = "page-disconnect pace-done";
                    //localStorageService.cookie.remove("optimoc_user_api_key");
                    localStorageService.cookie.clearAll();
                    localStorageService.clearAll();
                }
                else {
                }
            });
        }]);
}());