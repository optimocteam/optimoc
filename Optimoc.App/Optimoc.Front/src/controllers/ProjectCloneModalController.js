﻿"use strict";

(function () {

    appControllers.controller("ProjectCloneModalInstanceCtrl", ["$scope", "$uibModalInstance", "ajaxService", "projectID", "projectName",
        function ($scope, $uibModalInstance, ajaxService, projectID, projectName) {

            $scope.projectName = projectName;

            $scope.createProjectFormData = {
                projectName: "",
                projectDescription: ""
            };

            $scope.errors = {
                alreadyExist: false
            };

            $scope.ok = function () {

                $scope.showSpinner = true;
                $scope.disableAll = true;

                var promiseCloneProject = ajaxService.cloneProject(
                    projectID,
                    $scope.createProjectFormData.projectName,
                    $scope.createProjectFormData.projectDescription == "" ? "N/A" : $scope.createProjectFormData.projectDescription)

                promiseCloneProject.then(function (res) {

                     var error = res.data["error"];

                    $scope.showSpinner = false;
                    $scope.disableAll = false;

                    if (!error) {

                        $uibModalInstance.close(res.data["project"]);
                    }
                    else {

                        if (res.data["reason"]) {
                            var reason = res.data["reason"];

                            if (reason.toUpperCase() == "projectName_exist".toUpperCase())
                                $scope.errors.alreadyExist = true;
                        }
                    }
                });
            };

            $scope.cancel = function () {
                $uibModalInstance.dismiss("cancel");
            };
        }]);

} ());