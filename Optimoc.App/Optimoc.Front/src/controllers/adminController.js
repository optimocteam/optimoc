"use strict";

(function () {
    appControllers.controller("adminCtrl", ["$scope", "$locale", "ajaxService", "dtTranslationService", "DTOptionsBuilder", "DTColumnBuilder", "toastr", "Utils",
        function ($scope, $locale, ajaxService, dtTranslationService, DTOptionsBuilder, DTColumnBuilder, toastr, Utils) {

            let langCode = $locale.id.split("-")[0];
            let tableUsers = [];

            //
            //
            (function () {

                $scope.showLoading = true;

                ajaxService.getUsers()
                    .then(function (result) {
                        $scope.tableUsers = tableUsers = result.users;
                        $scope.showLoading = false;
                            
                    }, function (error) {
                        console.error(error);
                    });
            }());
            
            //
            //
            $scope.tableUsersHeaders = ["firstName", "lastName", "email", "phoneNumber", "registerDate"];
            $scope.dtInstance = {};

            $scope.dtOptions = DTOptionsBuilder.newOptions()
                //.withOption("lengthChange", false)
                .withOption("paging", true)
                .withOption("info", false)
                .withOption("oLanguage", { sEmptyTable: "Vous n'avez aucun ustilisateur actuellement." })
                .withDOM("lrtp")
                .withLanguage(dtTranslationService.translations[langCode]);

            $scope.dtColumns = [
                DTColumnBuilder.newColumn("firstName").withTitle("PRENOM"),
                DTColumnBuilder.newColumn("lastName").withTitle("NOM"),
                DTColumnBuilder.newColumn("email").withTitle("EMAIL"),
                DTColumnBuilder.newColumn("phoneNumber").withTitle("TELEPHONE"),
                DTColumnBuilder.newColumn("registerDate").withTitle("DATE D\'INSCRIPTION"),
            ]

            //
            //
            $scope.searchTable = function () {
                $scope.dtInstance.DataTable.search($scope.searchText).draw();
            };

            //$scope.sendEmail = function (email) {
            //    console.log(email);
            //}
        }]);

} ());