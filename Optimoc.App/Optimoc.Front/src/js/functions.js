﻿//
function invalidRequiredMsg(input) {

    if (input.value == "")
        input.setCustomValidity("Ce champ est obligatoire.");
    else
        input.setCustomValidity("");
}

//
function invalidEmailMsg(input) {

    if (input.value == "")
        input.setCustomValidity("Ce champ est obligatoire.");
    else if (input.validity.typeMismatch || input.validity.patternMismatch)
        input.setCustomValidity("Cette adresse e-mail n'est pas valide.");
    else
        input.setCustomValidity("");
};

//
function invalidPwdMsg(input) {

    if (input.value == "")
        input.setCustomValidity("Ce champ est obligatoire.");
    else if (input.value.length < 8)
        input.setCustomValidity("Le mot de passe doit comporter au moins 8 caractères.");
    else
        input.setCustomValidity("");
};

//
function invalidPhoneNumberMsg(input) {

    if (input.value == "")
        input.setCustomValidity("Ce champ est obligatoire.");
    else if (input.validity.typeMismatch || input.validity.patternMismatch)
        input.setCustomValidity("Ce numéro de téléphone n'est pas valide");
    else
        input.setCustomValidity("");
};
