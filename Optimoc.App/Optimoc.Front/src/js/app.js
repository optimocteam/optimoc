"use strict";

/*
 * Module optimoc
 */
var app = angular.module("optimoc",
    [
        "ui.router",
        "ngAnimate",
        "ngCookies",
        "ngRoute",
        "ngSanitize",
        "LocalStorageModule",
        "jsonFormatter",
        "optimoc.controllers",
        "optimoc.filters",
        "optimoc.services",
        "optimoc.directives",
        "ui.bootstrap",
        "toastr",
        "ngFileUpload",
        "ui.sortable",
        "angular.filter",
        "ui.select",
        "com.2fdevs.videogular",
        "com.2fdevs.videogular.plugins.controls",
        "com.2fdevs.videogular.plugins.overlayplay",
        "com.2fdevs.videogular.plugins.poster"
    ]);

app.config(["$stateProvider", "$urlRouterProvider", "$locationProvider", "toastrConfig", "localStorageServiceProvider",
    function ($stateProvider, $urlRouterProvider, $locationProvider, toastrConfig, localStorageServiceProvider) {

        $stateProvider.state("debug", {
            url: "/debug",
            templateUrl: "views/debug.html",
            controller: "debugCtrl"
        }).state("login", {
            url: "/login",
            templateUrl: "views/login.html",
            controller: "loginCtrl"
        }).state("register", {
            url: "/register",
            templateUrl: "views/register.html",
            controller: "registerCtrl"
        }).state("forgotPassword", {
            url: "/forgotPassword",
            templateUrl: "views/forgot-password.html",
            controller: "forgotPasswordCtrl"
        }).state("confirmEmail", {
            url: "/account/confirmEmail",
            templateUrl: "views/confirm-email.html",
            controller: "confirmEmailCtrl"
        }).state("resetPassword", {
            url: "/account/resetPassword",
            templateUrl: "views/reset-password.html",
            controller: "resetPasswordCtrl"
        }).state("logout", {
            url: "/logout",
            templateUrl: "views/logout.html",
            controller: "logoutCtrl",
            resolve: {
                tmp: function ($timeout) {
                    return $timeout(function () { }, 500);
                }
            }
        }).state("unauthorized", {
            url: "/unauthorized",
            templateUrl: "views/401.html"
        }).state("app", {
            url: "/app",
            templateUrl: "views/application.html",
            controller: "applicationCtrl",
            resolve: {

                projectsData: ["ajaxService", function (ajaxService) {

                    return ajaxService.getProjects()
                        .then(function (result) {
                            return { projectsData: result.projectsData, aggregateData: result.aggregateData };
                        });
                }],

                companyData: ["ajaxService", "userService", function (ajaxService, userService) {

                    return ajaxService.getCompanyInfo(userService.getPropValue("companyID"))
                        .then(function (result) {

                            if (!result.error)
                                return result.companyData;
                        });
                }]
            }
        });

        $urlRouterProvider.otherwise("/app");
        $locationProvider.html5Mode(true);

        angular.extend(toastrConfig, {
            positionClass: "toast-top-right",
            closeButton: true,
            timeOut: 3000,
            allowHtml: true
        });

        localStorageServiceProvider
            .setStorageCookie(1, "/", true)
            .setPrefix("");

    }]).run(function ($rootScope, $transitions, localStorageService, $location, toastr, $window, userService, Access) {

        //
        // login state
        $transitions.onBefore({ to: "login" }, function (transition) {
            
            if (userService.isAuthenticated() && transition.to().name === "login")
                return transition.router.stateService.target("app");
        });

        //
        // confirmEmail state
        $transitions.onBefore({ to: "confirmEmail" }, function (transition) {

            let query = $location.search();

            if (!query.emailAddress && !query.key)
                return transition.router.stateService.target("login");
        });

        //
        // resetPassword state
        $transitions.onBefore({ to: "resetPassword" }, function (transition) {

            let query = $location.search();

            if (!query.emailAddress && !query.token)
                return transition.router.stateService.target("login");
        });
        
        //
        // app state
        $transitions.onSuccess({ to: "app" }, function (transition) {
            $location.path("/app/dashboard");
        });

        //
        // disconnect state
        $transitions.onSuccess({ to: "logout" }, function (transition) {
            setTimeout(function () {
                $window.location.reload();
            }, 1000);
        });

        $transitions.onBefore({ to: "logout" }, function (transition) {

            if (!userService.isAuthenticated()) {
                return transition.router.stateService.target("login");
            }
        });

        //
        // login state
        $transitions.onSuccess({ from: "login" }, function (transition) {
            if (userService.isAuthenticated() && transition.from().name === "login")
                setTimeout(function () {
                    toastr.info("Connect&eacute;!");
                }, 1000);
        });

        const criteria = {
            to: (state) => state.name !== "login" && state.name !== "register" && state.name !== "confirmEmail" && state.name !== "forgotPassword" & state.name !== "resetPassword"
        };

        //
        // all
        $transitions.onBefore(criteria, function (transition) {
            
            if (!userService.isAuthenticated()) {
                localStorageService.cookie.clearAll();
                localStorageService.clearAll();

                setTimeout(function () {
                    toastr.warning("Votre session a expir&eacute;. Veuillez vous reconnecter");
                }, 500);

                $window.location.reload();
                return transition.router.stateService.target("login");
            }
        });

        $transitions.onError(criteria, function (transition) {

            if (transition._error.detail === Access.UNAUTHORIZED || transition._error.detail === Access.FORBIDDEN) {
                localStorageService.cookie.clearAll();
                localStorageService.clearAll();
                //$location.path("/login");
                $window.location.reload();
            }
        });
    });

/*
 * Services
 * 
 */

var appServices = angular.module("optimoc.services", []);

appServices.factory("Array", function () {

    return {

        projectType: ["NEUF", "RENOVATION", "VIERGE"],
        
        projectStatus: ["AVANT_PROJET", "EN_COURS"]
    };
});

appServices.factory('ImageLoader', ['$q', function ($q) {

    var defaultImage;

    return {

        setDefaultImage: function (image) {
            defaultImage = image;
        },

        loadImage: function (image, srcProperty) {

            srcProperty = typeof srcProperty !== "undefined" ? srcProperty : "src";
            var deferred = $q.defer()
            var imageObject = new Image();

            imageObject.onload = function () {

                if (typeof image === "object") {
                    image[srcProperty] = imageObject.src;
                }
                else if (typeof image === "string") {
                    image = imageObject.src;
                }

                deferred.resolve(image);
            };

            imageObject.onerror = function () {
                deferred.resolve(defaultImage);
            }

            try {
                if (typeof image === "object") {

                    imageObject.src = image[srcProperty];
                }
                else if (typeof image === "string") {

                    imageObject.src = image;
                }
                else {
                    throw new TypeError("No image provided");
                }

                return deferred.promise;

            } catch (e) {
                console.error(e);
            };
        }
    };
}]);

appServices.factory('Utils', [function () {

    return {

        formatAddress: function (address) {

            if (address.street && address.zip && address.city && address.country) {
                return (address.additional)
                    ? address.street + " " + address.additional + ", " + address.zip + " " + address.city + ", " + address.country
                    : address.street + ", " + address.zip + " " + address.city + ", " + address.country;
            }
            else
                return "";
        },

        validateNumber: function (localeId, value) {

            if (localeId && value != "") {

                switch (localeId) {

                    case "fr-fr":
                        if (!/^\d+(\,\d{1,2})?$/.test(value))
                            return "La saisie est non valide";
                        break;

                    case "en-us":
                        if (!/^\d+(\.\d{1,2})?$/.test(value))
                            return "La saisie est non valide";
                        break;
                }
            }
            else if (value == "" || isNaN(value.replace(",", ".")))
                return "La saisie est non valide";
        },

        objectToArray: function (object) {

            return Object.keys(object).map(key => {

                var obj = new Object();

                for (var prop in object[key])
                    obj[prop] = object[key][prop];

                return obj;

            });
        },

        cloneArray: function (arraySource) {

            var arrayDest = [];

            for (var i = 0; i < arraySource.length; i++)
                arrayDest.push(arraySource[i]);

            return arrayDest;
        }
    };
}]);

appServices.factory('Math', function () {

    var round = Math.round;

    return {

        round: function (value, decimals) {
            decimals = decimals || 0;
            return Number(round(value + 'e' + decimals) + 'e-' + decimals);
        }
    }
});

appServices.factory('Access', function () {

    return {
        OK: 200,
        UNAUTHORIZED: 401,
        FORBIDDEN: 403
    }
});

appServices.service("ajaxService", ["$http", "$q", "localStorageService", "$location", "userService", "companyService", "ImageLoader", "projectService", "$window", "Access",
    function ($http, $q, localStorageService, $location, userService, companyService, ImageLoader, projectService, $window, Access) {

        //
        // Only for localhost
        var ajaxBasePort = 42e3;
        var ajaxBaseURL, ajaxBasePath;

        if ($location.$$host == "app.optimoc.fr") {
            ajaxBasePath = "https://app.optimoc.fr";
            //this.ajaxBaseURL = this.ajaxBasePath + ":" + this.ajaxBasePort;
            ajaxBaseURL = ajaxBasePath;
        } else if ($location.$$host == "dev.optimoc.fr") {
            ajaxBasePath = "https://dev.optimoc.fr";
            ajaxBaseURL = ajaxBasePath;
        }
        else {
            ajaxBasePath = "https://localhost";
            ajaxBaseURL = ajaxBasePath + ":" + ajaxBasePort;
        }

        var methods = {

            //#region ajaxRequest method

            ajaxRequest: function (config) {
                
                var _method = "GET";
                var _apiModule = "/user";
                if (config.method != null) _method = config.method;
                if (config.apiModule != null) _apiModule = config.apiModule;

                var _url = ajaxBaseURL + _apiModule;
                if (config.url != null) _url = config.url;

                var _responseType;
                if (config.responseType != null) _responseType = config.responseType;

                var _data = {};
                if (config.data != null) {
                    _data = config.data;
                }

                if (_method == "GET")
                    return $http({
                        method: _method,
                        url: _url,
                        params: _data,
                        responseType: _responseType
                    });
                else
                    return $http({
                        method: _method,
                        url: _url,
                        data: _data,
                        responseType: _responseType
                    });
            },

            //#endregion

            //#region getUserInfo method

            getUserInfo: function (email, password) {

                //console.log("[ajaxService]: getUserInfo");
                var uapikey = localStorageService.cookie.get("optimoc_user_api_key");
                var userData;
                var params;

                if (uapikey != null)
                    params = { userAPIKey: uapikey }
                else if (email != null && password != null) {
                    params = {
                        email: email,
                        password: password
                    }
                }

                var promise =
                    this.ajaxRequest({
                        method: "POST",
                        apiModule: "/user",
                        data: {
                            action: "getUser",
                            data: params
                        }
                    });

                return promise.then(function (res) {

                    var data = res.data["data"];
                    
                    if (data && res.status === 200) {

                        var tmpUserData = {

                            firstName: data.firstName,
                            lastName: data.lastName,
                            email: data.email,
                            userID: data.userID,
                            userAPIKey: data.userAPIKey,
                            registerDate: data.registerDate,
                            role: data.role,
                            phoneNumber: data.phoneNumber,
                            companyID: data.companyID,
                            isAdmin: data.isAdmin,
                            avatar: ""
                        };

                        userData = tmpUserData;
                        return userData.userID;
                    }

                }).then(function (userID) {

                    var defaultAvatar = "/images/avatars/default-avatar.png";
                    var imgSrc = "/images/avatars/u" + userID + ".jpg";

                    ImageLoader.setDefaultImage(defaultAvatar);

                    return ImageLoader.loadImage(imgSrc).then(function (loadedSrc) {
                        return loadedSrc;
                    });

                }).then(function (avatar) {

                    userData.avatar = avatar;
                    userService.setUserData(userData);

                    return {
                        userData: userData,
                        error: false
                    };

                }).catch((error) => {
                    
                    if (error.status === 401)
                        return {
                            error: true,
                            error_code: error.data.error_code
                        };

                    if (error.status === Access.FORBIDDEN) {
                        return $q.reject(Access.FORBIDDEN);
                    }
                });
            },

            //#endregion

            //#region getUsers method

            getUsers: function () {

                //console.log("[ajaxService]: getUsers");

                var uapikey = localStorageService.cookie.get("optimoc_user_api_key");

                return this.ajaxRequest({
                    method: "POST",
                    apiModule: "/user",
                    data: {
                        action: "getUsers",
                        data: {
                            userAPIKey: uapikey
                        }
                    }
                }).then(function (res) {

                    if (res.status === 200) {

                        return {
                            users: res.data
                        };
                    }

                }).catch((error) => {
                    if (error.status === Access.UNAUTHORIZED || (error.status === Access.UNAUTHORIZED && error.data.error_code === "API_USER_FAILED_TO_AUTHENTICATE_ERROR")) {
                        $location.path("/unauthorized");
                        return $q.reject(Access.UNAUTHORIZED);
                    } else if (error.status === Access.FORBIDDEN && error.data.error_code === "API_USER_AUTH_TOKEN_NOT_SUPPLIED_ERROR") {
                        return $q.reject(Access.FORBIDDEN);
                    }
                });;
            },

            //#endregion

            //#region logoutUser method

            logoutUser: function () {

                //console.log("[ajaxService]: logoutUser");
                var uapikey = localStorageService.cookie.get("optimoc_user_api_key");
                var params = {
                    userAPIKey: uapikey       
                };
                var isLoggedOut = true;

                var promise =
                    this.ajaxRequest({
                        method: "POST",
                        apiModule: "/user",
                        data: {
                            action: "logoutUser",
                            data: params
                        }
                    });

                return promise.then(function (res) {

                    isLoggedOut = res.status === 200;
                    return isLoggedOut;

                }).catch((error) => {
                    return $q.reject(error.message);
                });
            },

            //#endregion

            //#region confirmEmail method

            confirmEmail: function (email, key) {

                //console.log("[ajaxService]: confirmEmail");

                var promise =
                    this.ajaxRequest({
                        method: "POST",
                        apiModule: "/user",
                        data: {
                            action: "confirmEmail",
                            data: {
                                email: email,
                                key: key
                            }
                        }
                    });

                return promise.then(function (res) {

                    var error = res.data["error"];
                    var error_code = res.data["error_code"];

                    return {
                        error_code: error_code,
                        error: error
                    };

                }, function (error) {
                    return error;
                });
            },

            //#endregion

            //#region forgotPassword method

            forgotPassword: function (email) {

                //console.log("[ajaxService]: forgotPassword");

                var promise =
                    this.ajaxRequest({
                        method: "POST",
                        apiModule: "/user",
                        data: {
                            action: "forgotPassword",
                            data: {
                                email: email
                            }
                        }
                    });

                return promise
                .then(function (res) {
                     if (res.status === 200)
                        return {
                            error: false
                        };

                }).catch((error) => {

                    if (error.status === 400)
                        return {
                            error: true,
                            error_code: error.data.error_code
                        };

                    if (error.status === 500) {
                        return $q.reject(500);
                    }
                });
            },

            //#region resetPassword method

            resetPassword: function (newPassword, confirmPassword, email, token) {

                //console.log("[ajaxService]: resetPassword");

                var promise =
                    this.ajaxRequest({
                        method: "POST",
                        apiModule: "/user",
                        data: {
                            action: "resetPassword",
                            data: {
                                email: email,
                                token: token,
                                newPassword: newPassword,
                                confirmPassword: confirmPassword
                            }
                        }
                    });

                return promise
                .then(function (res) {
                    
                    if (res.status === 200)
                        return {
                            error: false
                        };

                }).catch((error) => {

                    if (error.status === 400) {
                        return {
                            error: true,
                            error_code: error.data.error_code
                        };
                    }

                    if (error.status === 500) {
                        return $q.reject(500);
                    }
                });
            },

            //#endregion

            //#region getCompanyInfo method

            getCompanyInfo: function (companyID) {

                //console.log("[ajaxService]: getCompanyInfo");
                var companyData;

                var promise =
                    this.ajaxRequest({
                        method: "POST",
                        apiModule: "/company",
                        data: {
                            action: "getCompany",
                            data: {
                                companyID: companyID
                            }
                        }
                    });

                return promise.then(function (res) {

                    var data = res.data["data"];

                    if (data && res.status === 200) {

                        var tmpCompanyData = {

                            companyID: data.companyID,
                            name: data.name,
                            informations: data.informations,
                            phoneNumber: data.phoneNumber,
                            address: data.address,
                            logo: ""
                        }

                        companyData = tmpCompanyData;
                    }
                }).then(function () {

                    var defaultLogo = "/images/logos/default-logo.png";
                    var imgSrc = "/images/logos/u" + userService.getPropValue("userID") + "_" + companyData.companyID + ".jpg";

                    ImageLoader.setDefaultImage(defaultLogo);

                    return ImageLoader.loadImage(imgSrc).then(function (loadedSrc) {
                        return loadedSrc + "?_ts=" + new Date().getTime();
                    });

                }).then(function (logo) {

                    companyData.logo = logo;
                    companyService.setCompanyData(companyData);

                    return {
                        companyData: companyData,
                        error: false
                    };

                }).catch((error) => {
                    if (error.status === Access.FORBIDDEN) {
                        return $q.reject(Access.FORBIDDEN);
                    }
                });
            },

            //#endregion

            //#region getProjects method

            getProjects: function () {

                //console.log("[ajaxService]: getProjects");

                var uapikey = localStorageService.cookie.get("optimoc_user_api_key");

                return this.ajaxRequest({
                    method: "POST",
                    apiModule: "/project",
                    data: {
                        action: "getProjects",
                        data: {
                            userAPIKey: uapikey
                        }
                    }
                }).then(function (res) {

                    var projectsData = res.data["data"];
                    var aggregateData = res.data["aggregate"];

                    if (projectsData && aggregateData && res.status === 200) {
                        projectService.setProjectsData(projectsData);

                        return {
                            projectsData: projectsData,
                            aggregateData: aggregateData
                        };
                    }

                }).catch((error) => {
                    if (error.status === Access.UNAUTHORIZED && error.data.error_code === "API_USER_FAILED_TO_AUTHENTICATE_ERROR") {
                        return $q.reject(Access.UNAUTHORIZED);
                    } else if (error.status === Access.FORBIDDEN && error.data.error_code === "API_USER_AUTH_TOKEN_NOT_SUPPLIED_ERROR") {
                        return $q.reject(Access.FORBIDDEN);
                    }
                });
            },

            //#endregion

            //#region getDisclaimerText method

            getDisclaimerText: function (projectID) {
                
                var uapikey = localStorageService.cookie.get("optimoc_user_api_key");

                return this.ajaxRequest({
                    method: "POST",
                    apiModule: "/project",
                    data: {
                        action: "getDisclaimerText",
                        data: {
                            userAPIKey: uapikey,
                            projectID: projectID
                        }
                    }
                }).then(function (res) {

                    var error = res.data["error"];

                    if (!error) {

                        return {
                            disclaimerText: res.data["disclaimerText"],
                            error: false
                        };
                    }

                }, function (error) {
                    return error;
                });
            },

            //#endregion

            //#region createProject method

            createProject: function (args) {

                //console.log("[ajaxService]: createProject");

                var data = { userAPIKey: localStorageService.cookie.get("optimoc_user_api_key") };

                if (typeof args !== "undefined") {

                    if (args.projectName) {
                        data.projectName = args.projectName;
                    }

                    if (args.projectDescription)
                        data.projectDescription = args.projectDescription;

                    if (args.projectType)
                        data.projectType = args.projectType;
                }

                return this.ajaxRequest({
                    method: "POST",
                    apiModule: "/project",
                    data: {
                        action: "createProject",
                        data: data
                    }
                }).then(function (res) {

                    let error = res.data["error"];
                    let project = res.data["project"];

                    if (!error) {

                        return {
                            project: project,
                            error: false
                        };
                    }
                    else {

                        return {
                            reason: res.data["reason"],
                            error: true
                        };
                    }

                }, function (error) {
                    return {
                        err: error
                    };
                });
            },

            //#endregion

            //#region archiveProject method

            archiveProject: function (projectID) {

                //console.log("[ajaxService]: archiveProject");

                var data = {
                    userAPIKey: localStorageService.cookie.get("optimoc_user_api_key"),
                    projectID: projectID
                };

                return this.ajaxRequest({
                    method: "POST",
                    apiModule: "/project",
                    data: {
                        action: "archiveProject",
                        data: data
                    }
                }).then(function (res) {

                    let error = res.data["error"];

                    if (!error) {

                        return {
                            error: false
                        };
                    }
                    else {

                        return {
                            error: true
                        };
                    }

                }, function (error) {
                    return {
                        err: error
                    };
                });
            },

            //#endregion

            //#region unArchiveProject method

            unArchiveProject: function (projectID) {

                //console.log("[ajaxService]: unArchiveProject");

                var data = {
                    userAPIKey: localStorageService.cookie.get("optimoc_user_api_key"),
                    projectID: projectID
                };

                return this.ajaxRequest({
                    method: "POST",
                    apiModule: "/project",
                    data: {
                        action: "unArchiveProject",
                        data: data
                    }
                }).then(function (res) {

                    let error = res.data["error"];

                    if (!error) {

                        return {
                            error: false,
                            metricsCaracteristics: res.data["metricsCaracteristics"],
                            metrics: res.data["metrics"]
                        };
                    }
                    else {

                        return {
                            error: true
                        };
                    }

                }, function (error) {
                    return {
                        err: error
                    };
                });
            },

            //#endregion

            //#region updateProject method

            updateProject: function (args) {

                //console.log("[ajaxService]: updateProject");

                var data = { userAPIKey: localStorageService.cookie.get("optimoc_user_api_key") };

            if (typeof args != undefined) {

                Object.keys(args).map(prop => {
                    data[prop] = args[prop];
                });
            }
            
            return this.ajaxRequest({
                method: "POST",
                apiModule: "/project",
                data: {
                    action: "updateProject",
                    data: data
                }
            }).then(function (res) {

                    var error = res.data["error"];

                if (!error) {

                    var project = projectService.getProjectsData().find(function (p) { return p.projectID == args.projectID; });
                    
                    Object.keys(args).map(prop => {
                        project[prop] = args[prop];
                    });
                }

                    return res;

                }, function (error) {
                    return {
                        err: error
                    };
                });
            },

            //#endregion

            //#region cloneProject method

            cloneProject: function (projectID, projectName, projectDescription) {

                //console.log("[ajaxService]: cloneProject");

                var data = {
                    userAPIKey: localStorageService.cookie.get("optimoc_user_api_key"),
                    projectID: projectID,
                    projectName: projectName,
                    projectDescription: projectDescription
                };

                return this.ajaxRequest({
                    method: "POST",
                    apiModule: "/project",
                    data: {
                        action: "cloneProject",
                        data: data
                    }
                }).then(function (res) {

                    return res;

                }, function (error) {
                    return {
                        err: error
                    };
                });
            },

            //#endregion

            //#region getCustomers method

            getCustomers: function (fields) {

                //console.log("[ajaxService]: getCustomers");

                var uapikey = localStorageService.cookie.get("optimoc_user_api_key");

                return this.ajaxRequest({
                    method: "POST",
                    apiModule: "/customer",
                    data: {
                        action: "getCustomers",
                        data: {
                            userAPIKey: uapikey,
                            fields: fields
                        }
                    }
                }).then(function (res) {

                    var customersData = res.data["data"];

                    if (customersData && res.status === 200) {

                        return {
                            customersData: customersData
                        };
                    }

                }).catch((error) => {
                    if (error.status === Access.UNAUTHORIZED && error.data.error_code === "API_USER_FAILED_TO_AUTHENTICATE_ERROR") {
                        return $q.reject(Access.UNAUTHORIZED);
                    } else if (error.status === Access.FORBIDDEN && error.data.error_code === "API_USER_AUTH_TOKEN_NOT_SUPPLIED_ERROR") {
                        return $q.reject(Access.FORBIDDEN);
                    }
                });;
            },

            //#endregion

            //#region getCustomer method

            getCustomer: function (customerID) {

                //console.log("[ajaxService]: getCustomer");

                var uapikey = localStorageService.cookie.get("optimoc_user_api_key");

                return this.ajaxRequest({
                    method: "POST",
                    apiModule: "/customer",
                    data: {
                        action: "getCustomer",
                        data: {
                            userAPIKey: uapikey,
                            customerID: customerID
                        }
                    }
                }).then(function (res) {

                    var error = res.data["error"];
                    var customerData = res.data["data"];

                    if (!error) {

                        return {
                            customerData: customerData,
                            error: false
                        };
                    }

                }, function (error) {
                    return error;
                });
            },

            //#endregion

            //#region updateCustomer method

            updateCustomer: function (customer) {

                //console.log("[ajaxService]: updateCustomer");

                return this.ajaxRequest({
                    method: "POST",
                    apiModule: "/customer",
                    data: {
                        action: "updateCustomer",
                        data: {
                            customer: customer
                        }
                    }
                }).then(function (res) {

                    var error = res.data["error"];

                    if (!error)
                        return error;

                }, function (error) {
                    return {
                        err: error
                    };
                });
            },

            //#endregion

            //#region createCustomer method

            createCustomer: function (customer) {

                //console.log("[ajaxService]: createCustomer");

                return this.ajaxRequest({
                    method: "POST",
                    apiModule: "/customer",
                    data: {
                        action: "createCustomer",
                        data: {
                            customer: customer
                        }
                    }
                }).then(function (res) {

                    var error = res.data["error"];
                    var customer = res.data["customer"];

                    if (!error)
                        return customer;

                }, function (error) {
                    return {
                        err: error
                    };
                });
            },

            //#endregion

            //#region deleteCustomer method

            deleteCustomer: function (customerID, userID) {

                //console.log("[ajaxService]: deleteCustomer");

                var uapikey = localStorageService.cookie.get("optimoc_user_api_key");

                return this.ajaxRequest({
                    method: "POST",
                    apiModule: "/customer",
                    data: {
                        action: "deleteCustomer",
                        data: {
                            customerID: customerID,
                            userAPIKey: uapikey
                        }
                    }
                }).then(function (res) {

                    var error = res.data["error"];

                    if (!error)
                        return error;

                }, function (error) {
                    return {
                        err: error
                    };
                });
            },

            //#endregion

            //#region setCustomerToProject method

            setCustomerToProject: function (projectID, customerID) {

                //console.log("[ajaxService]: setCustomerToProject");

                var uapikey = localStorageService.cookie.get("optimoc_user_api_key");

                return this.ajaxRequest({
                    method: "POST",
                    apiModule: "/project",
                    data: {
                        action: "setCustomerToProject",
                        data: {
                            userAPIKey: uapikey,
                            projectID: projectID,
                            customerID: customerID
                        }
                    }
                }).then(function (res) {

                    var error = res.data["error"];

                    if (!error) {

                        var project = projectService.getProjectsData().find(function (p) { return p.projectID == projectID; });
                        project.customerID = customerID;
                    }

                    return error;

                }, function (error) {
                    return {
                        err: error
                    };
                });
            },

            //#endregion

            //#region getRoles method

            getRoles: function () {

                //console.log("[ajaxService]: getRoles");

                return this.ajaxRequest({
                    method: "POST",
                    apiModule: "/role",
                    data: {
                        action: "getRoles"
                    }
                }).then(function (res) {

                    var error = res.data["error"];
                    var rolesData = res.data["data"];

                    if (!error) {
                        return rolesData;
                    }

                }, function (error) {
                    return {
                        err: error
                    };
                });
            },

            //#endregion

            //#region getTitles method

            getTitles: function () {

                //console.log("[ajaxService]: getTitles");

                return this.ajaxRequest({
                    method: "POST",
                    apiModule: "/reference",
                    data: {
                        action: "getTitles"
                    }
                }).then(function (res) {

                    var error = res.data["error"];
                    var titlesData = res.data["data"];

                    if (!error) {
                        return titlesData;
                    }

                }, function (error) {
                    return {
                        err: error
                    };
                });
            },

            //#endregion

            //#region getStatus method

            getStatus: function () {

                //console.log("[ajaxService]: getStatus");

                return this.ajaxRequest({
                    method: "POST",
                    apiModule: "/reference",
                    data: {
                        action: "getStatus"
                    }
                }).then(function (res) {

                    var error = res.data["error"];
                    var statusData = res.data["data"];

                    if (!error) {
                        return statusData;
                    }

                }, function (error) {
                    return {
                        err: error
                    };
                });
            },

            //#endregion

            //#region setMetricCaracteristics method

            setMetricCaracteristics: function (projectID, projectType, metricsCaracteristics) {

                //console.log("[ajaxService]: setMetricCaracteristics " + projectID);

                var uapikey = localStorageService.cookie.get("optimoc_user_api_key");
                var deferred = $q.defer();

                this.ajaxRequest({
                    method: "POST",
                    apiModule: "/metric",
                    data: {
                        action: "setMetricCaracteristics",
                        data: {
                            projectID: projectID,
                            userAPIKey: uapikey,
                            metricsCaracteristics: metricsCaracteristics,
                            projectType: projectType
                        }
                    }
                }).success(function (data) {

                    if (data.error) {
                        return deferred.reject(data.error);
                    } else {
                        return deferred.resolve(data);
                    }
                }).error(function () {
                    deferred.reject("500");
                });

                return deferred.promise;
            },

            //#endregion

            //#region getMetrics method

            getMetrics: function (projectID) {
                //console.log("[ajaxService]: getMetrics " + projectID);

                var uapikey = localStorageService.cookie.get("optimoc_user_api_key");
                var deferred = $q.defer();

                this.ajaxRequest({
                    method: "POST",
                    apiModule: "/metric",
                    data: {
                        action: "getMetrics",
                        data: {
                            projectID: projectID,
                            userAPIKey: uapikey
                        }
                    }
                }).success(function (res) {

                    var data = res.data;

                    if (res.error)
                        deferred.reject(data.error);
                    else
                        deferred.resolve(data);
                }).error(function () {
                    deferred.reject("500");
                });

                return deferred.promise;
            },

            //#endregion

            //#region getMetricsCaracteristics method

            getMetricsCaracteristics: function (projectID) {
                //console.log("[ajaxService]: getMetricsCaracteristics " + projectID);

                var uapikey = localStorageService.cookie.get("optimoc_user_api_key");
                var deferred = $q.defer();

                this.ajaxRequest({
                    method: "POST",
                    apiModule: "/metric",
                    data: {
                        action: "getMetricsCaracteristics",
                        data: {
                            projectID: projectID,
                            userAPIKey: uapikey
                        }
                    }
                }).success(function (res) {

                    var data = res.data;

                    if (res.error)
                        deferred.reject(data.error);
                    else
                        deferred.resolve(data);
                }).error(function () {
                    deferred.reject("500");
                });

                return deferred.promise;
            },

            //#endregion

            //#region getRateBases method

            getRateBases: function () {

                var uapikey = localStorageService.cookie.get("optimoc_user_api_key");
                var deferred = $q.defer();

                this.ajaxRequest({
                    method: "POST",
                    apiModule: "/rateBase",
                    data: {
                        action: "getRateBases",
                        data: {
                            userAPIKey: uapikey
                        }
                    }
                }).success(function (res) {

                    var data = res.data;

                    if (res.error)
                        deferred.reject(res.error);
                    else
                        deferred.resolve(data);

                }).error(function () {
                    deferred.reject("500");
                });

                return deferred.promise;
            },

            //#endregion

            //#region getRateBaseInCSV method

            getRateBaseInCSV: function (rateBaseName) {

                var uapikey = localStorageService.cookie.get("optimoc_user_api_key");
                var deferred = $q.defer();

                this.ajaxRequest({
                    method: "POST",
                    apiModule: "/rateBase",
                    data: {
                        action: "getRateBaseInCSV",
                        data: {
                            userAPIKey: uapikey,
                            rateBaseName: rateBaseName
                        }
                    }
                }).success(function (res) {

                    if (res.error)
                        deferred.reject(res.error);
                    else
                        deferred.resolve(res);

                }).error(function () {
                    deferred.reject("500");
                });

                return deferred.promise;
            },

            //#endregion

            //#region createLot method

            createLot: function (apimodule, args) {

                //console.log("[ajaxService]: createLot");

                var data = { userAPIKey: localStorageService.cookie.get("optimoc_user_api_key") };

                if (typeof args != undefined) {

                    if (args.projectID != undefined)
                        data.projectID = args.projectID;

                    if (args.rateBaseName != undefined)
                        data.rateBaseName = args.rateBaseName;
                }

                return this.ajaxRequest({
                    method: "POST",
                    apiModule: "/" + apimodule,
                    data: {
                        action: "createLot",
                        data: data
                    }
                }).then(function (res) {

                    return {
                        error: res.data["error"],
                        lotIndex: res.data["lotIndex"],
                        newLot: res.data["newLot"]
                    };

                }, function (error) {
                    return {
                        err: error
                    };
                });
            },

            //#endregion

            //#region deleteLot method

            deleteLot: function (apimodule, args) {

                //console.log("[ajaxService]: deleteLot");

                var data = { userAPIKey: localStorageService.cookie.get("optimoc_user_api_key") };

                if (typeof args != undefined) {

                    if (args.lotIndex != undefined)
                        data.lotIndex = args.lotIndex;

                    if (args.lotName != undefined)
                        data.lotName = args.lotName;

                    if (args.projectID != undefined)
                        data.projectID = args.projectID;

                    if (args.rateBaseName != undefined)
                        data.rateBaseName = args.rateBaseName;
                }

                return this.ajaxRequest({
                    method: "POST",
                    apiModule: "/" + apimodule,
                    data: {
                        action: "deleteLot",
                        data: data
                    }
                }).then(function (res) {

                    return {
                        error: res.data["error"]
                    };

                }, function (error) {
                    return {
                        err: error
                    };
                });
            },

            //#endregion

            //#region createDesignation method

            createDesignation: function (apimodule, args) {

                //console.log("[ajaxService]: createDesignation");

                var data = { userAPIKey: localStorageService.cookie.get("optimoc_user_api_key") };

                if (typeof args != undefined) {

                    if (args.lotIndex != undefined)
                        data.lotIndex = args.lotIndex;

                    if (args.lotName != undefined)
                        data.lotName = args.lotName;

                    if (args.projectID != undefined)
                        data.projectID = args.projectID;

                    if (args.rateBaseName != undefined)
                        data.rateBaseName = args.rateBaseName;
                }

                return this.ajaxRequest({
                    method: "POST",
                    apiModule: "/" + apimodule,
                    data: {
                        action: "createDesignation",
                        data: data
                    }
                }).then(function (res) {

                    return {
                        error: res.data["error"],
                        designationIndex: res.data["designationIndex"],
                        newDesignation: res.data["newDesignation"],
                        diversSeparatorIndex: res.data["diversSeparatorIndex"],
                        diversSeparator: res.data["diversSeparator"]
                    };

                }, function (error) {
                    return {
                        err: error
                    };
                });
            },

            //#endregion

            //#region deleteDesignation method

            deleteDesignation: function (apimodule, args) {

                //console.log("[ajaxService]: deleteDesignation");

                var data = { userAPIKey: localStorageService.cookie.get("optimoc_user_api_key") };

                if (typeof args != undefined) {

                    if (args.lotIndex != undefined)
                        data.lotIndex = args.lotIndex;

                    if (args.designationIndex != undefined)
                        data.designationIndex = args.designationIndex;

                    if (args.lotName != undefined)
                        data.lotName = args.lotName;

                    if (args.designationName != undefined)
                        data.designationName = args.designationName;

                    if (args.projectID != undefined)
                        data.projectID = args.projectID;

                    if (args.rateBaseName != undefined)
                        data.rateBaseName = args.rateBaseName;
                }

                return this.ajaxRequest({
                    method: "POST",
                    apiModule: "/" + apimodule,
                    data: {
                        action: "deleteDesignation",
                        data: data
                    }
                }).then(function (res) {

                    return {
                        error: res.data["error"],
                        diversSeparatorIndex: res.data["diversSeparatorIndex"]
                    };

                }, function (error) {
                    return {
                        err: error
                    };
                });
            },

            //#endregion
            // #region getAjaxURL method

            getAjaxURL: function () {

                $.ajaxSetup({
                    headers: {
                        'X-XSRF-TOKEN': localStorageService.cookie.get("XSRF-TOKEN")
                    },
                });

                return ajaxBaseURL;
            },

            //#endregion

        }

        return methods
    }]);

appServices.service("userService", ["localStorageService", function (localStorageService) {

    this.setUserData = function (userData) {
        localStorageService.set("userData", JSON.stringify(userData));
    };

    this.setPropValue = function (prop, value) {

        let userData = this.getUserData();
        userData[prop] = value;
        localStorageService.set("userData", JSON.stringify(userData));
    };

    this.getUserData = function () {
        return JSON.parse(localStorageService.get("userData"));
    };

    this.getPropValue = function (prop) {
        let userData = this.getUserData();
        return userData[prop];
    };

    this.isAuthenticated = () => { return localStorageService.cookie.get("optimoc_user_api_key"); };

}]);;

appServices.service("companyService", ["localStorageService", function (localStorageService) {

    this.setCompanyData = function (companyData) {
        return localStorageService.set("companyData", JSON.stringify(companyData));
    };

    this.setPropValue = function (prop, value) {

        let companyData = this.getCompanyData();

        if (prop.split(".").length === 2)
            companyData[prop.split(".")[0]][prop.split(".")[1]] = value;
        else
            companyData[prop] = value;

        return localStorageService.set("companyData", JSON.stringify(companyData));
    };

    this.getCompanyData = function () {
        return JSON.parse(localStorageService.get("companyData"));
    };

    this.getPropValue = function (prop) {
        let companyData = this.getCompanyData();
        return companyData[prop];
    };

}]);

appServices.service("projectService", function () {

    var methods = {

        setProjectsData: function (data) {
            this.projectsData = data;
        },

        getProjectsData: function () {
            return this.projectsData;
        }
    }

    return methods;
});

appServices.service("metricService", ["$location", "ajaxService", "localStorageService", "Utils", "projectService",
    function ($location, ajaxService, localStorageService, Utils, projectService) {

        return {

            // #region saveMetricsCaracteristics method

            saveMetricsCaracteristics: function (projectID, projectType, quantitative, caracteristics, fundation, crawlSpace, groundElevation, firstElevation, secondElevation, gable) {

                var metricsCaracteristics = {
                    quantitative: quantitative,
                    caracteristics: caracteristics,
                    fundation: fundation,
                    crawlSpace: crawlSpace,
                    groundElevation: groundElevation,
                    firstElevation: firstElevation,
                    secondElevation: secondElevation,
                    gable: gable
                };

                return ajaxService.setMetricCaracteristics(projectID, projectType, metricsCaracteristics)
                    .then(function (res) {

                        var error = res.error;
                        var metrics = res.metrics;
                        var metricsCaracteristics = res.metricsCaracteristics;

                        if (!error) {

                            let project = projectService.getProjectsData().find(function (p) { return p.projectID == projectID; });
                            project.metrics = metrics;

                            if (project.projectType == "NEUF") {
                                project.metricsCaracteristics = metricsCaracteristics;
                                project.hasMetricsCaracteristics = true;
                                $location.path("/app/project/" + projectID + "/metric");
                            }

                            return metrics;
                        }
                    }, function (error) {
                        console.error(error);
                    });
            },

            // #endregion

            // #region buildTableMetrics method

            buildTableMetrics: function (metricsData, hideSelected, hideSeparator) {

                //console.log("******** BUILD TABLE METRICS *********");

                var tableMetrics = {};
                var totalHT = 0;
                var totalTTC = 0;
                var lotTotalHT;
                var lotTotalTTC;

                Object.keys(metricsData).map(lotIndex => {

                    var lotName = metricsData[lotIndex].name;
                    var position = metricsData[lotIndex].position;
                    var selected = metricsData[lotIndex].selected;
                    var isCustom = metricsData[lotIndex].isCustom;
                    var lotData = {};
                    var lotDataTemp = metricsData[lotIndex].data;
                    let lotTotal1 = 0;
                    let lotTotal2 = 0;

                    if (hideSelected) {

                        if (!metricsData[lotIndex].selected) {

                            for (var elemIndex in lotDataTemp) {

                                if (lotDataTemp[elemIndex].qte != 0)
                                    lotData[elemIndex] = lotDataTemp[elemIndex];
                            }
                        }
                    }
                    else if (lotIndex == 8 && lotName.startsWith("Menuiseries exterieures")) {

                        for (var elemIndex in lotDataTemp) {

                            if (lotDataTemp[elemIndex].qte != 0)
                                lotData[elemIndex] = lotDataTemp[elemIndex];
                        }
                    }
                    else
                        lotData = lotDataTemp;

                    if (hideSeparator) {

                        if ((lotIndex == 10 && lotName.startsWith("Electricite")) || (lotIndex == 11 && lotName.startsWith("Plomberie"))) {

                            for (var elemIndex in lotDataTemp) {

                                if (lotDataTemp[elemIndex].isSeparator)
                                    delete lotData[elemIndex];
                            }
                        }
                    }

                    for (var elemIndex in lotData) {

                        if (lotData[elemIndex] != null && typeof lotData[elemIndex] == "object" && !lotData[elemIndex].isSeparator) {

                            lotTotalHT = lotData[elemIndex].totalHT;
                            lotTotal1 += lotTotalHT;

                            lotTotalTTC = lotData[elemIndex].totalTTC;
                            lotTotal2 += lotTotalTTC;

                            if (!metricsData[lotIndex].selected) {
                                totalHT += lotTotalHT;
                                totalTTC += lotTotalTTC;
                            }
                        }
                    }

                    if (hideSelected) {
                        if (lotTotal1 != 0)
                            tableMetrics[lotIndex] = {
                                lotId: lotIndex,
                                lotName: lotName,
                                data: lotData,
                                lotTotalHT: lotTotal1,
                                lotTotalTTC: lotTotal2
                            };
                    }
                    else {
                        tableMetrics[lotIndex] = {
                            lotId: lotIndex,
                            position: position,
                            selected: selected,
                            isCustom: (isCustom === undefined) ? false : isCustom,
                            lotName: lotName,
                            data: lotData,
                            lotTotalHT: lotTotal1,
                            lotTotalTTC: lotTotal2
                        };
                    }
                });
                
                return { tableMetrics: tableMetrics, totalHT: totalHT, totalTTC: totalTTC };
            },

            // #endregion

            // #region setMetricsCarpentyData method

            setMetricsCarpentyData: function (metricsCarpentyData) {
                return localStorageService.set("metricsCarpentyData", JSON.stringify(metricsCarpentyData));
            },

            // #endregion

            // #region getMetricsCarpentyData method

            getMetricsCarpentyData: function () {
                return JSON.parse(localStorageService.get("metricsCarpentyData"));
            }

            // #endregion
        };
    }]);

appServices.service("browserService", ["$window", function ($window) {

    return {

        // #region openNewTab method

        openNewTab: function (url) {

            let newTabWindow = (url) ? $window.open(url) : $window.open(url);
            return newTabWindow;
        },

        // #endregion

        // #region updateTabLocation method

        updateTabLocation: function (tabLocation, tab) {

            if (!tabLocation)
                tab.close();

            tab.location.href = tabLocation;
        }

        // #endregion
    };
}]);

/*
 * Filters
 * 
 */

var appFilters = angular.module("optimoc.filters", []);

appFilters.filter('commaToDot', function () {
    return function (value) {
        //return value ? parseFloat(value).toFixed(2).toString().replace(",", ".") : "0.00";
        return value ? value.toString().replace(",", ".") : "0.00";
    };
});

appFilters.filter('tsToDate', function () {
    return function (value) {
        return new Date(value);
    };
});

appFilters.filter("percentage", ["$filter", function ($filter) {
    return function (input, decimals) {
        return $filter('number')(input * 100, decimals) + '%';
    };
}]);

/*
 * Controllers
 * 
 */

var appControllers = angular.module("optimoc.controllers", ["ngResource", "datatables"]);

appControllers.config(["$stateProvider", "$urlRouterProvider", function ($stateProvider, $urlRouterProvider) {

    $stateProvider.state("app.dashboard", {
        url: "/dashboard",
        views: {
            header: {
                templateUrl: "views/partials/topmenu.html",
            },
            sidebar: {
                templateUrl: "views/partials/sidebar.html"
            },
            content: {
                templateUrl: "views/app/dashboard.html",
                controller: "dashboardCtrl"
            },
            footer: {
                templateUrl: "views/partials/footer.html"
            }
        }
    }).state("app.project", {
        url: "/project",
        views: {
            header: {
                templateUrl: "views/partials/topmenu.html"
            },
            sidebar: {
                templateUrl: "views/partials/sidebar.html"
            },
            content: {
                templateUrl: "views/app/project.html",
                controller: "projectCtrl"
            },
            footer: {
                templateUrl: "views/partials/footer.html"
            }
        }
    }).state("app.project-detail", {
        url: "/project/:detail",
        views: {
            header: {
                templateUrl: "views/partials/topmenu.html"
            },
            sidebar: {
                templateUrl: "views/partials/sidebar.html"
            },
            content: {
                templateUrl: "views/app/project-detail.html",
                controller: "projectDetailCtrl"
            },
            footer: {
                templateUrl: "views/partials/footer.html"
            }
        }
    }).state("app.project-detail-metric", {
        url: "/project/:detail/metric",
        views: {
            header: {
                templateUrl: "views/partials/topmenu.html"
            },
            sidebar: {
                templateUrl: "views/partials/sidebar.html"
            },
            content: {
                templateUrl: "views/app/project-detail-metric.html",
                controller: "projectMetricCtrl"
            },
            footer: {
                templateUrl: "views/partials/footer.html"
            }
        }
    }).state("app.project-detail-metric-form", {
        url: "/project/:detail/metric-form",
        views: {
            header: {
                templateUrl: "views/partials/topmenu.html"
            },
            sidebar: {
                templateUrl: "views/partials/sidebar.html"
            },
            content: {
                templateUrl: "views/app/project-detail-metric-form.html",
                controller: "projectMetricFormCtrl"
            },
            footer: {
                templateUrl: "views/partials/footer.html"
            }
        }
    }).state("app.project-detail-quote", {
        url: "/project/:detail/quote",
        views: {
            header: {
                templateUrl: "views/partials/topmenu.html"
            },
            sidebar: {
                templateUrl: "views/partials/sidebar.html"
            },
            content: {
                templateUrl: "views/app/project-detail-quote.html",
                controller: "projectQuoteCtrl"
            },
            footer: {
                templateUrl: "views/partials/footer.html"
            }
        }
    }).state("app.project-detail-edit", {
        url: "/project/:detail/edit",
        views: {
            header: {
                templateUrl: "views/partials/topmenu.html"
            },
            sidebar: {
                templateUrl: "views/partials/sidebar.html"
            },
            content: {
                templateUrl: "views/app/project-detail-edit.html",
                controller: "projectDetailCtrl"
            },
            footer: {
                templateUrl: "views/partials/footer.html"
            }
        }
    }).state("app.agenda", {
        url: "/agenda",
        views: {
            header: {
                templateUrl: "views/partials/topmenu.html"
            },
            sidebar: {
                templateUrl: "views/partials/sidebar.html"
            },
            content: {
                templateUrl: "views/app/agenda.html",
                controller: "agendaCtrl"
            },
            footer: {
                templateUrl: "views/partials/footer.html"
            }
        }
    }).state("app.tiers", {
        url: "/tiers",
        views: {
            header: {
                templateUrl: "views/partials/topmenu.html"
            },
            sidebar: {
                templateUrl: "views/partials/sidebar.html"
            },
            content: {
                templateUrl: "views/tiers/tiers.html",
                controller: "tiersCtrl"
            },
            footer: {
                templateUrl: "views/partials/footer.html"
            }
        }
    }).state("app.cloud", {
        url: "/cloud",
        views: {
            header: {
                templateUrl: "views/partials/topmenu.html"
            },
            sidebar: {
                templateUrl: "views/partials/sidebar.html"
            },
            content: {
                templateUrl: "views/app/cloud.html",
                controller: "cloudCtrl"
            },
            footer: {
                templateUrl: "views/partials/footer.html"
            }
        }
    }).state("app.rate-base", {
        url: "/rate-base",
        views: {
            header: {
                templateUrl: "views/partials/topmenu.html"
            },
            sidebar: {
                templateUrl: "views/partials/sidebar.html"
            },
            content: {
                templateUrl: "views/app/rate-base.html",
                controller: "rateBaseCtrl"
                //controllerAs: "rateBaseCtrlInst"
            },
            footer: {
                templateUrl: "views/partials/footer.html"
            }
        }/*,
        resolve: {

            rateBases: ["ajaxService", function (ajaxService) {
                
                return ajaxService.getRateBases()
                    .then(function (data) {
                        return data;
                    });
            }]
        }*/
    }).state("app.account", {
        url: "/account",
        views: {
            header: {
                templateUrl: "views/partials/topmenu.html"
            },
            sidebar: {
                templateUrl: "views/partials/sidebar.html"
            },
            content: {
                templateUrl: "views/app/account.html",
                controller: "accountCtrl"
            },
            footer: {
                templateUrl: "views/partials/footer.html"
            }
        }
    }).state("app.project-detail-metric-carpenty", {
        url: "/project/:detail/metric-carpenty",
        views: {
            header: {
                templateUrl: "views/partials/topmenu.html"
            },
            sidebar: {
                templateUrl: "views/partials/sidebar.html"
            },
            content: {
                templateUrl: "views/app/project-detail-metric-carpenty.html",
                controller: "projectMetricCarpentyCtrl"
            },
            footer: {
                templateUrl: "views/partials/footer.html"
            }
        }
    }).state("app.third", {
        url: "/third",
        views: {
            header: {
                templateUrl: "views/partials/topmenu.html"
            },
            sidebar: {
                templateUrl: "views/partials/sidebar.html"
            },
            content: {
                templateUrl: "views/app/third.html",
                controller: "thirdCtrl"
            },
            footer: {
                templateUrl: "views/partials/footer.html"
            }
        },
        resolve: {

            tableTitles: ["ajaxService", function (ajaxService) {

                return ajaxService.getTitles()
                    .then(function (titlesData) {

                        var tableTitles = [];

                        for (var elemIndex in titlesData) {

                            tableTitles.push({

                                titleID: titlesData[elemIndex].titleID,
                                titleName: titlesData[elemIndex].titleName
                            });
                        }

                        return tableTitles;
                    });
            }],

            tableStatus: ["ajaxService", function (ajaxService) {

                return ajaxService.getStatus()
                    .then(function (statusData) {

                        var tableStatus = [];

                        for (var elemIndex in statusData) {

                            tableStatus.push({

                                statusID: statusData[elemIndex].statusID,
                                statusName: statusData[elemIndex].statusName
                            });
                        }

                        return tableStatus;
                    });
            }]
        }
    }).state("app.admin", {
        url: "/admin",
        views: {
            header: {
                templateUrl: "views/partials/topmenu.html"
            },
            sidebar: {
                templateUrl: "views/partials/sidebar.html"
            },
            content: {
                templateUrl: "views/app/admin.html",
                controller: "adminCtrl"
            },
            footer: {
                templateUrl: "views/partials/footer.html"
            }
        }
    }).state("app.help", {
        url: "/help",
        views: {
            header: {
                templateUrl: "views/partials/topmenu.html"
            },
            sidebar: {
                templateUrl: "views/partials/sidebar.html"
            },
            content: {
                templateUrl: "views/app/help.html",
                controller: "helpCtrl"
            },
            footer: {
                templateUrl: "views/partials/footer.html"
            }
        }
    });

    $urlRouterProvider.otherwise("/app/dashboard");
}]);

"use strict";

appControllers.controller("debugCtrl", ["$scope", "$q", "$timeout", "userService", "ajaxService", "$http", function ($scope, $q, $timeout, userService, ajaxService, $http) {
    var getHelloWorld = function () {
        return ajaxService.ajaxRequest({
            method: "GET",
            url: "http://www.optimoc.back"
        });
    };
    var cleanDatabase = function () {
        var rq = ajaxService.ajaxRequest({
            method: "POST",
            apiModule: "/system",
            data: {
                action: "cleanDatabase",
                data: {}
            }
        });
        return rq;
    };
    var backupDatabase = function () {
        var rq = ajaxService.ajaxRequest({
            method: "POST",
            apiModule: "/system",
            data: {
                action: "backupDatabase",
                data: {}
            }
        });
        return rq;
    };
    var displayDatabase = function () {
        var rq = ajaxService.ajaxRequest({
            method: "POST",
            apiModule: "/system",
            data: {
                action: "displayDatabase",
                data: {}
            }
        });
        return rq;
    };
    var createDemoAccount = function () {
        var rq = ajaxService.ajaxRequest({
            method: "POST",
            apiModule: "/user",
            data: {
                action: "createUser",
                data: {
                    firstname: "John",
                    lastname: "Doe",
                    password: "demo",
                    email: "demo@optimoc.fr"
                }
            }
        });
        return rq;
    };
    var createAdminAccount = function () {
        var rq = ajaxService.ajaxRequest({
            method: "POST",
            apiModule: "/user",
            data: {
                action: "createUser",
                data: {
                    firstname: "Ad",
                    lastname: "Ministrator",
                    password: "admin",
                    email: "admin@optimoc.fr"
                }
            }
        });
        return rq;
    };
    var getDemoAccount = function () {
        var rq = ajaxService.ajaxRequest({
            method: "POST",
            apiModule: "/user",
            data: {
                action: "getUser",
                data: {
                    email: "demo@optimoc.fr",
                    password: "demo"
                }
            }
        });
        return rq;
    };
    var editDemoAccount = function () {
        var rq = ajaxService.ajaxRequest({
            method: "POST",
            apiModule: "/user",
            data: {
                action: "editUser",
                data: {
                    firstname: "Agent",
                    lastname: "Smith",
                    userID: 1
                }
            }
        });
        return rq;
    };
    var deleteDemoAccount = function () {
        var rq = ajaxService.ajaxRequest({
            method: "POST",
            apiModule: "/user",
            data: {
                action: "deleteUser",
                data: {
                    userID: 1
                }
            }
        });
        return rq;
    };
    var deleteAccounts = function () {
        var rq = ajaxService.ajaxRequest({
            method: "POST",
            apiModule: "/user",
            data: {
                action: "deleteUser",
                data: {
                    userID: null
                }
            }
        });
        return rq;
    };
    var createProject = function () {
        var rq = ajaxService.ajaxRequest({
            method: "POST",
            apiModule: "/project",
            data: {
                action: "createProject",
                data: {
                    projectName: "Villa des acacias",
                    projectDescription: "Ma description",
                    userAPIKey: "e55a4956858156d17bd1deb8fe00960f41f05ef047c93a81ca8e975d4a0d1e28"
                }
            }
        });
        return rq;
    };
    var editProject = function () {
        var rq = ajaxService.ajaxRequest({
            method: "POST",
            apiModule: "/project",
            data: {
                action: "editProject",
                data: {
                    projectDescription: "Ma description amélioré",
                    projectID: 1,
                    userAPIKey: "e55a4956858156d17bd1deb8fe00960f41f05ef047c93a81ca8e975d4a0d1e28"
                }
            }
        });
        return rq;
    };
    var deleteProject = function () {
        var rq = ajaxService.ajaxRequest({
            method: "POST",
            apiModule: "/project",
            data: {
                action: "deleteProject",
                data: {
                    userAPIKey: "e55a4956858156d17bd1deb8fe00960f41f05ef047c93a81ca8e975d4a0d1e28",
                    projectID: 1
                }
            }
        });
        return rq;
    };
    var deleteProject2 = function () {
        var rq = ajaxService.ajaxRequest({
            method: "POST",
            apiModule: "/project",
            data: {
                action: "deleteProject",
                data: {
                    userAPIKey: "e55a4956858156d17bd1deb8fe00960f41f05ef047c93a81ca8e975d4a0d1e28",
                    projectID: 2
                }
            }
        });
        return rq;
    };
    var deleteProjects = function () {
        var rq = ajaxService.ajaxRequest({
            method: "POST",
            apiModule: "/project",
            data: {
                action: "deleteProject",
                data: {
                    userAPIKey: "e55a4956858156d17bd1deb8fe00960f41f05ef047c93a81ca8e975d4a0d1e28",
                    projectID: null
                }
            }
        });
        return rq;
    };
    var getProject = function () {
        var rq = ajaxService.ajaxRequest({
            method: "POST",
            apiModule: "/project",
            data: {
                action: "getProject",
                data: {
                    userAPIKey: "e55a4956858156d17bd1deb8fe00960f41f05ef047c93a81ca8e975d4a0d1e28",
                    projectID: 1
                }
            }
        });
        return rq;
    };
    var getProjects = function () {
        var rq = ajaxService.ajaxRequest({
            method: "POST",
            apiModule: "/project",
            data: {
                action: "getProject",
                data: {
                    userAPIKey: "e55a4956858156d17bd1deb8fe00960f41f05ef047c93a81ca8e975d4a0d1e28",
                    projectID: null
                }
            }
        });
        return rq;
    };
    var informationUser = function () {
        var rq = ajaxService.ajaxRequest({
            method: "POST",
            apiModule: "/user",
            data: {
                action: "userInformation",
                data: {
                    userAPIKey: "demo@optimoc.fr"
                }
            }
        });
    };
    $scope.promises = [["GET Hello World", getHelloWorld], ["POST Create User", createDemoAccount], ["POST Login User", getDemoAccount], ["POST Information User", informationUser]];
    console.log($scope.promises);
    $scope.promisesNb = $scope.promises.length;
    function chainedPromise(promises, limit, callback) {
        var i = 0, next = function () {
            var promise = promises[i];
            var promiseName = promise[0];
            $scope.appendToLog("Test N°" + i + " - " + promiseName + " - Result :");
            var runPromise = promise[1]();
            runPromise.then(function (data) {
                if (callback) {
                    callback(false, data, promiseName);
                }
            }, function (data) {
                if (callback) {
                    callback(true, data, promiseName);
                }
            }).finally(function () {
                i++;
                if (i < limit) next();
            });
        };
        next();
    }
    $scope.runAllTest = function () {
        chainedPromise($scope.promises, $scope.promisesNb, function (error, data, promiseName) {
            if (error) {
                $scope.appendToLog(" Failed\n");
                $scope.setTotalBar("fail");
            } else {
                $scope.appendToLog(" Success\n");
                $scope.setTotalBar("success");
            }
        });
    };
    var defaultCallbackForUnitTest = function (error, data, promiseName) {
        if (error) $scope.appendToLog("Failed\n"); else {
            $scope.appendToLog("Success\n");
            $scope.request.name = promiseName;
            $scope.request.jsonData = data;
        }
    };
    $scope.runCleanDatabase = function () {
        chainedPromise([["POST Clean Database", cleanDatabase]], 1, defaultCallbackForUnitTest);
    };
    $scope.runBackupDatabase = function () {
        chainedPromise([["POST Backup Database", backupDatabase]], 1, defaultCallbackForUnitTest);
    };
    $scope.runDisplayDatabase = function () {
        chainedPromise([["POST Display Database", displayDatabase]], 1, defaultCallbackForUnitTest);
    };
    $scope.runCreateDemoAccount = function () {
        chainedPromise([["POST Create Demo Account", createDemoAccount]], 1, defaultCallbackForUnitTest);
    };
    $scope.runCreateAdminAccount = function () {
        chainedPromise([["POST Create Demo Account", createAdminAccount]], 1, defaultCallbackForUnitTest);
    };
    $scope.runGetDemoAccount = function () {
        chainedPromise([["POST Get Demo Account", getDemoAccount]], 1, defaultCallbackForUnitTest);
    };
    $scope.runEditDemoAccount = function () {
        chainedPromise([["POST Edit Demo Account", editDemoAccount]], 1, defaultCallbackForUnitTest);
    };
    $scope.runDeleteDemoAccount = function () {
        chainedPromise([["POST Delete Demo Account", deleteDemoAccount]], 1, defaultCallbackForUnitTest);
    };
    $scope.runDeleteAccounts = function () {
        chainedPromise([["POST Delete Account", deleteAccounts]], 1, defaultCallbackForUnitTest);
    };
    $scope.runCreateProject = function () {
        chainedPromise([["POST Create Project", createProject]], 1, defaultCallbackForUnitTest);
    };
    $scope.runGetProject = function () {
        chainedPromise([["POST Get Project", getProject]], 1, defaultCallbackForUnitTest);
    };
    $scope.runGetProjects = function () {
        chainedPromise([["POST Get Projects", getProjects]], 1, defaultCallbackForUnitTest);
    };
    $scope.runEditProject = function () {
        chainedPromise([["POST Edit Project 1", editProject]], 1, defaultCallbackForUnitTest);
    };
    $scope.runDeleteProject = function () {
        chainedPromise([["POST Delete Project 1", deleteProject]], 1, defaultCallbackForUnitTest);
    };
    $scope.runDeleteProject2 = function () {
        chainedPromise([["POST Delete Project 2", deleteProject2]], 1, defaultCallbackForUnitTest);
    };
    $scope.runDeleteProjects = function () {
        chainedPromise([["POST Delete Projects", deleteProjects]], 1, defaultCallbackForUnitTest);
    };
    $scope.appendToLog = function (txt) {
        var log = $("#testLog");
        log.val(log.val() + txt);
    };
    $scope.setTotalBar = function (state) {
        var percent = parseFloat(100 / $scope.promisesNb);
        var successHTML = '<div class="progress-bar progress-bar-success" style="width: ' + percent + '%"><span class="sr-only">35% Complete (success)</span></div>';
        var warningHTML = '<div class="progress-bar progress-bar-warning" style="width: ' + percent + '%"><span class="sr-only">35% Complete (success)</span></div>';
        var failHTML = '<div class="progress-bar progress-bar-danger" style="width: ' + percent + '%"><span class="sr-only">35% Complete (success)</span></div>';
        var HTML;
        switch (state) {
            case "success":
                HTML = successHTML;
                break;

            case "warning":
                HTML = warningHTML;
                break;

            case "fail":
                HTML = failHTML;
                break;
        }
        $("#totalProgression").append(HTML);
    };
    $scope.request = {
        name: "",
        jsonData: {},
        data: {}
    };
    var getUser = function () {
        return $http.get("http://www.optimoc.back/");
    };
    function async1(value) {
        var deferred = $q.defer();
        var asyncCalculation = value * 2;
        deferred.resolve(asyncCalculation);
        return deferred.promise;
    }
    function async2(value) {
        var deferred = $q.defer();
        $timeout(function () {
            deferred.reject("rejected for demonstration!");
        }, 500);
        return deferred.promise;
    }
    var handleSuccessPromise = function (d) {
        console.log(d);
        $scope.appendToLog("Success");
        $scope.setTotalBar("success \n");
    };
    var handleErrorPromise = function (d) {
        console.log(d);
        $scope.appendToLog("Fail");
        $scope.setTotalBar("fail \n");
    };
    var getPromise = function (success, lag) {
        var d = $q.defer();
        $timeout(function () {
            if (success) d.resolve("sucess"); else d.reject("fail");
        }, lag);
        return d.promise;
    };
    var promiseGet = function () {
        return $timeout(function () {
            return $http.get("http://www.optimoc.back/");
        }, 1e3);
    };
    var falsePromise = function () {
        var d = $q.defer();
        d.reject("fail");
        return d.promise;
    };
}]);

"use strict";

appControllers.controller("agendaCtrl", ["$scope", function ($scope) {
    var drag = function () {
        $(".calendar-event").each(function () {
            $(this).data("event", {
                title: $.trim($(this).text()),
                stick: true
            });
            $(this).draggable({
                zIndex: 1111999,
                revert: true,
                revertDuration: 0
            });
        });
    };
    var removeEvent = function () {
        $(".remove-calendar-event").click(function () {
            $(this).closest(".calendar-event").fadeOut();
            return false;
        });
    };
    $(".add-event").keypress(function (e) {
        if (e.which == 13 && !$(this).val().length == 0) {
            $('<div class="calendar-event"><p>' + $(this).val() + '</p><a href="javascript:void(0);" class="remove-calendar-event"><i class="fa fa-remove"></i></a></div>').insertBefore(".add-event");
            $(this).val("");
        } else if (e.which == 13) {
            alert("Please enter event name");
        }
        drag();
        removeEvent();
    });
    drag();
    removeEvent();
    var date = new Date();
    var day = date.getDate();
    var month = date.getMonth();
    var year = date.getFullYear();
    $("#calendar").fullCalendar({
        lang: "fr",
        header: {
            left: "prev,next today",
            center: "title",
            right: "month,agendaWeek,agendaDay"
        },
        editable: true,
        droppable: true,
        eventLimit: true,
        events: [{
            title: "Création du calendrier",
            start: new Date(year, month, day - 8)
        }]
    });
}]);

"use strict";

appControllers.controller("cloudCtrl", ["$scope", function ($scope) { }]);

"use strict";

appControllers.controller("tiersCtrl", ["$scope", function ($scope) {
    $scope.tabs = [{
        title: "Messagerie",
        url: "views/tiers/tiers-messagerie.html",
        "class": "btn btn-default btn-addon",
        icon: "glyphicon glyphicon-envelope",
        badge: "4"
    }, {
        title: "Prospect",
        url: "views/tiers/tiers-prospects.html",
        "class": "btn btn-default btn-addon",
        icon: "fa fa-users"
    }, {
        title: "Clients",
        url: "views/tiers/tiers-clients.html",
        "class": "btn btn-default btn-addon",
        icon: "fa fa-building"
    }, {
        title: "Fournisseurs",
        url: "views/tiers/tiers-fournisseurs.html",
        "class": "btn btn-default btn-addon",
        icon: "fa fa-archive"
    }, {
        title: "Créer une fiche tiers",
        url: "views/tiers/tiers-nouvelle-fiche.html",
        "class": "btn btn-info btn-addon",
        icon: "fa fa-plus"
    }];
    $scope.currentTabTitle = "Messagerie";
    $scope.currentTab = "views/tiers/tiers-messagerie.html";
    $scope.onClickTab = function (tab) {
        $scope.currentTabTitle = tab.title;
        $scope.currentTab = tab.url;
    };
    $scope.isActiveTab = function (tabUrl) {
        return tabUrl == $scope.currentTab;
    };
}]);

/*
 * Directives
 * 
 */

var appDirectives = angular.module("optimoc.directives", []);

appDirectives.directive("clickLink", ["$location", function ($location) {
    return {
        link: function (scope, element, attrs) {
            element.on("click", function () {
                scope.$apply(function () {
                    $location.path(attrs.clickLink);
                });
            });
        }
    };
}]);

appDirectives.directive("decimalOnly", function () {
    return {
        require: "ngModel",
        restrict: "A",
        link: function (scope, element, attr, ctrl) {

            element.bind("keypress", function (e) {

                var newVal = $(this).val() /*+ (e.charCode !== 0 ? String.fromCharCode(e.charCode) : '')*/;

                if ($(this).val().search(/(.*)\.[0-9][0-9]/) === 0 /*&& newVal.length > $(this).val().length*/)
                    e.preventDefault();
            });

            //function inputValue(val) {
            //    if (val) {

            //        var digits = val.toLocaleString().replace(/[^0-9,]/g, "");

            //        if (digits !== val.toLocaleString()) {
            //            ctrl.$setViewValue(digits);
            //            ctrl.$render();
            //        }
            //        return parseFloat(digits);
            //    }
            //    return undefined;
            //}

            //ctrl.$parsers.push(inputValue);
        }
    }
});

appDirectives.directive('integerOnly', function () {
    return {
        require: 'ngModel',
        restrict: 'A',
        link: function (scope, element, attr, ctrl) {

            function inputValue(val) {

                if (val) {
                    var digits = val.toLocaleString().replace(/[^0-9]/g, "");

                    if (digits !== val) {
                        ctrl.$setViewValue(digits);
                        ctrl.$render();
                    }
                    return parseInt(digits);
                }
                return undefined;
            }
            ctrl.$parsers.push(inputValue);
        }
    }
});

appDirectives.directive("emptyInput", function ($parse) {
    return {
        require: "?ngModel",
        link: function (scope, element, attrs, ngModel) {
            var ngModelGet = $parse(attrs.ngModel);
            scope.$watch(attrs.ngModel, function () {

                if (ngModelGet(scope) == undefined && angular.isObject(ngModel) && attrs.type) {

                    var model = $parse(attrs.ngModel);

                    if (attrs.type === "number")
                        model.assign(scope, null);
                    else if (attrs.type === "text")
                        model.assign(scope, "");
                }
            });
        }
    }
});

appDirectives.directive("navigatable", function () {

    return function (scope, element, attr) {

        element.on("keypress", 'input[type="number"]', handleNavigation);

        function handleNavigation(e) {

            var key = e.keyCode ? e.keyCode : e.which;

            if (key === 13) {
                var focusedElement = $(e.target);
                var table = focusedElement.closest("tbody");

                var cellIndex = focusedElement.parent()[0].cellIndex;
                var nextElement = focusedElement.parent().parent().next();

                if (nextElement.find("input").length > 0) {
                    nextElement.find("input")[cellIndex - 1].focus();
                } else {
                    nextElement = table.find("tr").first();

                    if (nextElement.find("input").length > 0) {
                        nextElement.find("input")[cellIndex].focus();
                    }
                }
            }
        }
    };
});

appDirectives.directive("uiSelectRequired", function () {
    return {
        require: "ngModel",
        link: function (scope, elm, attrs, ctrl) {
            ctrl.$validators.uiSelectRequired = function (modelValue, viewValue) {
                return modelValue && modelValue.length;
            };
        }
    };
});