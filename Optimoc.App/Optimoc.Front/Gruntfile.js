module.exports = function (grunt) {
    
    grunt.initConfig({

        distFolder: "dist",

        pkg: grunt.file.readJSON("package.json"),

        jshint: {
            files: ['Gruntfile.js', 'src/**/*.js'],
            options: {
              globals: {
                jQuery: true
              }
            }
        },
        babel: {
            options: {
                "sourceMap": true,
                "presets": [
                    ["env", {
                        "targets": {
                            "browsers": ["last 2 versions", "safari >= 7"]
                        }
                    }]
                ],
                "plugins": [
                //   ["transform-async-to-module-method", {
                //        "module": "bluebird",
                //        "method": "coroutine",
                    "transform-async-to-generator"
                ]
            },
            dist: {
                files: [
                    {
                        expand: true,
                        cwd: "src/controllers",
                        src: ["**/*Controller.js"],
                        dest: "<%= distFolder %>/js-compiled/controllers",
                        "ext": "-compiled.js"
                    },
                    {
                        expand: true,
                        cwd: "src/js",
                        src: ["**/app.js"],
                        dest: "<%= distFolder %>/js-compiled/",
                        ext: "-compiled.js"
                    },
                    {
                        expand: true,
                        cwd: "src/services",
                        src: ["**/dtTranslationService.js"],
                        dest: "<%= distFolder %>/js-compiled/services/",
                        ext: "-compiled.js"
                    }
                ]
            }
        },
        uglify: {
            target: {
                options: {
                    //sourceMap: true,
                    //sourceMapName: "<%= distFolder %>/build/sourceMap.map",
                    mangle: false
                },
                files: [
                    {
                        src: "<%= distFolder %>/js-compiled/controllers/*-compiled.js",
                        dest: "<%= distFolder %>/build/controllers/controllers.min.js"
                    },
                    {
                        src: "<%= distFolder %>/js-compiled/app-compiled.js",
                        dest: "<%= distFolder %>/build/js/app.min.js"
                    },
                    {
                        src: "<%= distFolder %>/js-compiled/services/dtTranslationService-compiled.js",
                        dest: "<%= distFolder %>/build/services/dtTranslationService.min.js"
                    }
                ]
            }
        },
        copy: {
            main: {
                files: [
                    {
                        expand: true,
                        cwd: "src",
                        src: ["./js/**", "!./js/app.js", "./css/**", "./images/**", "./views/**", "index.html"],
                        dest: "<%= distFolder %>/build/"
                    }
                ],
            }
        },
        watch: {
            options: {
                livereload: {
                    host: "localhost",
                    port: 9000,
                    key: grunt.file.read("../Optimoc.Back/src/cert/optimoc.fr.key.pem"),
                    cert: grunt.file.read("../Optimoc.Back/src/cert/optimoc.fr.crt.pem"),
                }
            },
            app: {
                files: ["src/js/app.js"],
                tasks: ["babel", "uglify"]
            },
            controllers: {
                files: ["src/controllers/*Controller.js"],
                tasks: ["babel", "uglify", "copy"]
            },
            services: {
                files: ["src/services/dtTranslationService.js"],
                tasks: ["babel", "uglify", "copy"]
            },
            js: {
                files: ["src/js/**", "!./js/app.js"],
                tasks: ["copy"]
            },
            css: {
                files: ["src/css/**"],
                tasks: ["copy"]
            },
            html: {
                files: ["src/views/**", "src/index.html"],
                tasks: ["copy"]
            }
            /*jshint: {
                files: ["<%= jshint.files %>"],
                tasks: ["jshint"]
            }*/
        }
    });

    //
    // Load the tasks
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks("grunt-babel");
    grunt.loadNpmTasks("grunt-contrib-uglify");
    grunt.loadNpmTasks("grunt-contrib-copy");
    grunt.loadNpmTasks('grunt-contrib-watch');

    //
    // Register custom task alias
    grunt.registerTask("default", ["jshint", "babel", "uglify", "copy"]);
    grunt.registerTask("build", ["babel", "uglify", "copy"]);
};