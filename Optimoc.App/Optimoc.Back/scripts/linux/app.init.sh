#!/bin/bash

set -e

node_env=${NODE_ENV:-production}
cd ../..
rm -rf node_modules/

if [[ $node_env == "production" ]]; then
    npm install -unsafe-perm --production
else
    npm install
fi

# copy file for log4js
cp ./src/db/mongodbAppender.js ./node_modules/log4js/lib/appenders/mongodb.js
cp ./src/log4js/layouts.js ./node_modules/log4js/lib/layouts.js
cp ./src/log4js/levels.js ./node_modules/log4js/lib/levels.js