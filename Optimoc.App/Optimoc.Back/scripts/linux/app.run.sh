#!/bin/bash

set -e
node_env=${NODE_ENV:-production}
cd ../..

if [[ $node_env == "production" ]]; then
    NODE_ENV=production npm run start
else
    npm run dev
fi
