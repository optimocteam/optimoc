﻿"use strict"

var _db;
var async = require("async");
var log = require('log4js').getLogger(require("path").basename(__filename, ".js"));

setTimeout(function () {

    _db = require('../db/db');
    log.addContext("methodName", stackTrace.get()[0].getMethodName());
    log.trace("Company Controller Loaded.");

}, 500);

var companyController = {

    /**
     * 
     *
     * 
     * 
     * 
     */
    getCompany: function (res, data) {

        log.addContext("methodName", stackTrace.get()[0].getFunctionName());

        try {

            var keys = Object.keys(data);

            async.waterfall([

                function (callback) {
                    
                    if (!!~keys.indexOf("companyID")) {

                        log.addContext("userAPIKey", data.userAPIKey);
                        log.addContext("userName", data.fullName);
                        log.trace(`Get company - companyID : ${data["companyID"]}`);

                        _db.getCompanyByCompanyID(data["companyID"], function (err, company) {

                            if (err)
                                throw err;
                                                        
                            callback(null, company);
                        });
                    }
                },
                function (company, callback) {

                    if (company != null) {

                        company = {

                            companyID: data["companyID"],
                            name: company.name,
                            informations: company.informations,
                            phoneNumber: company.phoneNumber,
                            address: company.address
                        };
                    }
                    else
                        company = null;

                    callback(null, company);
                }
            ], function (err, company) {
                res.status(200).json({ data: company });
            });
        }
        catch (err) {
            log.error("Error occured" + " - " + err.message);
            res.status(500).json({ error_code: error.code });
        }
    },

    /**
     * 
     *
     * 
     * 
     * 
     */
    updateCompany: function (res, data) {

         log.addContext("methodName", stackTrace.get()[0].getFunctionName());

        try {

            var keys = Object.keys(data);
            var id = data.name;
            var pk = data.pk;
            var value = data.value;
            var originalValue = data.originalValue;
            var company;
            
            async.waterfall([
                function (callback) {

                    if (!!~keys.indexOf("companyID")) {

                        log.addContext("userAPIKey", data.userAPIKey);
                        log.addContext("userName", data.fullName);
                        log.trace(`Update company - companyID : ${data["companyID"]} - id : ${id} - originalValue : ${originalValue} - new value ${value}`);
                        
                        _db.getCompanyByCompanyID(data["companyID"], function (err, comp) {

                            if (err)
                                throw err;

                            callback(null, comp);
                        });
                    }
                },
                function (comp, callback) {

                    if (comp != null) {
                        company = comp;
                        
                        if (id.split(".").length == 2) {

                            /*if (id == "informations.vat")
                                company[id.split(".")[0]][id.split(".")[1]] = parseFloat(value)
                            else*/
                                company[id.split(".")[0]][id.split(".")[1]] = value;
                        }
                        else
                            company[id] = value;
                    }
                    else
                        company = null;

                    callback(null, company);
                },
                function (company, callback) {

                    _db.updateCompany(data["companyID"], company, function (err, res) {
                        callback(err, res.nModified);
                    });
                }
            ], function (err, nModified) {

                if (err)
                    res.json({ error: true });
                else {

                    log.debug("nModified : " + nModified);
                    log.info("Companny with companyID \"%s\" has been updated", data["companyID"]);
                    res.json({ error: false, data: { id: id, newValue: value } });
                }
            });
        }
        catch (err) {
            log.error("Error occured" + " - " + err.message);
            res.json({ error: true, data: null });
        }
    },
};

module.exports = companyController;
module.id = "companyController";