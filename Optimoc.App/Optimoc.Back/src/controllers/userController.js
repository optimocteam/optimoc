"use strict"

var _db, _defaultMetrics;
var appSettings = config.settings();
var async = require("async");
//var cryptoJS = require("crypto-js");
var errors = config.errors();
var utils = require("../utils/utils");
var path = require('path');
var fs = require('fs');
var moment = require("moment");

let mongoose = require('mongoose'),
    jwt = require('jsonwebtoken'),
    User;

const JWT_SECRET = appSettings.JWT_SECRET;

var log = require('log4js').getLogger(require("path").basename(__filename, ".js"));

setTimeout(function () {
    
    _db = require('../db/db');
    User = mongoose.model('User');
    log.addContext("methodName", stackTrace.get()[0].getMethodName());
    log.trace("User Controller Loaded.");

}, 3000);

var userController = {

    /**
     * Given user data, create the user in database.
     *
     * @method createUser
     * @param {Object} data A data object with all user information needed
     * @return {Boolean} Returns true on success
     */
    createUser: function (res, data) {

         log.addContext("methodName", stackTrace.get()[0].getFunctionName());

        try {

            var keys = Object.keys(data);
            var registerToken, userAPIKey, url;
            var user, error_code = "", inserted = 0, error;

            if (!!~keys.indexOf("email") && !!~keys.indexOf("firstname") && !!~keys.indexOf("lastname") && !!~keys.indexOf("password") && !!~keys.indexOf("role")) {

                log.addContext("userAPIKey", null);
                log.addContext("userName", data["firstname"] + " " + data["lastname"]);

                log.trace("Trying to register  %s %s [%s]", data["firstname"], data["lastname"], data["email"]);

                //
                // New user
                user = new User();

                userAPIKey = utils.generateToken(32);
                registerToken = utils.generateToken(16);

                log.trace(userAPIKey);
                log.trace(registerToken);

                user["email"] = data["email"];
                user["firstname"] = data["firstname"];
                user["lastname"] = data["lastname"];
                user["role"] = data["role"];
                user["userAPIKey"] = userAPIKey;
                user["password"] = data.password;
                user["registerToken"] = registerToken;
                user["registerDate"] = moment.utc().toISOString();

                //
                // Rate Base
                var rateBases = [];
                var rateBase = {};
                _defaultMetrics = _db.metrics;

                for (var index in _defaultMetrics) {

                    rateBase[_defaultMetrics[index].metricsID - 1] = {
                        name: _defaultMetrics[index].metricsName,
                        data: {}
                    };

                    var rateBaseData = {};

                    for (var indexDesignation in _defaultMetrics[index].designations) {

                        if (!indexDesignation.startsWith("SEP"))
                            rateBaseData[indexDesignation] = {
                                name: _defaultMetrics[index].designations[indexDesignation].name,
                                unit: _defaultMetrics[index].designations[indexDesignation].unit,
                                pricePerUnit: _defaultMetrics[index].designations[indexDesignation].pricePerUnit
                            }
                    }

                    rateBase[_defaultMetrics[index].metricsID - 1].data = rateBaseData;
                }

                rateBases[0] = {};
                rateBases[0].name = "NEUF";
                rateBases[0].rates = rateBase;
                rateBases[1] = {};
                rateBases[1].name = "RENOVATION";
                rateBases[1].rates = rateBase;
                user["rateBases"] = rateBases;

                //
                // Creation with some controls before
                async.waterfall([

                    function (callback) {

                        _db.getUserByEmail(user["email"], function (err, usr) {

                            if (err)
                                throw err;

                            callback(null, usr);
                        });
                    },
                    function (usr, callback) {

                        if (usr != null) {

                            log.error("Registration failed for %s: %s", user.email, errors["API"]["USER_EXIST"].error_message);
                            
                            error = new Error(errors["API"]["USER_EXIST"].error_message);
                            error_code = errors["API"]["USER_EXIST"].error_code;
                        }
                        else {

                            if (data.password != data.confirmPassword) {

                                log.error("Registration failed for %s: %s", user.email, errors["API"]["USER_PASSWORDS_NOT_MATCH"].error_message);

                                error = new Error(errors["API"]["USER_PASSWORDS_NOT_MATCH"].error_message);
                                error_code = errors["API"]["USER_PASSWORDS_NOT_MATCH"].error_code;
                            }
                            else {
                                error = null;
                                error_code = "";
                            }
                        }

                        callback(error);
                    },
                    function (callback) {

                        //
                        // Company
                        _db.createCompany(function (err, res) {

                            if (err) {

                                log.error("Registration failed for %s: %s", user.email, errors["TECH"]["COMPANY_CREATION_FAILED"].error_message);

                                error = new Error("%s - Error occured : %s", errors["TECH"]["COMPANY_CREATION_FAILED"].error_message, err.message);
                                error_code = errors["TECH"]["COMPANY_CREATION_FAILED"].error_code;
                            }
                            else {

                                error = null;
                                error_code = "";
                            }
                            
                            callback(error, res.companyID);
                        });
                    },
                    function (companyID, callback) {

                        user.companyID = companyID;

                        _db.createUser(user, function (err, res) {

                            if (err) {

                                log.error("Registration failed for %s: %s", user.email, errors["TECH"]["USER_CREATION_FAILED"].error_message);

                                error = new Error("%s - Error occured : %s", errors["TECH"]["USER_CREATION_FAILED"].error_message, err.message);
                                error_code = errors["TECH"]["USER_CREATION_FAILED"].error_code;

                                _db.deleteCompany(companyID, function (err, res) {

                                    if (err)
                                        throw err;

                                    log.trace("RollBack : DEL_COMPANY OK");
                                });
                            }
                            else {

                                error = null;
                                error_code = "";
                                inserted = res.inserted;
                            }

                            callback(error);
                        });
                    },
                    function (callback) {

                        url = data["baseUrl"] + "/account/confirmEmail?emailAddress=" + user["email"] + "&key=" + registerToken.toString();

                        log.trace(`url : ${url}`)

                        var emailValidationFileNamePath = path.join(__dirname, "/../templates/email-validation.html");

                        fs.readFile(emailValidationFileNamePath, "utf8", function (err, data) {

                            var body = data
                                        .replace("{userFirstName}", user.firstname)
                                        .replace("{userLastName}", user.lastname)
                                        .replace("{userEmail}", user["email"])
                                        .replace("{url}", url)
                                        .replace("{year}", new Date().getFullYear())
                                        .replace("{expirationTime}", appSettings.REGISTER_TOKEN_EXPIRATION_MINUTES / 60);

                            utils.sendEmail(user["email"] , appSettings.SUBJECT_CONFIRM, body, false, function (err, res) {

                                if (err) {
                                    
                                    log.error(err.message);
                                }

                                if (!res) {

                                    log.error("%s for %s", errors["TECH"]["SEND_EMAIL_FAILED"].error_message, user["email"]);
                                    error = new Error(errors["TECH"]["SEND_EMAIL_FAILED"].error_message);
                                    error_code = errors["TECH"]["SEND_EMAIL_FAILED"].error_code;
                                }
                                else {

                                    log.trace("Email sent successfully to %s", user["email"]);
                                    error = null;
                                    error_code = "";
                                }
                            });

                            callback(error);

                        });
                    }
                ], function (err) {

                    if (err)
                        res.json({ error: true, error_code: error_code });
                    else {

                        //
                        // Log userName
                        log.addContext("userName", user.firstname + " " + user.lastname);

                        log.info("CREATE_USR OK - email : %s", user.email);
                        log.debug("inserted : " + inserted);
                        res.json({ error: false, error_code: error_code });
                    }
                });
            }
        }
        catch (err) {
            log.error("Error occured" + " - " + err.message);
            res.json({ error: true, error_code: err.code });
        }
    },

    /**
     * Given user email, send email with reset password link.
     *
     * @method forgotPassword
     * @param {Object} data A data object with user email needed
     * @return {Response} Returns 200 on success
     */
    forgotPassword: function (res, data) {

            log.addContext("methodName", stackTrace.get()[0].getFunctionName());

            let keys = Object.keys(data);
            let user, error_code = "", url;
            var error;

            try {

                async.waterfall([

                    function (callback) {

                        if (!!~keys.indexOf("email")) {
                            
                            _db.getUserByEmail(data["email"], function (err, usr) {
    
                                if (err)
                                    throw err;
    
                                if (usr != null) {
    
                                    log.addContext("userName", usr.firstname + " " + usr.lastname);
                                    log.info("User found - email : %s", data["email"]);
                                    callback(null, usr);
                                }
                                else {

                                    log.error("Reset password - sending email failed for %s: %s", data.email, errors["API"]["USER_NOT_EXIST"].error_message);
                                    
                                    error = new Error(errors["API"]["USER_NOT_EXIST"].error_message);
                                    error_code = errors["API"]["USER_NOT_EXIST"].error_code;

                                    callback(error, null);
                                }
                            });
                        }
                    },
                    function (usr, callback) {
    
                        let token;

                        if (usr)
                             // create the random token
                            token = utils.generateToken(16);
    
                        callback(null, usr, token);
                    },
                    function (usr, token, callback) {

                        _db.updateUser(usr.userID, { resetPasswordToken: token, resetPasswordDate: moment.utc().toISOString() }, function (err, res) {
                            
                            if (err)
                                throw err;

                            log.debug("nModified : " + res.nModified);
                            log.info("resetPasswordToken/resetPasswordDate have been updated");

                            callback(null, usr, token);
                        });
                    },
                    function (usr, resetPasswordToken, callback) {

                        url = `${data["baseUrl"]}/account/resetPassword?emailAddress=${usr["email"]}&token=${resetPasswordToken.toString()}`;

                        var forgotPasswordFileNamePath = path.join(__dirname, "/../templates/email-forgot-password.html");

                        fs.readFile(forgotPasswordFileNamePath, "utf8", function (err, data) {

                            var body = data
                                        .replace("{userFirstName}", usr.firstname)
                                        .replace("{userLastName}", usr.lastname)
                                        .replace("{userEmail}", usr["email"])
                                        .replace("{url}", url)
                                        .replace("{year}", new Date().getFullYear())
                                        .replace("{expirationTime}", appSettings.RESET_PASSWORD_TOKEN_EXPIRATION_MINUTES / 60);

                            utils.sendEmail(usr["email"] , appSettings.SUBJECT_RESET_PASSWORD, body, false, function (err, res) {

                                if (err)
                                    log.error(err.message);

                                if (!res) {

                                    log.error("%s for %s", errors["TECH"]["SEND_EMAIL_FAILED"].error_message, usr["email"]);
                                    error = new Error(errors["TECH"]["SEND_EMAIL_FAILED"].error_message);
                                    error_code = errors["TECH"]["SEND_EMAIL_FAILED"].error_code;
                                }
                                else {

                                    log.trace("Email sent successfully to %s", usr["email"]);
                                    error = null;
                                    error_code = "";
                                }
                            });

                            callback(error);

                        });
                    }
                ], function (err) {
    
                    if (err)
                        res.status(400).json({ error_code: error_code });
                    else
                        res.status(200).json();
                });
            }
            catch (err) {
                log.error("Error occured" + " - " + err.message);
                res.status(500).json({ error_code: err.code });
            }
    },

    /**
     * Gives user information from any userID, userAPIKey or email data passed.
     *
     * @method getUser
     * @param {Object} data A data object with either userId, userAPIkey or email.
     * @return {Object} Returns user Object or null
     */
    getUser: function (res, data) {

         log.addContext("methodName", stackTrace.get()[0].getFunctionName());

        try {

            var keys = Object.keys(data);
            var user, error, error_code = "";

            async.waterfall([

                function (callback) {

                    if (!!~keys.indexOf("userAPIKey")) {

                        log.addContext("userAPIKey", data["userAPIKey"]);

                        //
                        // Login user
                        _db.multiClient.get("kUserName:" + data["userAPIKey"], function (err, userName) {

                            if (userName == undefined)
                                _db.getUserNameByUserAPIKey(data["userAPIKey"], function (err, userName) {
                                    log.addContext("userName", userName);
                                    log.info("Get user information");
                                });
                            else {
                                log.addContext("userName", userName);
                                log.info("Get user information");
                            }

                        });
                        
                        _db.getUserByUserAPIKey(data["userAPIKey"], function (err, usr) {

                            if (err)
                                throw err;

                            callback(null, usr);
                        });
                    }
                    else if (!!~keys.indexOf("email") && !!~keys.indexOf("password")) {
                        
                        _db.getUserByEmail(data["email"], function (err, usr) {

                            if (err)
                                throw err;

                            if (usr != null) {

                                if (usr.comparePassword(data.password)) {

                                    log.addContext("userName", usr.firstname + " " + usr.lastname);
                                    log.info("AUTH_USR OK - email : %s", data["email"]);
                                    callback(null, usr);
                                }
                                else {
                                    log.warn("AUTH_USR NOK - email : %s - %s", data.email, errors["API"]["USER_PASSWORDS_NOT_MATCH"].error_message);

                                    error = new Error(errors["API"]["USER_PASSWORDS_NOT_MATCH"].error_message);
                                    error_code = errors["API"]["USER_PASSWORDS_NOT_MATCH"].error_code;

                                    callback(error, null);
                                }
                            }
                            else {
                                log.warn("AUTH_USR NOK - email : %s - %s", data.email, errors["API"]["USER_NOT_EXIST"].error_message);
    
                                error = new Error(errors["API"]["USER_NOT_EXIST"].error_message);
                                error_code = errors["API"]["USER_NOT_EXIST"].error_code;
    
                                callback(error, null);
                            }
                        });
                    }
                },
                function (usr, callback) {

                    if (usr != null) {

                        if (usr.isActive && usr.isEmailConfirmed) {

                            log.info("Load all informations...");

                            user = {
                                firstName: usr.firstname,
                                lastName: usr.lastname,
                                email: usr.email,
                                userID: usr.userID,
                                userAPIKey: usr.userAPIKey,
                                registerDate: usr.registerDate,
                                role: usr.role,
                                phoneNumber: usr.phoneNumber,
                                address: usr.address,
                                companyID: usr.companyID,
                                isAdmin: usr.isAdmin
                            };
                        }
                        else {
                            log.warn("AUTH_USR NOK - email : %s - %s", data.email, errors["API"]["USER_EMAIL_NOT_CONFIRMED"].error_message);

                            error = new Error(errors["API"]["USER_EMAIL_NOT_CONFIRMED"].error_message);
                            error_code = errors["API"]["USER_EMAIL_NOT_CONFIRMED"].error_code;

                            callback(error, null);
                        }
                    }
                    else
                        user = null;

                    callback(null, user);
                }
            ], function (err, user) {

                if (err)
                    res.status(401).json({ error_code: error_code });
                else {
                    const authToken = jwt.sign(
                        {
                            email: user.email,
                            fullName: `${user.firstName} ${user.lastName}`,
                            userID: user.userID,
                            isAdmin: user.isAdmin
                        },
                        JWT_SECRET,
                        {
                            algorithm: "HS256",
                            //expiresIn: 3600 // 1 hour
                        });

                    res.cookie("optimoc_auth_token", authToken, { httpOnly: true, expires: 0, secure: true });
                    res.cookie("optimoc_user_api_key", user.userAPIKey, { expires: 0, secure: true });
                    res.status(200).json({ data: user });
                }
            });
        }
        catch (err) {
            log.error("Error occured" + " - " + err.message);
            res.status(500).json({ error_code: err.code });
        }
    },

    /**
     * Given any user information in addition with either userID or userAPIKey, change the user information.
     *
     * @method updateUser
     * @param {Object} data A data object with user information and either  userId or userAPIkey
     * @return {Object} Returns user Object or null
     */
    updateUser: function (res, data) {

        log.addContext("methodName", stackTrace.get()[0].getFunctionName());

        try {

            var keys = Object.keys(data);
            var id = data.name;
            //var pk = data.pk;
            var value = data.value;
            var originalValue = data.originalValue;
            var obj = {};

            async.waterfall([
                function (callback) {

                    if (!!~keys.indexOf("userAPIKey")) {

                        log.addContext("userAPIKey", data["userAPIKey"]);

                        //
                        // Log userName
                        _db.multiClient.get("kUserName:" + data["userAPIKey"], function (err, userName) {

                            if (userName == undefined)
                                _db.getUserNameByUserAPIKey(data["userAPIKey"], function (err, userName) {
                                    log.addContext("userName", userName);
                                    log.info("Update user - " + id + " - originalValue : " + originalValue + " - new value : " + value);
                                });
                            else {
                                log.addContext("userName", userName);
                                log.info("[Update user " + id + "] - originalValue : \"" + originalValue + "\" - new value : \"" + value + "\"");
                            }

                        });

                        _db.getUserByUserAPIKey(data["userAPIKey"], function (err, usr) {

                            if (err)
                                throw err;

                            callback(null, usr);
                        });
                    }
                },
                function (usr, callback) {

                    if (usr != null) {

                        if (id.split(".").length == 2) {
                            obj[id.split(".")[0]][[id.split(".")[1]]] = value;
                        }
                        else
                            obj[id.split(".")[0]] = value;
                    }

                    callback(null, usr.userID, obj);
                },
                function (userID, obj, callback) {

                    _db.updateUser(userID, obj, function (err, res) {
                        callback(err, res.nModified);
                    });
                }
            ], function (err, nModified) {

                if (err)
                    throw err;

                log.debug("nModified : " + nModified);
                log.info("User informations has been updated");
                res.json({ error: false, data: { id: id, newValue: value } });
            });
        }
        catch (err) {
            log.error("Error occured" + " - " + err.message);
            res.json({ error: true, data: null });
        }
    },

    /**
     * Given userAPIKey or userID, delete the user.
     *
     * @method deleteUser
     * @param {Object} data A data object with a userAPIkey or userID
     * @return {Boolean} Returns True or False
     */
    deleteUser: function (data) {

        var keys = Object.keys(data);
        if(!!~keys.indexOf("userAPIKey")){
            var user = _users.where(function(obj){
                return obj.userAPIKey== data.userAPIKey;
            });

        }
        if(!!~keys.indexOf("userID")){
            if(data['userID']==null){
                _users.removeDataOnly();

                return "All user deleted";
            }else{
                var user = _users.where(function(obj){
                    return obj.userID== data.userID;
                });
            }
        }
        if(user.length>0){
            _users.remove(user);
            return user;//Todo : In order to handle error properly, wouldn't it better to have to return an array : {error:false,data:user} ?
        }
        return "Not found";
    },

    /**
     * 
     *
     */
    confirmEmail: function (res, data) {

         log.addContext("methodName", stackTrace.get()[0].getFunctionName());

        try {

            var keys = Object.keys(data);
            var obj = {};
            var error, error_code = "", user;
            let now = moment(), registerDate;

            async.waterfall([

                function (callback) {

                    if (!!~keys.indexOf("email")) {

                        _db.getUserByEmail(data["email"], function (err, usr) {

                            if (err)
                                throw err;

                            user = usr;
                            callback(null, usr);
                        });
                    }
                },
                function (usr, callback) {

                    if (usr != null) {

                        log.trace("Activation of user %s %s - email : %s", usr.firstname, usr.lastname, usr.email);

                        if (!!~keys.indexOf("key")) {

                            if (data["key"] != null) {

                                if (usr.registerToken == data["key"] && !usr.isActive && !usr.isEmailConfirmed) {

                                    registerDate = usr.registerDate;
                                    log.trace(`Now : ${now.format("DD-MM-YYYY HH:mm:ss")}`);
                                    log.trace(`Register date : ${moment(registerDate).local().format("DD-MM-YYYY HH:mm:ss")}`);

                                    if (now.diff(registerDate, "minutes") > appSettings.REGISTER_TOKEN_EXPIRATION_MINUTES) {

                                        log.warn(`Activation failed for ${usr.firstname} ${usr.lastname} : ${errors["API"]["USER_REGISTER_TOKEN_EXPIRED"].error_message}`);

                                        error = new Error(errors["API"]["USER_REGISTER_TOKEN_EXPIRED"].error_message);
                                        error_code = errors["API"]["USER_REGISTER_TOKEN_EXPIRED"].error_code;

                                        callback(error, 0);
                                    }
                                    else {

                                        obj.isActive = true;
                                        obj.isEmailConfirmed = true;
                                        error = null;
                                        error_code = "";

                                        callback(error, usr.userID, obj);
                                    }
                                }
                                else if (usr.registerToken == data["key"] && usr.isActive && usr.isEmailConfirmed) {

                                    log.warn("Activation failed for %s %s: %s", usr.firstname, usr.lastname, errors["API"]["USER_EMAIL_CONFIRMED"].error_message);

                                    error = new Error(errors["API"]["USER_EMAIL_CONFIRMED"].error_message);
                                    error_code = errors["API"]["USER_EMAIL_CONFIRMED"].error_code;

                                    callback(error, 0);
                                }
                                else if (usr.registerToken != data["key"]) {

                                    log.warn("Activation failed for %s %s: %s", usr.firstname, usr.lastname, errors["API"]["USER_BAD_REGISTER_TOKEN"].error_message);

                                    error = new Error(errors["API"]["USER_BAD_REGISTER_TOKEN"].error_message);
                                    error_code = errors["API"]["USER_BAD_REGISTER_TOKEN"].error_code;

                                    callback(error, 0);
                                }
                            }
                        }
                    }
                    else {
                        log.warn("Activation failed for %s : %s", data["email"], errors["API"]["USER_NOT_EXIST"].error_message);

                        error = new Error(errors["API"]["USER_NOT_EXIST"].error_message);
                        error_code = errors["API"]["USER_NOT_EXIST"].error_code;

                        callback(error, 0);
                    }
                },
                function (userID, obj, callback) {

                    _db.updateUser(userID, obj, function (err, res) {
                        callback(err, res.nModified);
                    });
                },
                function (nModified, callback) {

                    var emailNotificationFileNamePath = path.join(__dirname, "/../templates/email-notification.html");

                    fs.readFile(emailNotificationFileNamePath, "utf8", function (err, data) {

                        var body = data
                            .replace("{userFirstName}", user.firstname)
                            .replace("{userLastName}", user.lastname)
                            .replace("{userEmail}", user.email)
                            .replace("{loginPageUrl}", appSettings.LOGIN_PAGE_URL)
                            .replace("{year}", new Date().getFullYear());

                        utils.sendEmail(user.email, appSettings.SUBJECT_NOTIFICATION, body, false, function (err, res) {

                            if (err) {

                                log.error("%s for %s", errors["TECH"]["SEND_EMAIL_FAILED"].error_message, user.email);

                                error = new Error("%s - Error occured : %s", errors["TECH"]["SEND_EMAIL_FAILED"].error_message, err.message);
                                error_code = errors["TECH"]["SEND_EMAIL_FAILED"].error_code;
                            }

                            if (!res) {

                                log.error("%s for %s", errors["TECH"]["SEND_EMAIL_FAILED"].error_message, user.email);
                                error = new Error(errors["TECH"]["SEND_EMAIL_FAILED"].error_message);
                                error_code = errors["TECH"]["SEND_EMAIL_FAILED"].error_code;
                            }
                            else {

                                log.trace("Email sent successfully to %s", user.email);
                                error = null;
                                error_code = "";
                            }
                        });

                        callback(error, nModified);

                    });
                }
            ], function (err, nModified) {
                
                if (err) {

                    if (error_code === "API_USER_REGISTER_TOKEN_EXPIRED_ERROR")
                        _db.deleteUser(user.userAPIKey, function (err, res) {
                            if (res.deleted === 1)
                                log.trace(`User ${user.firstname} ${user.lastname} has been deleted.`);
                        });

                    res.json({ error: true, error_code: error_code });
                }
                else {
                    log.debug("nModified : " + nModified);
                    log.info("Activation succesful for email : %s", data["email"]);
                    res.json({ error: false, error_code: error_code });
                }
            });
        }
        catch (err) {
            log.error("[confirmEmail]: Error occured" + " - " + err.message);
            res.json({ error: true, error_code: err.code });
        }
    },

    /**
     * 
     *
     */
    resetPassword: function (res, data) {

        log.addContext("methodName", stackTrace.get()[0].getFunctionName());

        try {

           var keys = Object.keys(data);
           var error, error_code = "";
           let now = moment(), resetPasswordDate;

           async.waterfall([

                function (callback) {

                    if (data.newPassword != data.confirmPassword) {

                        log.warn("Resetting password failed for %s: %s", data.email, errors["API"]["USER_PASSWORDS_NOT_MATCH"].error_message);

                        error = new Error(errors["API"]["USER_PASSWORDS_NOT_MATCH"].error_message);
                        error_code = errors["API"]["USER_PASSWORDS_NOT_MATCH"].error_code;

                        callback(error);
                    }
                    else
                        callback(null);
                },
                function (callback) {

                   if (!!~keys.indexOf("email")) {

                       _db.getUserByEmail(data["email"], function (err, usr) {

                           if (err)
                               throw err;

                           callback(null, usr);
                       });
                   }
                },
                function (usr, callback) {

                    if (usr != null) {

                        log.trace("Resetting password for user %s %s - email : %s", usr.firstname, usr.lastname, usr.email);

                        if (!!~keys.indexOf("token")) {

                            if (data["token"] != null) {

                                if (usr.resetPasswordToken === data["token"]) {

                                    resetPasswordDate = usr.resetPasswordDate;
                                    log.trace(`Now : ${now.format("DD-MM-YYYY HH:mm:ss")}`);
                                    log.trace(`ResetPassword date : ${moment(resetPasswordDate).local().format("DD-MM-YYYY HH:mm:ss")}`);

                                    if (now.diff(resetPasswordDate, "minutes") > appSettings.RESET_PASSWORD_TOKEN_EXPIRATION_MINUTES) {

                                        log.warn(`Resetting password failed for ${usr.firstname} ${usr.lastname} : ${errors["API"]["USER_RESET_PASSWORD_TOKEN_EXPIRED"].error_message}`);

                                        error = new Error(errors["API"]["USER_RESET_PASSWORD_TOKEN_EXPIRED"].error_message);
                                        error_code = errors["API"]["USER_RESET_PASSWORD_TOKEN_EXPIRED"].error_code;

                                        callback(error);
                                   }
                                   else {

                                        usr.password = data.newPassword;
                                        usr.resetPasswordToken = undefined;
                                        usr.resetPasswordDate = undefined;
                                        usr.userID = undefined;
                                        error_code = "";

                                        callback(null, usr);
                                    }
                               }
                               else {

                                    log.warn("Resetting password failed for %s %s: %s", usr.firstname, usr.lastname, errors["API"]["USER_BAD_RESET_PASSWORD_TOKEN"].error_message);

                                    error = new Error(errors["API"]["USER_BAD_RESET_PASSWORD_TOKEN"].error_message);
                                    error_code = errors["API"]["USER_BAD_RESET_PASSWORD_TOKEN"].error_code;

                                    callback(error);
                               }
                           }
                       }
                    }
                    else {
                        log.warn("Resetting password failed for %s : %s", data["email"], errors["API"]["USER_NOT_EXIST"].error_message);

                        error = new Error(errors["API"]["USER_NOT_EXIST"].error_message);
                        error_code = errors["API"]["USER_NOT_EXIST"].error_code;

                        callback(error);
                    }
                },
                function (usr, callback) {

                    usr.save(function (err) {
                        if (err)
                            throw err;

                        log.info("New Password has been updated");
                        callback(null, usr);
                    });
                },
                function (usr, callback) {

                   const forgotPasswordPageUrl = `${data["baseUrl"]}/forgotPassword`;
                   var emailResetPasswordNotificationFileNamePath = path.join(__dirname, "/../templates/email-reset-password-notification.html");

                   fs.readFile(emailResetPasswordNotificationFileNamePath, "utf8", function (err, data) {

                       var body = data
                           .replace("{userFirstName}", usr.firstname)
                           .replace("{userLastName}", usr.lastname)
                           .replace("{userEmail}", usr.email)
                           .replace("{forgotPasswordPageUrl}", forgotPasswordPageUrl)
                           .replace("{year}", new Date().getFullYear());

                       utils.sendEmail(usr.email, appSettings.SUBJECT_RESET_PASSWORD_NOTIFICATION, body, false, function (err, res) {

                           if (err) {

                               log.error("%s for %s", errors["TECH"]["SEND_EMAIL_FAILED"].error_message, usr.email);

                               error = new Error("%s - Error occured : %s", errors["TECH"]["SEND_EMAIL_FAILED"].error_message, err.message);
                               error_code = errors["TECH"]["SEND_EMAIL_FAILED"].error_code;
                           }

                           if (!res) {

                               log.error("%s for %s", errors["TECH"]["SEND_EMAIL_FAILED"].error_message, usr.email);
                               error = new Error(errors["TECH"]["SEND_EMAIL_FAILED"].error_message);
                               error_code = errors["TECH"]["SEND_EMAIL_FAILED"].error_code;
                           }
                           else {

                               log.trace("Email sent successfully to %s", usr.email);
                               error = null;
                               error_code = "";
                           }
                       });

                       callback(error);

                   });
                }
            ], function (err) {
               
                if (err)
                    res.status(400).json({ error_code: error_code });
                else {
                    log.info("Resetting password successful for email : %s", data["email"]);
                    res.status(200).json();
               }
           });
       }
       catch (err) {
            log.error("Error occured" + " - " + err.message);
            res.status(500).json({ error_code: err.code });
       }
    },

    /**
     * 
     * @param {*} res 
     * @param {*} data 
     */
    logoutUser: function (res, data){
        log.addContext("methodName", stackTrace.get()[0].getFunctionName());

        try {

            var keys = Object.keys(data);
            var user;

            async.waterfall([

                function (callback) {

                    if (!!~keys.indexOf("userAPIKey")) {

                        log.addContext("userAPIKey", data["userAPIKey"]);

                        //
                        // Log userName
                        _db.multiClient.get("kUserName:" + data["userAPIKey"], function (err, userName) {

                            if (userName == undefined)
                                _db.getUserNameByUserAPIKey(data["userAPIKey"], function (err, userName) {
                                    log.addContext("userName", userName);
                                    log.info("Logout user");
                                });
                            else {
                                log.addContext("userName", userName);
                                log.info("Logout user");
                            }

                        });
                        
                        callback(null);
                    }
                }
            ], function (err) {

                    res.cookie("optimoc_auth_token", undefined);
                    res.cookie("optimoc_user_api_key", undefined);
                    res.status(200).json();
            });
        }
        catch (err) {
            log.error("Error occured" + " - " + err.message);
            res.status(500).json({ error_code: err.code });
        }
    },

    /**
     * 
     * @param {*} res 
     * @param {*} data 
     */
    getUsers: (res, data) => {

        log.addContext("methodName", stackTrace.get()[0].getFunctionName());

        try {

            let keys = Object.keys(data);
            //let users;

            async.waterfall([

                function (callback) {

                    if (!!~keys.indexOf("userAPIKey")) {

                        log.addContext("userAPIKey", data["userAPIKey"]);

                        //
                        // Log userName
                        _db.multiClient.get("kUserName:" + data["userAPIKey"], function (err, userName) {

                            if (userName == undefined)
                                _db.getUserNameByUserAPIKey(data["userAPIKey"], function (err, userName) {
                                    log.addContext("userName", userName);
                                    log.trace("Get customers");
                                });
                            else {
                                log.addContext("userName", userName);
                                log.trace("Get users");
                            }
                        });
                        
                        User.find({}, 
                        {
                            firstname: 1,
                            lastname: 1,
                            email: 1,
                            phoneNumber: 1,
                            registerDate: 1
                        }, function (err, users) {
                
                            if (err)
                                callback(err, null);
                
                            if (users != null)
                                callback(null, users);
                        });
                    }
                }
            ], function (err, users) {

                if (err)
                    res.status(401).json({  error_code: err.code });
                else {
                    res.status(200).json(users);
                }
            });
        }
        catch (err) {
            log.error("Error occured" + " - " + err.message);
            res.status(500).json({ error_code: err.code });
        }
    },
};

module.exports = userController;
module.id = "userController";