﻿"use strict"

var _db, _titles, _status;
var log = require('log4js').getLogger(require("path").basename(__filename, ".js"));

setTimeout(function () {

     log.addContext("methodName", stackTrace.get()[0].getMethodName());
    _db = require('../db/db');

    //
    // Load titles collection
    _db.getTitles(function (err, titles) {

        if (err)
            throw err;

        _titles = titles;
        log.debug("DB : titles collection have been loaded");
    });

    //
    // Load status collection
    _db.getStatus(function (err, status) {

        if (err)
            throw err;

        _status = status;
        log.debug("DB : status collection have been loaded");
    });
    
    log.trace("Reference Controller Loaded.");
}, 3000);


var referenceController = {
    referenceController: function () {
        log.trace("Reference Controller");
    },

    /**
     * @method getTitles
     * @return {Object} Returns titles objects
     */
    getTitles: function (res) {

         log.addContext("methodName", stackTrace.get()[0].getFunctionName());

        try {
            
            res.json({ error: false, data: _titles });
        }
        catch (err) {
            log.error("Error occured" + " - " + err.message);
            res.json({ error: true, data: null });
        }
    },
    /**
    * @method getStatus
    * @return {Object} Returns status objects
    */
    getStatus: function (res) {

         log.addContext("methodName", stackTrace.get()[0].getFunctionName());

        try {
            
            res.json({ error: false, data: _status });
        }
        catch (err) {
            log.error("Error occured" + " - " + err.message);
            res.json({ error: true, data: null });
        }
    }
};

module.exports = referenceController;
module.id = "referenceController";