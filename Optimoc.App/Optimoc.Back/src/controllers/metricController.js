﻿"use strict"

var _db, _defaultMetrics;
var log = require('log4js').getLogger(require("path").basename(__filename, ".js"));
var async = require("async");
var utils = require("../utils/utils");
var moment = require("moment");

setTimeout(function () {

    log.addContext("methodName", stackTrace.get()[0].getMethodName());
    _db = require('../db/db');

    //
    // Load metrics collection
    if (_db.metrics == null)
        _db.getMetrics(function (err, metrics) {

            if (err)
                throw err;

            _defaultMetrics = _db.metrics = metrics;
            log.debug("DB : metrics collection have been loaded");
        });
    else
        _defaultMetrics = _db.metrics;
    
     log.trace('Metric Controller Loaded.');

}, 2000);

var metricController = {

    metricController: function () {
         log.trace("Metric Controller");
    },

    //#region getMetricsCaracteristics function

    /**
     * Given userAPIKey and a projectID and return project's metricsCaracteristics.
     * @method getMetrics
     * @param {Object} res : The response object.
     * @param {Object} data A data object with userAPIKey and {int|null} ProjectID
     * @return {Object} Returns error / metricsCaracteristics Object or null
     */
    getMetricsCaracteristics: function (res, data) {

        log.addContext("methodName", stackTrace.get()[0].getFunctionName());

        try {

            var keys = Object.keys(data);
            //var userName;

            if (!!~keys.indexOf("projectID") && !!~keys.indexOf("userAPIKey")) {

                async.waterfall([

                    function (callback) {

                        log.addContext("userAPIKey", data["userAPIKey"]);

                        _db.multiClient.get("kUserName:" + data["userAPIKey"], function (err, userName) {

                            if (userName == undefined)
                                _db.getUserNameByUserAPIKey(data["userAPIKey"], function (err, userName) {
                                    log.addContext("userName", userName);
                                    log.trace("Get project's metricsCaracteristics - projectID : %s", data["projectID"]);
                                });
                            else {
                                log.addContext("userName", userName);
                                log.trace("Get project's metricsCaracteristics - projectID : %s", data["projectID"]);
                            }
                        });

                        var userAPIKey = data.userAPIKey;
                        var projectID = data.projectID;

                        _db.getProjectByUAPIKeyAndProjectID(userAPIKey, projectID, function (err, project) {

                            if (err)
                                throw err;

                            callback(null, project);
                        });
                    },
                    function (project, callback) {

                        callback(null, project.metricsCaracteristics);
                    }
                ], function (err, metricsCaracteristics) {

                    if (err)
                        res.json({ error: true });
                    else
                        res.json({ error: false, data: metricsCaracteristics });

                });
            }
        }
        catch (err) {
            log.error("Error occured" + " - " + err.message);
            res.json({ error: true });
        }
    },

    //#endregion

    //#region getMetrics function

    /**
     * Given userAPIKey and a projectID and return project's metrics.
     * @method getMetrics
     * @param {Object} res : The response object.
     * @param {Object} data A data object with userAPIKey and {int|null} ProjectID
     * @return {Object} Returns error / metrics Object or null
     */
    getMetrics: function (res, data) {

        log.addContext("methodName", stackTrace.get()[0].getFunctionName());
        
        try {

            var keys = Object.keys(data);
            //var userName;

            if (!!~keys.indexOf("projectID") && !!~keys.indexOf("userAPIKey")) {

                async.waterfall([

                    function (callback) {

                        log.addContext("userAPIKey", data["userAPIKey"]);

                        _db.multiClient.get("kUserName:" + data["userAPIKey"], function (err, userName) {

                            if (userName == undefined)
                                _db.getUserNameByUserAPIKey(data["userAPIKey"], function (err, userName) {
                                    log.addContext("userName", userName);
                                    log.trace("Get project's metrics - projectID : %s", data["projectID"]);
                                });
                            else {
                                log.addContext("userName", userName);
                                log.trace("Get project's metrics - projectID : %s", data["projectID"]);
                            }
                        });

                        var userAPIKey = data.userAPIKey;
                        var projectID = data.projectID;
                        
                        _db.getProjectByUAPIKeyAndProjectID(userAPIKey, projectID, function (err, project) {

                            if (err)
                                throw err;

                            callback(null, project);
                        });
                    },
                    function (project, callback) {

                        //
                        // Patch if vat not exists
                        /*for (let indexLot in project.metrics) {
                            let lotData = project.metrics[indexLot].data;

                            for (let indexEl in lotData) {
                                let elData = lotData[indexEl];

                                if (elData != null && !elData.isSeparator) {
                                    
                                    if(!elData.vat)
                                        elData.vat = 0
                                    elData.totalTTC = elData.vat ? (elData.totalHT * elData.vat) / 100 + elData.totalHT  : 0;
                                }
                            }
                        }*/

                        callback(null, project.metrics);
                    }
                ], function (err, metrics) {

                    if (err)
                        res.json({ error: true });
                    else
                        res.json({ error: false, data: metrics });

                });
            }
        }
        catch (err) {
            log.error("Error occured" + " - " + err.message);
            res.json({ error: true });
        }
    },

    //#endregion

    //#region updateMetric function

    /**
   * Given userAPIKey, projectID, colName, pk, value and update project's metric.
   * @method updateMetric
   * @param {Object} res : The response object.
   * @param {Object} data : A data object with userAPIKey, projectID, colName, pk, value.
   * @return {Object} Returns  error(true or false) / null Object 
   */
    updateMetric: function (res, data) {

        log.addContext("methodName", stackTrace.get()[0].getFunctionName());

        try {

            var userAPIKey = data.userAPIKey;
            var projectID = data.projectID;
            var colName = data.name == undefined ? "" : data.name;
            var pk = data.pk == undefined ? "" : data.pk;
            var value = data.value == undefined ? "" : data.value.replace(",", ".");
            var isChecked = data.isChecked == undefined ? null : data.isChecked;
            var lotIndex = data.lotIndex == undefined ? null : data.lotIndex;;
            var designationIndex;
            var projectFields = {};

            if (userAPIKey !== undefined && projectID !== undefined) {

                async.waterfall([

                    function (callback) {

                        log.addContext("userAPIKey", userAPIKey);

                        _db.multiClient.get("kUserName:" + userAPIKey, function (err, userName) {

                            if (userName == undefined)
                                _db.getUserNameByUserAPIKey(userAPIKey, function (err, userName) {
                                    log.addContext("userName", userName);
                                    log.trace("Update project's metric - projectID : " + projectID + " - column name : " + colName + " - pk : " + pk + " - value : " + value);
                                    log.trace("Update project's metric - projectID : " + projectID + " - lotIndex : " + lotIndex + " - isChecked : " + isChecked);
                                });
                            else {
                                log.addContext("userName", userName);
                                log.trace("Update project's metric - projectID : " + projectID + " - column name : " + colName + " - pk : " + pk + " - value : " + value);
                                log.trace("Update project's metric - projectID : " + projectID + " - lotIndex : " + lotIndex + " - isChecked : " + isChecked);
                            }
                        });
                        
                        _db.getProjectByUAPIKeyAndProjectID(userAPIKey, projectID, function (err, project) {

                            if (err)
                                throw err;

                            callback(null, project);
                        });
                    },
                    function (project, callback) {

                        var metrics = project.metrics;
                        var metricsCaracteristics = project.metricsCaracteristics;

                        if (isChecked != null) {
                            metrics[lotIndex].selected = isChecked;
                        }
                        else {
                            for (var index1 in metrics) {

                                if (index1 == pk && colName == "lotName") {

                                    lotIndex = index1;
                                    metrics[index1].name = value;
                                    break;
                                }

                                for (var index2 in metrics[index1].data) {

                                    if (index2 == pk) {

                                        lotIndex = index1;
                                        designationIndex = index2;

                                        switch (colName) {

                                            case "qte":
                                                metrics[index1].data[index2].qte = parseFloat(value);
                                                break;

                                            case "pu":
                                                metrics[index1].data[index2].pricePerUnit = parseFloat(value);
                                                break;

                                            case "designation":
                                                metrics[index1].data[index2].name = value;
                                                break;

                                            case "width":
                                                metrics[index1].data[index2].largeur = parseFloat(value);
                                                break;

                                            case "height":
                                                metrics[index1].data[index2].hauteur = parseFloat(value);
                                                break;

                                            case "unit":
                                                metrics[index1].data[index2].unit = value;
                                                break;

                                            case "vat":
                                                metrics[index1].data[index2].vat = parseFloat(value);
                                                log.trace("vat" + value)
                                                break;

                                            case "Q":

                                                metrics[index1].data[index2].Q = parseFloat(value);

                                                if (metrics[index1].data[index2].VR != null)
                                                    metrics[index1].data[index2].VR = parseFloat(value);
                                                else if (metrics[index1].data[index2].linteau != null)
                                                    metrics[index1].data[index2].linteau = parseFloat(value) * 0.06;
                                        }

                                        if (index1 == 8) {
                                            metrics[index1].data[index2].tableaux = metrics[index1].data[index2].Q * (metrics[index1].data[index2].hauteur * 2);
                                            metrics[index1].data[index2].seuils = metrics[index1].data[index2].Q * metrics[index1].data[index2].largeur;
                                        }
                                    }
                                }
                            }

                            if ((colName == "qte" || colName == "pu" || colName == "Q" || colName == "vat"))
                                calcMetrics(metrics, metricsCaracteristics, project.projectType);
                        }

                        //projectFields["metricsCaracteristics"] = project.metricsCaracteristics;
                        projectFields["metrics"] = project.metrics;

                        callback(null, userAPIKey, projectID, projectFields);
                    },
                    function (userAPIKey, projectID, projectFields, callback) {

                        _db.updateProject(userAPIKey, projectID, projectFields, function (err, res) {
                            callback(err, res.nModified);
                        });
                    }
                ], function (err, nModified) {

                    if (err)
                        res.json({ error: true });
                    else {

                        log.debug("nModified : " + nModified);

                        if (nModified == 1) {

                            log.info("Project with projectID %s has been updated", projectID);
                            res.json({ error: false, newValue: value, metrics: projectFields["metrics"] });
                        }
                    }
                });
            }
        }
        catch (err) {
            log.error("Error occured" + " - " + err.message);
            res.json({ error: true });
        }
    },

    //#endregion

    //#region setMetricCaracteristics function

    setMetricCaracteristics: function (res, data) {

        log.addContext("methodName", stackTrace.get()[0].getFunctionName());

        try {
            var userAPIKey, projectID;
            var projectFields = {};
            var rateBaseName = data.rateBaseName == undefined ? "" : data.rateBaseName;
            var projectType = data.projectType == undefined ? "" : data.projectType;
            
            if (data.userAPIKey !== undefined && data.projectID !== undefined && data.metricsCaracteristics !== undefined) {

                async.waterfall([

                    function (callback) {

                        userAPIKey = data.userAPIKey;
                        projectID = data.projectID;

                        log.addContext("userAPIKey", userAPIKey);

                        _db.multiClient.get("kUserName:" + userAPIKey, function (err, userName) {

                            if (userName == undefined)
                                _db.getUserNameByUserAPIKey(userAPIKey, function (err, userName) {
                                    log.addContext("userName", userName);
                                    log.trace("Set metrics caracteristics - projectID : %s", projectID);
                                });
                            else {
                                log.addContext("userName", userName);
                                log.trace("Set metrics caracteristics - projectID : %s", projectID);
                            }
                        });

                        _db.getProjectByUAPIKeyAndProjectID(userAPIKey, projectID, function (err, project) {
                            
                            if (err)
                                throw err;

                            callback(null, project);
                        });
                    },
                    function (project, callback) {
                        
                        _db.getRateBaseByUserAPIKey(userAPIKey, projectType, function (err, rateBase) {

                            if (err)
                                throw err;

                            callback(null, project, rateBase);
                        });
                    },
                    function (project, rateBase, callback) {
                                               
                        //
                        // Metrics Caracteristics
                        if (project.projectType === "NEUF") {
                            setMetricCaracteristicsDefaultValues(data.metricsCaracteristics);
                            projectFields["metricsCaracteristics"] = data.metricsCaracteristics;
                        }

                        //
                        // Metrics
                        if (Object.keys(project.metrics).length === 0)
                            projectFields["metrics"] = setDefaultMetrics(rateBase, project.projectType);
                        else
                            projectFields["metrics"] = project.metrics;

                        //
                        // Calculate metrics
                        calcMetrics(projectFields["metrics"], projectFields["metricsCaracteristics"], project.projectType);

                        callback(null, userAPIKey, projectID, projectFields);
                    },
                    function (userAPIKey, projectID, projectFields, callback) {

                        _db.updateProject(userAPIKey, projectID, projectFields, function (err, res) {
                            callback(err, res.nModified);
                        });
                    }
                ], function (err, nModified) {
                    
                    if (err)
                        res.json({ error: true });
                    else {

                        log.debug("nModified : " + nModified);

                        if (nModified == 1) {

                            log.info("Project with projectID %s has been updated", projectID);
                            res.json({ error: false, metrics: projectFields["metrics"], metricsCaracteristics: projectFields["metricsCaracteristics"]});
                        }
                    }
                });
            }
        }
        catch (err) {
            log.error("Error occured" + " - " + err.message);
            res.json({ error: true });
        }
    },

    //#endregion
    
    // #region createLot function

    /**
     * 
     */
    createLot: function (res, data) {

        log.addContext("methodName", stackTrace.get()[0].getFunctionName());

        try {

            var userAPIKey = data.userAPIKey;
            var projectID = data.projectID;
            var lotIndex;
            var newLot;
            var projectFields = {};
            
            if (userAPIKey !== undefined && projectID !== undefined) {

                async.waterfall([

                    function (callback) {

                        log.addContext("userAPIKey", userAPIKey);

                        _db.multiClient.get("kUserName:" + userAPIKey, function (err, userName) {

                            if (userName == undefined)
                                _db.getUserNameByUserAPIKey(userAPIKey, function (err, userName) {
                                    log.addContext("userName", userName);
                                    log.trace("Create a lot - projectID: " + projectID);
                                });
                            else {
                                log.addContext("userName", userName);
                                log.trace("Create a lot - projectID: " + projectID);
                            }
                        });

                        _db.getProjectByUAPIKeyAndProjectID(userAPIKey, projectID, function (err, project) {

                            if (err)
                                throw err;

                            callback(null, project);
                        });
                    },
                    function (project, callback) {
                        
                        lotIndex = moment(new Date()).format("YYYYMMDDHHmmsss");

                        newLot = {
                            name: "Nouveau lot",
                            selected: false,
                            isCustom: true,
                            data: {}
                        };

                        project.metrics[lotIndex] = newLot;
                        projectFields["metrics"] = project.metrics;
                        
                        callback(null, userAPIKey, projectID, projectFields);
                    },
                    function (userAPIKey, projectID, projectFields, callback) {

                        _db.updateProject(userAPIKey, projectID, projectFields, function (err, res) {

                            callback(err, res.nModified);
                        });
                    }
                ], function (err, nModified) {

                    if (err)
                        res.json({ error: true });
                    else {

                        log.debug("nModified : " + nModified);

                        log.info("\"%s\" [Id : %s] has been created", newLot.name, lotIndex);
                        res.json({ error: false, lotIndex: lotIndex, newLot: newLot });
                    }
                });
            }
        }
        catch (err) {
            log.error("Error occured" + " - " + err.message);
            res.json({ error: true });
        }
    },

    // #endregion

    // #region deleteLot function

    /**
     * 
     */
    deleteLot: function (res, data) {

        log.addContext("methodName", stackTrace.get()[0].getFunctionName());

        try {

            var userAPIKey = data.userAPIKey;
            var projectID = data.projectID;
            var lotIndex = data.lotIndex;
            var lotName;
            var projectFields = {};
            
            if (userAPIKey !== undefined) {

                async.waterfall([

                    function (callback) {

                        log.addContext("userAPIKey", userAPIKey);

                        _db.multiClient.get("kUserName:" + userAPIKey, function (err, userName) {

                            if (userName == undefined)
                                _db.getUserNameByUserAPIKey(userAPIKey, function (err, userName) {
                                    log.addContext("userName", userName);
                                    log.trace("Delete a lot - projectID: " + projectID);
                                });
                            else {
                                log.addContext("userName", userName);
                                log.trace("Delete a lot - projectID: " + projectID);
                            }
                        });

                        _db.getProjectByUAPIKeyAndProjectID(userAPIKey, projectID, function (err, project) {

                            if (err)
                                throw err;

                            callback(null, project);
                        });
                    },
                    function (project, callback) {
                        
                        lotName = project.metrics[lotIndex].name;
                        delete project.metrics[lotIndex];
                        projectFields["metrics"] = project.metrics;

                        callback(null, userAPIKey, projectID, projectFields);
                    },
                    function (userAPIKey, projectID, projectFields, callback) {

                        _db.updateProject(userAPIKey, projectID, projectFields, function (err, res) {

                            callback(err, res.nModified);
                        });
                    }
                ], function (err, nModified) {

                    if (err)
                        res.json({ error: true });
                    else {
                        log.debug("nModified : " + nModified);

                        log.info("\"%s\" [Id : %s] has been deleted", lotName, lotIndex);
                        res.json({ error: false });
                    }
                });
            }
        }
        catch (err) {
            log.error("Error occured" + " - " + err.message);
            res.json({ error: true, newValue: null });
        }
    },

    // #endregion

    // #region createDesignation function

    /**
     * 
     */
    createDesignation: function (res, data) {

        log.addContext("methodName", stackTrace.get()[0].getFunctionName());

        try {

            var userAPIKey = data.userAPIKey;
            var projectID = data.projectID;
            var lotIndex = data.lotIndex;
            var designationIndex, designationName;
            var newDesignation, diversSeparator, diversSeparatorIndex;
            var projectFields = {};

            if (userAPIKey !== undefined && projectID !== undefined) {

                async.waterfall([

                    function (callback) {

                        log.addContext("userAPIKey", userAPIKey);

                        _db.multiClient.get("kUserName:" + userAPIKey, function (err, userName) {

                            if (userName == undefined)
                                _db.getUserNameByUserAPIKey(userAPIKey, function (err, userName) {
                                    log.addContext("userName", userName);
                                    log.trace("Create a designation - projectID: " + projectID);
                                });
                            else {
                                log.addContext("userName", userName);
                                log.trace("Create a designation - projectID: " + projectID);
                            }
                        });

                        _db.getProjectByUAPIKeyAndProjectID(userAPIKey, projectID, function (err, project) {

                            if (err)
                                throw err;

                            callback(null, project);
                        });
                    },
                    function (project, callback) {

                        //
                        // Electricite ou Plomberie
                        if (lotIndex == 10 || lotIndex == 11)
                            designationName = "Divers : Nouvelle désignation"
                        else
                            designationName = "Nouvelle désignation"

                        designationIndex = "LO_" + lotIndex + "_DE_" + moment(new Date()).format("YYYYMMDDHHmmsss");

                        newDesignation = {
                            name: designationName,
                            qte: 0,
                            unit: "U",
                            pricePerUnit: 0,
                            vat: 0,
                            totalHT: 0,
                            totalTTC: 0,
                            isEditable: true,
                            isCustom: true
                        };

                        //
                        // Menuiseries Extérieures
                        if (lotIndex == 8) {

                            newDesignation["largeur"] = 0;
                            newDesignation["hauteur"] = 0;
                            newDesignation["Q"] = 0;
                            newDesignation["VR"] = null;
                            newDesignation["linteau"] = 0;
                            newDesignation["tableaux"] = 0;
                            newDesignation["seuils"] = 0;
                        }

                        //
                        // Plomberie - Divers Separator
                        if (lotIndex == 11) {

                            var custom = Object.keys(project.metrics[lotIndex].data).filter(function (key) {
                                return project.metrics[lotIndex].data[key].isCustom;
                            });

                            if (custom.length == 0) {

                                diversSeparatorIndex = "SEP04";
                                diversSeparator = {
                                    name: "DIVERS",
                                    isSeparator: true
                                };

                                project.metrics[lotIndex].data[diversSeparatorIndex] = diversSeparator;
                            }
                        }

                        project.metrics[lotIndex].data[designationIndex] = newDesignation;
                        projectFields["metrics"] = project.metrics;

                        callback(null, userAPIKey, projectID, projectFields);
                    },
                    function (userAPIKey, projectID, projectFields, callback) {

                        _db.updateProject(userAPIKey, projectID, projectFields, function (err, res) {

                            callback(err, res.nModified);
                        });
                    }
                ], function (err, nModified) {

                    if (err)
                        res.json({ error: true });
                    else {

                        log.debug("nModified : " + nModified);

                        log.info("\"%s\" [Id : %s] has been created", newDesignation.name, designationIndex);

                        res.json({
                            error: false,
                            designationIndex: designationIndex,
                            newDesignation: newDesignation,
                            diversSeparatorIndex: diversSeparatorIndex,
                            diversSeparator: diversSeparator
                        });
                    }
                });
            }
        }
        catch (err) {
            log.error("Error occured" + " - " + err.message);
            res.json({ error: true, newValue: null });
        }
    },

    // #endregion
    
    // #region deleteDesignation function

    /**
     * 
     */
    deleteDesignation: function (res, data) {

        log.addContext("methodName", stackTrace.get()[0].getFunctionName());

        try {

            var userAPIKey = data.userAPIKey;
            var projectID = data.projectID;
            var lotIndex = data.lotIndex,
                designationIndex = data.designationIndex;
            var lotName, designationName;
            var diversSeparatorIndex;
            var projectFields = {};
            
            if (userAPIKey !== undefined && projectID !== undefined) {

                async.waterfall([

                    function (callback) {

                        log.addContext("userAPIKey", userAPIKey);

                        _db.multiClient.get("kUserName:" + userAPIKey, function (err, userName) {

                            if (userName == undefined)
                                _db.getUserNameByUserAPIKey(userAPIKey, function (err, userName) {
                                    log.addContext("userName", userName);
                                    log.trace("Delete a designation - projectID: " + projectID + " - lotIndex: " + lotIndex + " - designationIndex: " + designationIndex);
                                });
                            else {
                                log.addContext("userName", userName);
                                log.trace("Delete a designation - projectID: " + projectID + " - lotIndex: " + lotIndex + " - designationIndex: " + designationIndex);
                            }
                        });

                        _db.getProjectByUAPIKeyAndProjectID(userAPIKey, projectID, function (err, project) {

                            if (err)
                                throw err;

                            callback(null, project);
                        });
                    },
                    function (project, callback) {
                        
                        lotName = project.metrics[lotIndex].name;
                        designationName = project.metrics[lotIndex].data[designationIndex].name;
                        delete project.metrics[lotIndex].data[designationIndex];

                        //
                        // Plomberie - Divers Separator
                        if (lotIndex == 11) {

                            var custom = Object.keys(project.metrics[lotIndex].data).filter(function (key) {
                                return project.metrics[lotIndex].data[key].isCustom;
                            });

                            if (custom.length == 0) {

                                diversSeparatorIndex = "SEP04";
                                delete project.metrics[lotIndex].data[diversSeparatorIndex];
                            }
                        }
                        
                        projectFields["metrics"] = project.metrics;

                        callback(null, userAPIKey, projectID, projectFields);
                    },
                    function (userAPIKey, projectID, projectFields, callback) {

                        _db.updateProject(userAPIKey, projectID, projectFields, function (err, res) {

                            callback(err, res.nModified);
                        });
                    }
                ], function (err, nModified) {

                    if (err)
                        res.json({ error: true });
                    else {
                        log.debug("nModified : " + nModified);

                        log.info("\"%s\" [Id : %s] in \"%s\" has been deleted", designationName, designationIndex, lotName);
                        res.json({ error: false, diversSeparatorIndex: diversSeparatorIndex });
                    }
                });
            }
        }
        catch (err) {
            log.error("Error occured" + " - " + err.message);
            res.json({ error: true, newValue: null });
        }
    },

    // #endregion
};

//#region setDefaultMetrics

function setDefaultMetrics(rateBase, projectType) {

    log.addContext("methodName", stackTrace.get()[0].getFunctionName());

    var metrics = {};
    var designations = {};
    var defaultMetrics = JSON.parse(JSON.stringify(_defaultMetrics));

    try {

        for (var lotIndex in defaultMetrics) {

            designations = defaultMetrics[lotIndex].designations;

            for (var designationIndex in designations) {

                for (var lotIndex2 in rateBase) {

                    for (var designationIndex2 in rateBase[lotIndex2].data) {

                        if (designationIndex2 == designationIndex) {
                            designations[designationIndex].name = rateBase[lotIndex2].data[designationIndex2].name;
                            designations[designationIndex].pricePerUnit = rateBase[lotIndex2].data[designationIndex2].pricePerUnit;

                            break;
                        }
                    }
                }
            }

            metrics[defaultMetrics[lotIndex].metricsID - 1] = {
                position: defaultMetrics[lotIndex].position,
                selected: defaultMetrics[lotIndex].selected,
                name: defaultMetrics[lotIndex].metricsName,
                data: designations
            };
        }

        //
        // Custom
        for (var lotIndex2 in rateBase) {

            var data = {};

            if (rateBase[lotIndex2].isCustom) {

                if (rateBase[lotIndex2].data) {

                    for (var designationIndex2 in rateBase[lotIndex2].data) {

                        if (rateBase[lotIndex2].data[designationIndex2].isCustom) {

                            data[designationIndex2] = rateBase[lotIndex2].data[designationIndex2];
                            data[designationIndex2].qte = 0;
                            data[designationIndex2].isEditable = true;
                        }
                    }
                }

                metrics[lotIndex2] = {

                    name: rateBase[lotIndex2].name,
                    selected: false,
                    isCustom: true,
                    data: data
                }
            }
            else {

                for (var designationIndex2 in rateBase[lotIndex2].data) {

                    if (rateBase[lotIndex2].data[designationIndex2].isCustom) {
                        // Plomberie
                        if (lotIndex2 == 11) {

                            metrics[lotIndex2].data["SEP04"] = {
                                name: "DIVERS",
                                isSeparator: true
                            };
                        }

                        data[designationIndex2] = rateBase[lotIndex2].data[designationIndex2];
                        data[designationIndex2].qte = 0;
                        data[designationIndex2].isEditable = true;

                        metrics[lotIndex2].data[designationIndex2] = data[designationIndex2];
                    }
                }
            }
        }

        //
        // All designation itemss are editable
        //log.trace(metrics);

        if (projectType === "RENOVATION") {
            for (var lotIndex in metrics) {
                
                designations = metrics[lotIndex].data;

                for (var designationIndex in designations)
                    designations[designationIndex].isEditable = true;
            }
        }
        
        return metrics;
    }
    catch (err) {
        log.error(new Error("Error occured" + " - " + err.message));
        return null;
    }
}

//#endregion

//#region setMetricCaracteristicsDefaultValues function

function setMetricCaracteristicsDefaultValues(metricsCaracteristics) {
    
    //
    // Quantitative
    for (var item in metricsCaracteristics.quantitative) {

        if (metricsCaracteristics.quantitative[item] == null) {
            metricsCaracteristics.quantitative[item] = 0;
        }
    }

    //
    // Caracteristics
    for (var item in metricsCaracteristics.caracteristics) {
        
        if (metricsCaracteristics.caracteristics[item] == null) {
            metricsCaracteristics.caracteristics[item] = 0;
        }
    }

    //
    // Fundation
    for (var item in metricsCaracteristics.fundation) {

        for (var subItem in metricsCaracteristics.fundation[item]) {

            if (metricsCaracteristics.fundation[item][subItem] == null)
                metricsCaracteristics.fundation[item][subItem] = 0;
        }
    }

    //
    // CrawlSpace
    for (var item in metricsCaracteristics.crawlSpace) {

        for (var subItem in metricsCaracteristics.crawlSpace[item]) {

            if (metricsCaracteristics.crawlSpace[item][subItem] == null)
                metricsCaracteristics.crawlSpace[item][subItem] = 0;
        }
    }

    if (metricsCaracteristics.crawlSpace.stiffenersStakeCrawlSpace == null)
        metricsCaracteristics.crawlSpace.stiffenersStakeCrawlSpace = 0;

    if (metricsCaracteristics.crawlSpace.beamCrawlSpace == null)
        metricsCaracteristics.crawlSpace.beamCrawlSpace = 0;

    if (metricsCaracteristics.crawlSpace.postCrawlSpace == null)
        metricsCaracteristics.crawlSpace.postCrawlSpace = 0;

    //
    // Ground elevation
    for (var item in metricsCaracteristics.groundElevation) {

        for (var subItem in metricsCaracteristics.groundElevation[item]) {

            if (metricsCaracteristics.groundElevation[item][subItem] == null)
                metricsCaracteristics.groundElevation[item][subItem] = 0;
        }
    }

    if (metricsCaracteristics.groundElevation.stiffenersStakeGroundElevation == null)
        metricsCaracteristics.groundElevation.stiffenersStakeGroundElevation = 0;

    if (metricsCaracteristics.groundElevation.beamGroundElevation == null)
        metricsCaracteristics.groundElevation.beamGroundElevation = 0;

    if (metricsCaracteristics.groundElevation.postGroundElevation == null)
        metricsCaracteristics.groundElevation.postGroundElevation = 0;

    //
    // First elevation
    for (var item in metricsCaracteristics.firstElevation) {

        for (var subItem in metricsCaracteristics.firstElevation[item]) {

            if (metricsCaracteristics.firstElevation[item][subItem] == null)
                metricsCaracteristics.firstElevation[item][subItem] = 0;
        }
    }

    if (metricsCaracteristics.firstElevation.stiffenersStakeFirstElevation == null)
        metricsCaracteristics.firstElevation.stiffenersStakeFirstElevation = 0;

    if (metricsCaracteristics.firstElevation.beamFirstElevation == null)
        metricsCaracteristics.firstElevation.beamFirstElevation = 0;

    if (metricsCaracteristics.firstElevation.postFirstElevation == null)
        metricsCaracteristics.firstElevation.postFirstElevation = 0;

    //
    // Second Elevation
    for (var item in metricsCaracteristics.secondElevation) {

        for (var subItem in metricsCaracteristics.secondElevation[item]) {

            if (metricsCaracteristics.secondElevation[item][subItem] == null)
                metricsCaracteristics.secondElevation[item][subItem] = 0;
        }
    }
    
    if (metricsCaracteristics.secondElevation.stiffenersStakeSecondElevation == null)
        metricsCaracteristics.secondElevation.stiffenersStakeSecondElevation = 0;

    if (metricsCaracteristics.secondElevation.beamSecondElevation == null)
        metricsCaracteristics.secondElevation.beamSecondElevation = 0;

    if (metricsCaracteristics.secondElevation.postSecondElevation == null)
        metricsCaracteristics.secondElevation.postSecondElevation = 0;


    //
    // Gable
    for (var item in metricsCaracteristics.gable) {

        if (metricsCaracteristics.gable[item] == null) {
            if (item == "slope")
                metricsCaracteristics.gable[item] = 0.33;
            else
                metricsCaracteristics.gable[item] = 0;
        }
    }
}

//#endregion

//#region calcMetrics function

function calcMetrics (metrics, metricsCaracteristics, projectType) {

    if (projectType === "NEUF") {
        /**
         * GROS ŒUVRE
         */
        var C9 = metrics[3]['data']["IMP01"];

        var C11 = metrics[3]['data']["GRO01"];
        var C12 = metrics[3]['data']["GRO02"];
        var C13 = metrics[3]['data']["GRO03"];
        var C14 = metrics[3]['data']["GRO04"];
        var C17 = metrics[3]['data']["GRO07"];
        var C18 = metrics[3]['data']["GRO08"];
        var C20 = metrics[3]['data']["GRO10"];
        var C21 = metrics[3]['data']["GRO11"];
        var C22 = metrics[3]['data']["GRO12"];
        var C24 = metrics[3]['data']["GRO14"];
        var C25 = metrics[3]['data']["GRO15"];
        var C26 = metrics[3]['data']["GRO16"];
        var C27 = metrics[3]['data']["GRO17"];
        var C28 = metrics[3]['data']["GRO18"];
        var C29 = metrics[3]['data']["GRO19"];
        var C31 = metrics[3]['data']["EMB01"];
        var C32 = metrics[3]['data']["EMB02"];
        var C33 = metrics[3]['data']["EMB03"];
        var C35 = metrics[3]['data']["DIV01"];
        var C36 = metrics[3]['data']["DIV02"];
        var C37 = metrics[3]['data']["DIV03"];
        //var C38 = metrics[3]['data']["DIV04"];
        var C39 = metrics[3]['data']["DIV04"];
        //var C40 = metrics[3]['data']["DIV06"];

        C11.qte = utils.sum(metricsCaracteristics.fundation.volumeFundation);
        C12.qte = utils.sum(metricsCaracteristics.crawlSpace.surfaceCrawlSpace);
        //C13.qte = utils.sum(metricsCaracteristics.crawlSpace.stiffenersStakeCrawlSpace) * 0.024;
        C13.qte = metricsCaracteristics.crawlSpace.stiffenersStakeCrawlSpace * 0.024;
        C14.qte = utils.sum(metricsCaracteristics.fundation.lengthFundation);

        C18.qte = metricsCaracteristics.caracteristics.surfaceShedSlab;
        C17.qte = C18.qte * 0.2;

        C20.qte = metricsCaracteristics.quantitative.crawlspaceFloor;
        C21.qte = metricsCaracteristics.quantitative.roofTerraceFloor;
        C22.qte = metricsCaracteristics.quantitative.rPlusOneFloor;

        C24.qte = (utils.sum(metricsCaracteristics.groundElevation.lengthGroundElevation) + utils.sum(metricsCaracteristics.firstElevation.lengthFirstElevation) + utils.sum(metricsCaracteristics.secondElevation.lengthSecondElevation)) * 0.04;

        var gableLengthHalf = metricsCaracteristics.gable.lengthTotal / 2;
        var gableSurface = parseFloat((gableLengthHalf * metricsCaracteristics.gable.slope * gableLengthHalf).toFixed(2));
        C25.qte = utils.sum(metricsCaracteristics.groundElevation.surfaceGroundElevation) + utils.sum(metricsCaracteristics.firstElevation.surfaceFirstElevation)
            + utils.sum(metricsCaracteristics.secondElevation.surfaceSecondElevation) + (gableSurface * metricsCaracteristics.gable.number);

        C26.qte = (metricsCaracteristics.groundElevation.stiffenersStakeGroundElevation + metricsCaracteristics.firstElevation.stiffenersStakeFirstElevation + metricsCaracteristics.secondElevation.stiffenersStakeSecondElevation) * 0.108;
        C27.qte = C24.qte;

        C36.qte = (metricsCaracteristics.crawlSpace.beamCrawlSpace
            + metricsCaracteristics.groundElevation.beamGroundElevation
            + metricsCaracteristics.firstElevation.beamFirstElevation
            + metricsCaracteristics.secondElevation.beamSecondElevation) * 0.08;

        C37.qte = (metricsCaracteristics.crawlSpace.postCrawlSpace
            + metricsCaracteristics.groundElevation.postGroundElevation
            + metricsCaracteristics.firstElevation.postFirstElevation
            + metricsCaracteristics.secondElevation.postSecondElevation) * 0.243;

        C39.qte = metricsCaracteristics.quantitative.acroterion;

        /**
         * CHARPENTE / COUVERTURE (SET)
         */
        var C87 = metrics[4]['data']["CHA01"];
        C87.qte = metricsCaracteristics.caracteristics.surfaceFrame - metricsCaracteristics.caracteristics.surfaceFrameTypeGarage;

        var C89 = metrics[4]['data']["CHA03"];
        C89.qte = metricsCaracteristics.quantitative.shores;

        var C90 = metrics[4]['data']["CHA04"];
        C90.qte = metricsCaracteristics.quantitative.faitage;

        var C97 = metrics[4]['data']["CHA11"];
        C97.qte = metricsCaracteristics.caracteristics.surfaceFrameTypeGarage;

        var C88 = metrics[4]['data']["CHA02"];
        C88.qte = (parseFloat(C87.qte) + parseFloat(C97.qte)) * 1.16;

        /**  
         * CARRELAGE / FAIENCE
         */
        var C125 = metrics[13]['data']["CAR01"];
        var C126 = metrics[13]['data']["CAR02"]
        var C127 = metrics[13]['data']["CAR03"];
        var C128 = metrics[13]['data']["CAR04"];
        var C130 = metrics[13]['data']["CAR06"];

        C125.qte = (metricsCaracteristics.caracteristics.surfaceRDC + metricsCaracteristics.caracteristics.surfaceRDplus) * 1.05;
        C126.qte = (metricsCaracteristics.caracteristics.surfaceRDC + metricsCaracteristics.caracteristics.surfaceRDplus) * 1.05;
        C127.qte = C125.qte;
        C128.qte = C125.qte;

        /**
         *  RACCORDEMENT RESEAUX
         */
        var C144 = metrics[2]['data']["RAC01"];
        C144.qte = utils.sum(metricsCaracteristics.fundation.lengthFundation);

        var C145 = metrics[2]['data']["RAC02"];

        C9.qte = C145.qte;

        /**
         *  RAVALEMENT DE FACADE
         */
        var C158 = metrics[14]['data']["RAV01"];
        var C159 = metrics[14]['data']["RAV02"];
        var C160 = metrics[14]['data']["RAV03"];

        if (metricsCaracteristics.caracteristics.coating == 0) {
            C158.qte = parseFloat(C12.qte) + parseFloat(C25.qte) + parseFloat(C39.qte);
            C159.qte = 0;
            C160.qte = 0;
        }
        else if (metricsCaracteristics.caracteristics.coating == 1) {
            C158.qte = 0;
            C159.qte = parseFloat(C12.qte) + parseFloat(C25.qte) + parseFloat(C39.qte);
            C160.qte = 0;
        }
        else if (metricsCaracteristics.caracteristics.coating == 2) {
            C159.qte = 0;
            C158.qte = 0;
            C160.qte = parseFloat(C12.qte) + parseFloat(C25.qte) + parseFloat(C39.qte);
        }

        /**
         *  CLOISONS / DOUBLAGE
         */
        var C174 = metrics[9]['data']["CDF01"];
        //var C175 = metrics[9]['data']["CDF02"];
        var C177 = metrics[9]['data']["CDD01"];
        //var C178 = metrics[9]['data']["CDD02"];
        var C180 = metrics[9]['data']["CDC01"];
        var C181 = metrics[9]['data']["CDC02"];
        var C183 = metrics[9]['data']["CDC04"];
        var C185 = metrics[9]['data']["CDC06"];
        C174.qte = (metricsCaracteristics.caracteristics.surfaceRDC + metricsCaracteristics.caracteristics.surfaceRDplus) * 1.1;
        C177.qte = metricsCaracteristics.quantitative.dubbing;
        C180.qte = metricsCaracteristics.quantitative.partitions;
        C181.qte = C130.qte;

        /**
         *  ETANCHEITE
         */
        var C199 = metrics[7]['data']["ETA01"];
        var C200 = metrics[7]['data']["ETA02"];

        if (metricsCaracteristics.caracteristics.waterproofing == 0) {
            C199.qte = parseFloat(C21.qte) + parseFloat(C39.qte);
            C200.qte = 0;
        }
        else {
            C199.qte = 0;
            C200.qte = parseFloat(C21.qte) + parseFloat(C39.qte);
        }

        /**
         *  ELECTRICITE
         */
        var C214 = metrics[10]['data']["ELS01"];
        var C215 = metrics[10]['data']["ELS02"];
        var C216 = metrics[10]['data']["ELS03"];
        var C217 = metrics[10]['data']["ELS04"];
        var C218 = metrics[10]['data']["ELS05"];
        var C222 = metrics[10]['data']["ELC01"];
        var C223 = metrics[10]['data']["ELC02"];
        var C224 = metrics[10]['data']["ELC03"];
        var C225 = metrics[10]['data']["ELC04"];
        var C226 = metrics[10]['data']["ELC05"];
        var C230 = metrics[10]['data']["ELCD01"];
        var C234 = metrics[10]['data']["ELCD05"];
        var C236 = metrics[10]['data']["ELWC01"];
        var C238 = metrics[10]['data']["ELCB01"];
        var C239 = metrics[10]['data']["ELCB02"];
        var C240 = metrics[10]['data']["ELCB03"];
        var C241 = metrics[10]['data']["ELCB04"];
        var C245 = metrics[10]['data']["ELSB01"];
        var C246 = metrics[10]['data']["ELSB02"];
        var C247 = metrics[10]['data']["ELSB03"];
        var C248 = metrics[10]['data']["ELSB04"];
        var C249 = metrics[10]['data']["ELSB05"];
        var C253 = metrics[10]['data']["ELDEG01"];
        var C254 = metrics[10]['data']["ELDEG02"];
        var C259 = metrics[10]['data']["ELGAR01"];
        var C260 = metrics[10]['data']["ELGAR02"];
        var C263 = metrics[10]['data']["ELDIV01"];
        var C264 = metrics[10]['data']["ELDIV02"];
        var C265 = metrics[10]['data']["ELDIV03"];
        var C266 = metrics[10]['data']["ELDIV04"];
        var C267 = metrics[10]['data']["ELDIV05"];
        var C268 = metrics[10]['data']["ELDIV06"];
        var C269 = metrics[10]['data']["ELDIV07"];
        var C270 = metrics[10]['data']["ELDIV08"];
        var C271 = metrics[10]['data']["ELDIV09"];
        var C272 = metrics[10]['data']["ELDIV10"];

        C214.qte = Math.round(metricsCaracteristics.caracteristics.surfaceLiving / 4);
        C215.qte = metricsCaracteristics.caracteristics.living;
        C216.qte = parseFloat(C215.qte);
        C217.qte = parseFloat(C216.qte);
        C218.qte = parseFloat(C217.qte);
        C222.qte = metricsCaracteristics.caracteristics.kitchen * 6;
        C223.qte = metricsCaracteristics.caracteristics.kitchen;
        C224.qte = parseFloat(C223.qte);
        C225.qte = parseFloat(C224.qte);
        C226.qte = parseFloat(C225.qte);
        C230.qte = metricsCaracteristics.caracteristics.cellar;
        C234.qte = metricsCaracteristics.caracteristics.cellar;
        C236.qte = metricsCaracteristics.caracteristics.sanitWC;
        C238.qte = (metricsCaracteristics.caracteristics.bedroom + metricsCaracteristics.caracteristics.office) * 3;
        C239.qte = metricsCaracteristics.caracteristics.bedroom + metricsCaracteristics.caracteristics.office;
        C240.qte = metricsCaracteristics.caracteristics.bedroom + metricsCaracteristics.caracteristics.office;
        C241.qte = metricsCaracteristics.caracteristics.bedroom + metricsCaracteristics.caracteristics.office;
        C245.qte = metricsCaracteristics.caracteristics.sanitSDB;
        C246.qte = metricsCaracteristics.caracteristics.sanitSDB;
        C247.qte = metricsCaracteristics.caracteristics.sanitSDB;
        C248.qte = metricsCaracteristics.caracteristics.sanitSDB;
        C249.qte = metricsCaracteristics.caracteristics.sanitSDB;
        C253.qte = metricsCaracteristics.caracteristics.area;
        C254.qte = metricsCaracteristics.caracteristics.area;
        C259.qte = metricsCaracteristics.caracteristics.garage * 3;
        C260.qte = metricsCaracteristics.caracteristics.garage;

        C263.qte = C29.qte;
        C264.qte = metricsCaracteristics.caracteristics.various;
        C265.qte = metricsCaracteristics.caracteristics.various;
        C266.qte = metricsCaracteristics.caracteristics.various;
        C267.qte = metricsCaracteristics.caracteristics.various;
        C268.qte = metricsCaracteristics.caracteristics.various;
        C269.qte = metricsCaracteristics.caracteristics.various;
        C270.qte = metricsCaracteristics.caracteristics.various;
        C271.qte = metricsCaracteristics.caracteristics.various;
        C272.qte = metricsCaracteristics.caracteristics.various;

        /**
         *  PLOMBERIE
         */
        var C285 = metrics[11]['data']["PLOAL04"];
        var C286 = metrics[11]['data']["PLOAL05"];
        var C296 = metrics[11]['data']["PLOAP06"]
        C285.qte = metricsCaracteristics.caracteristics.sanitSDB;
        C286.qte = metricsCaracteristics.caracteristics.sanitWC;
        C296.qte = C286.qte;

        /**
         *  MENUISERIES EXTERIEURES FOURNITURE ET POSE
         */
        var carpentyData = metrics[8]['data'];
        var sumVR = 0;
        var sumLinteau = 0;
        var sumTableaux = 0;
        var sumSeuils = 0;

        for (var elemIndex in carpentyData) {

            carpentyData[elemIndex].qte = parseFloat(metrics[8]['data'][elemIndex].Q);

            sumVR += carpentyData[elemIndex].VR != null ? parseFloat(carpentyData[elemIndex].VR) : 0;
            sumLinteau += carpentyData[elemIndex].linteau != null ? parseFloat(carpentyData[elemIndex].linteau) : 0;
            sumTableaux += carpentyData[elemIndex].tableaux != null ? parseFloat(carpentyData[elemIndex].tableaux) : 0;
            sumSeuils += carpentyData[elemIndex].seuils != null ? parseFloat(carpentyData[elemIndex].seuils) : 0;
        }

        C28.qte = sumLinteau;
        C29.qte = sumVR;
        C31.qte = sumTableaux;
        C32.qte = sumSeuils;
        C33.qte = parseFloat(carpentyData["MENEXL16"].seuils) + parseFloat(carpentyData["MENEXL17"].seuils);

        /**
         *  PEINTURE INTERIEURE
         */
        var C370 = metrics[15]['data']["PEINT01"];
        var C371 = metrics[15]['data']["PEINT02"];
        var C372 = metrics[15]['data']["PEINT03"];
        var C373 = metrics[15]['data']["PEINT04"];

        C370.qte = C174.qte;
        C371.qte = parseFloat(C177.qte) + (parseFloat(C180.qte) * 2);
        C372.qte = parseFloat(C183.qte) + parseFloat(C185.qte);
        C373.qte = C181.qte;

        /**
         *  DÉBORD DE TOIT
         */

        var C425 = metrics[6]['data']["DEB05"];
        C425.qte = metricsCaracteristics.quantitative.underPvcFace;

        /**
         *  ISOLATION DES COMBLES
         */
        var C436 = metrics[16]['data']["ISOL01"];
        //C436.qte = (parseFloat(metricsCaracteristics.caracteristics.surfaceRDC) + parseFloat(metricsCaracteristics.caracteristics.surfaceRDplus)) - parseFloat(metricsCaracteristics.caracteristics.surfaceUnder);
        C436.qte = parseFloat(metricsCaracteristics.caracteristics.surfaceFrame) - parseFloat(metricsCaracteristics.caracteristics.surfaceFrameTypeGarage) - parseFloat(metricsCaracteristics.caracteristics.surfaceUnder);
    }
        
    if (projectType === "RENOVATION") {

        /**
         *  MENUISERIES EXTERIEURES FOURNITURE ET POSE
         */
        var carpentyData = metrics[8]['data'];

        for (var elemIndex in carpentyData)
            carpentyData[elemIndex].qte = parseFloat(metrics[8]['data'][elemIndex].Q);
    }

    //
    //
    for (var indexLot in metrics) {
        //var lotName = metrics[indexLot].name;
        var lotData = metrics[indexLot].data;

        for (var indexEl in lotData) {
            var elData = lotData[indexEl];

            if (elData != null && !elData.isSeparator) {
                elData.totalHT = elData.qte * elData.pricePerUnit;
                if(!elData.vat) {
                    if(projectType === "NEUF")
                        elData.vat = 20
                    if(projectType === "RENOVATION")
                        elData.vat = 10
                }
                elData.totalTTC = elData.vat ? (elData.totalHT * elData.vat) / 100 + elData.totalHT  : 0;
            }
        }
    }
}

//#endregion

module.exports = metricController;
module.id = "metricController";