﻿"use strict"

var log = require('log4js').getLogger(require("path").basename(__filename, ".js"));
var utils = require("../utils/utils");

setTimeout(function () {
     log.addContext("methodName", stackTrace.get()[0].getMethodName());
    log.trace('Report Controller Loaded.');
}, 500);

var appSettings = config.settings();
var path = require("path");
var fs = require("fs-extra");
var moment = require('moment');
let uriObject = require("mongodb-uri").parse(appSettings.CONN_STR_JSREPORT);
let client = require("jsreport-client")(appSettings.URL_JSREPORT, uriObject["username"], uriObject["password"]);

var reportController = {
    
    reportController: function () {
        log.trace("Report Controller");
    },

    // #region getQuoteReport function

    /**
     *
     * 
     * 
     */
    getQuoteReport: function (res, userLastName, userID, reportData) {

         log.addContext("methodName", stackTrace.get()[0].getFunctionName());

         try {
             
             log.trace("Get Quote report - userID : %s - userLastName : %s", userID, userLastName);

             var today = new Date();
             var folderName = userLastName + "-" + userID;
             //var folderNamePath = path.join(__dirname, "/../../cloud/quotes/", folderName, moment(today).format("YYYYMMDD"));
             var folderNamePath = path.join(__dirname, "/../../cloud/quotes/", folderName);

             fs.ensureDirSync(folderNamePath)

             var projectName = JSON.parse(reportData).projectName.replace(" ", "_");
             //var fileName = "devis_" + userLastName.toLowerCase() + "_" + projectName.toLowerCase() + "_" + moment(today).format("YYYYMMDD") + ".pdf"
             var fileName = "devis_" + userLastName.toLowerCase() + "_" + projectName.toLowerCase() + ".pdf"
             var fileNamePath = path.join(folderNamePath, fileName);

             client.render(
                 {
                     template: {
                         name: "quote",
                         recipe: "phantom-pdf",
                         engine: "pug"
                     },

                     data: reportData

                 }).then((response) => {

                     //if (err)
                     //   log.error("Error occured" + " - " + err.message);

                     //res.setHeader("Content-Type", "application/pdf");
                     //res.setHeader("Content-Disposition", "inline;filename=devis.pdf");
                     //res.setHeader("Content-Disposition", "inline");

                     var fileStream = fs.createWriteStream(fileNamePath)
                     response.pipe(fileStream);

                     fileStream.on("finish", function () {
                         //res.json({ error: false, urlFile: "/cloud/quotes/" + folderName + "/" + moment(today).format("YYYYMMDD") + "/" + fileName });
                         res.json({ error: false, urlFile: "/cloud/quotes/" + folderName + "/" + fileName });
                     });

                 });
         }
         catch (err) {
             log.error("Error occured" + " - " + err.message);
             res.json({ error: true });
         }
    },

    // #endregion
}

module.exports = reportController;
module.id = "reportController";