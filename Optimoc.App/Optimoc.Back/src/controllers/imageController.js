﻿"use strict"

var _db;
var async = require("async");
var shortid = require('shortid');
var mime = require('mime');
var utils = require("../utils/utils");
var path = require('path');
var log = require("log4js").getLogger(require("path").basename(__filename, ".js"));

setTimeout(function () {

    _db = require('../db/db');
     log.addContext("methodName", stackTrace.get()[0].getMethodName());
    log.trace("Image Controller Loaded.");

}, 1000);

var imageController = {
    
    /**
     * 
     *
     * 
     * 
     * 
     */
    updateImage: function (res, userAPIKey, data) {

        log.addContext("methodName", stackTrace.get()[0].getFunctionName());
		var date, isoDate;
		
        try {

            async.waterfall([
                function (callback) {

                    if (data != null) {

                        log.addContext("userAPIKey", userAPIKey);

                        _db.multiClient.get("kUserName:" + userAPIKey, function (err, userName) {

                            if (userName == undefined)
                                _db.getUserNameByUserAPIKey(userAPIKey, function (err, userName) {
                                    log.addContext("userName", userName);
                                    log.trace("Update image");
                                });
                            else {
                                log.addContext("userName", userName);
                                log.trace("Update image");
                            }
                        });

                        date = new Date();
                        //var isoDate = new Date(date.getTime() - (date.getTimezoneOffset() * 60000)).toISOString();
                        isoDate = new Date(date.getTime() - (date.getTimezoneOffset() * 60000));

                        var image = {
                            "name": path.basename(data.fileName, path.extname(data.fileName)),
                            "contentType": mime.lookup(data.fileName),
                            "content":  Buffer.from(utils.base64_encode(data.path), "base64"),
                            "shortid": shortid.generate(),
                            "creationDate": isoDate,
                            "modificationDate": isoDate
                        }

                        _db.updateImage(image, function (err, res) {
                            callback(err, res.nModified);
                        });
                    }
                    else
                        throw new Error("Data conataining image informations is missing.");
                },
				function (nModifiedImage, callback) {

                    //if (data != null) {

                        date = new Date();
                        isoDate = new Date(date.getTime() - (date.getTimezoneOffset() * 60000));

                        var asset = {
                            "name": path.basename(data.fileName, path.extname(data.fileName)) + ".jpeg",
                            "content":  Buffer.from(utils.base64_encode(data.path), "base64"),
                            "shortid": shortid.generate(),
                            "modificationDate": isoDate
                        }

                        _db.updateAsset(asset, function (err, res) {
                            callback(err, nModifiedImage, res.nModified);
                        });
                    //}
                }
            ], function (err, nModifiedImage, nModifiedAsset) {

                if (err)
                    throw err;

                log.debug("nModifiedImage : " + nModifiedImage + " - nModifiedAsset : " + nModifiedAsset);
                res.json({ error: false, data: { "nModifiedImage" : nModifiedImage, "nModifiedAsset" : nModifiedAsset }});
            });
        }
        catch (err) {
            log.error("Error occured" + " - " + err.message);
            res.json({ error: true, data: null });
        }
    }
};

module.exports = imageController;
module.id = "imageController";