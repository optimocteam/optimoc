﻿"use strict"

var _db;
var async = require("async");
var log = require("log4js").getLogger(require("path").basename(__filename, ".js"));
var moment = require('moment');
var json2csv = require('json2csv');
var fs = require('fs');

setTimeout(function () {

    _db = require('../db/db');
     log.addContext("methodName", stackTrace.get()[0].getMethodName());
    log.trace("RateBase Controller Loaded.");

}, 1000);

var rateBaseController = {

    /**
     * Gives rate bases of a user.
     *
     * @method getRateBases
     * @param {Object} data A data object with userAPIkey
     * @return {Object} Returns user Object or null
     */
    getRateBases: function (res, data) {

        log.addContext("methodName", stackTrace.get()[0].getFunctionName());
        
        try {

            var keys = Object.keys(data);

            async.waterfall([

                function (callback) {

                    if (!!~keys.indexOf("userAPIKey")) {

                        log.addContext("userAPIKey", data["userAPIKey"]);

                        _db.multiClient.get("kUserName:" + data["userAPIKey"], function (err, userName) {

                            if (userName == undefined)
                                _db.getUserNameByUserAPIKey(data["userAPIKey"], function (err, userName) {
                                    log.addContext("userName", userName);
                                    log.trace("Get all rate bases");
                                });
                            else {
                                log.addContext("userName", userName);
                                log.trace("Get all rate bases");
                            }
                        });

                        _db.getRateBasesByUserAPIKey(data["userAPIKey"], function (err, data) {

                            if (err)
                                throw err;

                            callback(null, data);
                        });
                    }
                },
                function (rateBases, callback) {

                    callback(null, rateBases);
                }
            ], function (err, rateBases) {

                res.json({ error: false, data: rateBases });

            });
        }
        catch (err) {
            log.error("Error occured" + " - " + err.message);
            res.json({ error: true });
        }
    },

    /**
     * Gives rate base of a user in csv format.
     *
     * @method getRateBaseInCSV
     * @param {Object} data A data object with userAPIkey
     * @return {Object} Returns csv file
     */
    getRateBaseInCSV: function (res, data) {

        log.addContext("methodName", stackTrace.get()[0].getFunctionName());
        
        try {

            var userAPIKey = data.userAPIKey;
            var rateBaseName = data.rateBaseName == undefined ? "" : data.rateBaseName;

            if (userAPIKey !== undefined) {

                async.waterfall([

                    function (callback) {

                        log.addContext("userAPIKey", userAPIKey);

                        _db.multiClient.get("kUserName:" + userAPIKey, function (err, userName) {

                            if (userName == undefined)
                                _db.getUserNameByUserAPIKey(userAPIKey, function (err, userName) {
                                    log.addContext("userName", userName);
                                    log.trace("Get rate base in csv file - rateBaseName : " + rateBaseName);
                                });
                            else {
                                log.addContext("userName", userName);
                                log.trace("Get rate base in csv file - rateBaseName : " + rateBaseName);
                            }
                        });

                        _db.getRateBaseByUserAPIKey(userAPIKey, rateBaseName, function (err, data) {

                            if (err)
                                throw err;

                            callback(null, data);
                        });
                    },
                    function (rateBase, callback) {

                        //
                        // Remove lot "Menuiseries exterieures"
                        delete rateBase[8];

                        var rateBaseArray = Object.keys(rateBase).map(lotId => {

                            var obj = new Object();

                            for (var prop in rateBase[lotId]) {

                                if (prop === "data") {

                                    obj[prop] = [];

                                    Object.keys(rateBase[lotId][prop]).map(designationId => {

                                        var obj2 = new Object();

                                        for (var prop2 in rateBase[lotId][prop][designationId])
                                            if (prop2 === "pricePerUnit")
                                                obj2[prop2] = rateBase[lotId][prop][designationId][prop2].toFixed(2).replace(".", ",");
                                            else
                                                obj2[prop2] = rateBase[lotId][prop][designationId][prop2];

                                        obj[prop].push(obj2);
                                    });
                                }
                                else
                                    obj[prop] = rateBase[lotId][prop];
                            }

                            return obj;
                        });

                        //
                        //
                        var fields = ["name", "data.name", "data.unit", "data.pricePerUnit"];
                        var fieldNames = ["Lot", "Désignation", "Unité", "P.U HT"];
                        json2csv({ data: rateBaseArray, fields: fields, fieldNames: fieldNames, del: ";", unwindPath: ["data"] }, function (err, csv) {

                            if (err)
                                res.json({ error: false });

                            res
                                //.setHeader('Content-disposition', 'attachment; filename=data.csv');
                                .set({ "Content-Type": "text/csv" })
                                .status(200)
                                .send(csv);

                            //callback(null);
                        });

                    }
                ]);
            }
        }
        catch (err) {
            log.error("Error occured" + " - " + err.message);
            res.json({ error: true });
        }
    },

    // #region updateRateBase function

    /**
     * Given userAPIKey, colName, pk, value and update rateBase.
     * @method updateRateBase
     * @param {Object} res : The response object.
     * @param {Object} data : A data object with userAPIKey, colName, pk, value.
     * @return {Object} Returns  error(true or false) / null Object 
     */
    updateRateBase: function (res, data) {

        log.addContext("methodName", stackTrace.get()[0].getFunctionName());
        
        try {

            var userAPIKey = data.userAPIKey;
            var colName = data.name == undefined ? "" : data.name;
            var pk = data.pk == undefined ? "" : data.pk;
            var value = data.value == undefined ? "" : data.value.replace(",", ".");
            var rateBaseName = data.rateBaseName == undefined ? "" : data.rateBaseName;
            var lotIndex, designationIndex, updateParam = new Object();

            if (userAPIKey !== undefined) {

                async.waterfall([

                    function (callback) {

                        log.addContext("userAPIKey", userAPIKey);

                        _db.multiClient.get("kUserName:" + userAPIKey, function (err, userName) {

                            if (userName == undefined)
                                _db.getUserNameByUserAPIKey(userAPIKey, function (err, userName) {
                                    log.addContext("userName", userName);
                                    log.trace("Update rate base - rateBaseName : " + rateBaseName + " - column name : " + colName + " - pk : " + pk + " - value : " + value);
                                });
                            else {
                                log.addContext("userName", userName);
                                log.trace("Update rate base - rateBaseName : " + rateBaseName + " - column name : " + colName + " - pk : " + pk + " - value : " + value);
                            }
                        });

                        /*for (var index1 in rateBase) {

                            if (index1 == pk && colName == "lotName") {

                                lotIndex = index1;
                                rateBase[index1].name = value;
                                break;
                            }

                            for (var index2 in rateBase[index1].data) {

                                if (index2 == pk) {

                                    lotIndex = index1;
                                    designationIndex = index2;

                                    switch (colName) {

                                        case "pu":
                                            rateBase[index1].data[index2].pricePerUnit = parseFloat(value);
                                            break;

                                        case "designation":
                                            rateBase[index1].data[index2].name = value;
                                            break;

                                        case "unit":
                                            rateBase[index1].data[index2].unit = value;
                                            break;
                                    }
                                }
                            }
                        }*/

                        if (colName == "lotName") {

                            lotIndex = pk;
                            updateParam["rateBases.$.rates." + lotIndex + ".name"] = value;
                        }
                        else {

                            if (data.lotId != undefined) {

                                lotIndex = data.lotId;
                                designationIndex = pk;

                                switch (colName) {

                                    case "pu":
                                        updateParam["rateBases.$.rates." + lotIndex + ".data." + pk + ".pricePerUnit"] = parseFloat(value);
                                        break;

                                    case "designation":
                                        updateParam["rateBases.$.rates." + lotIndex + ".data." + pk + ".name"] = value;
                                        break;

                                    case "unit":
                                        updateParam["rateBases.$.rates." + lotIndex + ".data." + pk + ".unit"] = value;
                                        break;
                                }
                            }
                            else
                                throw new Error("updateRateBase - missing lotId");
                        }

                        callback(null, updateParam);
                    },
                    function (updateParam, callback) {

                        _db.updateRateBase(userAPIKey, rateBaseName, updateParam, "UPD", function (err, res) {

                            callback(err, res.nModified);
                        });
                    }
                ], function (err, nModified) {

                    if (err)
                        res.json({ error: true });
                    else {

                        if (nModified == 1) {
                            log.debug("nModified : " + nModified);
                            res.json({ error: false, newValue: value, lotIndex: lotIndex, designationIndex: designationIndex });
                        }
                    }
                });
            }
        }
        catch (err) {
            log.error("Error occured" + " - " + err.message);
            res.json({ error: true });
        }
    },

    // #endregion

    // #region createLot function

    /**
     * 
     */
    createLot: function (res, data) {

        log.addContext("methodName", stackTrace.get()[0].getFunctionName());
        
        try {

            var userAPIKey = data.userAPIKey;
            var rateBaseName = data.rateBaseName == undefined ? "" : data.rateBaseName;
            var lotIndex, date = new Date();
            var newLot, updateParam = new Object();

            if (userAPIKey !== undefined) {

                async.waterfall([

                    function (callback) {

                        log.addContext("userAPIKey", userAPIKey);

                        _db.multiClient.get("kUserName:" + userAPIKey, function (err, userName) {

                            if (userName == undefined)
                                _db.getUserNameByUserAPIKey(userAPIKey, function (err, userName) {
                                    log.addContext("userName", userName);
                                    log.trace("Create a lot - rateBaseName : " + rateBaseName);
                                });
                            else {
                                log.addContext("userName", userName);
                                log.trace("Create a lot - rateBaseName : " + rateBaseName);
                            }
                        });

                        /*var maxIndex = -1;
                        var i = 0;

                        Object.keys(rateBase).map(function (key) {

                            if (parseInt(key) > parseInt(maxIndex)) {
                                
                                if (parseInt(key) + 1 == parseInt(i) + 1)
                                    maxIndex = key;
                            }
                            i++;
                        });

                        lotIndex = parseInt(maxIndex) + 1;*/

                        lotIndex = moment(new Date()).format("YYYYMMDDHHmmsss");

                        newLot = {
                            name: "Nouveau lot",
                            isCustom: true,
                            data: {}
                        };

                        updateParam["rateBases.$.rates." + lotIndex] = newLot;

                        callback(null, updateParam);
                    },
                    function (updateParam, callback) {

                        _db.updateRateBase(userAPIKey, rateBaseName, updateParam, "UPD", function (err, res) {

                            callback(err, res.nModified);
                        });
                    }
                ], function (err, nModified) {

                    if (err)
                        res.json({ error: true });
                    else {

                        if (nModified == 1) {

                            log.debug("nModified : " + nModified);

                            log.info("\"%s\" [Id : %s] has been created", newLot.name, lotIndex);
                            res.json({ error: false, lotIndex: lotIndex, newLot: newLot });
                        }
                    }
                });
            }
        }
        catch (err) {
            log.error("Error occured" + " - " + err.message);
            res.json({ error: true });
        }
    },

    // #endregion

    // #region deleteLot function

    /**
     * 
     */
    deleteLot: function (res, data) {

         log.addContext("methodName", stackTrace.get()[0].getFunctionName());

        try {

            var userAPIKey = data.userAPIKey;
            var rateBaseName = data.rateBaseName == undefined ? "" : data.rateBaseName;
            var lotIndex = data.lotIndex == undefined ? "" : data.lotIndex;
            var lotName = data.lotName == undefined ? "" : data.lotName;
            var updateParam = new Object();
            
            if (userAPIKey !== undefined) {

                async.waterfall([
                    
                    function (callback) {

                        log.addContext("userAPIKey", userAPIKey);

                        _db.multiClient.get("kUserName:" + userAPIKey, function (err, userName) {

                            if (userName == undefined)
                                _db.getUserNameByUserAPIKey(userAPIKey, function (err, userName) {
                                    log.addContext("userName", userName);
                                    log.trace("Delete a lot - rateBaseName : " + rateBaseName + " - lotName: " + lotName);
                                });
                            else {
                                log.addContext("userName", userName);
                                log.trace("Delete a lot - rateBaseName : " + rateBaseName + " - lotName: " + lotName );
                            }
                        });

                        updateParam["rateBases.$.rates." + lotIndex] = "";

                        callback(null, updateParam);
                    },
                    function (updateParam, callback) {

                        _db.updateRateBase(userAPIKey, rateBaseName, updateParam, "DEL", function (err, res) {

                            callback(err, res.nModified);
                        });
                    }
                ], function (err, nModified) {

                    if (err)
                        res.json({ error: true });
                    else {

                        if (nModified == 1) {
                            log.debug("nModified : " + nModified);
                            log.info("\"%s\" [Id : %s] has been deleted", lotName, lotIndex);

                            res.json({ error: false });
                        }
                    }
                });
            }
        }
        catch (err) {
            log.error("Error occured" + " - " + err.message);
            res.json({ error: true });
        }
    },

    // #endregion

    // #region createDesignation function

    /**
     * 
     */
    createDesignation: function (res, data) {

         log.addContext("methodName", stackTrace.get()[0].getFunctionName());

        try {

            var userAPIKey = data.userAPIKey;
            var rateBaseName = data.rateBaseName == undefined ? "" : data.rateBaseName;
            var lotIndex = data.lotIndex == undefined ? "" : data.lotIndex;
            var lotName = data.lotName == undefined ? "" : data.lotName;
            var newDesignation, designationIndex, designationName, updateParam = new Object();
                        
            if (userAPIKey !== undefined) {

                async.waterfall([
                    
                    function ( callback) {

                        log.addContext("userAPIKey", userAPIKey);

                        _db.multiClient.get("kUserName:" + userAPIKey, function (err, userName) {

                            if (userName == undefined)
                                _db.getUserNameByUserAPIKey(userAPIKey, function (err, userName) {
                                    log.addContext("userName", userName);
                                    log.trace("Create a designation - rateBaseName : " + rateBaseName + " - lotName: " + lotName);
                                });
                            else {
                                log.addContext("userName", userName);
                                log.trace("Create a designation - rateBaseName : " + rateBaseName + " - lotName: " + lotName);
                            }
                        });

                        // Electricite ou Plomberie
                        if (lotIndex == 10 || lotIndex == 11)
                            designationName = "Divers : Nouvelle désignation"
                        else
                            designationName = "Nouvelle désignation"

                        designationIndex = "LO_" + lotIndex + "_DE_" + moment(new Date()).format("YYYYMMDDHHmmsss");
                        
                        newDesignation = {
                            name: designationName,
                            unit: "U",
                            pricePerUnit: 0,
                            isCustom: true
                        };

                        //
                        // Menuiseries Extérieures
                        if (lotIndex == 8) {

                            newDesignation["largeur"] = 0;
                            newDesignation["hauteur"] = 0;
                            newDesignation["Q"] = 0;
                            newDesignation["VR"] = null;
                            newDesignation["linteau"] = 0;
                            newDesignation["tableaux"] = 0;
                            newDesignation["seuils"] = 0;
                        }

                        updateParam["rateBases.$.rates." + lotIndex + ".data." + designationIndex] = newDesignation;
                        
                        callback(null, updateParam);
                    },
                    function (updateParam, callback) {

                        _db.updateRateBase(userAPIKey, rateBaseName, updateParam, "UPD", function (err, res) {

                            callback(err, res.nModified);
                        });
                    }
                ], function (err, nModified) {

                    if (err)
                        res.json({ error: true });
                    else {

                        if (nModified == 1) {

                            log.debug("nModified : " + nModified);
                            log.info("\"%s\" [Id : %s] has been created", newDesignation.name, designationIndex);

                            res.json({ error: false, designationIndex: designationIndex, newDesignation: newDesignation });
                        }
                    }
                });
            }
        }
        catch (err) {
            log.error("Error occured" + " - " + err.message);
            res.json({ error: true });
        }
    },

    // #endregion

    // #region deleteDesignation function

    /**
     * 
     */
    deleteDesignation: function (res, data) {

         log.addContext("methodName", stackTrace.get()[0].getFunctionName());

        try {

            var userAPIKey = data.userAPIKey;
            var rateBaseName = data.rateBaseName == undefined ? "" : data.rateBaseName;
            var lotIndex = data.lotIndex == undefined ? "" : data.lotIndex;
            var designationIndex = data.designationIndex == undefined ? "" : data.designationIndex;
            var lotName = data.lotName == undefined ? "" : data.lotName;
            var designationName = data.designationName == undefined ? "" : data.designationName;
            var updateParam = new Object();
            
            if (userAPIKey !== undefined) {

                async.waterfall([
                    
                    function (callback) {

                        log.addContext("userAPIKey", userAPIKey);

                        _db.multiClient.get("kUserName:" + userAPIKey, function (err, userName) {

                            if (userName == undefined)
                                _db.getUserNameByUserAPIKey(userAPIKey, function (err, userName) {
                                    log.addContext("userName", userName);
                                    log.trace("Delete a designation - rateBaseName : " + rateBaseName + " - lotName: " + lotName + " - designationName: " + designationName);
                                });
                            else {
                                log.addContext("userName", userName);
                                log.trace("Delete a designation - rateBaseName : " + rateBaseName + " - lotName: " + lotName + " - designationName: " + designationName);
                            }
                        });

                        updateParam["rateBases.$.rates." + lotIndex + ".data." + designationIndex] = "";

                        callback(null, updateParam);
                    },
                    function (updateParam, callback) {

                        _db.updateRateBase(userAPIKey, rateBaseName, updateParam, "DEL", function (err, res) {

                            callback(err, res.nModified);
                        });
                    }
                ], function (err, nModified) {

                    if (err)
                        res.json({ error: true });
                    else {

                        if (nModified == 1) {

                            log.debug("nModified : " + nModified);
                            log.info("\"%s\" [Id : %s] in \"%s\" has been deleted", designationName, designationIndex, lotName);

                            res.json({ error: false });
                        }
                    }
                });
            }
        }
        catch (err) {
            log.error("Error occured" + " - " + err.message);
            res.json({ error: true });
        }
    },

    // #endregion
};

module.exports = rateBaseController;
module.id = "rateBaseController";