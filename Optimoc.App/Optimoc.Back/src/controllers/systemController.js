"use strict"

var _db, _users;
setTimeout(function(){
    _db = require('../db/db');
    _users = _db.users;
    console.log('System Controller Loaded.');
}, 500);

var systemController = {
    SystemController: function () {
        console.log("Sys Controller");
    },
    backupController: function () {
        return "Backup is a TODO things!";
    },
    cleanDatabase: function () {
        if (_users !== null)
            _users.removeDataOnly();
        return this.displayDatabase();
    },
    displayDatabase: function () {
        return { information: "Users Collection", database: _users };
    }
};

module.exports = systemController;
module.id = "systemController";