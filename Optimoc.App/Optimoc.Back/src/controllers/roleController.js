﻿"use strict"

var _db, _roles;
var log = require('log4js').getLogger(require("path").basename(__filename, ".js"));

setTimeout(function () {

     log.addContext("methodName", stackTrace.get()[0].getMethodName());
    _db = require('../db/db');

    //
    // Load roles collection
    _db.getRoles(function (err, roles) {

        if (err)
            throw err;

        _roles = roles;
        log.debug("DB : roles collection have been loaded");
    });
    
    log.trace('Role Controller Loaded.');
}, 3000);


var roleController = {
    roleController: function () {
        log.trace("Role Controller");
    },

    /**
     * @method getRoles
     * @return {Object} Returns roles object
     */
    getRoles: function (res) {

         log.addContext("methodName", stackTrace.get()[0].getFunctionName());

        try {
            
            res.json({ error: false, data: _roles });
        }
        catch (err) {
            log.error("Error occured" + " - " + err.message);
            res.json({ error: true, data: null });
        }
    }
};

module.exports = roleController;
module.id = "roleController";