"use strict"

var _db;
var async = require("async");
var log = require("log4js").getLogger(require("path").basename(__filename, ".js"));
var moment = require("moment");
let utils = require("../utils/utils");
const zlib = require("zlib");

setTimeout(function () {
    _db = require('../db/db');
     log.addContext("methodName", stackTrace.get()[0].getMethodName());
    log.trace('Project Controller Loaded.');
}, 500);

var projectController = {
    ProjectController: function () {
        log.trace("Project Controller");
    },

    /**
     * Given userAPIKey return one or many project.
     * @method getProjects
     * @param {Object} data A data object with userAPIKey
     * @return {Object} Returns projects Object or null
     */
    getProjects: function (res, data) {

        log.addContext("methodName", stackTrace.get()[0].getFunctionName());

        try {

            var keys = Object.keys(data);
            let projects, aggregate = new Object();
            
            async.waterfall([
                
                function (callback) {

                    if (!!~keys.indexOf("userAPIKey")) {

                        log.addContext("userAPIKey", data["userAPIKey"]);

                        log.addContext("userName", data.fullName);
                        log.trace("Get all projects");
                                                
                        _db.getProjectsByUAPIKey(data["userAPIKey"], function (err, projectsData) {

                            if (err)
                                throw err;

                            callback(null, projectsData);
                        });
                    }
                    else
                        callback(null, null);
                },
                function (projectsData, callback) {
                    
                    if (projectsData.length > 0) {
                        projects = projectsData;

                        aggregate = projects.reduce(function (groups, item) {
                            const val = item["projectStatus"];
                            const val2 = item["isArchived"];

                            groups[val] = groups[val] || {};
                            groups["ARCHIVE"] = groups["ARCHIVE"] || {};

                            if (!val2)
                                groups[val]["count"] = groups[val]["count"] + 1 || 1;
                            else
                                groups["ARCHIVE"]["count"] = groups["ARCHIVE"]["count"] + 1 || 1;

                            return groups
                        }, {});
                        
                        if (!aggregate.hasOwnProperty("AVANT_PROJET"))
                            aggregate["AVANT_PROJET"] = { count: 0 };

                        if (!aggregate.hasOwnProperty("EN_COURS"))
                            aggregate["EN_COURS"] = { count: 0 };

                        if (!aggregate.ARCHIVE.hasOwnProperty("count"))
                            aggregate.ARCHIVE.count = 0;
                    }
                    else {
                        projects = projectsData;
                        aggregate["AVANT_PROJET"] = { count: 0 };
                        aggregate["EN_COURS"] = { count: 0 };
                        aggregate["ARCHIVE"] = { count: 0 };
                    }

                    callback(null, projects, aggregate);
                }
            ], function (err, projects, aggregate) {

                res.status(200).json({ data: projects, aggregate: aggregate });
            });
        }
        catch (err) {
            log.error("Error occured" + " - " + err.message);
            res.status(500).json({ error_code: error.code });
        }
    },

    /**
     * Given userAPIKey and a projectID and update project.
     * @method updateProject
     * @param {Object} data A data object with userAPIKey and {string} ProjectID
     * @return {Object} Returns error either true or false, nModified
     */
    updateProject: function (res, data) {

         log.addContext("methodName", stackTrace.get()[0].getFunctionName());

        try {

            var keys = Object.keys(data);
            var projectFields = {};

            async.waterfall([
                
                function (callback) {

                    if (!!~keys.indexOf("userAPIKey") && !!~keys.indexOf("projectID")) {

                        log.addContext("userAPIKey", data["userAPIKey"]);

                        _db.multiClient.get("kUserName:" + data["userAPIKey"], function (err, userName) {

                            if (userName == undefined)
                                _db.getUserNameByUserAPIKey(data["userAPIKey"], function (err, userName) {
                                    log.addContext("userName", userName);
                                    log.trace("Update project - projectID : %s", data["projectID"]);
                                });
                            else {
                                log.addContext("userName", userName);
                                log.trace("Update project - projectID : %s", data["projectID"]);
                            }
                        });

                        Object.keys(data).map(prop => {

                            if (prop == "projectTax")
                                projectFields[prop] = parseFloat(data[prop]);
                            else if (prop !== "userAPIKey" && prop !== "projectID")
                                projectFields[prop] = data[prop]
                        });

                        log.trace("Fields to update : %s", JSON.stringify(projectFields));

                        callback(null, data["userAPIKey"], data["projectID"], projectFields);
                    }
                },
                function (userAPIKey, projectID, projectFields, callback) {

                    _db.updateProject(userAPIKey, projectID, projectFields, function (err, res) {
                        callback(err, res.nModified);
                    });
                }
            ], function (err, nModified) {

                if (err)
                    res.json({ error: true });
                else {

                    log.debug("nModified : " + nModified);

                    if (nModified == -1) {
                        log.warn("Project already exists with this name \"%s\"", projectFields["projectName"]);
                        res.json({ error: true, reason: "projectName_exist" });
                    }

                    else if (nModified == 1) {

                        log.info("Project with projectID %s has been updated", data["projectID"]);
                        res.json({ error: false });
                    }
                }
            });
        }
        catch (err) {
            log.error("Error occured" + " - " + err.message);
            res.json({ error: true });
        }
    },

     /**
     *
     */
    getDisclaimerText: function (res, data) {

        log.addContext("methodName", stackTrace.get()[0].getFunctionName());

        try {

            var keys = Object.keys(data);
            let projects, aggregate = new Object();

            async.waterfall([

                function (callback) {

                    if (!!~keys.indexOf("userAPIKey") && !!~keys.indexOf("projectID")) {

                        log.addContext("userAPIKey", data["userAPIKey"]);

                        _db.multiClient.get("kUserName:" + data["userAPIKey"], function (err, userName) {

                            if (userName == undefined)
                                _db.getUserNameByUserAPIKey(data["userAPIKey"], function (err, userName) {
                                    log.addContext("userName", userName);
                                    log.trace("Get disclaimer text");
                                });
                            else {
                                log.addContext("userName", userName);
                                log.trace("Get disclaimer text");
                            }
                        });

                        _db.getDisclaimerTextByUAPIKeyAndProjectID(data["userAPIKey"], data["projectID"], function (err, disclaimerText) {

                            if (err)
                                throw err;

                            callback(null, disclaimerText);
                        });
                    }
                    else
                        callback(null, null);
                },
            ], function (err, disclaimerText) {

                res.json({ error: false, disclaimerText: disclaimerText });
            });
        }
        catch (err) {
            log.error("Error occured" + " - " + err.message);
            res.json({ error: true, data: null });
        }
    },

    /**
     *
     */
    createProject: function (res, data) {

         log.addContext("methodName", stackTrace.get()[0].getFunctionName());

        try {

            var keys = Object.keys(data);
            var project, date = new Date();

            async.waterfall([

                function (callback) {

                    if (!!~keys.indexOf("userAPIKey") && !!~keys.indexOf("projectName") && !!~keys.indexOf("projectDescription") && !!~keys.indexOf("projectType")) {

                        log.addContext("userAPIKey", data["userAPIKey"]);

                        _db.multiClient.get("kUserName:" + data["userAPIKey"], function (err, userName) {

                            if (userName == undefined)
                                _db.getUserNameByUserAPIKey(data["userAPIKey"], function (err, userName) {
                                    log.addContext("userName", userName);
                                    log.trace("Create a project");
                                });
                            else {
                                log.addContext("userName", userName);
                                log.trace("Create a project");
                            }
                        });
                        
                        project = {
                            projectName: data.projectName,
                            projectType: data.projectType,
                            projectStatus: "AVANT_PROJET",
                            isArchived: false,
                            projectDescription: data.projectDescription,
                            projectTax: 0,
                            projectCreationDate: new Date(date.getTime() - (date.getTimezoneOffset() * 60000)),
                            metricsCaracteristics: null,
                            metrics: {},
                            disclaimerText: "Les prix indiqu&eacute;s sont exprim&eacute;s en Euros<br />Application de la loi du 12/05/1980 les marchandises restent la propri&eacute;t&eacute; du vendeur jusqu'à paiement int&eacute;gral du prix<br />Le pr&eacute;sent devis sera valable 2 mois<br />Aucune acceptation ne sera prise en compte sans signature du devis"
                        };
                        
                        _db.createProject(data["userAPIKey"], project, function (err, res) {
                            callback(err, res.projectID, res.inserted);
                        });
                    }
                }
            ], function (err, projectID, inserted) {

                if (err)
                    res.json({ error: true, project: null });
                else {

                    log.debug("inserted : " + inserted);

                    if (inserted == -1) {
                        log.warn("Project already exists with this name \"%s\"", project.projectName);
                        res.json({ error: true, reason: "projectName_exist", project: null });
                    }
                    else if (inserted == 1) {

                        log.info("Project with projectID %s has been created", projectID);
                        project["projectID"] = projectID;
                        project["hasMetricsCaracteristics"] = false;
                        res.json({ error: false, project: project });
                    }
                }
            });
        }
        catch (err) {
            log.error("Error occured" + " - " + err.message);
            res.json({ error: true, project: null });
        }
    },

    /**
     *
     */
    cloneProject: function (res, data) {

         log.addContext("methodName", stackTrace.get()[0].getFunctionName());

        try {

            var keys = Object.keys(data);
            var project, date = new Date();

            if (!!~keys.indexOf("userAPIKey") && !!~keys.indexOf("projectID") && !!~keys.indexOf("projectName") && !!~keys.indexOf("projectDescription")) {

                async.waterfall([

                    function (callback) {

                        log.addContext("userAPIKey", data["userAPIKey"]);

                        _db.multiClient.get("kUserName:" + data["userAPIKey"], function (err, userName) {

                            if (userName == undefined)
                                _db.getUserNameByUserAPIKey(data["userAPIKey"], function (err, userName) {
                                    log.addContext("userName", userName);
                                    log.trace("Clone a project - projectID : %s", data["projectID"]);
                                });
                            else {
                                log.addContext("userName", userName);
                                log.trace("Clone a project - projectID : %s", data["projectID"]);
                            }
                        });

                        var userAPIKey = data.userAPIKey;
                        var projectID = data.projectID;

                        _db.getProjectByUAPIKeyAndProjectID(userAPIKey, projectID, function (err, project) {

                            if (err)
                                throw err;

                            callback(null, project);
                        });
                    },
                    function (prj, callback) {

                        project = prj;
                        //project.projectName = prj.projectName + "_" + moment(project.projectCreationDate.toISOString().replace('Z', '')).format('YYYYMMDD-HHmmss');
                        project.projectName = data["projectName"];
                        project.projectDescription = data["projectDescription"];
                        project.projectCreationDate = new Date(date.getTime() - (date.getTimezoneOffset() * 60000));
                        
                        _db.createProject(data["userAPIKey"], project, function (err, res) {
                            callback(err, res.projectID, res.inserted);
                        });
                    }
                ], function (err, projectID, inserted) {

                    if (err)
                        res.json({ error: true, project: null });
                    else {

                        log.debug("inserted : " + inserted);

                        if (inserted == -1) {
                            log.warn("Project already exists with this name \"%s\"", project.projectName);
                            res.json({ error: true, reason: "projectName_exist", project: null });
                        }

                        else if (inserted == 1) {

                            log.info("Project with projectID %s has been cloned", projectID);
                            project["projectID"] = projectID;
                            res.json({ error: false, project: project });
                        }
                    }
                });
            }
        }
        catch (err) {
            log.error("Error occured" + " - " + err.message);
            res.json({ error: true, project: null });
        }
    },

    /**
     *
     */
    deleteProject: function (res, data) {

         log.addContext("methodName", stackTrace.get()[0].getFunctionName());

        try {

            var keys = Object.keys(data);
            var projectID;

            async.waterfall([
                function (callback) {

                    if (!!~keys.indexOf("userAPIKey") && !!~keys.indexOf("projectID")) {

                        log.addContext("userAPIKey", data["userAPIKey"]);

                        _db.multiClient.get("kUserName:" + data["userAPIKey"], function (err, userName) {

                            if (userName == undefined)
                                _db.getUserNameByUserAPIKey(data["userAPIKey"], function (err, userName) {
                                    log.addContext("userName", userName);
                                    log.trace("Delete a project - projectID : %s", data["projectID"]);
                                });
                            else {
                                log.addContext("userName", userName);
                                log.trace("Delete a project - projectID : %s", data["projectID"]);
                            }
                        });
                        
                        projectID = data.projectID;
                        callback(null, data["userAPIKey"], projectID);
                    }
                },
                function (userAPIKey, projectID, callback) {

                    _db.deleteProject(userAPIKey, projectID, function (err, res) {
                        callback(err, res.deleted);
                    });
                }
            ], function (err, deleted) {

                if (err)
                    res.json({ error: true });
                else {

                    log.debug("deleted : " + deleted);

                    if (deleted == 1) {
                        log.info("Project with projectID %s has been deleted", projectID);
                        res.json({ error: false });
                    }
                }
            });
    }
        catch (err) {
            log.error("Error occured" + " - " + err.message);
            res.json({ error: true });
        }
    },

    /**
     *
     */
    archiveProject: function (res, data) {

        log.addContext("methodName", stackTrace.get()[0].getFunctionName());

        try {
            var keys = Object.keys(data);
            var projectFields = {};

            async.waterfall([

                function (callback) {

                    if (!!~keys.indexOf("userAPIKey") && !!~keys.indexOf("projectID")) {

                        log.addContext("userAPIKey", data["userAPIKey"]);

                        _db.multiClient.get("kUserName:" + data["userAPIKey"], function (err, userName) {

                            if (userName == undefined)
                                _db.getUserNameByUserAPIKey(data["userAPIKey"], function (err, userName) {
                                    log.addContext("userName", userName);
                                    log.trace("Archive project - projectID : %s", data["projectID"]);
                                });
                            else {
                                log.addContext("userName", userName);
                                log.trace("Archive project - projectID : %s", data["projectID"]);
                            }
                        });

                        //
                        //
                        _db.getProjectByUAPIKeyAndProjectID(data["userAPIKey"], data["projectID"], function (err, prj) {

                            if (err)
                                throw err;
                            
                            callback(null, data["userAPIKey"], data["projectID"], prj.metricsCaracteristics, prj.metrics);
                        });
                    }
                },
                function (userAPIKey, projectID, metricsCaracteristics, metrics, callback) {

                    if (metricsCaracteristics != null)
                        if (!utils.isEmpty(metricsCaracteristics))
                            projectFields["metricsCaracteristics"] = zlib.gzipSync(JSON.stringify(metricsCaracteristics)).toString("base64");

                    if (!utils.isEmpty(metrics))
                        projectFields["metrics"] = zlib.gzipSync(JSON.stringify(metrics)).toString("base64");

                    projectFields["isArchived"] = true;
                    //log.trace("Fields to update : %s", JSON.stringify(projectFields));
                    callback(null, userAPIKey, projectID, projectFields);
                },
                function (userAPIKey, projectID, projectFields, callback) {

                    _db.updateProject(userAPIKey, projectID, projectFields, function (err, res) {
                        callback(err, res.nModified);
                    });
                }
            ], function (err, nModified) {

                if (err)
                    res.json({ error: true });
                else {

                    log.debug("nModified : " + nModified);
                    
                    if (nModified == 1) {
                        
                        log.info("Project with projectID %s has been archived", data["projectID"]);
                        res.json({ error: false });
                    }
                }
            });
        }
        catch (err) {
            log.error("Error occured" + " - " + err.message);
            res.json({ error: true });
        }
    },

    /**
    *
    */
    unArchiveProject: function (res, data) {

        log.addContext("methodName", stackTrace.get()[0].getFunctionName());

        try {
            var keys = Object.keys(data);
            var projectFields = {};

            async.waterfall([

                function (callback) {

                    if (!!~keys.indexOf("userAPIKey") && !!~keys.indexOf("projectID")) {

                        log.addContext("userAPIKey", data["userAPIKey"]);

                        _db.multiClient.get("kUserName:" + data["userAPIKey"], function (err, userName) {

                            if (userName == undefined)
                                _db.getUserNameByUserAPIKey(data["userAPIKey"], function (err, userName) {
                                    log.addContext("userName", userName);
                                    log.trace("Unarchive project - projectID : %s", data["projectID"]);
                                });
                            else {
                                log.addContext("userName", userName);
                                log.trace("Unarchive project - projectID : %s", data["projectID"]);
                            }
                        });

                        //
                        //
                        _db.getProjectByUAPIKeyAndProjectID(data["userAPIKey"], data["projectID"], function (err, prj) {

                            if (err)
                                throw err;

                            callback(null, data["userAPIKey"], data["projectID"], prj.metricsCaracteristics, prj.metrics);
                        });
                    }
                },
                function (userAPIKey, projectID, metricsCaracteristics, metrics, callback) {

                    if (metricsCaracteristics != null) {
                        if (!utils.isEmpty(metricsCaracteristics))
                            projectFields["metricsCaracteristics"] = JSON.parse(zlib.unzipSync(Buffer.from(metricsCaracteristics, "base64")));
                        else
                            projectFields["metricsCaracteristics"] = metricsCaracteristics;
                    }
                    else
                        projectFields["metricsCaracteristics"] = null;

                    if (!utils.isEmpty(metrics))
                        projectFields["metrics"] = JSON.parse(zlib.unzipSync(Buffer.from(metrics, "base64")));
                    else
                        projectFields["metrics"] = metrics;
                    
                    projectFields["isArchived"] = false;
                    callback(null, userAPIKey, projectID, projectFields);
                },
                function (userAPIKey, projectID, projectFields, callback) {

                    _db.updateProject(userAPIKey, projectID, projectFields, function (err, res) {
                        callback(err, res.nModified);
                    });
                }
            ], function (err, nModified) {

                if (err)
                    res.json({ error: true });
                else {

                    log.debug("nModified : " + nModified);

                    if (nModified == 1) {

                        log.info("Project with projectID %s has been unarchived", data["projectID"]);
                        res.json({ error: false, metricsCaracteristics: projectFields.metricsCaracteristics, metrics: projectFields.metrics });
                    }
                }
            });
        }
        catch (err) {
            log.error("Error occured" + " - " + err.message);
            res.json({ error: true });
        }
    },

    /**
    *
    */
    setCustomerToProject: function (res, data) {

         log.addContext("methodName", stackTrace.get()[0].getFunctionName());

        try {

            var keys = Object.keys(data);
            var projectFields = {};

            async.waterfall([
                
                function (callback) {
                    
                    if (!!~keys.indexOf("userAPIKey") && !!~keys.indexOf("projectID") && !!~keys.indexOf("customerID")) {

                        log.addContext("userAPIKey", data["userAPIKey"]);

                        _db.multiClient.get("kUserName:" + data["userAPIKey"], function (err, userName) {

                            if (userName == undefined)
                                _db.getUserNameByUserAPIKey(data["userAPIKey"], function (err, userName) {
                                    log.addContext("userName", userName);
                                    log.trace("Set a customer to a project - projectID : %s - customerID : %s", data["projectID"], data["customerID"]);
                                });
                            else {
                                log.addContext("userName", userName);
                                log.trace("Set a customer to a project - projectID : %s - customerID : %s", data["projectID"], data["customerID"]);
                            }
                                
                        });
                        
                        projectFields["customerID"] = data["customerID"];
                    }

                    callback(null, data["userAPIKey"], data["projectID"], projectFields);
                },
                function (userAPIKey, projectID, projectFields, callback) {

                    _db.updateProject(userAPIKey, projectID, projectFields, function (err, res) {
                        callback(err, res.nModified);
                    });
                }
            ], function (err, nModified) {

                if (err)
                    res.json({ error: true });
                else {

                    log.debug("nModified : " + nModified);

                    if (nModified == 1) {
                        log.info("Project with projectID %s has been updated", data["projectID"]);
                        res.json({ error: false });
                    }
                }
            });
        }
        catch (err) {
            log.error("Error occured" + " - " + err.message);
            res.json({ error: true });
        }
    }
};

module.exports = projectController;
module.id = "projectController";