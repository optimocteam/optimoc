﻿"use strict"

var _db;
var async = require("async");
var log = require("log4js").getLogger(require("path").basename(__filename, ".js"));
var moment = require('moment');

setTimeout(function () {

    _db = require('../db/db');
    log.addContext("methodName", stackTrace.get()[0].getMethodName());
    log.trace("Customer Controller Loaded.");
	
}, 500);

var customerController = {

    /**
     * Given customer data, update the customer in database.
     *
     * @method updateCustomer
     * @param {Object} data A data object with all customer information needed
     * @return {Boolean} Returns true on success
     */
    updateCustomer: function (res, data) {

        log.addContext("methodName", stackTrace.get()[0].getFunctionName());

        try {

            var keys = Object.keys(data),
                customer;

            if (!!~keys.indexOf("customer")) {

                customer = data["customer"];

                //
                // Log userName
                _db.multiClient.get("kUserName:" + customer.userAPIKey, function (err, userName) {

                    if (userName == undefined)
                        _db.getUserNameByUserAPIKey(customer.userAPIKey, function (err, userName) {
                            log.addContext("userName", userName);
                            log.trace("Update a customer : customerID : %s", customer.customerID);
                        });
                    else {
                        log.addContext("userName", userName);
                        log.trace("Update a customer : customerID : %s", customer.customerID);
                    }
                });

                async.series([
                    function (callback) {

                        if (customer.customerID != null) {

                            if ((customer.socialReason == null || customer.socialReason == "")
                                && (customer.identificationNumbers.siret == null || customer.identificationNumbers.siret == "")
                                && (customer.identificationNumbers.ape == null || customer.identificationNumbers.ape == "")
                                && (customer.identificationNumbers.tva == null || customer.identificationNumbers.tva == ""))
                                customer.type = "Particulier";
                            else
                                customer.type = "Professionnel";

                            _db.updateCustomer(customer, function (err, res) {
                                callback(err, res.nModified);
                            });
                        }
                    }
                ], function (err, nModified) {

                    if (err)
                        throw err;

                    log.debug("nModified : " + nModified);

                    log.info("Customer with customerID %s has been updated", customer.customerID);
                    res.json({ error: false });
                });
            }
        }
        catch (err) {
            log.error("Error occured" + " - " + err.message);
            res.json({ error: true });
        }
    },

    /**
     * Given customer data, insert the customer in database.
     *
     * @method createCustomer
     * @param {Object} data A data object with all customer information needed
     * @return {Boolean} Returns true on success
     */
    createCustomer: function (res, data) {

        log.addContext("methodName", stackTrace.get()[0].getFunctionName());

        try {

            var keys = Object.keys(data),
                customer;

            async.waterfall([

                function (callback) {

                    /*if (customers.length > 0) {

                        var maxCustomerID = 0;

                        customers.map(function (c) {

                            if (c.customerID > maxCustomerID)
                                maxCustomerID = c.customerID;
                        });

                        customer.customerID = parseInt(maxCustomerID) + 1;
                    }
                    else
                        customer.customerID = 1;*/

                    //customer.customerID = parseInt(moment(new Date()).format("YYYYMMDDHHmmsss").toString());

                    if (!!~keys.indexOf("customer")) {

                        customer = data["customer"];

                        //
                        // Log userName
                        _db.multiClient.get("kUserName:" + customer.userAPIKey, function (err, userName) {

                            if (userName == undefined)
                                _db.getUserNameByUserAPIKey(customer.userAPIKey, function (err, userName) {
                                    log.addContext("userName", userName);
                                    log.trace("Create a customer");
                                });
                            else {
                                log.addContext("userName", userName);
                                log.trace("Create a customer");
                            }
                        });

                        if ((customer.socialReason == null || customer.socialReason == "")
                            && (customer.identificationNumbers.siret == null || customer.identificationNumbers.siret == "")
                            && (customer.identificationNumbers.ape == null || customer.identificationNumbers.ape == "")
                            && (customer.identificationNumbers.tva == null || customer.identificationNumbers.tva == ""))
                            customer.type = "Particulier";
                        else
                            customer.type = "Professionnel";

                        //
                        //
                        customer.email = customer.email.toLowerCase();

                        _db.createCustomer(customer, function (err, res) {
                            callback(err, res.customerID, res.inserted);
                        });
                    }
                }
            ], function (err, customerID, inserted) {

                if (err)
                    res.json({ error: true, customer: null });
                else if (inserted == 1) {

                    log.debug("inserted : " + inserted);

                    log.info("Customer with customerID %s has been created", customerID);
                    res.json({ error: false, customer: customer });
                }
            });
        }
        catch (err) {
            log.error("Error occured" + " - " + err.message);
            res.json({ error: true });
        }
    },

    /**
     * Given customerID, delete the customer in database.
     *
     * @method deleteCustomer
     * @param {Object} data A data object containing customerID and userID
     * @return {Boolean} Returns true on success
     */
    deleteCustomer: function (res, data) {

        log.addContext("methodName", stackTrace.get()[0].getFunctionName());

        try {

            var keys = Object.keys(data);

            if (~keys.indexOf("userAPIKey") && !!~keys.indexOf("customerID")) {

                //
                // Log userName
                _db.multiClient.get("kUserName:" + data["userAPIKey"], function (err, userName) {

                    if (userName == undefined)
                        _db.getUserNameByUserAPIKey(data["userAPIKey"], function (err, userName) {
                            log.addContext("userName", userName);
                            log.trace("Delete a customer : customerID : %s", data["customerID"]);
                        });
                    else {
                        log.addContext("userName", userName);
                        log.trace("Delete a customer : customerID : %s", data["customerID"]);
                    }
                });

                async.series([
                    function (callback) {

                        if (data["customerID"] != undefined) {

                            _db.deleteCustomer(data["customerID"], data["userAPIKey"], function (err, res) {
                                callback(err, res.deleted);
                            });
                        }
                        else {
                            log.trace("Customer with customerID undefined can not be deleted");
                            throw err;
                        }
                    }
                ], function (err, deleted) {

                    if (err)
                        res.json({ error: true });
                    else {

                        log.debug("deleted : " + deleted);

                        if (deleted == 1) {
                            log.info("Customer with customerID %s has been deleted", data["customerID"]);
                            res.json({ error: false });
                        }
                    }
                });
            }
        }
        catch (err) {
            log.error("Error occured" + " - " + err.message);
            res.json({ error: true });
        }
    },

    /**
     * Given userAPIKey int return one or many customer.
     * @method getCustomers
     * @param {Object} data A data object with userAPIKey
     * @return {Object} Returns customers Object or null
     */
    getCustomers: function (res, data) {

        log.addContext("methodName", stackTrace.get()[0].getFunctionName());

        try {

            var keys = Object.keys(data);
            var customers;

            async.waterfall([

                function (callback) {

                    if (!!~keys.indexOf("userAPIKey") && !!~keys.indexOf("fields")) {

                        log.addContext("userAPIKey", data["userAPIKey"]);

                        //
                        // Log userName
                        _db.multiClient.get("kUserName:" + data["userAPIKey"], function (err, userName) {

                            if (userName == undefined)
                                _db.getUserNameByUserAPIKey(data["userAPIKey"], function (err, userName) {
                                    log.addContext("userName", userName);
                                    log.trace("Get customers");
                                });
                            else {
                                log.addContext("userName", userName);
                                log.trace("Get customers");
                            }
                        });
                        
                        _db.getCustomersByUserAPIKey(data["userAPIKey"], data["fields"],  function (err, customersData) {

                            if (err)
                                throw err;
                            
                            callback(null, customersData);
                        });
                    }
                },
                function (customersData, callback) {

                    if (customersData != null)
                        customers = customersData;
                    else
                        customers = null;

                    callback(null, customers);
                }
            ], function (err, customers) {

                res.json({ error: false, data: customers });
            });
        }
        catch (err) {
            log.error("Error occured" + " - " + err.message);
            res.json({ error: true, data: null });
        }
    },

    /**
     * Given userAPIKey/customerID int return one customer.
     * @method getCustomers
     * @param {Object} data A data object with userAPIKey and {string|null} customerID
     * @return {Object} Returns customer Object or null
     */
    getCustomer: function (res, data) {

        log.addContext("methodName", stackTrace.get()[0].getFunctionName());

        try {

            var keys = Object.keys(data);
            var customer;

            async.waterfall([

                function (callback) {

                    if (!!~keys.indexOf("userAPIKey") && !!~keys.indexOf("customerID")) {

                        log.addContext("userAPIKey", data["userAPIKey"]);
                        log.addContext("userName", data.fullName);

                        _db.getCustomerByUserAPIKeyAndCustomerID(data["userAPIKey"], data["customerID"], function (err, customerData) {

                            if (err)
                                throw err;

                            callback(null, customerData);
                        });
                    }
                },
                function (customerData, callback) {
                    
                    customer = (customerData != null) ? customerData : customer = null;
                    callback(null, customer);
                }
            ], function (err, customer) {

                res.json({ data: customer });
            });
        }
        catch (err) {
            log.error("Error occured" + " - " + err.message);
            res.status(500).json({ error_code: error.code });
        }
    }
};

module.exports = customerController;
module.id = "customerController";