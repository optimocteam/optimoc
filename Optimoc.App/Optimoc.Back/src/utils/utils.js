﻿var crypto = require("crypto");
var appSettings = config.settings();
var fs = require('fs');
const nodemailer = require("nodemailer");
let jwt = require('jsonwebtoken');
const JWT_SECRET = appSettings.JWT_SECRET,
      errors = config.errors(),
      log = require('log4js').getLogger(require("path").basename(__filename, ".js"));

/*
 * 
 */
function generateToken(keylen) {

    var salt = crypto.randomBytes(parseInt(appSettings.SALT_WORK_FACTOR));
    var key = crypto.pbkdf2Sync(appSettings.CRYPTO_SECRET, salt, 10000, keylen, "sha512");
    return key.toString("hex");
}

/*
 * 
 */
function encrypt(text) {

    const algorithm = "aes-256-cbc",
        iv = Buffer.alloc(16, 0),
        key = crypto.scryptSync(appSettings.CRYPTO_SECRET, appSettings.SALT, 32),
        cipher = crypto.createCipheriv(algorithm, key, iv);

    let crypted = cipher.update(text, "utf8", "hex");
    crypted += cipher.final("hex");
    return crypted;
}

/*
 * 
 */
function decrypt(text) {

    const algorithm = "aes-256-cbc",
        iv = Buffer.alloc(16, 0),
        key = crypto.scryptSync(appSettings.CRYPTO_SECRET, appSettings.SALT, 32),
        decipher = crypto.createDecipheriv(algorithm, key, iv)

    let decrypted = decipher.update(text, "hex", "utf8");
    decrypted += decipher.final('utf8');
    return decrypted;
}

/*
 * 
 */
function sendEmail(to, subject, body, hasAttachmentFiles, callback) {

    try {
        var pwdDecrypted = decrypt(appSettings.PASSWORD);

        // create reusable transporter object using the default SMTP transport
        let transporter = nodemailer.createTransport({
            host: appSettings.SMTP_HOST,
            port: parseInt(appSettings.SMTP_PORT),
            secure: false,
            auth: {
                user: appSettings.ADDRESS,
                pass: pwdDecrypted
            }
        });

        let mailOptions = {
            from: `${appSettings.DISPLAY_NAME} <${appSettings.ADDRESS}>`,
            to: to,
            subject: subject,
            text: "",
            html: body
        }

        transporter.sendMail(mailOptions, function (error, res) {

            transporter.close();

            if (error) {
                callback(error, null);
            }

            if (callback && typeof (callback) === "function")
                callback(null, res);
        });


    }
    catch (error) {
        callback(error, null);
    }
}

/*
 * function to encode file data to base64 encoded string
 * usage : var base64str = base64_encode('kitten.jpg');
 */
function base64_encode(file) {
    // read binary data
    var bitmap = fs.readFileSync(file);
    // convert binary data to base64 encoded string
    return Buffer.from(bitmap).toString('base64');
}

/*
 * function to create file from base64 encoded string
 * usage : base64_decode(base64str, 'copy.jpg');
 */
function base64_decode(base64str, file) {
    // create buffer object from base64 encoded string, it is important to tell the constructor that the string is base64 encoded
    var buffer = new Buffer(base64str, 'base64');
    // write buffer to file
    fs.writeFileSync(file, buffer);
}

/**
 * function to sum values of an object
 * @param obj
 */
function sum(obj) {

    return Object.keys(obj).reduce(function (sum, key) {
        return sum + obj[key];
    }, 0);


}

/**
 * function to validate french SIRET
 * @param siret
 */
function isSIRET (siret) {
    return verifySIREN(siret, 14);
}

/**
 * function to validate french SIREN
 * @param siren
 */
function isSIREN (siren) {
    return verifySIREN(siren, 9)
}

/**
 * function to check french SIRET / SIREN number
 * @param number
 * @param size
 */
function verifySIREN(number, size) {

    if (isNaN(number) || number.length != size) return false;
    var bal = 0;
    var total = 0;
    for (var i = size - 1; i >= 0; i--) {
        var step = (number.charCodeAt(i) - 48) * (bal + 1);
        /*if (step>9) { step -= 9; }
         total += step;*/
        total += (step > 9) ? step - 9 : step;
        bal = 1 - bal;
    }
    return (total % 10 == 0) ? true : false;
}

/**
 * function to check if an object is empty
 * @param object
 */

function isEmpty(obj) {
    if (!obj || typeof obj === "object")
        return (Object.keys(obj).length === 0);
}

function checkToken (req, res, next) {

    const headers = req.headers;
    const cookies = req.cookies;
    let message, errorCode;

    log.addContext("methodName", "checkToken");
    log.addContext("userAPIKey", cookies["optimoc_user_api_key"]);
    
    try
    {
        if ((req.body.action === "getUser" ||
            req.body.action === "createUser" ||
            req.body.action === "confirmEmail" ||
            req.body.action === "forgotPassword" ||
            req.body.action === "resetPassword") &&
            !req.body.data.userAPIKey) {
            next();
        } else {
            let token = headers && (headers["x-access-token"] || headers.authorization) ? headers["x-access-token"] || headers.authorization : "";
            token = token.startsWith("Bearer") ? token.slice(7, token.length) : "";

            if (!token)
                token = cookies.optimoc_auth_token ? cookies.optimoc_auth_token : "";

            if (token)
                jwt.verify(token, JWT_SECRET, (err, payload) => {

                    if (err) {
                        message = errors["API"]["USER_FAILED_TO_AUTHENTICATE"].error_message;
                        errorCode = errors["API"]["USER_FAILED_TO_AUTHENTICATE"].error_code;
                        log.warn(`${message} - ${err.message}`);
                        return res.status(401).json({ error_code: errorCode });
                    }

                    log.addContext("userName", payload.fullName);
                    log.info(`payload : ${JSON.stringify(payload)}`);
                    req.fullName = payload.fullName;
                    req.isAdmin = payload.isAdmin;
                    next();
                });
            else {
                message = errors["API"]["USER_AUTH_TOKEN_NOT_SUPPLIED"].error_message;
                errorCode = errors["API"]["USER_AUTH_TOKEN_NOT_SUPPLIED"].error_code;
                log.warn(`${message}`);
                return res.status(403).json({ error_code: errorCode });
            }
        }
    }
    catch (error) {
        log.error(`Error occured : ${error.message}`);
        return res.status(500).json({ error_code: error.code });
    }
}

exports.generateToken = generateToken;
exports.sendEmail = sendEmail;
exports.base64_encode = base64_encode;
exports.sum = sum;
exports.isSIRET = isSIRET;
exports.isSIREN = isSIREN;
exports.isEmpty = isEmpty;
exports.checkToken = checkToken;
