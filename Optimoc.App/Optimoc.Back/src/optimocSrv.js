/**
 * @property cluster : Multi-core server manager.
 * @final
 * Uses : Zero-downtime; graceful-shutdown, multi-worker with resuscitates worker on crash.
 *
  */
let cluster = require("cluster");
let cpus = require('os').cpus().length;
let application = require("./optimoc");

global.config = require("./config/configuration");
global.environment = config.environment;
global.stackTrace = require("stack-trace");

try {
    require('fs').mkdirSync('../logs');
} catch (err) {
    if (err.code != 'EEXIST') {
        log.error("Could not set up log directory, error occured: ", err.message);
        //process.exit(1);
    }
}

//
// Init log4js
var log4js = require("log4js");

if (environment === "development")
    log4js.configure("./config/log4js-dev.json");
else if (environment === "production")
    log4js.configure("./config/log4js-prod.json");

var log = log4js.getLogger(require("path").basename(__filename, ".js"));

if (cluster.isMaster) {

    if (environment === "development")
        cluster.fork();
    else
        for (let i = 0; i < cpus; i++) {
            cluster.fork();
        }

    cluster.on("exit", function (worker) {

        log.addContext("methodName", stackTrace.get()[0].getMethodName());
        log.debug("Worker " + worker.id + " died..");

        setTimeout(function () {
            cluster.fork();
        }, 1000);
    });
} else {
    try {
        const NS_PER_SEC = 1e9;
        const start = process.hrtime();
        application.init();
        const end = process.hrtime(start);
        log.debug(`optimocLaunch in ${(end[0] + end[1] / NS_PER_SEC).toFixed(3)} seconds...`);
    }
    catch (err) {
        log.addContext("methodName", stackTrace.get()[0].getMethodName());
        log.error(err.stack);
        log4js.shutdown(function () { process.exit(1); });
    }

    //Handle uncaughtException
    process.on('uncaughtException', function (err) {
        log.addContext("methodName", stackTrace.get()[0].getMethodName());
        log.error(err.stack);
        log4js.shutdown(function () { process.exit(1); });
    });

    //Handle SIGIT (useful for remote shutdown
    process.on("SIGINT", function () {

        log.addContext("methodName", stackTrace.get()[0].getMethodName());
        log.info("SIGINT (Crtl-C)");

        //Kill workers
        for (var id in cluster.workers) {
            cluster.workers[id].kill();
            log.debug("Worker " + cluster.workers[id] + " died..");
        }

        // exit the master process
        process.exit(0);

        // Sortie
        log4js.shutdown(function () { process.exit(1); });
    });
}