'use strict';

var mongoose = require('mongoose'),
    bcrypt = require('bcrypt'),
    Schema = mongoose.Schema,
    ObjectId = mongoose.Schema.Types.ObjectId,
    appSettings = config.settings();

const SALT_WORK_FACTOR = parseInt(appSettings.SALT_WORK_FACTOR);

/**
 * User Schema
 */
var UserSchema = new Schema({

    userID: {
        type: ObjectId,
        default: null
    },
    lastname: {
        type: String,
        trim: true,
        required: [true, "User lastname is required."]
    },
    firstname: {
        type: String,
        trim: true,
        required: [true, "User firstname is required."]
    },
    password: {
        type: String,
        required: [true, "User password is required."]
    },
    email: {
        type: String,
        unique: true,
        lowercase: true,
        trim: true,
        required: [true, "User email is required."]
    },
    isEmailConfirmed: {
        type: Boolean,
        default: false
    },
    phoneNumber: {
        type: String,
        unique: true,
        default: ""
        //required: [true, "User phone number is required."]
    },
    isPhoneNumberConfirmed: {
        type: Boolean,
        default: false
    },
    isLockoutEnabled: {
        type: Boolean,
        default: false
    },
    accessFailedCount: {
        type: Number,
        default: 0,
        validate: {
            validator: Number.isInteger,
            message: '{VALUE} is not an integer value'
        }
    },
    role: {
        type: String,
        required: true
    },
    isActive: {
        type: Boolean,
        default: false
    },
    userAPIKey: {
        type: String
    },
    registerToken: {
        type: String
    },
    registerDate: {
        type: Date,
        default: Date.now
    },
    companyID: {
        type: ObjectId
    },
    rateBases: {
        type: Array
    },
    resetPasswordToken: {
        type: String
    },
    resetPasswordDate: {
        type: Date
    },
    isAdmin: {
        type: Boolean,
        default: false
    }
});


UserSchema.pre("save", function (next) {
    var user = this;

    if (!user.isModified("password"))
        return next();

    bcrypt.genSalt(SALT_WORK_FACTOR, function (err, salt) {
        if (err)
            return next(err);

        bcrypt.hash(user.password, salt, function (err, hash) {
            if (err)
                return next(err);

            user.password = hash;
            next();
        });
    });
});

UserSchema.methods.comparePassword = function (candidatePassword) {
    return bcrypt.compareSync(candidatePassword, this.password);
};

/*UserSchema.methods.comparePassword = function (candidatePassword, cb) {
    bcrypt.compare(candidatePassword, this.password, function (err, isMatch) {
        if (err) return cb(err)
        cb(null, isMatch)
    })
}*/

UserSchema.method('toJSON', function () {
    const { __v, _id, ...object } = this.toObject();
    object.userID = _id;
    return object;
 });

mongoose.model('User', UserSchema);