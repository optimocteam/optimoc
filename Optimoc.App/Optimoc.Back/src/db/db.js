"use strict"
var util = require('util');

var mongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectId;

//
// Log4js
var log = require('log4js').getLogger(require("path").basename(__filename, ".js"));

//
// Configuration
var appSettings = config.settings();
var url = appSettings.CONN_STR_OPTIMOC;
const dbName = appSettings.OPTIMOC_DB_NAME;
const jsreportDbName = appSettings.JSREPORT_DB_NAME;
var uriJSReport = appSettings.CONN_STR_JSREPORT;

//
// REDIS
var redisClient = require('redis').createClient;
var multipleRedis = require('multiple-redis');
var redis, redis1, redis2, redis3, multiClient;

//Mongoose
let mongoose = require('mongoose'),
    User = mongoose.model('User');

if (environment === "development") {
    redis = redisClient(appSettings.PORT_REDIS, appSettings.HOST_REDIS);
    multiClient = multipleRedis.createClient([redis]);
}
else {
    redis1 = redisClient(appSettings.PORT_REDIS.split(",")[0], appSettings.HOST_REDIS);
    redis2 = redisClient(appSettings.PORT_REDIS.split(",")[1], appSettings.HOST_REDIS);
    redis3 = redisClient(appSettings.PORT_REDIS.split(",")[2], appSettings.HOST_REDIS);
    multiClient = multipleRedis.createClient([redis1, redis2, redis3]);
}

var database = {
    
    multiClient: null,

    //roles: null,

    metrics: null,

    /*titles: null,

    status: null,*/

    //#region init function

    init: function () {

        log.addContext("methodName", stackTrace.get()[0].getFunctionName());

        log.debug("ENVIRONMENT: " + environment);
        log.debug("OPTIMOC - [Host/database] : " + url.split("@")[1]);
        log.debug("JSREPORT - [Host/database] : " + uriJSReport.split("@")[1]);

        //
        // Flush previous redis commands
        if (environment === "development")
            redis.flushall();
        else {
            redis1.flushall();
            redis2.flushall();
            redis3.flushall();
        }

        //
        this.multiClient = multiClient;
    },

    //#endregion

    // #region getProjectByUAPIKeyAndProjectID function

    getProjectByUAPIKeyAndProjectID: function (userAPIKey, projectID, callback) {

         log.addContext("methodName", stackTrace.get()[0].getFunctionName());

         multiClient.get("kProject:" + userAPIKey + "-" + projectID, function (err, project) {

             if (err)
                 callback(err, null);

             else if (project) {

                 log.debug("getProjectByUAPIKeyAndProjectID cache called");
                 callback(null, JSON.parse(project));
             }

             else {
                 mongoClient.connect(url, { useUnifiedTopology: true }, function (err, client) {

                     if (err) {
                         callback(err, null);
                         return;
                     }

                     var collection = client.db(dbName).collection("projects");

                     collection.findOne(
                         {
                             userAPIKey: userAPIKey,
                             //projectID: parseInt(projectID)
                             _id: ObjectId(projectID)
                         },

                         function (err, doc) {

                             if (err)
                                 callback(err, null);

                             client.close();

                             if (doc != null) {

                                 var project = doc;
                                 project["projectID"] = projectID;
                                 delete project["_id"];

                                 multiClient.set("kProject:" + userAPIKey + "-" + projectID, JSON.stringify(project), function (err) {

                                     if (err)
                                         callback(err, null);
                                     else
                                         callback(null, project);
                                 });
                             }
                         });
                 });
             }
         });
    },

    // #endregion

    // #region getProjectsByUAPIKey function

    getProjectsByUAPIKey: function (userAPIKey, callback) {

         log.addContext("methodName", stackTrace.get()[0].getFunctionName());

         mongoClient.connect(url, { useUnifiedTopology: true}, function (err, client) {

             if (err)
                 callback(err, null);

             var collection = client.db(dbName).collection("projects");

             collection.aggregate([
                 {
                     $match: { userAPIKey: userAPIKey }
                 },
                 {
                     $addFields: { projectID: "$_id" }
                 },
                 {
                     "$lookup": {
                         "localField": "customerID",
                         "from": "customers",
                         "foreignField": "_id",
                         "as": "customer"
                     }
                 },
                 {
                     "$unwind": {
                         path: '$customer',
                         preserveNullAndEmptyArrays: true
                     }
                 },
                 {
                     //
                     // MongoDB $project stage
                     $project: {
                         _id: 0,
                         projectID: 1,
                         customerID: 1,
                         projectName: 1,
                         projectType: 1,
                         projectStatus: 1,
                         projectDescription: 1,
                         projectTax: 1,
                         projectCreationDate: 1,
                         isArchived: 1,
                         "customer.lastName": 1,
                         "customer.firstName": 1,
                         "customer.title": 1,
                         "customer.socialReason": 1,
                         "customer.phoneNumber": 1,
                         "customer.address": 1,
                         //hasMetricsCaracteristics:
                         //{
                         //    $cond: [
                         //        { $ifNull: ["$metricsCaracteristics", false] },
                         //        false,
                         //        true
                         //    ]
                         //}
                         hasMetricsCaracteristics:
                         {
                             $cond: {
                                 if: { $or: [{ $eq: ["$metricsCaracteristics", {}] }, { $eq: ["$metricsCaracteristics", null] }] },
                                 then: false,
                                 else: true
                             }
                         }
                     }
                 }
             ]).sort({ "projectCreationDate": 1 }).toArray(function (err, doc) {

                 if (err)
                     callback(err, null);

                 client.close();

                 if (doc != null)
                     callback(null, doc);
                 else
                     callback(null, null);
             });
         });
    },

    // #endregion

    // #region getNProjects function

    getNProjects: function (userAPIKey, callback) {

        log.addContext("methodName", stackTrace.get()[0].getFunctionName());

        multiClient.get("nProjects:" + userAPIKey, function (err, nProjects) {

            if (err) {
                callback(err, null);
                return;
            }

            else if (nProjects) {

                log.debug("getNProjects cache called");
                callback(null, nProjects);
            }

            else {
                mongoClient.connect(url, { useUnifiedTopology: true}, function (err, client) {

                    if (err)
                        callback(err, null);

                    var collection = client.db(dbName).collection("users");

                    collection.aggregate([
                        { $match: { userAPIKey: userAPIKey } },
                        {
                            $group: {
                                _id: "$projectStatus",
                                "count": {
                                    $sum: 1
                                }
                            }
                        }], function (err, doc) {

                            if (err)
                                callback(err, null);

                            client.close();

                            if (doc != null)
                                multiClient.set("nProjects:" + userAPIKey, doc, function (err) {

                                    if (err)
                                        callback(err, null);
                                    else
                                        callback(null, doc);
                                });
                            else
                                callback(null, null);
                        });
                });
            }
        });
    },

    // #endregion

    //#region updateProject function

    updateProject: function (userAPIKey, projectID, fields, callback) {

         log.addContext("methodName", stackTrace.get()[0].getFunctionName());

        mongoClient.connect(url, { useUnifiedTopology: true}, function (err, client) {

            if (err)
                callback(err, null);

            let collection = client.db(dbName).collection("projects");
            let fieldsToUpdate = new Object();
            let keys = Object.keys(fields);

            keys.map(key => {
                if (key === "customerID")
                    fieldsToUpdate[key] = ObjectId(fields[key]);
                else
                    fieldsToUpdate[key] = fields[key];
            });

            collection.findOneAndUpdate(
                {
                    userAPIKey: userAPIKey,
                    _id: ObjectId(projectID)
                },
                {
                    $set:
                        fieldsToUpdate
                },
                //{ new: true},// for mongoose
                { returnOriginal: false},

                function (err, res) {

                    if (err) {
                        if (/E11000/.test(err.message))
                            callback(null, { nModified: -1 });
                        else
                            callback(err, { nModified: -1 });
                    }

                    client.close();

                    if (res !== null) {

                        if (res.ok === 1) {

                            log.trace("Project successfully updated...");
                            
                            var project = res.value;
                            project["projectID"] = projectID;
                            delete project["_id"];
                            let prefixKeyToSet, valueToSet;

                            if (keys.length === 1 && keys[0] === "disclaimerText") {
                                prefixKeyToSet = "kProjectDisclaimerText";
                                valueToSet = fieldsToUpdate[keys[0]];
                            }
                            else {
                                prefixKeyToSet = "kProject";
                                valueToSet = JSON.stringify(project);
                            }

                            multiClient.set(prefixKeyToSet + ":" + userAPIKey + "-" + projectID, valueToSet, function (err) {

                                if (err)
                                    callback(err, { nModified: -1 });
                                else
                                    callback(null, { nModified: 1 });
                            });
                        }
                        else {

                            log.warn("No project updated...");
                            callback(null, { nModified: 0 });
                        }
                    }
                });
        });
    },

    //#endregion

    //#region createProject function

    createProject: function (userAPIKey, project, callback) {

        log.addContext("methodName", stackTrace.get()[0].getFunctionName());

        mongoClient.connect(url, { useUnifiedTopology: true}, function (err, client) {

            if (err) {
                callback(err, null);
                return;
            }

            let collection = client.db(dbName).collection("projects");
            let projectToInsert = {
                "userAPIKey": userAPIKey,
                "projectName": project.projectName,
                "projectType": project.projectType,
                "projectStatus": project.projectStatus,
                "isArchived": project.isArchived,
                "projectDescription": project.projectDescription,
                "projectTax": project.projectTax,
                "projectCreationDate": project.projectCreationDate,
                "metricsCaracteristics": project.metricsCaracteristics,
                "metrics": project.metrics,
                "disclaimerText": project.disclaimerText
            }
            
            collection.insertOne(
                //{
                //    userAPIKey: userAPIKey,
                //    projectName: { $not: new RegExp("^" + project.projectName + "$", "i") }
                //},
                projectToInsert,

                function (err, res) {

                    if (err) {
                        if (/E11000/.test(err.message))
                            callback(null, { inserted: -1 });
                        else
                            callback(err, null);
                    }

                    client.close();

                    if (res != null) {

                        if (res.insertedCount == 1) {

                            log.trace("Project successfully created...");

                            var project = res.ops[0];
                            project["projectID"] = project["_id"];
                            delete project["_id"];

                            multiClient.set("kProject:" + userAPIKey + "-" + res.insertedId, JSON.stringify(project), function (err) {

                                if (err)
                                    callback(err, null);
                                else
                                    callback(null, { inserted: res.insertedCount, projectID: res.insertedId });
                            });
                        }
                        else {
                            log.warn("No project created...");
                            callback(null, { inserted: 0 });
                        }
                    }
                });
        });
    },

    // #endregion

    //#region deleteProject function

    deleteProject: function (userAPIKey, projectID, callback) {

         log.addContext("methodName", stackTrace.get()[0].getFunctionName());

        mongoClient.connect(url, { useUnifiedTopology: true}, function (err, client) {

            if (err) {
                callback(err, null);
                return;
            }

            var collection = client.db(dbName).collection("projects");

            collection.deleteOne(
                {
                    userAPIKey: userAPIKey,
                    _id: ObjectId(projectID)
                },

                function (err, res) {

                    if (err)
                        callback(err, null);

                    client.close();

                    if (res != null) {

                        if (res.deletedCount == 1) {

                            log.trace("Project successfully deleted...");

                            multiClient.del("kProject:" + userAPIKey + "-" + projectID, function (err) {

                                if (err)
                                    callback(err, null);
                                else
                                    callback(null, { deleted: res.deletedCount });
                            });
                        }
                        else {
                            log.warn("No project deleted...");
                            callback(null, { deleted: 0 });
                        }
                    }
                });
        });
    },

    // #endregion
    
    // #region getDisclaimerTextByUAPIKeyAndProjectID function

    getDisclaimerTextByUAPIKeyAndProjectID: function (userAPIKey, projectID, callback) {

        log.addContext("methodName", stackTrace.get()[0].getFunctionName());

        multiClient.get("kProjectDisclaimerText:" + userAPIKey + "-" + projectID, function (err, disclaimerText) {

            if (err)
                callback(err, null);

            else if (disclaimerText) {

                log.debug("getDisclaimerTextByUAPIKeyAndProjectID cache called");
                callback(null, disclaimerText);
            }

            else {
                mongoClient.connect(url, { useUnifiedTopology: true}, function (err, client) {

                    if (err) {
                        callback(err, null);
                        return;
                    }

                    var collection = client.db(dbName).collection("projects");

                    collection.findOne(
                        {
                            userAPIKey: userAPIKey,
                            _id: ObjectId(projectID)
                        },
                        {
                            disclaimerText: 1
                        },
                        function (err, doc) {

                            if (err)
                                callback(err, null);

                            client.close();

                            if (doc !== null) {

                                multiClient.set("kProjectDisclaimerText:" + userAPIKey + "-" + projectID, doc.disclaimerText, function (err) {

                                    if (err)
                                        callback(err, null);
                                    else
                                        callback(null, doc.disclaimerText);
                                });
                            }
                        });
                });
            }
        });
    },

    // #endregion

    // #region createUser function

    createUser: function (newUser, callback) {

        log.addContext("methodName", stackTrace.get()[0].getFunctionName());

        newUser
            .save()
            .then(() => {
                log.trace("User created successfully for %s - new userID : %s", newUser.email, newUser._id);
                callback(null, { inserted: 1 });
            })
            .catch(err => {
                const message = err.message;

                log.error(`User creation failed - ${message}`);
                callback(err, { inserted: null });
            });
    },

    // #endregion

    // #region deleteUser function

    deleteUser: function (userAPIKey, callback) {

        log.addContext("methodName", stackTrace.get()[0].getFunctionName());

        mongoClient.connect(url, { useUnifiedTopology: true}, function (err, client) {

            if (err) {
                callback(err, null);
                return;
            }

            var collection = client.db(dbName).collection("users");

            collection.deleteOne(
                {
                    userAPIKey: userAPIKey
                },
                function (err, res) {

                    if (err)
                        callback(err, null);

                    client.close();

                    if (res != null) {

                        if (res.deletedCount == 1) {

                            log.trace("User successfully deleted...");

                            multiClient.del(userAPIKey, function (err) {

                                if (err)
                                    callback(err, null);
                                else
                                    callback(null, { deleted: res.deletedCount });
                            });
                        }
                        else {
                            log.warn("No user deleted...");
                            callback(null, { deleted: 0 });
                        }
                    }
                });
        });
    },

    // #endregion

    // #region updateUser function

    updateUser: function (userID, obj, callback) {

        log.addContext("methodName", stackTrace.get()[0].getFunctionName());

        User.findByIdAndUpdate(
            ObjectId(userID),
            { $set: obj },
            { new: true },
            function (err, usr) {

                if (err) {
                    callback(err, { nModified: 0 });
                    return;
                }

                if (usr != null) {

                    log.trace("Document updated successfully for userID : %s", userID);

                    let user = {

                        userID: usr._id,
                        firstname: usr.firstname,
                        lastname: usr.lastname,
                        email: usr.email,
                        userAPIKey: usr.userAPIKey,
                        registerDate: usr.registerDate,
                        role: usr.role,
                        phoneNumber: usr.phoneNumber,
                        companyID: usr.companyID,
                        password: usr.password,
                        isEmailConfirmed: usr.isEmailConfirmed,
                        isActive: usr.isActive,
                        registerToken: usr.registerToken
                    };

                    multiClient.set(usr.userAPIKey, JSON.stringify(user), function (err) {

                        if (err)
                            callback(err, null);
                        else
                            callback(null, { nModified: 1 });
                    });
                }
                else {
                    log.warn("No document updated for userID : %s", userID);
                    callback(null, { nModified: 0 });
                }
            });
    },

    // #endregion

    // #region getUserNameByUserAPIKey function

    getUserNameByUserAPIKey: function (userAPIKey, callback) {

        log.addContext("methodName", stackTrace.get()[0].getFunctionName());

        User.findOne({
            userAPIKey: userAPIKey
        }, {
            firstname: 1,
            lastname: 1
        }, function (err, doc) {

            if (err)
                callback(err, null);

            if (doc != null) {

                const userName = `${doc.firstname} ${doc.lastname}`;

                multiClient.set("kUserName:" + userAPIKey, userName, function (err) {

                    if (err)
                        callback(err, null);
                    else
                        callback(null, userName);
                });
            }
        });
    },

    // #endregion

    // #region getAllUsersName function

    getAllUsersName: function (callback) {

        log.addContext("methodName", stackTrace.get()[0].getFunctionName());

        multiClient.get("allUsersName", function (err, allUsersName) {

            if (err) {
                callback(err, null);
                return;
            }

            else if (allUsersName) {

                log.debug("getAllUsersName cache called");
                callback(null, JSON.parse(allUsersName));
            }
            else {
                mongoClient.connect(url, { useUnifiedTopology: true }, function (err, client) {

                    if (err) {
                        callback(err, null);
                        return;
                    }

                    log.debug("getAllUsersName called");

                    var collection = client.db(dbName).collection("users");

                    collection.aggregate([
                        {
                            $project:
                            {
                                userAPIKey: "$userAPIKey",
                                userName: { $concat: ["[", "$firstname", " ", "$lastname", "]"] }
                            }
                        }
                    ], function (err, doc) {

                        if (err) {
                            callback(err, null);
                            return;
                        }

                        client.close();

                        if (doc != null)
                            multiClient.set("allUsersName", JSON.stringify(doc), function (err) {

                                if (err)
                                    callback(err, null);
                                else
                                    callback(null, doc);
                            });
                        else
                            callback(null, null);
                    });
                });
            }
        });
    },

    // #endregion

    // #region getUserByUserAPIKey function

    getUserByUserAPIKey: function (userAPIKey, callback) {

        log.addContext("methodName", stackTrace.get()[0].getFunctionName());

        multiClient.get(userAPIKey, function (err, user) {

            if (err) {
                callback(err, null);
                return;
            }
            else if (user) {
                log.debug("getUserByUserAPIKey cache called");
                callback(null, JSON.parse(user));
            }

            else {
                User.findOne({
                    userAPIKey: userAPIKey
                },
                    {
                        _id: 1,
                        //userID: 1,
                        firstname: 1,
                        lastname: 1,
                        email: 1,
                        userAPIKey: 1,
                        registerDate: 1,
                        role: 1,
                        phoneNumber: 1,
                        companyID: 1,
                        password: 1,
                        isEmailConfirmed: 1,
                        isActive: 1,
                        registerToken: 1,
                        resetPasswordToken: 1,
                        resetPasswordDate: 1,
                        isAdmin: 1
                    },
                    function (err, doc) {

                        if (err) {
                            callback(err, null);
                        }

                        if (doc != null) {

                            doc["userID"] = doc["_id"];
                            //delete doc["_id"];

                            multiClient.set(userAPIKey, JSON.stringify(doc), function (err) {

                                multiClient.set("kUserName:" + userAPIKey, doc.firstname + " " + doc.lastname);

                                if (err)
                                    callback(err, null);
                                else
                                    callback(null, doc);
                            });
                        }
                        else
                            callback(null, null);
                    });
            }
        });
    },

    // #endregion

    // #region getUserByEmail function

    getUserByEmail: function (email, callback) {

        log.addContext("methodName", stackTrace.get()[0].getFunctionName());

        User.findOne(
            {
                email: email
            },
            {
                _id: 1,
                //userID: 1,
                firstname: 1,
                lastname: 1,
                email: 1,
                userAPIKey: 1,
                registerDate: 1,
                role: 1,
                phoneNumber: 1,
                companyID: 1,
                password: 1,
                isEmailConfirmed: 1,
                isActive: 1,
                registerToken: 1,
                resetPasswordToken: 1,
                resetPasswordDate: 1,
                isAdmin: 1
            })
            .exec(function (err, doc) {

                if (err) {
                    callback(err);
                    return;
                }

                if (doc != null) {

                    doc["userID"] = doc["_id"];

                    multiClient.set(doc.userAPIKey, JSON.stringify(doc), function (err) {

                        multiClient.set("kUserName:" + doc.userAPIKey, doc.firstname + " " + doc.lastname);

                        if (err)
                            callback(err, null);
                        else
                            callback(null, doc);
                    });
                }
                else
                    callback(null, null);
            });
    },

    // #endregion
    
    // #region getRateBasesByUserAPIKey function

    getRateBasesByUserAPIKey: function (userAPIKey, callback) {

        log.addContext("methodName", stackTrace.get()[0].getFunctionName());

        mongoClient.connect(url, { useUnifiedTopology: true}, function (err, client) {

            if (err) {
                callback(err, null);
                return;
            }

            var collection = client.db(dbName).collection("users");

            collection.findOne({ userAPIKey: userAPIKey }, { rateBases: 1, _id: 0 }, function (err, doc) {

                if (err)
                    callback(err, null);

                client.close();

                if (doc != null) {

                    multiClient.set("kRateBase:" + userAPIKey + "-" + doc.rateBases[0].name, JSON.stringify(doc.rateBases[0].rates))
                    multiClient.set("kRateBase:" + userAPIKey + "-" + doc.rateBases[1].name, JSON.stringify(doc.rateBases[1].rates));

                    callback(null, doc.rateBases);
                }
                else
                    callback(null, null);
            });
        });
    },

    // #endregion

    // #region getRateBaseByUserAPIKey function

    getRateBaseByUserAPIKey: function (userAPIKey, rateBaseName, callback) {

         log.addContext("methodName", stackTrace.get()[0].getFunctionName());

         multiClient.get("kRateBase:" + userAPIKey + "-" + rateBaseName, function (err, rateBase) {

             if (err)
                 callback(err, null);

             else if (rateBase) {

                 log.debug("getRateBaseByUserAPIKey cache called");
                 callback(null, JSON.parse(rateBase));
             }

             else {
                 mongoClient.connect(url, { useUnifiedTopology: true}, function (err, client) {

                     if (err)
                         callback(err, null);

                     var collection = client.db(dbName).collection("users");

                     collection.findOne(
                         {
                             userAPIKey: userAPIKey,
                             "rateBases.name": rateBaseName
                         },
                         {
                             "rateBases.$": 1, _id: 0
                         },
                         function (err, doc) {

                             if (err)
                                 callback(err, null);

                             client.close();

                             if (doc != null) {

                                 multiClient.set("kRateBase:" + userAPIKey + "-" + rateBaseName, JSON.stringify(doc.rateBases[0].rates));
                                 callback(null, doc.rateBases[0].rates);
                             }
                             else
                                 callback(null, null);
                         });
                 });
             }
         });
    },

    // #endregion

    //#region updateRateBase function

    updateRateBase: function (userAPIKey, rateBaseName, updateParam, action, callback) {

         log.addContext("methodName", stackTrace.get()[0].getFunctionName());

        var update = (action == "UPD") ? { $set: updateParam } : (action == "DEL") ? { $unset: updateParam } : "";
        
        mongoClient.connect(url, { useUnifiedTopology: true}, function (err, client) {

            if (err)
                callback(err, null);

            var collection = client.db(dbName).collection("users");

            collection.updateOne(
                {
                    userAPIKey: userAPIKey,
                    "rateBases.name": rateBaseName
                },
                //{ $set: { "rateBases.$.rates": rateBase } },
                update,
                { upsert: true },

                function (err, res) {

                    if (err)
                        callback(err, null);

                    client.close();

                    if (res != null) {

                        if (res.result.n == 1) {

                            log.trace("RateBase \"%s\" updated successfully for userAPIKey %s", rateBaseName, userAPIKey);

                            multiClient.get("kRateBase:" + userAPIKey + "-" + rateBaseName, function (err, reply) {
                                
                                if (err)
                                    callback(err, null);

                                else if (reply) {

                                    log.debug("updateRateBase cache called");

                                    var rateBase = JSON.parse(reply);
                                    var splitted = Object.keys(updateParam)[0].split(".");
                                    var lotIndex;

                                    switch (splitted.length) {

                                        case 4:
                                            lotIndex = splitted[3];

                                            if (action == "UPD")
                                                rateBase[lotIndex] = updateParam[Object.keys(updateParam)[0]];
                                            else if (action == "DEL")
                                                delete rateBase[lotIndex];
                                            break;

                                        case 5 :
                                             lotIndex = splitted[3];

                                            if (action == "UPD")
                                                rateBase[lotIndex].name = updateParam[Object.keys(updateParam)[0]];
                                            break;

                                        case 6:

                                            lotIndex = splitted[3];
                                            var designationIndex = splitted[5];

                                            if (action == "UPD")
                                                rateBase[lotIndex].data[designationIndex] = updateParam[Object.keys(updateParam)[0]];
                                            else if (action == "DEL")
                                                delete rateBase[lotIndex].data[designationIndex];

                                            break;

                                        case 7:
                                            lotIndex = splitted[3];
                                            var designationIndex = splitted[5];
                                            var designationProp = splitted[6];

                                            if (action == "UPD")
                                                rateBase[lotIndex].data[designationIndex][designationProp] = updateParam[Object.keys(updateParam)[0]];
                                    }

                                    multiClient.set("kRateBase:" + userAPIKey + "-" + rateBaseName, JSON.stringify(rateBase));
                                    callback(null, { nModified: 1 });

                                }
                            });
                        }
                        else {

                            log.trace("RateBase \"%s\" not updated for userAPIKey %s", rateBaseName, userAPIKey);
                            callback(null, { nModified: 0 });
                        }
                    }
                });
        });
    },

    //#endregion

    // #region getCustomersByUserAPIKey function

    getCustomersByUserAPIKey: function (userAPIKey, fields, callback) {

        log.addContext("methodName", stackTrace.get()[0].getFunctionName());

        mongoClient.connect(url, { useUnifiedTopology: true}, function (err, client) {

            if (err)
                callback(err, null);

            var filedsToReturn = new Object();
            filedsToReturn["_id"] = 0;
            filedsToReturn["customerID"] = 1;

            fields.forEach(function (field) {
                filedsToReturn[field] = 1;
            });

            var collection = client.db(dbName).collection("customers");

            collection.aggregate([
                {
                    $match: { userAPIKey: userAPIKey }
                },
                {
                    $addFields: { customerID: "$_id" }
                },
                {
                    //
                    // MongoDB $project stage
                    $project: filedsToReturn
                }
            ]).sort({ "lastName": 1 }).toArray(function (err, doc) {

                if (err)
                    callback(err, null)

                client.close();

                if (doc != null)
                    callback(null, doc);
                else
                    callback(null, null);
            });
        });
    },

    // #endregion

    // #region getCustomerByUserAPIKeyAndCustomerID function

    getCustomerByUserAPIKeyAndCustomerID: function (userAPIKey, customerID, callback) {

        log.addContext("methodName", stackTrace.get()[0].getFunctionName());

        multiClient.get("kCustomer:" + userAPIKey + "-" + customerID, function (err, customer) {

            if (err)
                callback(err, null);

            else if (customer) {

                log.debug("getCustomerByUserAPIKeyAndCustomerID cache called");
                callback(null, JSON.parse(customer));
            }
            else {

                mongoClient.connect(url, { useUnifiedTopology: true}, function (err, client) {

                    if (err)
                        callback(err, null);

                    const db = client.db(dbName);
                    const collection = db.collection("customers");

                    collection.findOne(
                        {
                            
                                userAPIKey: userAPIKey,
                                _id: ObjectId(customerID)
                        
                        },
                        /*{
                            $addFields: { customerID: "$_id" }
                        },*/
                        {
                            //
                            // MongoDB $project stage
                            $project: {
                                _id: 0
                            }
                        }
                       
                    , function (err, doc) {

                        if (err)
                            callback(err, null);

                        client.close();

                        if (doc != null){

                            doc.customerID = doc._id;
                            delete doc._id;

                            multiClient.set("kCustomer:" + userAPIKey + "-" + customerID, JSON.stringify(doc), function (err) {
                                callback(null, doc);
                            });
                        }
                        else
                            callback(null, null);
                    });
                });
            }
        });
    },

    // #endregion

    //#region createCustomer function

    createCustomer: function (customer, callback) {

         log.addContext("methodName", stackTrace.get()[0].getFunctionName());

         mongoClient.connect(url, { useUnifiedTopology: true}, function (err, client) {

             if (err)
                 callback(err, null);

             var collection = client.db(dbName).collection("customers");

             collection.insertOne(
                 customer,

                 function (err, res) {

                     if (err)
                         callback(err, null);

                     client.close();

                     if (res != null) {

                         if (res.insertedCount == 1) {

                             log.trace("Customer successfully created...");

                             var customer = res.ops[0];
                             customer["customerID"] = customer["_id"];
                             delete customer["_id"];

                             multiClient.set("kCustomer:" + customer.userAPIKey + "-" + res.insertedId, JSON.stringify(customer), function (err) {

                                 if (err)
                                     callback(err, null);
                                 else
                                     callback(null, { inserted: res.insertedCount, customerID: res.insertedId });
                             });
                         }
                         else {
                             log.warn("No customer created...");
                             callback(null, { inserted: 0 });
                         }
                     }
                 });
         });
    },

    // #endregion

    // #region updateCustomer function

    updateCustomer: function (customer, callback) {

         log.addContext("methodName", stackTrace.get()[0].getFunctionName());

         mongoClient.connect(url, { useUnifiedTopology: true}, function (err, client) {

             if (err)
                 callback(err, null);

             var collection = client.db(dbName).collection("customers");

             var customerID = customer["customerID"];
             delete customer["customerID"];

             collection.findAndModify(
                 {
                     userAPIKey: customer.userAPIKey,
                     _id: ObjectId(customerID)
                 },
                 [],
                 { $set: customer },
                 //{ upsert: true },
                 { new: true },

                 function (err, res) {

                     if (err)
                         callback(err, null);

                     client.close();

                     if (res != null) {

                         if (res.ok == 1) {

                             log.trace("Customer successfully updated...");

                             customer["customerID"] = customerID;

                             multiClient.set("kCustomer:" + customer.userAPIKey + "-" + customerID, JSON.stringify(customer), function (err) {

                                 if (err)
                                     callback(err, null);
                                 else
                                     callback(null, { nModified: 1 });
                             });
                         }
                         else {

                             log.warn("No customer updated...");
                             callback(null, { nModified: 0 });
                         }
                     }
                 });
         });
    },

    // #endregion

    // #region deleteCustomer function

    deleteCustomer: function (customerID, userAPIKey, callback) {

         log.addContext("methodName", stackTrace.get()[0].getFunctionName());

         mongoClient.connect(url, { useUnifiedTopology: true}, function (err, client) {

             if (err) {
                 callback(err, null);
                 return;
             }

             var collection = client.db(dbName).collection("customers");

             collection.deleteOne(
                 {
                     userAPIKey: userAPIKey,
                     _id: ObjectId(customerID)
                 },

                 function (err, res) {

                     if (err)
                         callback(err, null);

                     client.close();

                     if (res != null) {

                         if (res.deletedCount == 1) {

                             log.trace("Customer successfully deleted...");

                             multiClient.del("kCustomer:" + userAPIKey + "-" + customerID, function (err) {

                                 if (err)
                                     callback(err, null);
                                 else
                                     callback(null, { deleted: res.deletedCount });
                             });
                         }
                         else {

                             log.trace("No customer deleted...");
                             callback(null, { deleted: 0 });
                         }
                     }
                 });
         });
    },

    // #endregion

    // #region getRoles function

    getRoles: function (callback) {

         log.addContext("methodName", stackTrace.get()[0].getFunctionName());

        mongoClient.connect(url, { useUnifiedTopology: true}, function (err, client) {

            if (err) {
                callback(err);
                return;
            }

            var collection = client.db(dbName).collection("roles");

            collection.aggregate([
                {
                    $addFields: { roleID: "$_id" }
                },
                {
                    //
                    // MongoDB $project stage
                    $project: { _id: 0 }
                }
            ]).toArray(function (err, doc) {

                if (err) {
                    callback(err);
                    return;
                }

                client.close();

                if (doc != null);
                    callback(null, doc);
            });
        });
    },

    // #endregion

    // #region getMetrics function

    getMetrics: function (callback) {

         log.addContext("methodName", stackTrace.get()[0].getFunctionName());

        mongoClient.connect(url, { useUnifiedTopology: true}, function (err, client) {

            if (err) {
                callback(err);
                return;
            }

            var collection = client.db(dbName).collection("metrics");

            collection.find().toArray(function (err, doc) {

                if (err) {
                    callback(err);
                    return;
                }

                client.close();

                if (doc != null)
                    callback(null, doc);

            });
        });
    },

    // #endregion

    // #region getTitles function

    getTitles: function (callback) {

         log.addContext("methodName", stackTrace.get()[0].getFunctionName());

        mongoClient.connect(url, { useUnifiedTopology: true}, function (err, client) {

            if (err) {
                callback(err);
                return;
            }

            var collection = client.db(dbName).collection("titles");

            collection.aggregate([
                {
                    $addFields: { titleID: "$_id" }
                },
                {
                    //
                    // MongoDB $project stage
                    $project: { _id: 0 }
                }
            ]).toArray(function (err, doc) {

                if (err) {
                    callback(err);
                    return;
                }

                client.close();

                if (doc != null);
                callback(null, doc);
            });
        });
    },

    // #endregion

    // #region getStatus function

    getStatus: function (callback) {

         log.addContext("methodName", stackTrace.get()[0].getFunctionName());

         mongoClient.connect(url, { useUnifiedTopology: true}, function (err, client) {

             if (err) {
                 callback(err);
                 return;
             }

             var collection = client.db(dbName).collection("status");

             collection.aggregate([
                 {
                     $addFields: { statusID: "$_id" }
                 },
                 {
                     //
                     // MongoDB $project stage
                     $project: { _id: 0 }
                 }
             ]).toArray(function (err, doc) {

                 if (err) {
                     callback(err);
                     return;
                 }

                 client.close();

                 if (doc != null);
                 callback(null, doc);
             });
         });
    },

    // #endregion

    // #region getCompanyByID function

    getCompanyByCompanyID: function (companyID, callback) {

        log.addContext("methodName", stackTrace.get()[0].getFunctionName());

        mongoClient.connect(url, { useUnifiedTopology: true}, function (err, client) {

            if (err)
                callback(err, null);

            var collection = client.db(dbName).collection("companies");

            collection.findOne({ _id: ObjectId(companyID) }, function (err, doc) {

                if (err)
                    callback(err, null);

                client.close();
                
                if (doc != null)
                    callback(null, doc);

            });
        });
    },

    // #endregion

    // #region updateCompany function

    updateCompany: function (companyID, company, callback) {

         log.addContext("methodName", stackTrace.get()[0].getFunctionName());

         mongoClient.connect(url, { useUnifiedTopology: true}, function (err, client) {

             if (err)
                 callback(err, null);

             var collection = client.db(dbName).collection("companies");

             collection.updateOne(
                 {
                     _id: ObjectId(companyID)
                 },
                 {
                     $set: company
                 },
                 function (err, res) {

                     if (err)
                         callback(err, null);

                     if (res.result.nModified == 1) {
                         log.trace("Document updated successfully for companyID : %s", companyID);
                         //log.trace("Company updated :\r\n%s", util.inspect(company));
                     }
                     else
                         log.warn("No document updated for companyID : %s", companyID);

                     callback(null, { nModified: res.result.nModified });
                     client.close();
                 });
         });
    },

    // #endregion

    // #region createCompany function

    createCompany: function (callback) {

         log.addContext("methodName", stackTrace.get()[0].getFunctionName());

         mongoClient.connect(url, { useUnifiedTopology: true}, function (err, client) {

             if (err)
                 callback(err, null);
             
             var collection = client.db(dbName).collection("companies");

             collection.insertOne(
                 {
                     //"companyID": companyID,
                     "name": "",
                     "phoneNumber": "",
                     "informations": {
                         "socialReason": "",
                         "legalForm": "",
                         "siret": "",
                         //"vat": parseFloat(0),
                         "ape": "",
                         "employeesNumber": "",
                         "taxNumber": "",
                         "insurance": ""
                     },
                     "address": {
                         "street": "",
                         "additional": "",
                         "zip": "",
                         "city": "",
                         "country": "",
                         "state": ""
                     }
                 },
                 function (err, res) {

                     if (err)
                         callback(err, null);

                     client.close();

                     if (res.result.n == 1) {
                         log.trace("Document inserted successfully - new companyID : %s", res.insertedId);
                         callback(null, { inserted: res.insertedCount, companyID: res.insertedId });
                     }
                     else {
                         log.warn("No document inserted in companies");
                         callback(null, null);
                     }
                 });
         });
    },

    // #endregion

    // #region deleteCompany function

    deleteCompany: function (companyID, callback) {

        log.addContext("methodName", stackTrace.get()[0].getFunctionName());

        mongoClient.connect(url, { useUnifiedTopology: true}, function (err, client) {

            if (err) {
                callback(err, null);
            }

            var collection = client.db(dbName).collection("companies");

            collection.deleteOne(
                {
                    _id: ObjectId(companyID)
                },

                function (err, res) {

                    if (err)
                        callback(err, null);

                    client.close();

                    if (res != null) {

                        if (res.deletedCount == 1) {

                            log.trace("Company successfully deleted...");
                            callback(null, { deleted: res.deletedCount });
                        }
                        else {

                            log.trace("No company deleted...");
                            callback(null, { deleted: 0 });
                        }
                    }
                });
        });
    },

    // #endregion

    // #region updateImage function

    updateImage: function (image, callback) {

         log.addContext("methodName", stackTrace.get()[0].getFunctionName());
        
         const client = new mongoClient(uriJSReport, { useNewUrlParser: true });
         client.connect(err => {

            if (err) {
                callback(err);
                return;
            }

            const collection = client.db(jsreportDbName).collection("images");

            collection.updateOne(
            {
                name: image.name
            },
            {
                $set: {
                    
                    "name" : image.name,
                    "contentType" : image.contentType,
                    "content": image.content,
                    "shortid": image.shortid,
                    "creationDate": image.creationDate,
                    "modificationDate": image.modificationDate
                },
            },
            {
                upsert: true
            },
            function (err, res) {
                
                if (err) {
                    callback(err, { nModified: null });
                    return;
                }

                if (res.result.n == 1)
                    log.trace("Document updated successfully for shortid : %s", image.shortid);
                else
                    log.warn("No document updated for shortid : %s", image.shortid);

                callback(null, { nModified: res.result.n });
                client.close();
            });
        });
    },
	
	// #endregion

	// #region updateAsset function
	
	updateAsset: function (asset, callback) {

         log.addContext("methodName", stackTrace.get()[0].getFunctionName());
        
         const client = new mongoClient(uriJSReport, { useNewUrlParser: true });
         client.connect(err => {

            if (err) {
                callback(err);
                return;
            }

            const collection = client.db(jsreportDbName).collection("assets");

            collection.updateOne(
            {
                name: asset.name
            },
            {
                $set: {
                    
                    "name" : asset.name,
                    "content": asset.content,
                    "shortid": asset.shortid,
                    "modificationDate": asset.modificationDate
                },
            },
            {
                upsert: true
            },
            function (err, res) {
                
                if (err) {
                    callback(err, { nModified: null });
                    return;
                }

                if (res.result.n == 1)
                    log.trace("Document updated successfully for shortid : %s", asset.shortid);
                else
                    log.warn("No document updated for shortid : %s", asset.shortid);

                callback(null, { nModified: res.result.n });
                client.close();
            });
        });
    }
	
	// #endregion
};

module.exports = database;
module.id = "database";