'use strict';

var express = require('express');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var urlencoded=require('urlencode');
var json = require('json');
var path = require('path');
var multiparty = require('connect-multiparty')();
var fs = require('fs-extra');
var http = require('http');
var https = require('https');
//var serveIndex = require('serve-index');
var csrf = require('csurf');

let mongoose = require('mongoose');
let utils = require("./utils/utils");

const cors = require('cors');

/*
 * Log4js
 */
var log4js = require("log4js");
var log = log4js.getLogger(require("path").basename(__filename, ".js"));

/*
 * Configuration : App Settings
 */
var appSettings = config.settings();

/*
 * cert options
 */
try {
    var options = {};

    /*if (environment === "development")

        options = {
            pfx  : require('fs').readFileSync('./cert/beta.optimoc.pfx'),
            passphrase: 'passphrase',
            requestCert: true
        };
    else*/
        options = {
            key: fs.readFileSync("./cert/optimoc.fr.key.pem"),
            cert: fs.readFileSync("./cert/optimoc.fr.crt.pem"),
            ca: fs.readFileSync("./cert/issuer.crt.pem")
            //passphrase: "optimoc"
        };
}
catch (err) {
    log.error(new Error("[app]: Error occured" + " - " + err.message));
}

/*
 * Mongoose
 */
require('./models/userModel');
mongoose.set('useUnifiedTopology', true);
mongoose.set('useNewUrlParser', true);
mongoose.set('useCreateIndex', true);
mongoose.set('useFindAndModify', false);
mongoose.connect(appSettings.CONN_STR_OPTIMOC);

/*
 * Init db
 */
var db = require('./db/db');
db.init();

/*
 * Api
 */
var metricAPI = require('./api/metricAPI');
var userAPI = require('./api/userAPI');
var companyAPI = require('./api/companyAPI');
var projectAPI = require('./api/projectAPI');
//var sysAPI = require('./api/sysAPI');
var roleAPI = require('./api/roleAPI');
var reportAPI = require('./api/reportAPI');
var imageAPI = require('./api/imageAPI');
var rateBaseAPI = require('./api/rateBaseAPI');
var customerAPI = require('./api/customerAPI');
var referenceAPI = require('./api/referenceAPI');

/*
 * Optimoc Server app
 */
var optimocApp = {

    init: function (callback) {
        this.app = express();
		this.httpApp = express(),
        this.setup();
        callback();
    },

    setup: function () {

        log.addContext("methodName", stackTrace.get()[0].getFunctionName());

        var app = this.app,
            httpApp = this.httpApp,
            __frontDirname = __dirname + "/../../Optimoc.Front/dist/build";

        httpApp.set("port", appSettings.HTTP_PORT);

        //
        // CORS
        var allowedOrigins = ["https://app.optimoc.fr", "https://dev.optimoc.fr"];

        app.use(cors({
            origin: function (origin, callback) {

                if (!origin) return callback(null, true);

                if (allowedOrigins.indexOf(origin) === -1) {
                    var msg = 'The CORS policy for this site does not ' + 'allow access from the specified Origin.';
                    return callback(new Error(msg), false);
                }

                return callback(null, true);
            },
            methods: ["GET", "PUT", "POST", "DELETE", "OPTIONS"],
            allowedHeaders: ["Content-Type", "Authorization", "Content-Length", "X-Requested-With"],
            credentials: true
        }));

        log.warn("CROSS ORIGIN ACTIVATED");

        app.use(bodyParser.urlencoded({ extended: false }));
        app.use(bodyParser.json());
        app.use(cookieParser());
        app.use(csrf({ cookie: true }));
        app.use(express.static(__frontDirname));

        app.use("/cloud", express.static(__dirname + "/../cloud"))
        //app.use("/cloud", serveIndex(__dirname + "/../cloud", { "icons": true, "view": "details" }));
        app.use(log4js.connectLogger(log4js.getLogger("http"), { level: "auto" }));

        var paths = ["/app/*", "/login", "/forgotPassword", "/register", "/logout", "/account/confirmEmail", "/account/resetPassword", "/unauthorized"];

        /**
         * [GET]
         * 
         */
        httpApp.get("*", function (req, res, next) {

            if (req.headers.host !== undefined)
                if (req.headers.host.split(':')[0] === "app.optimoc.fr" || req.headers.host.split(':')[0] === "dev.optimoc.fr") {
                    res.redirect("https://" + req.headers.host.split(':')[0] + req.url);
                }
                else {
                    res.redirect("https://" + req.headers.host.split(':')[0] + ":" + appSettings.HTTPS_PORT + req.url);
                }
        });

        app.get(paths, function (req, res) {
            res.cookie('XSRF-TOKEN', req.csrfToken(), { secure: true });
            res
                .status(200)
                .set({ 'content-type': 'text/html; charset=utf-8' })
                .sendFile('index.html', { root: __frontDirname });
        });

        /**
         * UserAPI [POST]
         *      Disponible compte demo :
         *          Login : demo@optimoc.fr
         *          Password : demo
         *
         * Params : : - action {String} "userRegistration" || "userLogin" ...
         *            - login {String} username || email
         *            - password {String} password
         */
        app.post('/user', utils.checkToken, userAPI.load);

        /**
         * CompanyAPI [POST]
         *
         */
        app.post('/company', utils.checkToken, companyAPI.load);

        /**
         * ProjectAPI [POST]
         *  params : - action {String} "createProject" || "getProjects" ...
         *           - userID {String}
         *           - userAPIKey {String}
         *           - requestData {Object}
         */
        app.post('/project', utils.checkToken, projectAPI.load);

        /**
         * MetricAPI [POST]
         *  params : - action {String} "setMetrics" || "getMetrics" ...
         *           - userID {String}
         *           - userAPIKey {String}
         *           - requestData {Object}
         */
        app.post('/metric', metricAPI.load);

        /**
         * SysAPI [POST]
         * params : - action {string} : "cleanDatabase" || "backupDatabase"
         *          - data {object} :
         */
        //app.post('/system', sysAPI.load);

        /**
        * RoleAPI [POST]
        * params : - action {string} : "getRoles"
        *          - data {object} :
        */
        app.post('/role', roleAPI.load);

        /**
         * ReportAPI [GET]
         * params : - action {string} : "quote || ..."
         *          - data {object} :
         */
        app.post('/report', reportAPI.load);

        /**
         * rateBaseAPI [POST]
         * params : - action {string} : "getRateBases || ..."
         *          - data {object} :
         */
        app.post('/rateBase', rateBaseAPI.load);

        /**
         * customerAPI [POST]
         * params : - action {string} : "getCustomers || ..."
         *          - data {object} :
         */
        app.post('/customer', utils.checkToken, customerAPI.load);

        /**
         * titleAPI [POST]
         * params : - action {string} : "getTitles || ..."
         *          - data {object} :
         */
        app.post('/reference', referenceAPI.load);


        /**
        *
        */
        app.post("/upload", multiparty, function (req, res) {

            var folder = req.body.folder;
            var tempPath = req.files.file.path;
            var fileName = req.files.file.originalFilename;
            var destFile =  path.join(__frontDirname, "images", folder, fileName);

            fs.copy(tempPath, destFile, function (err) {

                if (err) {

                    log.error(new Error("[upload]: Error occured" + " - " + err.message));
                    res.json({ error: true, data: err.message });
                    //return;
                }

                if (folder == "logos") {

                    var data = {
                        fileName: fileName,
                        path: destFile
                    };

                    imageAPI.updateImage(res, req.cookies["ls.userAPIKey"], data);
                }
                else
                    res.json({ error: false, data: null });

            });
        });

        /**
         * Set Response header and not found case
         */
        app.use(function (err, req, res, next) {

            if (err.code !== 'EBADCSRFTOKEN') return next(err)

            //var data = { "message": "There is no request pointing at these address." };
            //res.status(404).send(data);
            res
                .status(200)
                .set({ "content-type": "text/html; charset=utf-8" })
                .sendFile("/views/404.html", { root: __frontDirname });
        });

        http.createServer(httpApp).listen(appSettings.HTTP_PORT, function () {
            log.trace("HTTP server listening on port " + appSettings.HTTP_PORT);
        });

        https.createServer(options, app).listen(appSettings.HTTPS_PORT, function () {
            log.trace("HTTPS server listening on port " + appSettings.HTTPS_PORT);
        });
    }
};

module.exports = optimocApp;
module.exports.id = "optimocApp";