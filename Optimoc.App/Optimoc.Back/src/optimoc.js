"use strict";

//var dateFormat = require("dateformat");
//var now = dateFormat("yyyy-mm-dd HH:MM:ss");

//var debugLogger = require("debug-logger");
//debugLogger.levels.error.prefix = debugLogger.levels.debug.prefix = debugLogger.levels.info.prefix = debugLogger.levels.trace.prefix = debugLogger.levels.trace.prefix = "[" + now + "] ";

//var log = debugLogger("optimoc");
var log = require("log4js").getLogger(require("path").basename(__filename, ".js"));

var application = {

    init: function () {

        var app = require("./app");

        log.addContext("methodName", stackTrace.get()[0].getFunctionName());
        app.init(function () {
            log.trace("Optimoc app Launched...");
        });
    },

    //run: function (server) {
    //    server.listen();
    //}
};

module.exports = application;