'use strict';

/**
  * @param profil 0:DEV, 1:TEST, 2:PROD
  */
var configuration = (function () {

    const environment = process.env.NODE_ENV.trim();
    const dotenv = require("dotenv");
    const path = require("path");

    const config = dotenv.config({ path: path.join(__dirname, `${environment}.env`) });

    if (config.error)
        throw config.error

    return {

        environment: environment,
        settings: function () {
            return config.parsed;
        },
        errors: function () {
            return require('./errors.json').errors;
        }
    }

})();

module.exports = configuration;
module.id = "configuration";