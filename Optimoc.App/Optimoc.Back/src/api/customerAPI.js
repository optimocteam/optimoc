﻿'use strict';

var log = require('log4js').getLogger(require("path").basename(__filename, ".js"));

setTimeout(function () {
    log.addContext("methodName", stackTrace.get()[0].getMethodName());
    log.trace('Customer API Loaded.');
}, 500);

var customerController = require('../controllers/customerController');

var customerAPI = {

    //#region load function

    load: function (req, res, next) {

        var response;

        if (req.body == undefined) {

            response = {
                error_code: "POST_PARAMS_NOT_SUPPLIED."
            };

            res.status(400).json(response);

        } else if (req.body.action == undefined) {

            response = {
                error_code: "ACTION_NOT_DEFINED"
            };

            res.staus(400).json(response);
        }
        else if (req.body.data == undefined) {
            response = {
                error_code: "DATA_NOT_DEFINED"
            };

            res.status(400).json(response);
        }
        else {

            var action = req.body.action,
                data;

            switch (action) {
                case "getCustomers":
                    data = req.body.data;
                    customerAPI.getCustomers(res, data);
                    break;

                case "getCustomer":
                    data = req.body.data;
                    customerAPI.getCustomer(res, data);
                    break;

                case "updateCustomer":
                    data = req.body.data;
                    customerAPI.updateCustomer(res, data);
                    break;

                case "createCustomer":
                    data = req.body.data;
                    customerAPI.createCustomer(res, data);
                    break;

                case "deleteCustomer":
                    data = req.body.data;
                    customerAPI.deleteCustomer(res, data);
                    break;
            }
        }
    },

    // #endregion

    // #region getCustomers function

    getCustomers: function (res, data) {
        customerController.getCustomers(res, data);
    },

    // #endregion

    // #region getCustomer function

    getCustomer: function (res, data) {
        customerController.getCustomer(res, data);
    },

    // #endregion

    // #region updateCustomer function

    updateCustomer: function (res, data) {
        customerController.updateCustomer(res, data);
    },

    // #endregion

    // #region createCustomer function

    createCustomer: function (res, data) {
        customerController.createCustomer(res, data);
    },

    // #endregion

    // #region deleteCustomer function

    deleteCustomer: function (res, data) {
        customerController.deleteCustomer(res, data);
    }

    // #endregion
};

module.exports = customerAPI;
module.exports.id = "customerAPI";