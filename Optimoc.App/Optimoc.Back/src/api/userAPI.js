'use strict';

var log = require('log4js').getLogger(require("path").basename(__filename, ".js"));

setTimeout(function () {
     log.addContext("methodName", stackTrace.get()[0].getMethodName());
    log.trace('User Api Loaded.');
}, 500);

var userController = require('../controllers/userController');

var userAPI = {

    load: function (req, res, next) {
        
        var response;

        if (req.body == undefined) {
            response = {
                error_code: "POST_PARAMS_NOT_SUPPLIED."
            };

            res.status(400).json(response);
        } else if (req.body.action == undefined) {
            response = {
                error_code: "ACTION_NOT_DEFINED"
            };

            res.status(400).json(response);
        }
        else {

            var action = req.body.action,
                isAdmin = req.isAdmin,
                data;
            
            switch (action) {

                case "getUser":
                    data = req.body.data;
                    userAPI.getUser(res, data);
                    break;

                case "getUsers":
                    if(isAdmin) {
                        data = req.body.data;
                        userAPI.getUsers(res, data);
                    }
                    else
                        res.status(401).json();
                    break;

                case "updateUser":
                    data = req.body;
                    userAPI.updateUser(res, data);
                    break;

                case "createUser":
                    data = req.body.data;
                    data["baseUrl"] = `${req.protocol}://${req.headers.host}`;
                    userAPI.createUser(res, data);
                    break;

                case "forgotPassword":
                    data = req.body.data;
                    data["baseUrl"] = `${req.protocol}://${req.headers.host}`;
                    userAPI.forgotPassword(res, data);
                    break;

                case "resetPassword":
                    data = req.body.data;
                    data["baseUrl"] = `${req.protocol}://${req.headers.host}`;
                    userAPI.resetPassword(res, data);
                    break;

                case "deleteUser":
                    response = { "error": false, data: userAPI.deleteUser(data) };
                    break;

                case "confirmEmail":
                    data = req.body.data;
                    userAPI.confirmEmail(res, data);
                    break;

                case "logoutUser":
                    data = req.body.data;
                    userAPI.logoutUser(res, data);
                    break;
            }
        }
    },

    getUser: function (res, data) {
        userController.getUser(res, data);
    },

    getUsers: function (res, data) {
        userController.getUsers(res, data);
    },
    updateUser: function (res, data) {
        userController.updateUser(res, data);
    },

    createUser: function (res, data) {
        userController.createUser(res, data);
    },

    forgotPassword: function (res, data) {
        userController.forgotPassword(res, data);
    },

    resetPassword: function (res, data) {
        userController.resetPassword(res, data);
    },

    deleteUser: function (data) {
        return userController.deleteUser(data);
    },

    confirmEmail: function (res, data) {
        userController.confirmEmail(res, data);
    },

    logoutUser: function (res, data) {
        userController.logoutUser(res, data);
    },
};

module.exports = userAPI;
module.exports.id = "userAPI";