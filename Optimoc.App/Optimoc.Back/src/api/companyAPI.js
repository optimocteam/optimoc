﻿'use strict';

var log = require('log4js').getLogger(require("path").basename(__filename, ".js"));

setTimeout(function () {
    log.addContext("methodName", stackTrace.get()[0].getMethodName());
    log.trace("Company Api Loaded.");
}, 500);

var companyController = require("../controllers/companyController");

var companyAPI = {

    load: function (req, res, next) {

        var response;

        if (req.body == undefined) {
            response = {
                error_code: "POST_PARAMS_NOT_SUPPLIED."
            };

            res.status(400).json(response);

        } else if (req.body.action == undefined) {
            response = {
                error_code: "ACTION_NOT_DEFINED"
            };

            res.staus(400).json(response);
        }
        else {

            var action = req.body.action,
                data;

            switch (action) {

                case "getCompany":
                    data = req.body.data
                    data.fullName = req.fullName;
                    data.userAPIKey = req.cookies["optimoc_user_api_key"];
                    companyAPI.getCompany(res, data);
                    break;

                case "updateCompany":
                    data = req.body;
                    data.fullName = req.fullName;
                    data.userAPIKey = req.cookies["ls.optimoc_user_api_key"];
                    companyAPI.updateCompany(res, data);
                    break;
            }
        }
    },

    getCompany: function (res, userAPIKey, data) {
        companyController.getCompany(res, userAPIKey, data);
    },

    updateCompany: function (res, userAPIKey, data) {
        companyController.updateCompany(res, userAPIKey, data);
    }
};

module.exports = companyAPI;
module.exports.id = "companyAPI";