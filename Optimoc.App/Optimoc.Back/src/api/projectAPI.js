'use strict';

var log = require('log4js').getLogger(require("path").basename(__filename, ".js"));

setTimeout(function () {
    log.addContext("methodName", stackTrace.get()[0].getMethodName());
    log.trace('Project API Loaded.');
}, 500);

var projectController = require('../controllers/projectController');

var projectAPI = {

    //#region load function

    load: function (req, res, next) {

        var response;

        if (req.body == undefined) {

            response = {
                error_code: "POST_PARAMS_NOT_SUPPLIED."
            };

            res.status(400).json(response);

        } else if (req.body.action == undefined) {

            response = {
                error_code: "ACTION_NOT_DEFINED"
            };

            res.staus(400).json(response);
        }
        else if (req.body.data == undefined) {
            response = {
                error_code: "DATA_NOT_DEFINED"
            };

            res.status(400).json(response);
        }
        else {

            var action = req.body.action,
                data = req.body.data;
            
            data.fullName = req.fullName;

            switch (action) {
                case "getProjects":
                    projectAPI.getProjects(res, data);
                    break;

                case "updateProject":
                    projectAPI.updateProject(res, data);
                    break;

                case "createProject":
                    projectAPI.createProject(res, data)
                    break;

                case "cloneProject":
                    projectAPI.cloneProject(res, data)
                    break;

                case "deleteProject":
                    projectAPI.deleteProject(res, data);
                    break;

                case "archiveProject":
                    projectAPI.archiveProject(res, data);
                    break;

                case "unArchiveProject":
                    projectAPI.unArchiveProject(res, data);
                    break;

                case "setCustomerToProject":
                    projectAPI.setCustomerToProject(res, data);
                    break;

                case "getDisclaimerText":
                    projectAPI.getDisclaimerText(res, data);
                    break;
            }
        }
    },

    // #endregion

    // #region getProjects function

    getProjects: function (res, data) {

        projectController.getProjects(res, data);
    },

    // #endregion

    // #region updateProject function

    updateProject: function(res, data){
        projectController.updateProject(res, data);
    },

    // #endregion

    // #region createProject function

    createProject: function(res, data){
        return projectController.createProject(res, data);
    },

    // #endregion

    // #region cloneProject function

    cloneProject: function (res, data) {
        return projectController.cloneProject(res, data);
    },

    // #endregion

    // #region deleteProject function

    deleteProject: function(res, data){
        projectController.deleteProject(res, data);
    },

    // #endregion

    // #region archiveProject function

    archiveProject: function (res, data) {
        projectController.archiveProject(res, data);
    },

    // #endregion

    // #region unArchiveProject function

    unArchiveProject: function (res, data) {
        projectController.unArchiveProject(res, data);
    },

    // #endregion

    // #region setCustomerToProject function

    setCustomerToProject: function (res, data) {
        projectController.setCustomerToProject(res, data);
    },

    // #endregion

    // #region getDisclaimerText function

    getDisclaimerText: function (res, data) {
        projectController.getDisclaimerText(res, data);
    }

    // #endregion
};

module.exports = projectAPI;
module.exports.id = "projectAPI";