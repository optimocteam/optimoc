﻿'use strict';

var log = require('log4js').getLogger(require("path").basename(__filename, ".js"));

setTimeout(function () {
    log.addContext("methodName", stackTrace.get()[0].getMethodName());
    log.trace('Image Api Loaded.');
}, 500);

var imageController = require('../controllers/imageController');

var imageAPI = {

    /*load: function (req, res) {

        var response;

        if (req.body == undefined) {
            response = {
                "error": true, data: "Please send POST params : action, data."
            };

            res.json(response);

        } else if (req.body.action == undefined) {
            response = {
                "error": true, data: "Action is not defined"
            };

            res.json(response);

        }
        else if (req.body.data == undefined) {
            response = {
                "error": true, data: "Data is not defined"
            };

            res.json(response);
        }
        else {

            var action = req.body.action,
                data = req.body.data;

            switch (action) {

                case "updateimage":
                    imageAPI.updateImage(res, data);
                    break;
            }
        }
    },*/

    updateImage: function (res, userAPIKey, data) {
        log.trace("updateImage called");
        imageController.updateImage(res, userAPIKey, data);
    }
};

module.exports = imageAPI;
module.exports.id = "imageAPI";