'use strict';

var log = require('log4js').getLogger(require("path").basename(__filename, ".js"));

setTimeout(function () {
    log.addContext("methodName", stackTrace.get()[0].getMethodName());
    log.trace('Metric API Loaded.');
}, 500);

var metricController = require("../controllers/metricController");


var metricAPI = {

    // #region load function

    load: function (req, res) {

        var response;

        if (req.body == undefined) {
            response = {
                "error": true, data: "Please send POST params (action, data)"
            };

            res.json(response);

        } else if (req.body.action == undefined) {
            response = {
                "error": true, data: "Action is not defined"
            };

            res.json(response);
        }
        else {

            var action = req.body.action;
            var data;

            switch (action) {

                case "getMetrics":

                    data = req.body.data;
                    metricAPI.getMetrics(res, data);
                    break;

                case "getMetricsCaracteristics":

                    data = req.body.data;
                    metricAPI.getMetricsCaracteristics(res, data);
                    break;

                case "setMetricCaracteristics":

                    data = req.body.data;
                    metricAPI.setMetricCaracteristics(res, data);

                    break;

                case "updateMetric":

                    data = req.body;
                    metricAPI.updateMetric(res, data);
                                        
                    break;

                case "createLot":

                    data = req.body.data;
                    metricAPI.createLot(res, data);
                    break;

                case "deleteLot":

                    data = req.body.data;
                    metricAPI.deleteLot(res, data);
                    break;

                case "createDesignation":

                    data = req.body.data;
                    metricAPI.createDesignation(res, data);
                    break;

                case "deleteDesignation":

                    data = req.body.data;
                    metricAPI.deleteDesignation(res, data);
                    break;
            }
        }
    },

    //#endregion

    // #region getMetric function

    getMetrics: function (res, data) {

        metricController.getMetrics(res, data);
    },

    // #endregion

    // #region getMetricsCaracteristics function

    getMetricsCaracteristics: function (res, data) {

        metricController.getMetricsCaracteristics(res, data);
    },

    // #endregion

    // #region updateMetric function

    updateMetric: function (res, data) {

        metricController.updateMetric(res, data);
    },

    // #endregion
    
    // #region setMetricCaracteristics function

    setMetricCaracteristics: function (res, data) {

        metricController.setMetricCaracteristics(res, data);
    },

    // #endregion

     // #region createLot function

    createLot: function (res, data) {
        metricController.createLot(res, data);
    },

    // #endregion

    // #region deleteLot function

    deleteLot: function (res, data) {
        metricController.deleteLot(res, data);
    },

    // #endregion

    // #region createDesignation function

    createDesignation: function (res, data) {
        metricController.createDesignation(res, data);
    },

    // #endregion

    // #region deleteDesignation function

    deleteDesignation: function (res, data) {
        metricController.deleteDesignation(res, data);
    },

    // #endregion

};

module.exports = metricAPI;
module.exports.id = "metricAPI";