'use strict';

setTimeout(function(){
    console.log('System Api Loaded.');
}, 500);

var systemController = require('../controllers/systemController');

var sysAPI = {

    load: function (req, res) {

        if (req.body == undefined) {

            var response = {
                "error": true, data: "Please send POST params : action,data."
            };
        } else if (req.body.action == undefined) {
            var response = {
                "error": true, data: "Action is not defined"
            };
        } else if (req.body.data == undefined) {
            var response = {
                "error": true, data: "Data is not defined"
            };
        } else {
            var action = req.body.action, data = req.body.data;

            switch (action) {

                case "cleanDatabase":
                    var data = sysAPI.cleanDatabase(data);
                    break;

                case "backupDatabase":

                    var data = sysAPI.backupDatabase(data);
                    break;

                case "displayDatabase":

                    var data = sysAPI.displayDatabase(data);
                    break;
            }

            var response = { "error": false, data: data };
        }

        res.end(JSON.stringify(response));
        return;
    },

    cleanDatabase:function(data){
        return systemController.cleanDatabase();
    },

    backupDatabase:function(data){
        return systemController.backupController();
    },

    displayDatabase:function(data){
        return systemController.displayDatabase();
    }
};

module.exports = sysAPI;
module.exports.id = "sysAPI";