﻿'use strict';

var log = require('log4js').getLogger(require("path").basename(__filename, ".js"));

setTimeout(function () {
     log.addContext("methodName", stackTrace.get()[0].getMethodName());
    log.trace('Reference API Loaded.');
}, 500);

var referenceController = require('../controllers/referenceController.js');

var referenceAPI = {

    //#region load function

    load: function (req, res) {

        var response;

        if (req.body == undefined) {
            response = {
                "error": true, data: "Please send POST params : action, data."
            };

            res.json(response);

        } else if (req.body.action == undefined) {
            response = {
                "error": true, data: "Action is not defined"
            };

            res.json(response);
        }
        /*else if (req.body.data == undefined) {
            response = {
                "error": true, data: "Data is not defined"
            };

            res.json(response);
        }*/
        else {
            
            var action = req.body.action;
            
            switch (action) {

                case "getTitles":
                    referenceAPI.getTitles(res);
                    break;

                case "getStatus":
                    referenceAPI.getStatus(res);
                    break;

                default:
                    log.error("[referenceAPI]: Wrong action...")
                    break;
            }
        }
    },

    // #endregion

    // #region getTitles function

    getTitles: function (res, data) {

        referenceController.getTitles(res)
    },

    // #endregion

    // #region getStatus function

    getStatus: function (res, data) {

        referenceController.getStatus(res)
    },

    // #endregion

};

module.exports = referenceAPI;
module.exports.id = "referenceAPI";