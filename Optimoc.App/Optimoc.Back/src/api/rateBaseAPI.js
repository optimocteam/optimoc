﻿'use strict';

var log = require('log4js').getLogger(require("path").basename(__filename, ".js"));

setTimeout(function () {
     log.addContext("methodName", stackTrace.get()[0].getMethodName());
    log.trace('Rate Base Api Loaded.');
}, 500);

var rateBaseController = require('../controllers/rateBaseController');

var rateBaseAPI = {

    load: function (req, res) {

        var response;
        
        if (req.body == undefined) {
            response = {
                "error": true, data: "Please send POST params : action, data."
            };

            res.json(response);

        } else if (req.body.action == undefined) {
            response = {
                "error": true, data: "Action is not defined"
            };

            res.json(response);
        }
        else {

            var action = req.body.action,
                data;

            switch (action) {

                case "getRateBases":

                    data = req.body.data;
                    rateBaseAPI.getRateBases(res, data);
                    break;

                case "getRateBaseInCSV":

                    data = req.body.data;
                    rateBaseAPI.getRateBaseInCSV(res, data);
                    break;

                case "updateRateBase":

                    data = (req.body.data == undefined) ? req.body : req.body.data;
                    rateBaseAPI.updateRateBase(res, data);
                    break;

                case "createLot":

                    data = req.body.data;
                    rateBaseAPI.createLot(res, data);
                    break;

                case "deleteLot":

                    data = req.body.data;
                    rateBaseAPI.deleteLot(res, data);
                    break;

                case "createDesignation":

                    data = req.body.data;
                    rateBaseAPI.createDesignation(res, data);
                    break;

                case "deleteDesignation":

                    data = req.body.data;
                    rateBaseAPI.deleteDesignation(res, data);
                    break;
            }
        }
    },

    getRateBases: function (res, data) {
        rateBaseController.getRateBases(res, data);
    },

    getRateBaseInCSV: function (res, data) {
        rateBaseController.getRateBaseInCSV(res, data);
    },

    updateRateBase: function (res, data) {
        rateBaseController.updateRateBase(res, data);
    },

    createLot: function (res, data) {
        rateBaseController.createLot(res, data);
    },

    deleteLot: function (res, data) {
        rateBaseController.deleteLot(res, data);
    },

    createDesignation: function (res, data) {
        rateBaseController.createDesignation(res, data);
    },

    deleteDesignation: function (res, data) {
        rateBaseController.deleteDesignation(res, data);
    },
};

module.exports = rateBaseAPI;
module.exports.id = "rateBaseAPI";