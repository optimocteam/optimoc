﻿'use strict';

var log = require('log4js').getLogger(require("path").basename(__filename, ".js"));

setTimeout(function () {
     log.addContext("methodName", stackTrace.get()[0].getMethodName());
    log.trace('Role API Loaded.');
}, 500);

var roleController = require('../controllers/roleController.js');

var roleAPI = {

    //#region load function

    load: function (req, res) {

        var response;

        if (req.body == undefined) {
            response = {
                "error": true, data: "Please send POST params : action, data."
            };

            res.json(response);

        } else if (req.body.action == undefined) {
            response = {
                "error": true, data: "Action is not defined"
            };

            res.json(response);
        }
        /*else if (req.body.data == undefined) {
            response = {
                "error": true, data: "Data is not defined"
            };

            res.json(response);
        }*/
        else {

            var action = req.body.action;

            if (action == "getRoles")
                roleAPI.getRoles(res);
            else
                log.error("[roleAPI]: Wrong action...")
        }
    },

    // #endregion

    // #region getRoles function

    getRoles: function (res) {
                
        roleController.getRoles(res)
    }

    // #endregion

};

module.exports = roleAPI;
module.exports.id = "roleAPI";