﻿'use strict';

var log = require('log4js').getLogger(require("path").basename(__filename, ".js"));

setTimeout(function () {
    log.addContext("methodName", stackTrace.get()[0].getMethodName());
    log.trace('Report API Loaded.');
}, 500);

var reportController = require("../controllers/reportController");

var reportAPI = {

    //#region load function

    load: function (req, res) {

        var error;

        if (req.body.action == undefined) {
            error = "action is not defined";
            log.error("[reportAPI] : load - Error : " + error);
        }
        else if (req.body.reportData == undefined) {
            error = "data is not defined";
            log.error("[reportAPI] : load - Error : " + error);
        }
        else if (req.body.userLastName == undefined || req.body.userID == undefined) {
            error = "folderName is not defined";
            log.error("[reportAPI] : load - Error : " + error);
        }
        else {

            var action = req.body.action,
                reportData = req.body.reportData,
                userLastName = req.body.userLastName,
                userID = req.body.userID;

            switch (action) {

                case "getQuote":
                    reportAPI.getQuoteReport(res, userLastName, userID, reportData);
                    break;
            }
        }
    },

    // #region getQuoteReport function

    getQuoteReport: function (res, userLastName, userID, reportData) {

        reportController.getQuoteReport(res, userLastName, userID, reportData);
    }

    // #endregion
}

module.exports = reportAPI;
module.exports.id = "reportAPI";