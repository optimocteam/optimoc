use admin
db.createUser({ user: "sa", pwd: "8f17d1f9f7e3ead49838ec2258d45fd4", roles: ["userAdminAnyDatabase", "dbAdminAnyDatabase"] })

use admin
db.createUser({ user: "root", pwd: "396d4b2c73819259359e3c283428273d", roles: [ {role: "root", db: "admin" }] })

use optimoc
db.createUser({ user: "optimocDBAdmin", pwd: "ffdc01731406f88ad1891ddb94dca2ea", roles : [ {role: "dbOwner", db: "optimoc" }] })

use jsreport
db.createUser({ user: "jsreportDBAdmin", pwd: "74cb60eb87e9fa0b86c1c3e6f4beb88c", roles : [ {role: "dbOwner", db: "jsreport" }] })