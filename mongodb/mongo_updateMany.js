// Update - add field
db.getCollection("projects").updateMany(
	{userAPIKey : "bfbe35d94082d689208424d9fe1bfde85441971586f48171bbe633a878c93e02"},
	{ $set: { "isArchived" : false }}
)

// 
db.getCollection('projects').updateMany({}, {$set: {"disclaimerText": " Les prix indiqu&eacute;s sont exprim&eacute;s en Euros<br />Application de la loi du 12/05/1980 les marchandises restent la propri&eacute;t&eacute; du vendeur jusqu'à paiement int&eacute;gral du prix<br />Le pr&eacute;sent devis sera valable 2 mois<br />Aucune acceptation ne sera prise en compte sans signature du devis"}})

// Update - remove field
db.getCollection('projects').updateMany({}, {$unset: {"isCustomMetric": 1}})


-- ISODate
db.getCollection('users').find({registerDate : {$regex: /^((?!ISODate).)*$/} }).forEach(function(e,i) {
  e.registerDate = ISODate(e.registerDate);
    printjson(e.registerDate);
  db.getCollection('users').save(e);
});