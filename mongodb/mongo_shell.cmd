REM user optimocDBAdmin
mongo --host ds127536.mlab.com --port 27536 -u "optimocDBAdmin" -p "ffdc01731406f88ad1891ddb94dca2ea" --authenticationDatabase "optimoc-dev"

REM IMPORT datas JSON
mongoimport --host ds127536.mlab.com --port 27536 --username "optimocDBAdmin" --password "ffdc01731406f88ad1891ddb94dca2ea" --collection users --db optimoc-dev --drop --file "C:\Users\Georges\Desktop\data1.json"
mongoimport --host ds127536.mlab.com --port 27536 --username "optimocDBAdmin" --password "ffdc01731406f88ad1891ddb94dca2ea" --collection users --db optimoc-dev --file "C:\Users\Georges\Desktop\data2.json"
mongoimport --host ds127536.mlab.com --port 27536 --username "optimocDBAdmin" --password "ffdc01731406f88ad1891ddb94dca2ea" --collection users --db optimoc-dev --file "C:\Users\Georges\Desktop\data3.json"

REM IMPORT datas BSON
mongorestore --host ds127536.mlab.com --port 27536 --username "optimocDBAdmin" --password "ffdc01731406f88ad1891ddb94dca2ea" --collection users --db optimoc-dev --drop "C:\mongodb\backups\20171204\optimoc\users.bson"

REM DUMP database
mongodump --host ds141960.mlab.com --port 41960 --username "optimocDBAdmin" --password "17fa117ce727f1231281c95f03759343" --db optimoc --out C:\mongodb\backups\%date_YYYYMMDD%
mongodump --host ds143000.mlab.com --port 43000 --username "jsreportDBAdmin" --password "5be3d48680a8ae38df0fee0298d35f65" --db jsreport --out C:\mongodb\backups\%date_YYYYMMDD%

REM Restore database
mongorestore --host ds127536.mlab.com --port 27536 --username "optimocDBAdmin" --password "ffdc01731406f88ad1891ddb94dca2ea" --db optimoc-dev --drop C:\mongodb\backups\20171203\optimoc
mongorestore --host ds129166.mlab.com --port 29166 --username "jsreportDBAdmin" --password "5be3d48680a8ae38df0fee0298d35f65" --db jsreport-dev --drop C:\mongodb\backups\20171203\jsreport


REM Migration
REM DEV
mongodump --host ds127536.mlab.com --port 27536 --username "optimocDBAdmin" --password "c67fcaf04f08707183444f54aedf8b65" --db optimoc-dev --out C:\Users\Georges\Documents\backups
mongorestore --uri mongodb+srv://optimocDBAdmin:c67fcaf04f08707183444f54aedf8b65@cluster-optimoc-dev.ousnn.gcp.mongodb.net/optimoc-dev --drop C:\Users\Georges\Documents\backups\

REM PROD
mongodump --host ds141960.mlab.com --port 41960 --username "optimocDBAdmin" --password "dababe60c8db61ae52fb8c6a4a21b434" --db optimoc --out C:\Users\Georges\Documents\backups
mongorestore --uri mongodb+srv://optimocDBAdmin:dababe60c8db61ae52fb8c6a4a21b434@cluster-optimoc.v6ezp.mongodb.net/optimoc --drop C:\Users\Georges\Documents\backups\

mongodump --host ds143000.mlab.com --port 43000 --username "jsreportDBAdmin" --password "4d666afe3a585f4fdc14cc76d0fad3e6" --db jsreport --out C:\Users\Georges\Documents\backups
mongorestore --uri mongodb+srv://jsreportDBAdmin:4d666afe3a585f4fdc14cc76d0fad3e6@cluster-jsreport.3y5t8.azure.mongodb.net/jsreport --drop C:\Users\Georges\Documents\backups\