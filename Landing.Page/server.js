const express = require('express');
const https = require('https');
const fs = require('fs-extra');

const app = express();
app.set('port', (process.env.PORT || 4100));
app.use(express.static(__dirname));

app.get('*', function (req, res) {
    res
        .status(200)
        .set({ 'content-type': 'text/html; charset=utf-8' })
        .sendFile('index.html', { root: __dirname });
});

/*app.listen(app.get('port'), function() {
    console.log('app listening on port', app.get('port'));
});*/

 const options = {
     key: fs.readFileSync("./cert/optimoc.fr.key.pem"),
     cert: fs.readFileSync("./cert/optimoc.fr.crt.pem"),
     ca: fs.readFileSync("./cert/issuer.crt.pem")
};

https.createServer(options, app).listen(app.get('port'), function () {
    console.log("HTTPS server listening on port " + app.get('port'));
});